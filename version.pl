#!/usr/bin/env perl

=head1 NAME

version.pl - Parse the NASM version file and produce appropriate macros

=head1 SYNOPSIS

  version.pl $format < $filename
  
  echo 2.06rc10 | version.pl $format
  
  version.pl $format $filename

Where $format is one of:

  h mac sed make nsis id xid perl yaml json

=head1 DESCRIPTION

The NASM version number is assumed to consist of:

E<lt>majorE<gt>.E<lt>minorE<gt>[.E<lt>subminorE<gt>][
plE<lt>patchlevelE<gt> |
rcE<lt>numberE<gt>
]]E<lt>tailE<gt>

... where E<lt>tailE<gt> is not necessarily numeric, but if it is of the form
-E<lt>digitsE<gt> it is assumed to be a snapshot release.

=head1 Output Formats

=head2 id

  print "$id\n"

=head2 xid

  printf "0x%08x\n",$id
  
=cut


use warnings;
use strict;


# forward definition of subroutines
sub Load;
sub help;
sub h;
sub mac;
sub sed;
sub make;
sub nsis;
sub yaml;
sub json;
sub perl;



# jump table to subroutines / variables
my %jump = (
  id     => 'id',
  xid    => 'xid',
  hex_id => 'xid',
  
  h      => \&h,
  mac    => \&mac,
  sed    => \&sed,
  make   => \&make,
  nsis   => \&nsis,
  
  perl   => \&perl,
  dump   => \&perl,
  yaml   => \&yaml,
  yml    => \&yaml,
  json   => \&json,
  js     => \&json,
  
  help   => \&help,
  usage  => sub{
    require Pod::Usage;
    
    Pod::Usage::pod2usage(
      "run perldoc $0 or pod2text $0 for more information"
    );
  }
);


{
  use Scalar::Util 'reftype';
  
  my($cmd, $filename) = @ARGV;
  
  if(
    not $cmd or $cmd =~ m{
      ^
        -h |
        (?:--)?help |
        /[?]
      $
    }xi
  ){
    # in this case $filename is actually output format
    # we want to know more about
    $jump{help}->($filename);
    last;
    
  }elsif($cmd eq 'usage'){
    $jump{usage}->();
  }
  
  my $jump = $jump{$cmd};
  unless( $jump ){
    $jump{usage}->(cmd=>$cmd);
  }
  
  my $version = Load($filename);
  
  if( ref $jump ){
    my $reftype = reftype $jump;
    
    if($reftype eq 'CODE'){
      my $ret = $jump->($version);
      print "$ret\n" if defined $ret;
      
    }else{
      # an un-used reference type
      die;
    }
  }else{
    print $version->{$jump}, "\n";
  }
}

sub Load{
  my($filename) = @_;
  $filename ||= '-';
  my %version;
  
  {
    # only really required for this first match
    # could probably rewrite the match for earlier Perls
    require 5.010;
    my $line;
    
    if($filename and $filename ne '-'){
      open my $file, '<', $filename or die;
      
      $line = <$file>;
      close $file;
    }else{
      $line = <STDIN>;
    }
    chomp $line;
    die unless length $line;
    $version{_line} = $line;
    
    $line =~ m{ ^
      (?<major>\d+)[.](?<minor>\d+)
      (?:[.](?<subminor>\d+))?
      (?:
        pl(?<patchlevel>\d+) |
        rc(?<rc>\d+)
      )?
      (?:
        [-](?<snapshot>\d+) |
        (?<tail>.+)
      )?
      $
    }x;
    
    for my $key(qw'major minor subminor patchlevel rc'){
      my $value = $+{$key} || 0;
      
      # removes any leading zeros by forcing to a number
      $version{$key} = $value + 0;
    }
    for my $key(qw'snapshot tail'){
      if(exists $+{$key}){
        $version{$key} = $+{$key};
      }
    }
  }
  
  
  
  
  # modify %version if this is a release candidate 
  if($version{rc}){
    $version{patchlevel} = $version{rc} + 90;
    
    if($version{subminor}){
      $version{subminor}--;
    }else{
      $version{subminor} = 99;
      
      if($version{minor}){
        $version{minor}--;
      }else{
        $version{minor} = 99;
        $version{major}--;
      }
    }
  }
  
  
  
  # add 'id' and 'xid' to %version
  $version{id} =
    ($version{major}    << 24) +
    ($version{minor}    << 16) +
    ($version{subminor} <<  8) +
    $version{patchlevel};
  $version{xid} = sprintf('0x%08x',$version{id});
  
  
  
  # add 'mangled' to %version
  {
    my $mangled = sprintf("%d.%02d",$version{major},$version{minor});
    if(
      $version{subminor}   or
      $version{patchlevel} or
      $version{snapshot}
    ){
      $mangled .= sprintf(".%02d",$version{subminor});
      
      if(
        $version{patchlevel} or
        $version{snapshot}
      ){
        $mangled .= sprintf(".%01d",$version{patchlevel})
      }
    }
    
    if($version{snapshot}){
      $mangled .= '.'.$version{snapshot}
    }elsif( $version{tail}){
      my $tail = $version{tail};
      $tail =~ s/-/./g;
      $mangled .= $tail;
    }
    
    $version{mangled} = $mangled;
  }
  
  return  %version if wantarray;
  return \%version;
}


=head2 perl - returns a dump of internally used data

  {
    'rc' => 10,
    'subminor' => 99,
    'minor' => 5,
    'mangled' => '2.05.99.100',
    'patchlevel' => 100,
    'id' => 33907556,
    'xid' => '0x02056364',
    'major' => 2
  }

=cut

sub perl{
  my($version)=@_;
  no warnings qw'once';
  require Data::Dumper;
  local $Data::Dumper::Terse = 1;
  local $Data::Dumper::Indent = 1;
  
  my %ret = %$version;
  for( keys %ret ){
    # remove any "hidden" keys
    delete $ret{$_} if /^[_.]/;
  }
  return  Data::Dumper::Dumper(\%ret);
}

=head2 yaml - returns the same thing as perl, but in YAML format

  ---
  id: 33907556
  major: 2
  mangled: 2.05.99.100
  minor: 5
  patchlevel: 100
  rc: 10
  subminor: 99
  xid: 0x02056364

=cut

sub yaml{
  my($version)=@_;
  require YAML::XS;
  YAML::XS->import;
  
  my %ret = %$version;
  for( keys %ret ){
    # remove any "hidden" keys
    delete $ret{$_} if /^[_.]/;
  }
  return Dump(\%ret);
}

=head2 json - returns the same thing as perl, but in JSON format

  {
    "rc" : 10,
    "subminor" : 99,
    "minor" : 5,
    "mangled" : "2.05.99.100",
    "patchlevel" : 100,
    "id" : 33907556,
    "xid" : "0x02056364",
    "major" : 2
  }

=cut

sub json{
  my($version)=@_;
  require JSON;
  #JSON->import;
  
  my $json = new JSON;
  
  my %ret = %$version;
  for( keys %ret ){
    # remove any "hidden" keys
    delete $ret{$_} if /^[_.]/;
  }
  return $json->pretty->encode(\%ret);
}


=head2 h

  #ifndef NASM_VERSION_H
  #define NASM_VERSION_H
  #define NASM_MAJOR_VER      $major
  #define NASM_MINOR_VER      $minor
  #define NASM_SUBMINOR_VER   {$subminor   || 0}
  #define NASM_PATCHLEVEL_VER {$patchlevel || 0}
  #define NASM_SNAPSHOT       $snapshot         -- if snapshot
  #define NASM_VERSION_ID     $hex_id
  #define NASM_VER            "$ver"
  #endif /* NASM_VERSION_H */

=cut


  #NASM_MAJOR_VER
  #NASM_MINOR_VER
  #NASM_SUBMINOR_VER   -- this is zero if no subminor
  #NASM_PATCHLEVEL_VER -- this is zero is no patchlevel
  #NASM_SNAPSHOT       -- if snapshot
  #NASM_VERSION_ID     -- version number encoded
  #NASM_VER            -- whole version number as a string



sub h{
  my($version) = @_;
  printf <<END, @$version{'major','minor','subminor','patchlevel'};
#ifndef NASM_VERSION_H
#define NASM_VERSION_H
#define NASM_MAJOR_VER      %d
#define NASM_MINOR_VER      %d
#define NASM_SUBMINOR_VER   %d
#define NASM_PATCHLEVEL_VER %d
END

  if ($version->{snapshot}) {
    printf "#define NASM_SNAPSHOT       %d\n", $version->{snapshot};
  }
  
  printf <<END, @$version{'xid','_line'};
#define NASM_VERSION_ID     %s
#define NASM_VER            "%s"
#endif /* NASM_VERSION_H */
END
  return;
}



=head2 mac

  __NASM_MAJOR__ $major
  __NASM_MINOR__ $minor
  __NASM_SUBMINOR__ $subminor
  __NASM_PATCHLEVEL__ $patchlevel
  __NASM_SNAPSHOT__ $snapshot     -- if snapshot
  __NASM_VERSION_ID__ $hex_id
  __NASM_VER__ $ver

=cut

sub mac{
  my($version) = @_;
  printf <<'END', @$version{'major','minor','subminor','patchlevel'};
%%define __NASM_MAJOR__ %d
%%define __NASM_MINOR__ %d
%%define __NASM_SUBMINOR__ %d
%%define __NASM_PATCHLEVEL__ %d
END

  if ($version->{snapshot}) {
    printf "%%define __NASM_SNAPSHOT__ %d\n", $version->{snapshot};
  }
  
printf <<'END', @$version{'id','_line'};
%%define __NASM_VERSION_ID__ 0%08Xh
%%define __NASM_VER__ "%s"
END
  return;
}



=head2 sed

  s/@@NASM_MAJOR@@/$major/g
  s/@@NASM_MINOR@@/$minor/g
  s/@@NASM_SUBMINOR@@/$sub_minor/g
  s/@@NASM_PATCHLEVEL@@/$patchlevel/g
  s/@@NASM_SNAPSHOT@@/$snapshot/g
  s/@@NASM_VERSION_ID@@/$id/g
  s/@@NASM_VERSION_XID@@/$hex_id/g
  s/@@NASM_VER@@/$ver/g
  s/@@NASM_MANGLED_VER@@/$mangled/g

=cut

sub sed{
  my($version) = @_;
  my @rep = @$version{qw{
    major
    minor
    subminor
    patchlevel
    snapshot
    id
    xid
    _line
    mangled
  }};
  no warnings 'uninitialized';
sprintf <<'END', @rep;
s/@@NASM_MAJOR@@/%d/g
s/@@NASM_MINOR@@/%d/g
s/@@NASM_SUBMINOR@@/%d/g
s/@@NASM_PATCHLEVEL@@/%d/g
s/@@NASM_SNAPSHOT@@/%d/g
s/@@NASM_VERSION_ID@@/%d/g
s/@@NASM_VERSION_XID@@/%s/g
s/@@NASM_VER@@/%s/g
s/@@NASM_MANGLED_VER@@/%s/g
END
}



=head2 make

  NASM_VER=$ver
  NASM_MAJOR_VER=$major
  NASM_MINOR_VER=$minor
  NASM_SUBMINOR_VER=$subminor
  NASM_PATCHLEVEL_VER=$patchlevel

=cut

sub make{
  my($version) = @_;
 return sprintf <<END, @$version{'_line','major','minor','subminor','patchlevel'};
NASM_VER=%s
NASM_MAJOR_VER=%d
NASM_MINOR_VER=%d
NASM_SUBMINOR_VER=%d
NASM_PATCHLEVEL_VER=%d
END
}


=head2 nsis

  !define VERSION "$version"
  !define MAJOR_VER $major
  !define MINOR_VER $minor
  !define SUBMINOR_VER $subminor
  !define PATCHLEVEL_VER $patchlevel

=cut

sub nsis{
  my($version) = @_;
 return sprintf <<'END', @$version{'_line','major','minor','subminor','patchlevel'};
!define VERSION "%s"
!define MAJOR_VER %d
!define MINOR_VER %d
!define SUBMINOR_VER %d
!define PATCHLEVEL_VER %d
END
}


sub help{
  my($cmd) = @_;
  
  my %help = (
    sed  => 'strings for sed command',
    mac  => 'strings for nasm macros',
    h    => 'strings for headers',
    make => 'strings for makefiles',
    perl => 'dump of program data',
    nsis => 'what is nsis?',
    json => 'dump of program data in json format',
    yaml => 'dump of program data in yaml format'
  );
  
  if( $cmd and $help{$cmd} ){
    print $help{$cmd},"\n";
  }else{
    print "$0 [help]? [ ".join(' | ',keys %help)." ]\n";
  }
  return;
}
