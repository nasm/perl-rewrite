#ifndef NASM_OUTLIB_H
#define NASM_OUTLIB_H

#include "nasm.h"

uint64_t realsize(enum out_type type, uint64_t size);

#endif /* NASM_OUTLIB_H */

