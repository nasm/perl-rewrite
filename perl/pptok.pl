#! /usr/bin/env perl
use strict;
use warnings;
use 5.010;

use lib qw'lib';

sub load_pptok(;$);
sub h;
sub c;
sub ph;


our %jump = (
  h => \&h,
  c => \&c,
  ph => \&ph,
  dump => sub{
    use Data::Dump 'dump';
    say dump {@_};
  },
);

{
  my($what, $in, $out) = @ARGV;
  my %info = load_pptok();
  
  $in //= '';
  
  $info{out} = $out;
  
  my $jump = $jump{lc $what};
  given( ref $jump ){
    when( 'CODE' ){
      my $return = $jump->(%info);
      print $return if defined $return;
    }
  }
}

sub load_pptok(;$){
  my($filename) = @_;
  $filename = 'pptok.dat' unless @_;
  
  my @tokens_cond;
  my @conditions;
  
  my @tokens;
  my $first_uncond;
  
  {
    use autodie qw'open close';
    open( my $file, '<', $filename ) or die;
    
    while( my $line = <$file> ){
      $line =~ s/^\s+//;          # Remove leading whitespace
      $line =~ s/\s*(?:\#.*)?$//; # Remove comments and trailing whitespace
      next unless $line;
      
      given($line){
        when( /^\* (.*)    /x ){ push @conditions,  $1; }
        when( /^\% (.*) \*$/x ){ push @tokens_cond, $1; }
        when( /^\% (.*)   $/x ){ push @tokens,      $1; }
      }
    }
    
    close $file;
  }
  
  @conditions  = sort @conditions;
  @tokens      = sort @tokens;
  @tokens_cond = sort @tokens_cond;
  
  
  # Generate the expanded list including conditionals.  The conditionals
  # are at the beginning, padded to a power of 2, with the inverses
  # interspersed; this allows a simple mask to pick out the condition.
  
  while ((scalar @conditions) & (scalar @conditions)-1) {
    push(@conditions, undef);
  }
  
  $first_uncond = $tokens[0];
  {
    my @tokens_cond_p;
    for my $token ( @tokens_cond ){
      for my $cond ( @conditions ){
        if( defined $cond){
          push @tokens_cond_p, "${token}${cond}", "${token}n${cond}";
        }else{
          push @tokens_cond_p, undef, undef;
        }
      }
    }
    @tokens = ( @tokens_cond_p, @tokens );
  }
  
  my %return = (
    tokens => [@tokens],
    conditions => [@conditions],
    first_uncond => $first_uncond,
    in => $filename,
    tokens_cond => [@tokens_cond],
    #tokens_cond_p => \@tokens_cond_p
  );
  
  return  %return if wantarray;
  return \%return;
}

sub h{
  my %info = @_;
  
  my $output = <<END;
/* Automatically generated from $info{in} by $0 */
/* Do not edit */

enum preproc_token {
END
  {
    my $f = "  PP_%-13s = %3d\n";
    
    my $n = 0;
    for my $token ( @{$info{tokens}} ){
      if( defined($token) ){
        #printf OUT "    %-16s = %3d,\n", "PP_\U$token\E", $n;
        $output .= sprintf $f, uc $token, $n;
      }
      $n++;
    }
    
    $output .= sprintf $f, 'INVALID', -1;
  }
  $output .= <<END;
};

enum pp_conditional {
END
  {
    my $n = 0;
    for my $cc ( @{$info{conditions}} ) {
      if (defined($cc)) {
          $output .= sprintf "  PPC_IF%-10s = %3d,\n",uc $cc, $n;
      }
      $n += 2;
    }
  }
  my $pp_cond = (scalar(@{$info{conditions}})-1) << 1;
  $output .= sprintf <<END, $pp_cond, uc $info{first_uncond};
};

#define PP_COND(x)     ((enum pp_conditional)((x) & 0x%x))
#define PP_IS_COND(x)  ((unsigned int)(x) < PP_%s)
#define PP_NEGATIVE(x) ((x) & 1)

END

  use List::MoreUtils 'zip';
  
  for my $token( @{$info{tokens_cond}} ){
    my $token = uc $token;
    $output .= "#define CASE_PP_$token \\\n";
    
    my @cond = map {uc $_} grep { defined $_ } '', @{$info{conditions}};
    my @ncond = map {"N$_"} @cond;
    
    @cond = zip @cond, @ncond;
    
    @cond = map { "  case PP_${token}$_" } @cond;
    
    $output .= join ": \\\n", @cond;
    
    $output .= "\n";
  }
  
  return $output;
}








sub c{
  my %info = @_;
  
  # header
  my $output = <<END;
/* Automatically generated from $info{in} by $0 */
/* Do not edit */

#include "compiler.h"
#include <inttypes.h>
#include <ctype.h>
#include "nasmlib.h"
#include "hashtbl.h"
#include "preproc.h"

END
  
  
  
  
  # list of tokens, followed by list of the lengths of the tokens
  {
    my @tokens = @{$info{tokens}};
    $output .= sprintf "const char * const pp_directives[%d] = {\n", scalar @tokens;
    for my $d ( @tokens ){
      if (defined($d)) {
        $output .= "    \"%$d\",\n";
      } else {
        $output .= "    NULL,\n";
      }
    }
    $output .= "};\n";
    
    
    $output .= sprintf "const uint8_t pp_directives_len[%d] = {\n", scalar(@tokens);
    for my $d (@tokens) {
	$output .= sprintf "    %d,\n", defined($d) ? length($d)+1 : 0;
    }
    $output .= "};\n";
  }
  
  
  

  {
    my %tokens;
    #my @tokendata;
    {
      my $n = 0;
      for my $token( @{$info{tokens}} ){
        if (defined($token)) {
          $tokens{'%'.$token} = $n;
          if ($token =~ /[\@\[\]\\_]/) {
            # Fail on characters which look like upper-case letters
            # to the quick-and-dirty downcasing in the prehash
            # (see below)
            die "$info{in}: invalid character in token: $token";
          }
        }
        $n++;
      }
    }
    
    
    
    
    use phash;
    
    my @hashinfo = gen_perfect_hash(%tokens);
    if(! @hashinfo ){
	die "$0: no hash found\n";
    }

    # Paranoia...
    # no longer needed, gen_perfect_hash now runs verify_hash_table
    # verify_hash_table(\%tokens, \@hashinfo);
    
    my ($n, $sv, $g) = @hashinfo;
    my $sv2 = $sv+2;

    die if ($n & ($n-1));
  
    # Put a large value in unused slots.  This makes it extremely unlikely
    # that any combination that involves unused slot will pass the range test.
    # This speeds up rejection of unrecognized tokens, i.e. identifiers.
    $output .= <<END;
enum preproc_token pp_token_hash(const char *token)
{
#define UNUSED 16383
    static const int16_t hash1[$n] = {
END
    for( my $i = 0; $i < $n; $i++ ){
        my $h = ${$g}[$i*2+0];
        $output .= "        ".(defined($h) ? $h : 'UNUSED'). ",\n";
    }
    $output .= "    };\n";
  
    $output .= "    static const int16_t hash2[$n] = {\n";
    for( my $i = 0; $i < $n; $i++ ){
        my $h = ${$g}[$i*2+1];
        $output .= "        ".( defined($h) ? $h : 'UNUSED'). ",\n";
    }
    $output .= "    };\n";
    
    $output .= sprintf <<END, $$sv[0], $$sv[1], $n-1, $n-1, scalar @{$info{tokens}};
    uint32_t k1, k2;
    uint64_t crc;
    /* For correct overflow behavior, "ix" should be unsigned of the same
       width as the hash arrays. */
    uint16_t ix;

    crc = crc64i(UINT64_C(0x%08x%08x), token);
    k1 = (uint32_t)crc;
    k2 = (uint32_t)(crc >> 32);
    
    ix = hash1[k1 & 0x%x] + hash2[k2 & 0x%x];
    if (ix >= %d)
        return PP_INVALID;
        
    if (!pp_directives[ix] || nasm_stricmp(pp_directives[ix], token))
        return PP_INVALID;
    
    return ix;
}
END
  }
  
  return $output;
}




sub ph{
  my %info = @_;
  
  my $output = <<END;
# Automatically generated from $info{in} by $0
# Do not edit

%pptok_hash = (
END
  
  my $longest = 0;
  my @tokens =  @{$info{tokens}};
  map {
    my $len = $_ ? length $_ : 0;
    $longest = $len if $len > $longest;
  } @tokens;
  
  my $n = 0;
  for my $token ( @tokens ){
    if( $token ){
      my $pad = ' ' x ( $longest - length $token );
      $output .= "    '%$token' $pad=> $n,\n";
    }
    $n++;
  }
  
  $output .= <<END;
);
1;
END
  return $output;
}
