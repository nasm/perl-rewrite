#! /usr/bin/env perl
use strict;
use warnings;
use lib qw'lib';

use Nasm::insns;

my $self = Nasm::insns->new('insns.dat');
__END__
use Data::Dump 'dump';

use 5.010;

say dump $self;
