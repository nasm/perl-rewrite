#!/usr/bin/env perl

# Test that the syntax of our POD documentation is valid
use strict;
BEGIN {
  $|  = 1;
  $^W = 1;
}

my @MODULES = (
  'Test::Perl::Critic'
);

# Don't run tests during end-user installs
use Test::More;
unless (
  $ENV{RELEASE_TESTING}
){
  plan( skip_all => "test not required for installation" );
}

# Load the testing modules
foreach my $MODULE ( @MODULES ) {
  eval "use $MODULE;";
  if ( $@ ) {
    $ENV{RELEASE_TESTING}
    ? die( "Failed to load required release-testing module $MODULE" )
    : plan( skip_all => "$MODULE not available for testing" );
  }
}

all_critic_ok();

1;
