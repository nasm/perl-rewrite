use strict;
use warnings;
use YAML::XS ':all';


use Test::More tests => 1;


my $reference;
my $dat_string;
{
  local $/ = "...\n";
  my $datastream;
  
  ($datastream,$dat_string) = <DATA>;
  $reference = Load($datastream);
}

my $got_str = `echo "$dat_string" | perl regs.pl yaml -`;

my $yaml = Load $got_str;

for my $elem (values %$yaml){
  delete $elem->{Index};
}

is_deeply $yaml, $reference;

__DATA__
---
xmm0:
  AssemblerClass: XMM0
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 0
xmm1:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 1
xmm10:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 10
xmm11:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 11
xmm12:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 12
xmm13:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 13
xmm14:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 14
xmm15:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 15
xmm2:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 2
xmm3:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 3
xmm4:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 4
xmm5:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 5
xmm6:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 6
xmm7:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 7
xmm8:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 8
xmm9:
  AssemblerClass: XMMREG
  DisassemblerClasses:
  - xmmreg
  x86RegisterNumber: 9
...
xmm0    XMM0    xmmreg  0
xmm1-15 XMMREG  xmmreg  1
