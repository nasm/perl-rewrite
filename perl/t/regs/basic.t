use strict;
use warnings;
use YAML::XS ':all';


use Test::More tests => 1;


my($yaml_cmp,$dat_stream);
{
  my $yaml_stream;
  local $/ = "\n...\n";
  ($dat_stream,$yaml_stream) = <DATA>;
  chomp $dat_stream;
  $yaml_cmp = Load($yaml_stream);
}

my $got_yaml = Load( scalar `echo "$dat_stream" | perl regs.pl yaml -` );

for my $elem (values %$got_yaml){
  delete $elem->{Index};
}

is_deeply( $got_yaml, $yaml_cmp);

__DATA__
# General-purpose registers
al	REG_AL		reg8,reg8_rex	0
ah	REG_HIGH	reg8		4
ax	REG_AX		reg16		0
eax	REG_EAX		reg32		0
rax	REG_RAX		reg64		0
bl	REG8NA		reg8,reg8_rex	3
bh	REG_HIGH	reg8		7
bx	REG16NA		reg16		3
ebx	REG32NA		reg32		3
rbx	REG64NA		reg64		3
cl	REG_CL		reg8,reg8_rex	1
ch	REG_HIGH	reg8		5
cx	REG_CX		reg16		1
ecx	REG_ECX		reg32		1
rcx	REG_RCX		reg64		1
dl	REG_DL		reg8,reg8_rex	2
dh	REG_HIGH	reg8		6
dx	REG_DX		reg16		2
edx	REG_EDX		reg32		2
rdx	REG_RDX		reg64		2
spl	REG8NA		reg8_rex	4
sp	REG16NA		reg16		4
esp	REG32NA		reg32		4
rsp	REG64NA		reg64		4
bpl	REG8NA		reg8_rex	5
bp	REG16NA		reg16		5
ebp	REG32NA		reg32		5
rbp	REG64NA		reg64		5
sil	REG8NA		reg8_rex	6
si	REG16NA		reg16		6
esi	REG32NA		reg32		6
rsi	REG64NA		reg64		6
dil	REG8NA		reg8_rex	7
di	REG16NA		reg16		7
edi	REG32NA		reg32		7
rdi	REG64NA		reg64		7

# Segment registers
cs	REG_CS		sreg		1
ds	REG_DESS	sreg		3
es	REG_DESS	sreg		0
ss	REG_DESS	sreg		2
fs	REG_FSGS	sreg		4
gs	REG_FSGS	sreg		5
...
---
ah:
  AssemblerClass: REG_HIGH
  DisassemblerClasses:
  - reg8
  x86RegisterNumber: 4
al:
  AssemblerClass: REG_AL
  DisassemblerClasses:
  - reg8
  - reg8_rex
  x86RegisterNumber: 0
ax:
  AssemblerClass: REG_AX
  DisassemblerClasses:
  - reg16
  x86RegisterNumber: 0
bh:
  AssemblerClass: REG_HIGH
  DisassemblerClasses:
  - reg8
  x86RegisterNumber: 7
bl:
  AssemblerClass: REG8NA
  DisassemblerClasses:
  - reg8
  - reg8_rex
  x86RegisterNumber: 3
bp:
  AssemblerClass: REG16NA
  DisassemblerClasses:
  - reg16
  x86RegisterNumber: 5
bpl:
  AssemblerClass: REG8NA
  DisassemblerClasses:
  - reg8_rex
  x86RegisterNumber: 5
bx:
  AssemblerClass: REG16NA
  DisassemblerClasses:
  - reg16
  x86RegisterNumber: 3
ch:
  AssemblerClass: REG_HIGH
  DisassemblerClasses:
  - reg8
  x86RegisterNumber: 5
cl:
  AssemblerClass: REG_CL
  DisassemblerClasses:
  - reg8
  - reg8_rex
  x86RegisterNumber: 1
cs:
  AssemblerClass: REG_CS
  DisassemblerClasses:
  - sreg
  x86RegisterNumber: 1
cx:
  AssemblerClass: REG_CX
  DisassemblerClasses:
  - reg16
  x86RegisterNumber: 1
dh:
  AssemblerClass: REG_HIGH
  DisassemblerClasses:
  - reg8
  x86RegisterNumber: 6
di:
  AssemblerClass: REG16NA
  DisassemblerClasses:
  - reg16
  x86RegisterNumber: 7
dil:
  AssemblerClass: REG8NA
  DisassemblerClasses:
  - reg8_rex
  x86RegisterNumber: 7
dl:
  AssemblerClass: REG_DL
  DisassemblerClasses:
  - reg8
  - reg8_rex
  x86RegisterNumber: 2
ds:
  AssemblerClass: REG_DESS
  DisassemblerClasses:
  - sreg
  x86RegisterNumber: 3
dx:
  AssemblerClass: REG_DX
  DisassemblerClasses:
  - reg16
  x86RegisterNumber: 2
eax:
  AssemblerClass: REG_EAX
  DisassemblerClasses:
  - reg32
  x86RegisterNumber: 0
ebp:
  AssemblerClass: REG32NA
  DisassemblerClasses:
  - reg32
  x86RegisterNumber: 5
ebx:
  AssemblerClass: REG32NA
  DisassemblerClasses:
  - reg32
  x86RegisterNumber: 3
ecx:
  AssemblerClass: REG_ECX
  DisassemblerClasses:
  - reg32
  x86RegisterNumber: 1
edi:
  AssemblerClass: REG32NA
  DisassemblerClasses:
  - reg32
  x86RegisterNumber: 7
edx:
  AssemblerClass: REG_EDX
  DisassemblerClasses:
  - reg32
  x86RegisterNumber: 2
es:
  AssemblerClass: REG_DESS
  DisassemblerClasses:
  - sreg
  x86RegisterNumber: 0
esi:
  AssemblerClass: REG32NA
  DisassemblerClasses:
  - reg32
  x86RegisterNumber: 6
esp:
  AssemblerClass: REG32NA
  DisassemblerClasses:
  - reg32
  x86RegisterNumber: 4
fs:
  AssemblerClass: REG_FSGS
  DisassemblerClasses:
  - sreg
  x86RegisterNumber: 4
gs:
  AssemblerClass: REG_FSGS
  DisassemblerClasses:
  - sreg
  x86RegisterNumber: 5
rax:
  AssemblerClass: REG_RAX
  DisassemblerClasses:
  - reg64
  x86RegisterNumber: 0
rbp:
  AssemblerClass: REG64NA
  DisassemblerClasses:
  - reg64
  x86RegisterNumber: 5
rbx:
  AssemblerClass: REG64NA
  DisassemblerClasses:
  - reg64
  x86RegisterNumber: 3
rcx:
  AssemblerClass: REG_RCX
  DisassemblerClasses:
  - reg64
  x86RegisterNumber: 1
rdi:
  AssemblerClass: REG64NA
  DisassemblerClasses:
  - reg64
  x86RegisterNumber: 7
rdx:
  AssemblerClass: REG_RDX
  DisassemblerClasses:
  - reg64
  x86RegisterNumber: 2
rsi:
  AssemblerClass: REG64NA
  DisassemblerClasses:
  - reg64
  x86RegisterNumber: 6
rsp:
  AssemblerClass: REG64NA
  DisassemblerClasses:
  - reg64
  x86RegisterNumber: 4
si:
  AssemblerClass: REG16NA
  DisassemblerClasses:
  - reg16
  x86RegisterNumber: 6
sil:
  AssemblerClass: REG8NA
  DisassemblerClasses:
  - reg8_rex
  x86RegisterNumber: 6
sp:
  AssemblerClass: REG16NA
  DisassemblerClasses:
  - reg16
  x86RegisterNumber: 4
spl:
  AssemblerClass: REG8NA
  DisassemblerClasses:
  - reg8_rex
  x86RegisterNumber: 4
ss:
  AssemblerClass: REG_DESS
  DisassemblerClasses:
  - sreg
  x86RegisterNumber: 2
...
