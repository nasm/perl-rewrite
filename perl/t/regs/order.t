use strict;
use warnings;
use YAML::XS ':all';


use Test::More tests => 5;


my($dat_stream,@array);
{
  local $/ = "\n...\n";
  my $array;
  ($array,$dat_stream) = <DATA>;
  chomp $dat_stream;
  chomp $array;
  $array =~ s/^\s+//;
  
  (@array) = split '\s+', $array;
}

for( 1..5 ){
  my @order = split '\n', `echo "$dat_stream" | perl regs.pl order -`;
  is_deeply \@order, \@array;
}
__DATA__
  ah al ax bh bl bp bpl bx ch cl
  cr0 cr1 cr10 cr11 cr12 cr13 cr14 cr15 cr2 cr3 cr4 cr5 cr6 cr7 cr8 cr9
  cs cx dh di dil dl
  dr0 dr1 dr10 dr11 dr12 dr13 dr14 dr15 dr2 dr3 dr4 dr5 dr6 dr7 dr8 dr9
  ds dx
  eax ebp ebx ecx edi edx es esi esp
  fs gs
  mm0 mm1 mm2 mm3 mm4 mm5 mm6 mm7
  r10 r10b r10d r10w
  r11 r11b r11d r11w
  r12 r12b r12d r12w
  r13 r13b r13d r13w
  r14 r14b r14d r14w
  r15 r15b r15d r15w
  r8 r8b r8d r8w
  r9 r9b r9d r9w
  rax rbp rbx rcx rdi rdx rsi rsp
  segr6 segr7
  si sil sp spl ss
  st0 st1 st2 st3 st4 st5 st6 st7
  tr0 tr1 tr2 tr3 tr4 tr5 tr6 tr7
  xmm0 xmm1 xmm10 xmm11 xmm12 xmm13 xmm14 xmm15 xmm2 xmm3 xmm4 xmm5 xmm6 xmm7 xmm8 xmm9
  ymm0 ymm1 ymm10 ymm11 ymm12 ymm13 ymm14 ymm15 ymm2 ymm3 ymm4 ymm5 ymm6 ymm7 ymm8 ymm9
...
#
# List of registers and their classes; classes are defined in nasm.h
#
# The columns are:
#
# register name, assembler class, disassembler class(es), x86 register number
#
# If the register name ends in two numbers separated by a dash, then it is
# repeated as many times as indicated, and the register number is
# updated with it.
#

# General-purpose registers
al	REG_AL		reg8,reg8_rex	0
ah	REG_HIGH	reg8		4
ax	REG_AX		reg16		0
eax	REG_EAX		reg32		0
rax	REG_RAX		reg64		0
bl	REG8NA		reg8,reg8_rex	3
bh	REG_HIGH	reg8		7
bx	REG16NA		reg16		3
ebx	REG32NA		reg32		3
rbx	REG64NA		reg64		3
cl	REG_CL		reg8,reg8_rex	1
ch	REG_HIGH	reg8		5
cx	REG_CX		reg16		1
ecx	REG_ECX		reg32		1
rcx	REG_RCX		reg64		1
dl	REG_DL		reg8,reg8_rex	2
dh	REG_HIGH	reg8		6
dx	REG_DX		reg16		2
edx	REG_EDX		reg32		2
rdx	REG_RDX		reg64		2
spl	REG8NA		reg8_rex	4
sp	REG16NA		reg16		4
esp	REG32NA		reg32		4
rsp	REG64NA		reg64		4
bpl	REG8NA		reg8_rex	5
bp	REG16NA		reg16		5
ebp	REG32NA		reg32		5
rbp	REG64NA		reg64		5
sil	REG8NA		reg8_rex	6
si	REG16NA		reg16		6
esi	REG32NA		reg32		6
rsi	REG64NA		reg64		6
dil	REG8NA		reg8_rex	7
di	REG16NA		reg16		7
edi	REG32NA		reg32		7
rdi	REG64NA		reg64		7
r8-15b	REG8NA		reg8_rex	8
r8-15w	REG16NA		reg16		8
r8-15d	REG32NA		reg32		8
r8-15	REG64NA		reg64		8

# Segment registers
cs	REG_CS		sreg		1
ds	REG_DESS	sreg		3
es	REG_DESS	sreg		0
ss	REG_DESS	sreg		2
fs	REG_FSGS	sreg		4
gs	REG_FSGS	sreg		5
segr6-7	REG_SEG67	sreg		6

# Control registers
cr0-15	REG_CREG	creg		0

# Debug registers
dr0-15	REG_DREG	dreg		0

# Test registers
tr0-7	REG_TREG	treg		0

# Floating-point registers
st0	FPU0		fpureg		0
st1-7	FPUREG		fpureg		1

# MMX registers
mm0-7	MMXREG		mmxreg		0

# SSE registers
xmm0	XMM0		xmmreg		0
xmm1-15	XMMREG		xmmreg		1

# AVX registers
ymm0	YMM0		ymmreg		0
ymm1-15	YMMREG		ymmreg		1
...

