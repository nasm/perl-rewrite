#!/usr/bin/env perl
use strict;
use warnings;

use lib qw't perl/t ..';

my %MODULES = (
  'YAML::XS', 'qw[:all]',
  'version::input', '',
);

use Test::More;

# Load the testing modules
foreach my $MODULE ( keys %MODULES ) {
  eval "use $MODULE $MODULES{$MODULE}";
  if ( $@ ) {
    plan( skip_all => "$MODULE not available for testing" );
  }
}


our %test = load(
  filter =>[qw'version major minor subminor patchlevel']
);


plan( tests => scalar keys %test );


my %map = (
  ver            => 'version',
  major_ver      => 'major',
  minor_ver      => 'minor',
  subminor_ver   => 'subminor',
  patchlevel_ver => 'patchlevel'
);

for my $test ( sort keys %test ){
  my $expected = $test{$test};
  my $returned = `echo $test | perl version.pl make`;
  
  my %got;
  while( $returned =~ m{
    \G
    NASM_(\w+)=(.+?)\n
    }xgcs
  ){
    $got{lc $1} = $2;
  }
  
  for my $key ( keys %map ){
    if( defined $got{$key} ){
      my $value = $map{$key};
      $got{$value} = $got{$key} if defined $value;
      delete $got{$key};
    }
  }
  
  is_deeply(\%got,$expected,$test);
}
__END__
NASM_VER=2.06rc10
NASM_MAJOR_VER=2
NASM_MINOR_VER=5
NASM_SUBMINOR_VER=99
NASM_PATCHLEVEL_VER=100
