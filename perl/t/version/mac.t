#!/usr/bin/env perl
use strict;
use warnings;

use lib qw't perl/t ..';

my %MODULES = (
  'YAML::XS', 'qw[:all]',
  'version::input', '',
);

use Test::More;

# Load the testing modules
foreach my $MODULE ( keys %MODULES ) {
  eval "use $MODULE $MODULES{$MODULE}";
  if ( $@ ) {
    plan( skip_all => "$MODULE not available for testing" );
  }
}


our %test = load(
  filter => [qw' major minor subminor patchlevel snapshot'],
  map=>{
    version_id => sub{sprintf('0%08Xh',$_[3]{id})}
  },
  map =>{
    version => sub{'"'.$_[1].'"'}
  }
);


plan( tests => scalar keys %test );


my %map = (
  ver => 'version'
);

for my $test ( sort keys %test ){
  my $expected = $test{$test};
  my $returned = `echo $test | perl version.pl mac`;
  
  my %got;
  while( $returned =~ m{
    \G\s*
    %define \s+ __NASM_(\w+?)__\s+(.+?)\n
    }xgcms
  ){
    $got{lc $1} = $2;
  }
  
  for my $key ( keys %map ){
    if( defined $got{$key} ){
      my $value = $map{$key};
      $got{$value} = $got{$key} if defined $value;
      delete $got{$key};
    }
  }
  
  is_deeply( \%got, $expected, $test );
}

__END__
%define __NASM_MAJOR__ 2
%define __NASM_MINOR__ 5
%define __NASM_SUBMINOR__ 99
%define __NASM_PATCHLEVEL__ 100
%define __NASM_VERSION_ID__ 002056364h
%define __NASM_VER__ "2.06rc10"
