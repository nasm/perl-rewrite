#!/usr/bin/env perl
use strict;
use warnings;

use lib qw't perl/t ..';

my @MODULES = (
  'YAML::XS',
  'version::input',
  'JSON',
);

use Test::More;

# Load the testing modules
foreach my $MODULE ( @MODULES ) {
  eval "use $MODULE";
  if ( $@ ) {
    plan( skip_all => "$MODULE not available for testing" );
  }
}


our %test = load(
  'all'
);


use Test::More;
plan( tests => scalar keys %test );


for my $test ( sort keys %test ){
  my $expected = $test{$test};
  my $returned = `echo $test | perl version.pl json`;
  my $got = from_json( $returned) || $returned;
  
  is_deeply($got,$expected,$test);
}
__END__
{
   "rc" : 10,
   "subminor" : 99,
   "minor" : 5,
   "mangled" : "2.05.99.100",
   "patchlevel" : 100,
   "id" : 33907556,
   "xid" : "0x02056364",
   "major" : 2
}
