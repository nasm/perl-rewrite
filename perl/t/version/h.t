#!/usr/bin/env perl
use strict;
use warnings;

use lib qw't perl/t ..';

my @MODULES = (
  'YAML::XS',
  'version::input',
);

use Test::More;

# Load the testing modules
foreach my $MODULE ( @MODULES ) {
  eval "use $MODULE";
  if ( $@ ) {
    plan( skip_all => "$MODULE not available for testing" );
  }
}


our %test = load(
  filter => [qw' major minor subminor patchlevel xid snapshot'],
  map =>{
    version => sub{'"'.$_[1].'"'}
  }
);


plan( tests => scalar keys %test );


my %map = (
  mangled_ver    => 'mangled',
  version_id     => 'xid',
  ver            => 'version',
  major_ver      => 'major',
  minor_ver      => 'minor',
  subminor_ver   => 'subminor',
  patchlevel_ver => 'patchlevel'
);

for my $test ( sort keys %test ){
  my $expected = $test{$test};
  my $returned = `echo $test | perl version.pl h`;
  
  my %got;
  # skip
  $returned =~ m/#define NASM_VERSION_H\n/gc;
  while( $returned =~ m{
    \G\s*
    [#] define \s+ NASM_(\w+)\s+(.+?)\n
    }xgcs
  ){
    $got{lc $1} = $2;
  }
  
  for my $key ( keys %map ){
    if( defined $got{$key} ){
      my $value = $map{$key};
      $got{$value} = $got{$key} if defined $value;
      delete $got{$key};
    }
  }
  
  is_deeply(\%got,$expected,$test);
}
__END__
#ifndef NASM_VERSION_H
#define NASM_VERSION_H
#define NASM_MAJOR_VER      2
#define NASM_MINOR_VER      5
#define NASM_SUBMINOR_VER   99
#define NASM_PATCHLEVEL_VER 100
#define NASM_VERSION_ID     0x02056364
#define NASM_VER            "2.06rc10"
#endif /* NASM_VERSION_H */
