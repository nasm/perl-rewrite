#!/usr/bin/env perl
use strict;
use warnings;

use lib qw't perl/t ..';

my @MODULES = (
  'YAML::XS',
  'version::input',
);

use Test::More;

# Load the testing modules
foreach my $MODULE ( @MODULES ) {
  eval "use $MODULE";
  if ( $@ ) {
    plan( skip_all => "$MODULE not available for testing" );
  }
}


our %test = load(
  filter => [qw'id xid']
);


plan( tests => 2 * scalar keys %test );


for my $test ( sort keys %test ){
  my $expected = $test{$test};
  
  my $id = `echo $test | perl version.pl id`;
  chomp $id;
  is $id, $expected->{id}, $test.'->{id}';
  
  my $xid = `echo $test | perl version.pl xid`;
  chomp $xid;
  is $xid, $expected->{xid}, $test.'->{xid}';
}
__END__
33907556
0x02056364
