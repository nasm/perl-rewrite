#!/usr/bin/env perl
use strict;
use warnings;

use lib qw't perl/t ..';

my %MODULES = (
  'YAML::XS', 'qw[:all]',
  'version::input', '',
);

use Test::More;

# Load the testing modules
foreach my $MODULE ( keys %MODULES ) {
  eval "use $MODULE $MODULES{$MODULE}";
  if ( $@ ) {
    plan( skip_all => "$MODULE not available for testing" );
  }
}


our %test = load(
  filter =>[qw'major minor subminor patchlevel'],
  map    =>{
    version => sub{'"'.$_[1].'"'}
  }
);


plan( tests => scalar keys %test );


my %map = (
  major_ver      => 'major',
  minor_ver      => 'minor',
  subminor_ver   => 'subminor',
  patchlevel_ver => 'patchlevel'
);

for my $test ( sort keys %test ){
  my $expected = $test{$test};
  my $returned = `echo $test | perl version.pl nsis`;
  
  my %got;
  while( $returned =~ m{
    \G\s*
    !define \s+ (\w+) \s+ (.*?)\n
    }xgcs
  ){
    $got{lc $1} = $2;
  }
  
  for my $key ( keys %map ){
    if( defined $got{$key} ){
      my $value = $map{$key};
      $got{$value} = $got{$key} if defined $value;
      delete $got{$key};
    }
  }
  
  is_deeply(\%got,$expected,$test);
}
__END__
!define VERSION "2.06rc10"
!define MAJOR_VER 2
!define MINOR_VER 5
!define SUBMINOR_VER 99
!define PATCHLEVEL_VER 100
