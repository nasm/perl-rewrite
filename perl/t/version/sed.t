#!/usr/bin/env perl
use strict;
use warnings;

use lib qw't perl/t ..';

my %MODULES = (
  'YAML::XS', 'qw[:all]',
  'version::input', '',
);

use Test::More;

# Load the testing modules
foreach my $MODULE ( keys %MODULES ) {
  eval "use $MODULE $MODULES{$MODULE}";
  if ( $@ ) {
    plan( skip_all => "$MODULE not available for testing" );
  }
}


our %test = load(
  filter =>[qw'version major minor subminor patchlevel id xid mangled'],
  map => {
    snapshot => sub{$_[1]||0}
  }
);


plan( tests => scalar keys %test );


my %map = (
  mangled_ver => 'mangled',
  version_id  => 'id',
  version_xid => 'xid',
  ver         => 'version'
);

for my $test ( sort keys %test ){
  my $expected = $test{$test};
  my $returned = `echo $test | perl version.pl sed`;
  
  my %got;
  while( $returned =~ m{
    \G\s*
    s/\@\@NASM_(\w+)\@\@/(.+?)/g\s*
    }xgcs
  ){
    $got{lc $1} = $2;
  }
  
  for my $key ( keys %map ){
    if( defined $got{$key} ){
      my $value = $map{$key};
      $got{$value} = $got{$key} if defined $value;
      delete $got{$key};
    }
  }
  
  is_deeply(\%got,$expected,$test);
}
__END__
s/@@NASM_MAJOR@@/2/g
s/@@NASM_MINOR@@/5/g
s/@@NASM_SUBMINOR@@/99/g
s/@@NASM_PATCHLEVEL@@/100/g
s/@@NASM_SNAPSHOT@@/0/g
s/@@NASM_VERSION_ID@@/33907556/g
s/@@NASM_VERSION_XID@@/0x02056364/g
s/@@NASM_VER@@/2.06rc10/g
s/@@NASM_MANGLED_VER@@/2.05.99.100/g
