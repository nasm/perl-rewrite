package version::input;
use strict;
use warnings;
use YAML::XS ':all';

use base 'Exporter';
our @EXPORT = 'load';


BEGIN{
  eval{ require feature; feature->import('switch') } or
  eval{ require Switch; Switch->import('Perl6')} or
  die "can't find module for 'given' 'when'";
  
  use Scalar::Util 'reftype';
}


our( %test );
{
  # reopen file handle;
  open DATA_DUP, '<&DATA';
  seek( DATA_DUP, 0, 0 );
  
  {
    # seek to end of Perl code
    local $/ = "\n__DATA__\n";
    scalar <DATA_DUP>;
  }
  
  my $yaml_stream = do{
    # read the embedded YAML code
    local $/ = "\n...\n";
    scalar <DATA_DUP>;
  };
  close DATA_DUP;
  
  %test = %{Load($yaml_stream)};
}

# load add => {key=>'default'}, map=>{}, filter=>[]
sub load{
  local( $_, %_ );
  #my($add,$filter,$map,$all);
  my( @filter, %map, $all );
  
  my %return;
  while( my $next = shift ){
    given( $next ){
      when( 'map'    ){
        my $hashref = shift;
        my @keys = keys %$hashref;
        @map{@keys} = @$hashref{@keys};
      }
      when( 'filter' ){ push @filter, @{shift @_} }
      when( 'all'    ){ $all = 1; last }
      default{ die }
    }
  }
  
  for my $version(keys %test){
    my %details = %{$test{$version}};
    
    
    if($all){
      # if $all is set, the caller wanted everything
      $return{$version} = {%details};
    }else{
      # initialize $current;
      my $current = $return{$version} = {};
      
      # pseudo element {version}
      $details{version} = $version;
      
      # filter
      #
      # any element listed here will be added to the output
      for my $add (@filter){
        if('+' eq substr $add, 0, 1){
          # forced to exist
          $add = substr $add, 1;
          $current->{$add} = $details{$add}
        }else{
          # pass it on only if the element is defined
          $current->{$add} = $details{$add} if defined $details{$add};
        }
      }
      
      # map
      for my $id( keys %map ){
        my $ref = $map{$id};
        
        given( reftype $ref ){
          when(undef){ $current->{$id} = $details{$ref} }
          when('CODE'){
            %_ = %details;
            local $_ = $details{$id};
            
            my $return = $ref->(
              $id,
              $details{$id},
              $version,
              {%details}
            );
            
            $current->{$id} = $return;
          }
          default{ die }
        }
      }
    }
  }
  
  
  return %return if wantarray;
  return \%return;
}
1;
__DATA__
---
# currently must only have what would be output with version.pl perl
0.98.09b:
  major:      0
  mangled:    0.98.09b
  minor:      98
  xid:        0x00620900
  id:         6424832
  patchlevel: 0
  subminor:   9
  rc:         0
  tail:       'b'
0.98.24p1:
  major:      0
  mangled:    0.98.24p1
  minor:      98
  xid:        0x00621800
  id:         6428672
  patchlevel: 0
  subminor:   24
  rc:         0
  tail:       'p1'
0.98.25alt:
  major:      0
  mangled:    0.98.25alt
  minor:      98
  xid:        0x00621900
  id:         6428928
  patchlevel: 0
  subminor:   25
  rc:         0
  tail:       'alt'
0.98.39:
  major:      0
  mangled:    0.98.39
  minor:      98
  xid:        0x00622700
  id:         6432512
  patchlevel: 0
  subminor:   39
  rc:         0
0.98bf:
  major:      0
  mangled:    0.98bf
  minor:      98
  xid:        0x00620000
  id:         6422528
  patchlevel: 0
  subminor:   0
  rc:         0
  tail:       'bf'
0.98p1:
  major:      0
  mangled:    0.98p1
  minor:      98
  xid:        0x00620000
  id:         6422528
  patchlevel: 0
  subminor:   0
  rc:         0
  tail:       'p1'
0.98p3-hpa:
  major:      0
  mangled:    0.98p3.hpa
  minor:      98
  xid:        0x00620000
  id:         6422528
  patchlevel: 0
  subminor:   0
  rc:         0
  tail:       'p3-hpa'
0.98p3.5:
  major:      0
  mangled:    0.98p3.5
  minor:      98
  xid:        0x00620000
  id:         6422528
  patchlevel: 0
  subminor:   0
  rc:         0
  tail:       'p3.5'
'2.06':
  major:      2
  mangled:    '2.06'
  minor:      6
  xid:        0x02060000
  id:         33947648
  patchlevel: 0
  subminor:   0
  rc:         0
2.06-2009:
  major:      2
  mangled:    2.06.00.0.2009
  minor:      6
  xid:        0x02060000
  id:         33947648
  patchlevel: 0
  snapshot:   2009
  subminor:   0
  rc:         0
2.06-2009-04-03:
  major:      2
  mangled:    2.06.2009.04.03
  minor:      6
  xid:        0x02060000
  id:         33947648
  patchlevel: 0
  subminor:   0
  rc:         0
  tail:       '-2009-04-03'
2.06.09:
  major:      2
  mangled:    2.06.09
  minor:      6
  xid:        0x02060900
  id:         33949952
  patchlevel: 0
  subminor:   9
  rc:         0
2.06.09pl5rc8:
  major:      2
  mangled:    2.06.09.5rc8
  minor:      6
  xid:        0x02060905
  id:         33949957
  patchlevel: 5
  subminor:   9
  rc:         0
  tail:       'rc8'
2.06.09rc1:
  major:      2
  mangled:    2.06.08.91
  minor:      6
  xid:        0x0206085b
  id:         33949787
  patchlevel: 91
  rc:         1
  subminor:   8
2.06rc8:
  major:      2
  mangled:    2.05.99.98
  minor:      5
  xid:        0x02056362
  id:         33907554
  patchlevel: 98
  rc:         8
  subminor:   99
2.06rc10:
  major:      2
  minor:      5
  id:         33907556
  mangled:    2.05.99.100
  patchlevel: 100
  rc:         10
  subminor:   99
  xid:        0x02056364
...


