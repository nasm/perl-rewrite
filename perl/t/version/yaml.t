#!/usr/bin/env perl
use strict;
use warnings;

use lib qw't perl/t ..';

my @MODULES = (
  'YAML::XS',
  'version::input',
);

use Test::More;

# Load the testing modules
foreach my $MODULE ( @MODULES ) {
  eval "use $MODULE";
  if ( $@ ) {
    plan( skip_all => "$MODULE not available for testing" );
  }
}


our %test = load(
  'all'
);


plan( tests => scalar keys %test );


for my $test ( sort keys %test ){
  my $expected = $test{$test};
  my $returned = `echo $test | perl version.pl yaml`;
  my $got = YAML::XS::Load($returned) || $returned;
  
  is_deeply($got,$expected,$test);
}
__END__
---
id: 33907556
major: 2
mangled: 2.05.99.100
minor: 5
patchlevel: 100
rc: 10
subminor: 99
xid: 0x02056364
