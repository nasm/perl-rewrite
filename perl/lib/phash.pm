=head1 NAME

phash

=head1 DESCRIPTION

Perfect Minimal Hash Generator written in Perl, which produces
C output.

Requires the CPAN Graph module (tested against 0.81, 0.83, 0.84)


=cut

package phash;
use strict;
use warnings;

use random_sv_vectors;
use Nasm::crc64;

use base 'Exporter';

our @EXPORT = qw{
    prehash
    walk_graph
    gen_hash_n
    gen_perfect_hash
    read_input
    verify_hash_table
};


## no critic
sub prehash($$\@);
sub walk_graph(\@\@$$);
sub gen_hash_n($\@\%$);
sub gen_perfect_hash(\%);
sub read_input();
sub verify_hash_table(\%\@);
## use critic

=head2 prehash( $key, $N, @sv )

Compute the prehash for a key

=cut
## no critic
sub prehash($$\@) {
  ## use critic
  my($key, $n, $sv) = @_;
  my @c = crc64(@$sv, $key);

  # Create a bipartite graph...
  my $low_word  = (($c[1] & ($n-1)) << 1) + 0; # low word
  my $high_word = (($c[0] & ($n-1)) << 1) + 1; # high word

  return ($low_word, $high_word);
}

=head2 walk_graph( @node_val, @node_neighbor, $n, $v )

Walk the assignment graph, return true on success

=cut
## no critic
sub walk_graph(\@\@$$) {
  ## use critic
  my($node_val,$node_neighbor,$n,$v) = @_;

  # print STDERR "Vertex $n value $v\n";
  $node_val->[$n] = $v;

  for my $nx ( @{$node_neighbor->[$n]} ){
    # $nx -> [neigh, hash]
    my ($o, $e) = @$nx;
  
    # print STDERR "Edge $n,$o value $e: ";
    my $ov = $node_val->[$o];
    if( defined($ov) ){
      if ($v+$ov != $e) {
        # Cyclic graph with collision
        # print STDERR "error, should be ", $v+$ov, "\n";
        return 0;
      } else {
        # print STDERR "ok\n";
      }
    }elsif( not walk_graph( @$node_val, @$node_neighbor, $o, $e-$v )){
      return 0;
    }
  }
  return 1;
}

=head2 gen_hash_n( $N, @sv, %data, $run )

Generate the function assuming a given N.

=cut
## no critic
sub gen_hash_n($\@\%$) {
  ## use critic
  my($n, $sv, $href, $run) = @_;
  my @keys = keys(%{$href});
  my $gsize = 2*$n;
  my @node_val;
  my @node_neighbor;
  my %edges;

  for( my $i = 0; $i < $gsize; $i++ ){
    $node_neighbor[$i] = [];
  }

  %edges = ();
  for my $key( @keys ){
    my ($pf1, $pf2) = prehash($key, $n, @$sv);
    ($pf1,$pf2) = ($pf2,$pf1) if ($pf1 > $pf2); # Canonicalize order
    
    my $pf = "$pf1,$pf2";
    my $e = $href->{$key};
    my $xkey;
    
    if (defined($xkey = $edges{$pf})) {
      next if ($e == $href->{$xkey}); # Duplicate hash, safe to ignore
      if (defined($run)) {
        print STDERR "$run: Collision: $pf: $key with $xkey\n";
      }
      return;
    }
    
    # print STDERR "Edge $pf value $e from $k\n";
    
    $edges{$pf} = $key;
    push(@{$node_neighbor[$pf1]}, [$pf2, $e]);
    push(@{$node_neighbor[$pf2]}, [$pf1, $e]);
  }

  # Now we need to assign values to each vertex, so that for each
  # edge, the sum of the values for the two vertices give the value
  # for the edge (which is our hash index.)  If we find an impossible
  # sitation, the graph was cyclic.
  @node_val = (undef) x $gsize;

  for( my $i = 0; $i < $gsize; $i++ ){
    if (scalar(@{$node_neighbor[$i]})) {
      # This vertex has neighbors (is used)
      if (!defined($node_val[$i])) {
        # First vertex in a cluster
        unless (walk_graph( @node_val, @node_neighbor, $i, 0)) {
          if (defined($run)) {
            print STDERR "$run: Graph is cyclic\n";
          }
          return;
        }
      }
    }
  }

  # for ($i = 0; $i < $n; $i++) {
  # print STDERR "Vertex ", $i, ": ", $g[$i], "\n";
  # }

  if (defined($run)) {
    printf STDERR "$run: Done: n = $n, sv = [0x%08x, 0x%08x]\n",
    $$sv[0], $$sv[1];
  }

  return ($n, $sv, \@node_val);
}

=head2 gen_perfect_hash( %data )

Driver for generating the function

=cut
## no critic
sub gen_perfect_hash(\%) {
  ## use critic
  my($href) = @_;
  my @keys = keys(%{$href});
  #my @hashinfo;
  my( $n, $i, $j, $sv, $maxj );
  my $run = 1;
  
  # Minimal power of 2 value for N with enough wiggle room.
  # The scaling constant must be larger than 0.5 in order for the
  # algorithm to ever terminate.
  my $room = scalar(@keys)*0.8;
  $n = 1;
  while ($n < $room) {
    $n <<= 1;
  }
  
  # Number of times to try...
  $maxj = scalar @random_sv_vectors;
  
  for ($i = 0; $i < 4; $i++) {
    printf STDERR "%d vectors, trying n = %d...\n",
    scalar @keys, $n;
    for ($j = 0; $j < $maxj; $j++) {
      $sv = $random_sv_vectors[$j];
      my @hashinfo = gen_hash_n($n, @$sv, %$href, $run++);
      
      if( @hashinfo ){
        verify_hash_table(%$href,@hashinfo);
        return @hashinfo;
      }
    }
    $n <<= 1;
  }
  
  die "no hash";
  return;
}

=head2 read_input

Read input file

=cut
sub read_input() {
  my %out;
  my $x = 0;

  while ( my $line = <STDIN> ){
    $line =~ s/\s*(\#.*)?$//;
    next unless $line;
  
    if( $line =~ /^([^=]++)=([^=]++)$/ ){
      $out{$1} = $x = $2;
    } else {
      $out{$line} = $x;
    }
    $x++;
  }

  return  %out if wantarray;
  return \%out;
}

=head2 verify_hash_table( %href, @hashinfo )

Verify that the hash table is actually correct...

=cut
## no critic
sub verify_hash_table(\%\@){
  ## use critic
  my ($href, $hashinfo) = @_;
  my ($n, $sv, $g) = @{$hashinfo};
  my $err = 0;
  
  for my $k (keys(%$href)) {
    my ($pf1, $pf2) = prehash($k, $n, @$sv);
    my $g1 = ${$g}[$pf1];
    my $g2 = ${$g}[$pf2];
    
    if ($g1+$g2 != ${$href}{$k}) {
      printf STDERR "%s(%d,%d): %d+%d = %d != %d\n",
      $k, $pf1, $pf2, $g1, $g2, $g1+$g2, ${$href}{$k};
      $err = 1;
    } else {
      # printf STDERR "%s: %d+%d = %d ok\n",
      # $k, $g1, $g2, $g1+$g2;
    }
  }

  die "$0: hash validation error\n" if ($err);
}

1;
