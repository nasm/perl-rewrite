=head1 NAME

Nasm::Regs::Register

=head1 DESCRIPTION

=head1 Subroutines

=cut

package Nasm::Regs::Register;
use strict;
use warnings;

=head2 new

=cut
sub new{
  my($class,@v) = @_;
  $v[0] =~ s/^\s+|\s+$//g;
  
  my $self = bless \@v, $class;
  
  return $self;
}

=head2 RegisterName

=head2 AssemblerClass

=head2 x86RegisterNumber

=cut
BEGIN{
  ## no critic
  my $i = -1;
  for my $sub_name(qw'RegisterName AssemblerClass',undef,'x86RegisterNumber'){
    no strict 'refs';
    $i++;
    my $i = $i;
    next unless $sub_name;
    *$sub_name = sub{
      my($self) = @_;
      return $self->[$i];
    }
  }
  ## use critic
}

=head2 DisassemblerClasses

=cut
sub DisassemblerClasses{
  my($self) = @_;
  my @classes;
  if( ref $self->[2] ){
    @classes = @{$self->[2]}
  }else{
    @classes = $self->[2]
  }
  return  @classes if wantarray;
  return \@classes;
}

1;
