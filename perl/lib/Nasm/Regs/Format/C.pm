=head1 NAME

Nasm::Regs::Format::C

=cut

package Nasm::Regs::Format::C;
use strict;
use warnings;

sub _format{
	my($regs) = @_;
	my $filename = $regs->filename;
	
	my $out = <<END;
/* automatically generated from $filename - do not edit */

#include "tables.h"

const char * const nasm_reg_names[] = {
END
  
	my @names = $regs->names;
  
	$out .= qq[    "];
	$out .= join qq[",\n    "], @names;
	
	$out .= qq["\n};\n];
	
	return $out;
}

1;
