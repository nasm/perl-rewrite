=head1 NAME

Nasm::Regs::Format::DH

=cut

package Nasm::Regs::Format::DH;
use strict;
use warnings;

sub _format{
  my($regs) = @_;
  my $filename = $regs->filename;
  
  my $out = <<END;
/* automatically generated from $filename - do not edit */

#ifndef NASM_REGDIS_H
#define NASM_REGDIS_H

#include "regs.h"

END
  
  my @classes = $regs->DisassemblerClasses;
  
  for my $class (@classes){
    my @regs = $regs->DisassemblerClass($class);
    $out .= sprintf(
      "extern const enum reg_enum nasm_rd_%-8s[%2d];\n",
      $class,
      scalar @regs
    );
  }
  
  $out .= "\n#endif /* NASM_REGDIS_H */\n";
  
  return $out;
}

1;
__END__

# Output regdis.h
print "/* automatically generated from $file - do not edit */\n\n";
print "#ifndef NASM_REGDIS_H\n";
print "#define NASM_REGDIS_H\n\n";
print "#include \"regs.h\"\n\n";
foreach $class ( sort(keys(%disclass)) ) {
  printf "extern const enum reg_enum nasm_rd_%-8s[%2d];\n",
    $class, scalar @{$disclass{$class}};
}
print "\n#endif /* NASM_REGDIS_H */\n";
