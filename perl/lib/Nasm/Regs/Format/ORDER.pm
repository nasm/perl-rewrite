=head1 NAME

Nasm::Regs::Format::ORDER

=cut

package Nasm::Regs::Format::ORDER;
use strict;
use warnings;


sub _format{
  my($regs) = @_;
  
  join "\n", $regs->RegisterNames;
}
1;
