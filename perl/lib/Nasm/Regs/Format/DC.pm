=head1 NAME

Nasm::Regs::Format::DC

=cut

package Nasm::Regs::Format::DC;
use strict;
use warnings;

sub _format{
  my($regs) = @_;
  my $filename = $regs->filename;
  
  my $out = <<END;
/* automatically generated from $filename - do not edit */

#include "regdis.h"

END
  
  my @classes = $regs->DisassemblerClasses;
  
  for my $class (@classes){
    my @regs = $regs->DisassemblerClass($class);
    $out .= sprintf(
      "const enum reg_enum nasm_rd_%-8s[%2d] = {",
      $class,
      scalar @regs
    );
    
    my @cc = map {  'R_'.uc($_->RegisterName) } @regs;
    
    $out .= join ',', @cc;
    $out .= "};\n";
  }
  
  
  return $out;
}

1;
__END__

# Output regdis.c
print "/* automatically generated from $file - do not edit */\n\n";
print "#include \"regdis.h\"\n\n";
foreach $class ( sort(keys(%disclass)) ) {
  printf "const enum reg_enum nasm_rd_%-8s[%2d] = {",
  $class, scalar @{$disclass{$class}};
  @foo = @{$disclass{$class}};
  @bar = ();
  for ( $i = 0 ; $i < scalar(@foo) ; $i++ ) {
    if (defined($foo[$i])) {
      push(@bar, "R_\U$foo[$i]\E");
    } else {
      die "$0: No register name for class $class, value $i\n";
    }
  }
  print join(',', @bar), "};\n";
}
