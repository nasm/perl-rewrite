=head1 NAME

Nasm::Regs::Format::VC

=cut

package Nasm::Regs::Format::VC;
use strict;
use warnings;

sub _format{
  my($regs) = @_;
  my $filename = $regs->filename;
  
  my $out = <<END;
/* automatically generated from $filename - do not edit */

#include "tables.h"

const int nasm_regvals[] = {
    -1,
END

  my @names = $regs->names;

  #$out .= qq[    "];
  for my $name (@names){
    my $reg = $regs->Register($name)->x86RegisterNumber;
    $out .= sprintf("    %2d,  /* %-5s */\n", $reg, $name)
  }
  
  $out .= "};\n";
  
  return $out;
}

1;
__END__

# Output regvals.c
print "/* automatically generated from $file - do not edit */\n\n";
print "#include \"tables.h\"\n\n";
print "const int nasm_regvals[] = {\n";
print "    -1,\n";		# Dummy entry for 0
foreach $reg ( sort(keys(%regs)) ) {
    # Print the x86 value of the register
    printf "    %2d,  /* %-5s */\n", $regvals{$reg}, $reg;
}
print "};\n";
