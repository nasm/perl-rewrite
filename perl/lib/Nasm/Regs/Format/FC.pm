=head1 NAME

Nasm::Regs::Format::FC

=cut

package Nasm::Regs::Format::FC;
use strict;
use warnings;

sub _format{
  my($regs) = @_;
  my $filename = $regs->filename;
  
  my $out = <<END;
/* automatically generated from $filename - do not edit */

#include "tables.h"
#include "nasm.h"

const int32_t nasm_reg_flags[] = {
    0,
END

  my @names = $regs->names;

  #$out .= qq[    "];
  for my $name (@names){
    my $reg = $regs->Register($name)->AssemblerClass;
    $out .= sprintf("    %-15s /* %-5s */\n", $reg, $name)
  }
  
  $out .= "};\n";
  
  return $out;
}

1;

__END__
# Output regflags.c
print "/* automatically generated from $file - do not edit */\n\n";
print "#include \"tables.h\"\n";
print "#include \"nasm.h\"\n\n";
print "const int32_t nasm_reg_flags[] = {\n";
printf "    0,\n";		# Dummy entry for 0
foreach $reg ( sort(keys(%regs)) ) {
# Print the class of the register
printf "    %-15s /* %-5s */\n",
$regs{$reg}.',', $reg;
}
print "};\n";
