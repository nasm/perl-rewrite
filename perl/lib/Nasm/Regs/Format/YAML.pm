=head1 NAME

Nasm::Regs::Format::YAML

=cut

package Nasm::Regs::Format::YAML;
use strict;
use warnings;

use YAML::XS;

sub _format{
  my($regs) = @_;
  
  my @names = $regs->RegisterNames;
  my %data;
  
  my $count = 1;
  for my $name (@names){
    my $reg = $regs->Register($name);
    $data{$name} = {
      AssemblerClass      => $reg->AssemblerClass,
      DisassemblerClasses => [$reg->DisassemblerClasses],
      x86RegisterNumber   => $reg->x86RegisterNumber,
      Index               => $count++
    }
  }
  
  Dump \%data
}
1;
