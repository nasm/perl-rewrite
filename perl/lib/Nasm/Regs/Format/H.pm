=head1 NAME

Nasm::Regs::Format::H

=cut

package Nasm::Regs::Format::H;
use strict;
use warnings;


sub _format{
  my($regs) = @_;
  my $filename = $regs->filename;
  my $out = <<END;
/* automatically generated from $filename - do not edit */

#ifndef NASM_REGS_H
#define NASM_REGS_H
END
  $out .= _format_h_enum($regs);
  $out .= "\n";
  $out .= _format_h_define($regs);
  $out .= "\n\n#endif /* NASM_REGS_H */\n";
  return $out;
}




#  Internal for format h
sub _format_h_enum{
  my($regs) = @_;
  my $count = 1;
  my $out = <<END;

#define EXPR_REG_START $count

enum reg_enum {
    R_zero = 0,
    R_none = -1,
END
  
  my $append = ' = EXPR_REG_START';
  
  for my $name ( $regs->names ){
    next unless $name;
    my $reg = $regs->{$name};
    $out .= '    R_'.uc($name).$append.",\n";
    
    $append = '' if $append;
    $count++;
  }
  $count--;
  
  $out .= <<END;
    REG_ENUM_LIMIT
};

#define EXPR_REG_END $count
END
  
  return $out;
}




#  Internal for format h
sub _format_h_define{
  my($regs) = @_;
  my $out = '';
  for my $name ( $regs->names ) {
    my $register = $regs->Register($name);
    
    $out .= sprintf
    "#define %-15s %2d\n",
    'REG_NUM_'.uc($name),
    $register->x86RegisterNumber;
  }
  return $out;
}

1;
