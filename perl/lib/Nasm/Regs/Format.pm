=head1 NAME

Nasm::Regs::Format

=head1 DESCRIPTION



=head1 Subroutines

=cut

package Nasm::Regs::Format;
use strict;
use warnings;

#our @list = qw'h c fc vc dc dh';

=head2 format

Returns regs info in a given format

=cut
sub format{
  my($regs,$fmt) = @_;
  ## no critic
  no strict 'refs';
  
  my $package = __PACKAGE__.'::'.uc($fmt);
  eval "require $package;";
  ## use critic
  if($@){
    die qq[Failed to load format "$fmt"\n];
  }
  
  my $return;
  
  eval{
    $return = *{$package.'::_format'}{CODE}->($regs);
  };
  if($@){
    die qq[Problem with format plugin "$fmt":\n\t$@];
  }
  return $return;
}

1;
