=head1 NAME

Nasm::Regs

=head1 DESCRIPTION

=head1 Subroutines

=cut

package Nasm::Regs;
use strict;
use warnings;

require Nasm::Regs::Register;
require Nasm::Regs::Format;

=head2 new

Creates Nasm::Regs object

=cut
sub new{
  my($class,$filename) = @_;
  my $self = bless {}, $class;
  
  if( @_ > 1 and $filename ){
    $self->ProcessFile($filename);
  }
  
  return $self;
}


=head2 ProcessFile

Loads the file

=cut
sub ProcessFile{
  my($self,$filename) = @_;
  my $file;
  
  if( $filename eq '-'){
    $file = *{STDIN}{IO};
  }else{
    open $file, '<', $filename or die;
  }
  $self->{filename} = $filename;
  
  my %current =( line_number => -1 );
  
  while( my $line = <$file> ){
    $current{line_number}++;
    chomp $line;
    $line =~ s/\s*(?:\#.*)?$//;
    next unless length $line;
    
    $current{line} = $line;
    $self->_ProcessLine(\%current);
  }
  
  close $file;
  return $self;
}



sub _ProcessLine{
  my($self,$current) = @_;
  
  unless( $current->{line} =~ /^\s*(\S+)\s*(\S+)\s*(\S+)\s*([0-9]+)$/ ){
    die;
  }
  
  my ($register,$a_class,$d_classes,$x86regno) = ($1,$2,$3,$4);
  
  my @reg_list;
  
  if ( $register =~ m{
    ^
    (.*?)  (\d+)  -  (\d+)  (.*)
    $
  }x) {
    for my $i ($2..$3){
      push @reg_list, $1. $i . $4
    }
  } else {
    
    @reg_list = $register;
  }
  
  # force numeric conversion
  $x86regno += 0;
  
  my @d_classes = split ',', $d_classes;
  #my %d_is_class;
  
  for my $register(@reg_list){
    my $reg_obj = Nasm::Regs::Register->new($register,$a_class,[@d_classes],$x86regno);
    $self->{register}{$register} = $reg_obj;
    
    for my $class (@d_classes){
      $self->{disassembler}{$class}[$x86regno] = $reg_obj
    }
    
    $x86regno++;
  }
}




=head2 names

Returns a sorted list of RegisterNames

=cut
sub names{
  my($self) = @_;
  # deprecated
  return $self->RegisterNames;
}

=head2 RegisterNames

Returns a sorted list of RegisterNames

=cut
sub RegisterNames{
  my($self) = @_;
  my @keys = sort grep { length $_ } keys %{$self->{register}};
  
  return  @keys if wantarray;
  return \@keys;
}



=head2 Register( $register_name )

Returns a Register object

=cut
sub Register{
  my($self,$register_name) = @_;
  return $self->{register}{$register_name};
}

=head2 DisassemblerClasses

Returns a sorted list of DisassemblerClasses

=cut
sub DisassemblerClasses{
  my($self) = @_;
  my @return = sort keys %{$self->{disassembler}};
  
  return  @return if wantarray;
  return \@return;
}

=head2 DisassemblerClass( $class )



=cut
sub DisassemblerClass{
  my($self,$class) = @_;
  my $return = $self->{disassembler}{$class};
  return @$return if wantarray;
  return  $return;
}


=head2 format( $format_type )

Returns regs info in a given format

=cut
sub format{
  my($self,$fmt) = @_;
  require Nasm::Regs::Format;
  Nasm::Regs::Format::format($self,$fmt);
}


=head2 filename

Returns the name of the file the data was loaded from

=cut
sub filename{
  my($self) = @_;
  return $self->{filename};
}



1;
