=head1 NAME

Nasm::insns

=head1 DESCRIPTION

Loads info from insns.dat

=head1 Subroutines

=cut

package Nasm::insns;
use strict;
use warnings;

use base 'Exporter';

require Nasm::insns::Operands;
require Nasm::insns::Flags;

# Opcode prefixes which need their own opcode tables
# LONGER PREFIXES FIRST!
our @disasm_prefixes = qw(0F24 0F25 0F38 0F3A 0F7A 0FA6 0FA7 0F);

# This should match MAX_OPERANDS from nasm.h
our $MAX_OPERANDS = 5;

# Add VEX prefixes
our @vexlist;
for( my $m = 0; $m < 32; $m++ ){
  for( my $lp = 0; $lp < 8; $lp++ ){
    push(@vexlist, sprintf("VEX%02X%01X", $m, $lp));
  }
}
@disasm_prefixes = (@vexlist, @disasm_prefixes);

our @bytecode_count = (0) x 256;

=head2 new

creates a new insns object

=cut
sub new{
  my($class,$filename) = @_;
  
  my $self = bless {}, $class;
  
  if($filename){
    $self->ProcessFile($filename);
  }
  
  return $self;
}

=head2 ProcessFile

Reads the file

=cut
sub ProcessFile{
  my( $self, $filename ) = @_;
  open( my $file, '<', $filename ) || die "unable to open $filename";
  
  $self->{filename} = $filename;
  
  my $line_number = 0;
  while( my $line = <$file> ){
    $line_number++;
    chomp $line;
    
    # /^\s*(?:;\#)(.*)$/ # special lines
    next if $line =~ /^\s*(?:;|$)/ ;   # comments or blank lines
    
    $self->_ProcessLine($line,$line_number);
  }
}

sub _ProcessLine{
  my( $self, $line, $line_number ) = @_;
  
  unless( $line =~ m{
    ^\s*+
    (\S+)\s++
    (\S+)\s++
    (
      \[.*?\] |
      \S+
    )\s++
    (\S++)\s*+
    $
  }x){
    die;
  }
  
  my($name,$operands,$code_string,$flags) = ($1,$2,$3,$4);
  
  my $op_obj = Nasm::insns::Operands->new($operands);
  my $flag_obj = Nasm::insns::Flags->new($flags);
  
  my $ref = $self->{ops}{$name.':'.$op_obj->string} = {
    operands   => $op_obj,
    flags      => $flag_obj,
    codestring => $code_string
  };
}

1;
