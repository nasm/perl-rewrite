=head1 NAME

Nasm::Utils

=head1 DESCRIPTION

Several utilities used in other modules

=head1 Subroutines

=cut

package Nasm::Utils;
use strict;
use warnings;

use base 'Exporter';

our @EXPORT_OK = qw{
  str2hex
  addprefix
};

=head2 str2hex

Turn a numeric list into a hex string

=cut
sub str2hex{
  my @return = map {sprintf("%02X", $_)} @_;
  
  return @return if wantarray;
  return join '', @return;
}

=head2 addprefix( $prefix, @list )

Takes a prefix, and a list of strings, and
adds the prefix to each element in the list

=cut
sub addprefix{
  my ($prefix, @list) = @_;
  my @return = map {
      sprintf("%s%02X", $prefix, $_)
  } @list;
  
  return @return if wantarray;
  die "Don't know what to do in scalar context.";
}

1;
