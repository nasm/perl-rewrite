=head1 NAME

Nasm::insns::Flags

=cut

package Nasm::insns::Flags;
use strict;
use warnings;
use YAML::XS ':all';

#our @arch = qw{
#  AMD
#  8086 186 286 386 486
#  X64 X86_64 PENT CYRIX P6 IA64
#  PRESCOTT
#  FPU
#  MMX SSE SSE2 3DNOW
#};

our( %map2id, %also_enable, @arch);

# load up variables from the data below __DATA__
{
  my @yaml_streams;
  {
    ## no critic
    open( my $data, '<&DATA' ) or die;
    ## use critic
    seek( $data, 0, 0 );
    
    {
      # seek to end of Perl code
      local $/ = "\n__DATA__\n";
      scalar <$data>;
    }
    {
      # split the embedded YAML code on '...'
      local $/ = "\n...\n";
      @yaml_streams = <$data>;
    }
    close $data;
  }
  
  #use Data::Dump 'dump';
  #use 5.010;
  use Scalar::Util qw'reftype';
  
  my $dir = Load $yaml_streams[0];
  for my $variable_name ( keys %$dir ){
    my $index = $dir->{$variable_name};
    
    $variable_name =~ s/^([%@\$])//;
    my $type = $1;
    
    ## no critic
    no strict qw'refs';
    no warnings qw'once';
    ## use critic
    
    # load on demand
    my $ref = Load $yaml_streams[$index] or
      warn "unable to load YAML item \"$variable_name\"\n";
    
    my $reftype = reftype $ref;
    my %type_map = (
      HASH   => '%',
      ARRAY  => '@',
      SCALAR => '$',
      REF    => '$'
    );
    
    $type ||= $type_map{$reftype};
    
    if( $type eq '%' ){
      die unless $reftype eq 'HASH';
      %{*$variable_name} = %$ref;
    }elsif( $type eq '@' ){
      die unless $reftype eq 'ARRAY';
      @{*$variable_name} = @$ref;
    }elsif( $type eq '$' ){
      ${*$variable_name} = $ref;
    }
  }
}

#use 5.010;
#use Data::Dump 'dump';
#
#say dump $_ for ( \%map2id, \%also_enable, \@arch);
# end of initialization





=head2 new

Creates a new Nasm::insns::Flags object

=cut

sub new{
  my( $class, $string ) = @_;
  
  my $self = bless [], $class;
  
  $string =~ s/^ \s+  //gx;
  $string =~ s/  \s+ $//gx;
  
  return $self unless $string;
  return $self if $string eq 'ignore';
  
  @$self = split ',', $string;
  
  return $self;
}

1;

#
# All YAML streams must be seperated by a single "..." line
#
# The first YAML stream should start immediately after the __DATA__ line
#
# The first YAML stream is the dir stream, it is used to map the global variable
# to the YAML stream
#
__DATA__
---
# "global variable": "YAML stream number"
# all of the following global variables should have been
# defined with "our %global" at the top of the file
#
# any stream not referenced here will not be loaded
"%map2id":      1
"%also_enable": 3
"@arch":        4
...
---
# "external name": "internal id"
PENT:  Pentium
80186: 186
80286: 286
80386: 386
...
---
# "internal id": "external name"
# if an internal id is not in this stream
# then use the internal id for external name
Pentium: PENT
...
---
# when Pentium is enabled: 486 is also enabled
# which then enables 386, etc
Pentium:
- 486
486:
- 386
386:
- 286
286:
- 186
186:
- 8086
...
---
- KATMAI
- PRESCOTT
- NEHALEM
- WILLAMETTE
- WESTMERE
- SANDYBRIDGE
- SSE
- SSE2
- SSE3
- SSE4A
- SSE41
- SSE42
- SSE5
...
