=head1 NAME

Nasm::insns::Operands

=cut

package Nasm::insns::Operands;
use strict;
use warnings;
use Scalar::Util 'reftype';

=head2 new

Creates a new Nasm::insns::Operands object

=cut

sub new{
  my( $class, $string ) = @_;
  $string =~ s/^ \s+  //gx;
  $string =~ s/  \s+ $//gx;
  
  if( $string eq 'ignore' ){
    return bless \$string, $class;
  }
  
  my $self = bless [], $class;
  
  
  return $self unless $string;
  return $self if $string eq 'void';
  
  @$self = split ',', $string;
  
  return $self;
}


=head2 string

Stringifys an Nasm::insns::Operands object

=cut

sub string{
  my( $self ) = @_;
  
  return '' if reftype $self eq 'SCALAR';
  return '' if reftype $self eq 'REF';
  
  #return '()' unless @$self;
  return '('.join(',',@$self).')';
}
1;
