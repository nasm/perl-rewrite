#!/usr/bin/env perl

use strict;
use warnings;
use lib 'lib';

=head

reads a file with columns:

register name, assembler class, disassembler class(es), x86 register number


outputs in formats:

h c fc vc dc dh


regs.pl [output format] [input filename]

=cut

use Nasm::Regs;

my ($format,$filename) = @ARGV;
$filename ||= 'regs.dat';

unless($format){
  print help();
  exit;
}

my $self = Nasm::Regs->new($filename);

my $str = $self->format($format);
print $str;
print "\n" unless $str =~ /\n\Z/;

sub help{
  "$0 [output format] [input filename]\n"
}
