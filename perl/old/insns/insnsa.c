/* This file auto-generated from insns.dat by insns.pl - don't edit it */

#include "nasm.h"
#include "insns.h"

static const struct itemplate instrux_AAA[] = {
    {I_AAA, 0, {0,0,0,0,0}, nasm_bytecodes+20413, IF_8086|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_AAD[] = {
    {I_AAD, 0, {0,0,0,0,0}, nasm_bytecodes+19365, IF_8086|IF_NOLONG},
    {I_AAD, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+19369, IF_8086|IF_SB|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_AAM[] = {
    {I_AAM, 0, {0,0,0,0,0}, nasm_bytecodes+19373, IF_8086|IF_NOLONG},
    {I_AAM, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+19377, IF_8086|IF_SB|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_AAS[] = {
    {I_AAS, 0, {0,0,0,0,0}, nasm_bytecodes+20416, IF_8086|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_ADC[] = {
    {I_ADC, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+19381, IF_8086|IF_SM},
    {I_ADC, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+19381, IF_8086},
    {I_ADC, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+17490, IF_8086|IF_SM},
    {I_ADC, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17490, IF_8086},
    {I_ADC, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+17495, IF_386|IF_SM},
    {I_ADC, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17495, IF_386},
    {I_ADC, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+17500, IF_X64|IF_SM},
    {I_ADC, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17500, IF_X64},
    {I_ADC, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+10672, IF_8086|IF_SM},
    {I_ADC, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+10672, IF_8086},
    {I_ADC, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+17505, IF_8086|IF_SM},
    {I_ADC, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17505, IF_8086},
    {I_ADC, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+17510, IF_386|IF_SM},
    {I_ADC, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17510, IF_386},
    {I_ADC, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+17515, IF_X64|IF_SM},
    {I_ADC, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17515, IF_X64},
    {I_ADC, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+13980, IF_8086},
    {I_ADC, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+13986, IF_386},
    {I_ADC, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+13992, IF_X64},
    {I_ADC, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+19385, IF_8086|IF_SM},
    {I_ADC, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+13980, IF_8086|IF_SM},
    {I_ADC, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+17520, IF_8086|IF_SM},
    {I_ADC, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+13986, IF_386|IF_SM},
    {I_ADC, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17525, IF_386|IF_SM},
    {I_ADC, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+13992, IF_X64|IF_SM},
    {I_ADC, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17530, IF_X64|IF_SM},
    {I_ADC, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+17535, IF_8086|IF_SM},
    {I_ADC, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+13998, IF_8086|IF_SM},
    {I_ADC, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14004, IF_386|IF_SM},
    {I_ADC, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14010, IF_X64|IF_SM},
    {I_ADC, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+17535, IF_8086|IF_SM},
    {I_ADC, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+13998, IF_8086|IF_SM},
    {I_ADC, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14004, IF_386|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_ADD[] = {
    {I_ADD, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+19389, IF_8086|IF_SM},
    {I_ADD, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+19389, IF_8086},
    {I_ADD, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+17540, IF_8086|IF_SM},
    {I_ADD, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17540, IF_8086},
    {I_ADD, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+17545, IF_386|IF_SM},
    {I_ADD, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17545, IF_386},
    {I_ADD, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+17550, IF_X64|IF_SM},
    {I_ADD, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17550, IF_X64},
    {I_ADD, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+11323, IF_8086|IF_SM},
    {I_ADD, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+11323, IF_8086},
    {I_ADD, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+17555, IF_8086|IF_SM},
    {I_ADD, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17555, IF_8086},
    {I_ADD, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+17560, IF_386|IF_SM},
    {I_ADD, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17560, IF_386},
    {I_ADD, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+17565, IF_X64|IF_SM},
    {I_ADD, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17565, IF_X64},
    {I_ADD, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14016, IF_8086},
    {I_ADD, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14022, IF_386},
    {I_ADD, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14028, IF_X64},
    {I_ADD, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+19393, IF_8086|IF_SM},
    {I_ADD, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+14016, IF_8086|IF_SM},
    {I_ADD, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+17570, IF_8086|IF_SM},
    {I_ADD, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+14022, IF_386|IF_SM},
    {I_ADD, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17575, IF_386|IF_SM},
    {I_ADD, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+14028, IF_X64|IF_SM},
    {I_ADD, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17580, IF_X64|IF_SM},
    {I_ADD, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+17585, IF_8086|IF_SM},
    {I_ADD, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14034, IF_8086|IF_SM},
    {I_ADD, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14040, IF_386|IF_SM},
    {I_ADD, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14046, IF_X64|IF_SM},
    {I_ADD, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+17585, IF_8086|IF_SM},
    {I_ADD, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14034, IF_8086|IF_SM},
    {I_ADD, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14040, IF_386|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_ADDPD[] = {
    {I_ADDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15924, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_ADDPS[] = {
    {I_ADDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15210, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_ADDSD[] = {
    {I_ADDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15930, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_ADDSS[] = {
    {I_ADDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15216, IF_KATMAI|IF_SSE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_ADDSUBPD[] = {
    {I_ADDSUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16200, IF_PRESCOTT|IF_SSE3|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_ADDSUBPS[] = {
    {I_ADDSUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16206, IF_PRESCOTT|IF_SSE3|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_AESDEC[] = {
    {I_AESDEC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9171, IF_SSE|IF_WESTMERE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_AESDECLAST[] = {
    {I_AESDECLAST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9178, IF_SSE|IF_WESTMERE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_AESENC[] = {
    {I_AESENC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9157, IF_SSE|IF_WESTMERE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_AESENCLAST[] = {
    {I_AESENCLAST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9164, IF_SSE|IF_WESTMERE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_AESIMC[] = {
    {I_AESIMC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9185, IF_SSE|IF_WESTMERE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_AESKEYGENASSIST[] = {
    {I_AESKEYGENASSIST, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5911, IF_SSE|IF_WESTMERE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_AND[] = {
    {I_AND, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+19397, IF_8086|IF_SM},
    {I_AND, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+19397, IF_8086},
    {I_AND, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+17590, IF_8086|IF_SM},
    {I_AND, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17590, IF_8086},
    {I_AND, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+17595, IF_386|IF_SM},
    {I_AND, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17595, IF_386},
    {I_AND, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+17600, IF_X64|IF_SM},
    {I_AND, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17600, IF_X64},
    {I_AND, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+11610, IF_8086|IF_SM},
    {I_AND, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+11610, IF_8086},
    {I_AND, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+17605, IF_8086|IF_SM},
    {I_AND, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17605, IF_8086},
    {I_AND, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+17610, IF_386|IF_SM},
    {I_AND, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17610, IF_386},
    {I_AND, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+17615, IF_X64|IF_SM},
    {I_AND, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17615, IF_X64},
    {I_AND, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14052, IF_8086},
    {I_AND, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14058, IF_386},
    {I_AND, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14064, IF_X64},
    {I_AND, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+19401, IF_8086|IF_SM},
    {I_AND, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+14052, IF_8086|IF_SM},
    {I_AND, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+17620, IF_8086|IF_SM},
    {I_AND, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+14058, IF_386|IF_SM},
    {I_AND, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17625, IF_386|IF_SM},
    {I_AND, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+14064, IF_X64|IF_SM},
    {I_AND, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17630, IF_X64|IF_SM},
    {I_AND, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+17635, IF_8086|IF_SM},
    {I_AND, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14070, IF_8086|IF_SM},
    {I_AND, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14076, IF_386|IF_SM},
    {I_AND, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14082, IF_X64|IF_SM},
    {I_AND, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+17635, IF_8086|IF_SM},
    {I_AND, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14070, IF_8086|IF_SM},
    {I_AND, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14076, IF_386|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_ANDNPD[] = {
    {I_ANDNPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15936, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_ANDNPS[] = {
    {I_ANDNPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15222, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_ANDPD[] = {
    {I_ANDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15942, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_ANDPS[] = {
    {I_ANDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15228, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_ARPL[] = {
    {I_ARPL, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+19405, IF_286|IF_PROT|IF_SM|IF_NOLONG},
    {I_ARPL, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19405, IF_286|IF_PROT|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_BB0_RESET[] = {
    {I_BB0_RESET, 0, {0,0,0,0,0}, nasm_bytecodes+19409, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_BB1_RESET[] = {
    {I_BB1_RESET, 0, {0,0,0,0,0}, nasm_bytecodes+19413, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_BLENDPD[] = {
    {I_BLENDPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5607, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_BLENDPS[] = {
    {I_BLENDPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5615, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_BLENDVPD[] = {
    {I_BLENDVPD, 3, {XMMREG,RM_XMM,XMM0,0,0}, nasm_bytecodes+7918, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_BLENDVPS[] = {
    {I_BLENDVPS, 3, {XMMREG,RM_XMM,XMM0,0,0}, nasm_bytecodes+7925, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_BOUND[] = {
    {I_BOUND, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+17640, IF_186|IF_NOLONG},
    {I_BOUND, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+17645, IF_386|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_BSF[] = {
    {I_BSF, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14088, IF_386|IF_SM},
    {I_BSF, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14088, IF_386},
    {I_BSF, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14094, IF_386|IF_SM},
    {I_BSF, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14094, IF_386},
    {I_BSF, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+14100, IF_X64|IF_SM},
    {I_BSF, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14100, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_BSR[] = {
    {I_BSR, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14106, IF_386|IF_SM},
    {I_BSR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14106, IF_386},
    {I_BSR, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14112, IF_386|IF_SM},
    {I_BSR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14112, IF_386},
    {I_BSR, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+14118, IF_X64|IF_SM},
    {I_BSR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14118, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_BSWAP[] = {
    {I_BSWAP, 1, {REG32,0,0,0,0}, nasm_bytecodes+14124, IF_486},
    {I_BSWAP, 1, {REG64,0,0,0,0}, nasm_bytecodes+14130, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_BT[] = {
    {I_BT, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14136, IF_386|IF_SM},
    {I_BT, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14136, IF_386},
    {I_BT, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14142, IF_386|IF_SM},
    {I_BT, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14142, IF_386},
    {I_BT, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+14148, IF_X64|IF_SM},
    {I_BT, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14148, IF_X64},
    {I_BT, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+6735, IF_386|IF_SB},
    {I_BT, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+6742, IF_386|IF_SB},
    {I_BT, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+6749, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_BTC[] = {
    {I_BTC, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14154, IF_386|IF_SM},
    {I_BTC, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14154, IF_386},
    {I_BTC, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14160, IF_386|IF_SM},
    {I_BTC, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14160, IF_386},
    {I_BTC, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+14166, IF_X64|IF_SM},
    {I_BTC, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14166, IF_X64},
    {I_BTC, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+6756, IF_386|IF_SB},
    {I_BTC, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+6763, IF_386|IF_SB},
    {I_BTC, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+6770, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_BTR[] = {
    {I_BTR, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14172, IF_386|IF_SM},
    {I_BTR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14172, IF_386},
    {I_BTR, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14178, IF_386|IF_SM},
    {I_BTR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14178, IF_386},
    {I_BTR, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+14184, IF_X64|IF_SM},
    {I_BTR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14184, IF_X64},
    {I_BTR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+6777, IF_386|IF_SB},
    {I_BTR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+6784, IF_386|IF_SB},
    {I_BTR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+6791, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_BTS[] = {
    {I_BTS, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14190, IF_386|IF_SM},
    {I_BTS, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14190, IF_386},
    {I_BTS, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14196, IF_386|IF_SM},
    {I_BTS, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14196, IF_386},
    {I_BTS, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+14202, IF_X64|IF_SM},
    {I_BTS, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14202, IF_X64},
    {I_BTS, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+6798, IF_386|IF_SB},
    {I_BTS, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+6805, IF_386|IF_SB},
    {I_BTS, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+6812, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_CALL[] = {
    {I_CALL, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+17650, IF_8086},
    {I_CALL, 1, {IMMEDIATE|NEAR,0,0,0,0}, nasm_bytecodes+17650, IF_8086},
    {I_CALL, 1, {IMMEDIATE|FAR,0,0,0,0}, nasm_bytecodes+14208, IF_8086|IF_NOLONG},
    {I_CALL, 1, {IMMEDIATE|BITS16,0,0,0,0}, nasm_bytecodes+17655, IF_8086},
    {I_CALL, 1, {IMMEDIATE|BITS16|NEAR,0,0,0,0}, nasm_bytecodes+17655, IF_8086},
    {I_CALL, 1, {IMMEDIATE|BITS16|FAR,0,0,0,0}, nasm_bytecodes+14214, IF_8086|IF_NOLONG},
    {I_CALL, 1, {IMMEDIATE|BITS32,0,0,0,0}, nasm_bytecodes+17660, IF_386},
    {I_CALL, 1, {IMMEDIATE|BITS32|NEAR,0,0,0,0}, nasm_bytecodes+17660, IF_386},
    {I_CALL, 1, {IMMEDIATE|BITS32|FAR,0,0,0,0}, nasm_bytecodes+14220, IF_386|IF_NOLONG},
    {I_CALL, 2, {IMMEDIATE|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14226, IF_8086|IF_NOLONG},
    {I_CALL, 2, {IMMEDIATE|BITS16|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14232, IF_8086|IF_NOLONG},
    {I_CALL, 2, {IMMEDIATE|COLON,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14232, IF_8086|IF_NOLONG},
    {I_CALL, 2, {IMMEDIATE|BITS32|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14238, IF_386|IF_NOLONG},
    {I_CALL, 2, {IMMEDIATE|COLON,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14238, IF_386|IF_NOLONG},
    {I_CALL, 1, {MEMORY|FAR,0,0,0,0}, nasm_bytecodes+17665, IF_8086|IF_NOLONG},
    {I_CALL, 1, {MEMORY|FAR,0,0,0,0}, nasm_bytecodes+17670, IF_X64},
    {I_CALL, 1, {MEMORY|BITS16|FAR,0,0,0,0}, nasm_bytecodes+17675, IF_8086},
    {I_CALL, 1, {MEMORY|BITS32|FAR,0,0,0,0}, nasm_bytecodes+17680, IF_386},
    {I_CALL, 1, {MEMORY|BITS64|FAR,0,0,0,0}, nasm_bytecodes+17670, IF_X64},
    {I_CALL, 1, {MEMORY|NEAR,0,0,0,0}, nasm_bytecodes+17685, IF_8086},
    {I_CALL, 1, {MEMORY|BITS16|NEAR,0,0,0,0}, nasm_bytecodes+17690, IF_8086},
    {I_CALL, 1, {MEMORY|BITS32|NEAR,0,0,0,0}, nasm_bytecodes+17695, IF_386|IF_NOLONG},
    {I_CALL, 1, {MEMORY|BITS64|NEAR,0,0,0,0}, nasm_bytecodes+17700, IF_X64},
    {I_CALL, 1, {REG16,0,0,0,0}, nasm_bytecodes+17690, IF_8086},
    {I_CALL, 1, {REG32,0,0,0,0}, nasm_bytecodes+17695, IF_386|IF_NOLONG},
    {I_CALL, 1, {REG64,0,0,0,0}, nasm_bytecodes+17705, IF_X64},
    {I_CALL, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+17685, IF_8086},
    {I_CALL, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+17690, IF_8086},
    {I_CALL, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+17695, IF_386|IF_NOLONG},
    {I_CALL, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+17705, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_CBW[] = {
    {I_CBW, 0, {0,0,0,0,0}, nasm_bytecodes+19417, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_CDQ[] = {
    {I_CDQ, 0, {0,0,0,0,0}, nasm_bytecodes+19421, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_CDQE[] = {
    {I_CDQE, 0, {0,0,0,0,0}, nasm_bytecodes+19425, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_CLC[] = {
    {I_CLC, 0, {0,0,0,0,0}, nasm_bytecodes+19137, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_CLD[] = {
    {I_CLD, 0, {0,0,0,0,0}, nasm_bytecodes+20419, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_CLFLUSH[] = {
    {I_CLFLUSH, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19310, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CLGI[] = {
    {I_CLGI, 0, {0,0,0,0,0}, nasm_bytecodes+17710, IF_X64|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_CLI[] = {
    {I_CLI, 0, {0,0,0,0,0}, nasm_bytecodes+20422, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_CLTS[] = {
    {I_CLTS, 0, {0,0,0,0,0}, nasm_bytecodes+19429, IF_286|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMC[] = {
    {I_CMC, 0, {0,0,0,0,0}, nasm_bytecodes+20425, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMP[] = {
    {I_CMP, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+19433, IF_8086|IF_SM},
    {I_CMP, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+19433, IF_8086},
    {I_CMP, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+17715, IF_8086|IF_SM},
    {I_CMP, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17715, IF_8086},
    {I_CMP, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+17720, IF_386|IF_SM},
    {I_CMP, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17720, IF_386},
    {I_CMP, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+17725, IF_X64|IF_SM},
    {I_CMP, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17725, IF_X64},
    {I_CMP, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+11568, IF_8086|IF_SM},
    {I_CMP, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+11568, IF_8086},
    {I_CMP, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+17730, IF_8086|IF_SM},
    {I_CMP, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17730, IF_8086},
    {I_CMP, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+17735, IF_386|IF_SM},
    {I_CMP, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17735, IF_386},
    {I_CMP, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+17740, IF_X64|IF_SM},
    {I_CMP, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17740, IF_X64},
    {I_CMP, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14244, IF_8086},
    {I_CMP, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14250, IF_386},
    {I_CMP, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14256, IF_X64},
    {I_CMP, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+19437, IF_8086|IF_SM},
    {I_CMP, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+14244, IF_8086|IF_SM},
    {I_CMP, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+17745, IF_8086|IF_SM},
    {I_CMP, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+14250, IF_386|IF_SM},
    {I_CMP, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17750, IF_386|IF_SM},
    {I_CMP, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+14256, IF_X64|IF_SM},
    {I_CMP, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17755, IF_X64|IF_SM},
    {I_CMP, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+17760, IF_8086|IF_SM},
    {I_CMP, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14262, IF_8086|IF_SM},
    {I_CMP, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14268, IF_386|IF_SM},
    {I_CMP, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14274, IF_X64|IF_SM},
    {I_CMP, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+17760, IF_8086|IF_SM},
    {I_CMP, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14262, IF_8086|IF_SM},
    {I_CMP, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14268, IF_386|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPEQPD[] = {
    {I_CMPEQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5431, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPEQPS[] = {
    {I_CMPEQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5255, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPEQSD[] = {
    {I_CMPEQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5439, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPEQSS[] = {
    {I_CMPEQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5263, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPLEPD[] = {
    {I_CMPLEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5447, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPLEPS[] = {
    {I_CMPLEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5271, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPLESD[] = {
    {I_CMPLESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5455, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPLESS[] = {
    {I_CMPLESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5279, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPLTPD[] = {
    {I_CMPLTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5463, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPLTPS[] = {
    {I_CMPLTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5287, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPLTSD[] = {
    {I_CMPLTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5471, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPLTSS[] = {
    {I_CMPLTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5295, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNEQPD[] = {
    {I_CMPNEQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5479, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNEQPS[] = {
    {I_CMPNEQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5303, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNEQSD[] = {
    {I_CMPNEQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5487, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNEQSS[] = {
    {I_CMPNEQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5311, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNLEPD[] = {
    {I_CMPNLEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5495, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNLEPS[] = {
    {I_CMPNLEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5319, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNLESD[] = {
    {I_CMPNLESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5503, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNLESS[] = {
    {I_CMPNLESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5327, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNLTPD[] = {
    {I_CMPNLTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5511, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNLTPS[] = {
    {I_CMPNLTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5335, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNLTSD[] = {
    {I_CMPNLTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5519, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPNLTSS[] = {
    {I_CMPNLTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5343, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPORDPD[] = {
    {I_CMPORDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5527, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPORDPS[] = {
    {I_CMPORDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5351, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPORDSD[] = {
    {I_CMPORDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5535, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPORDSS[] = {
    {I_CMPORDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5359, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPPD[] = {
    {I_CMPPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+7624, IF_WILLAMETTE|IF_SSE2|IF_SM2|IF_SB|IF_AR2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPPS[] = {
    {I_CMPPS, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7365, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    {I_CMPPS, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7365, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPSB[] = {
    {I_CMPSB, 0, {0,0,0,0,0}, nasm_bytecodes+19441, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPSD[] = {
    {I_CMPSD, 0, {0,0,0,0,0}, nasm_bytecodes+17765, IF_386},
    {I_CMPSD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+7631, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPSQ[] = {
    {I_CMPSQ, 0, {0,0,0,0,0}, nasm_bytecodes+17770, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPSS[] = {
    {I_CMPSS, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7372, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    {I_CMPSS, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7372, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPSW[] = {
    {I_CMPSW, 0, {0,0,0,0,0}, nasm_bytecodes+17775, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPUNORDPD[] = {
    {I_CMPUNORDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5543, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPUNORDPS[] = {
    {I_CMPUNORDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5367, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPUNORDSD[] = {
    {I_CMPUNORDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5551, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPUNORDSS[] = {
    {I_CMPUNORDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5375, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPXCHG[] = {
    {I_CMPXCHG, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+17780, IF_PENT|IF_SM},
    {I_CMPXCHG, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+17780, IF_PENT},
    {I_CMPXCHG, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14280, IF_PENT|IF_SM},
    {I_CMPXCHG, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14280, IF_PENT},
    {I_CMPXCHG, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14286, IF_PENT|IF_SM},
    {I_CMPXCHG, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14286, IF_PENT},
    {I_CMPXCHG, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+14292, IF_X64|IF_SM},
    {I_CMPXCHG, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14292, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPXCHG16B[] = {
    {I_CMPXCHG16B, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+14310, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPXCHG486[] = {
    {I_CMPXCHG486, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+17785, IF_486|IF_SM|IF_UNDOC},
    {I_CMPXCHG486, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+17785, IF_486|IF_UNDOC},
    {I_CMPXCHG486, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14298, IF_486|IF_SM|IF_UNDOC},
    {I_CMPXCHG486, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14298, IF_486|IF_UNDOC},
    {I_CMPXCHG486, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14304, IF_486|IF_SM|IF_UNDOC},
    {I_CMPXCHG486, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14304, IF_486|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMPXCHG8B[] = {
    {I_CMPXCHG8B, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+14311, IF_PENT},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMEQPD[] = {
    {I_COMEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+189, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMEQPS[] = {
    {I_COMEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+45, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMEQSD[] = {
    {I_COMEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+477, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMEQSS[] = {
    {I_COMEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+333, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMFALSEPD[] = {
    {I_COMFALSEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+288, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMFALSEPS[] = {
    {I_COMFALSEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+144, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMFALSESD[] = {
    {I_COMFALSESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+576, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMFALSESS[] = {
    {I_COMFALSESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+432, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMISD[] = {
    {I_COMISD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15948, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMISS[] = {
    {I_COMISS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15234, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMLEPD[] = {
    {I_COMLEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+207, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMLEPS[] = {
    {I_COMLEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+63, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMLESD[] = {
    {I_COMLESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+495, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMLESS[] = {
    {I_COMLESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+351, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMLTPD[] = {
    {I_COMLTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+198, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMLTPS[] = {
    {I_COMLTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+54, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMLTSD[] = {
    {I_COMLTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+486, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMLTSS[] = {
    {I_COMLTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+342, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNEQPD[] = {
    {I_COMNEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+297, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNEQPS[] = {
    {I_COMNEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+153, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNEQSD[] = {
    {I_COMNEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+585, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNEQSS[] = {
    {I_COMNEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+441, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNLEPD[] = {
    {I_COMNLEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+315, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNLEPS[] = {
    {I_COMNLEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+171, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNLESD[] = {
    {I_COMNLESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+603, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNLESS[] = {
    {I_COMNLESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+459, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNLTPD[] = {
    {I_COMNLTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+306, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNLTPS[] = {
    {I_COMNLTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+162, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNLTSD[] = {
    {I_COMNLTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+594, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMNLTSS[] = {
    {I_COMNLTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+450, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMORDPD[] = {
    {I_COMORDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+252, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMORDPS[] = {
    {I_COMORDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+108, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMORDSD[] = {
    {I_COMORDSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+540, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMORDSS[] = {
    {I_COMORDSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+396, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMPD[] = {
    {I_COMPD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5775, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMPS[] = {
    {I_COMPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5767, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMSD[] = {
    {I_COMSD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5791, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMSS[] = {
    {I_COMSS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5783, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMTRUEPD[] = {
    {I_COMTRUEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+324, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMTRUEPS[] = {
    {I_COMTRUEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+180, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMTRUESD[] = {
    {I_COMTRUESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+612, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMTRUESS[] = {
    {I_COMTRUESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+468, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUEQPD[] = {
    {I_COMUEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+261, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUEQPS[] = {
    {I_COMUEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+117, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUEQSD[] = {
    {I_COMUEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+549, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUEQSS[] = {
    {I_COMUEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+405, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMULEPD[] = {
    {I_COMULEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+279, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMULEPS[] = {
    {I_COMULEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+135, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMULESD[] = {
    {I_COMULESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+567, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMULESS[] = {
    {I_COMULESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+423, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMULTPD[] = {
    {I_COMULTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+270, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMULTPS[] = {
    {I_COMULTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+126, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMULTSD[] = {
    {I_COMULTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+558, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMULTSS[] = {
    {I_COMULTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+414, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNEQPD[] = {
    {I_COMUNEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+225, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNEQPS[] = {
    {I_COMUNEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+81, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNEQSD[] = {
    {I_COMUNEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+513, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNEQSS[] = {
    {I_COMUNEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+369, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNLEPD[] = {
    {I_COMUNLEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+243, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNLEPS[] = {
    {I_COMUNLEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+99, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNLESD[] = {
    {I_COMUNLESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+531, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNLESS[] = {
    {I_COMUNLESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+387, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNLTPD[] = {
    {I_COMUNLTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+234, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNLTPS[] = {
    {I_COMUNLTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+90, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNLTSD[] = {
    {I_COMUNLTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+522, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNLTSS[] = {
    {I_COMUNLTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+378, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNORDPD[] = {
    {I_COMUNORDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+216, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNORDPS[] = {
    {I_COMUNORDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+72, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNORDSD[] = {
    {I_COMUNORDSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+504, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_COMUNORDSS[] = {
    {I_COMUNORDSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+360, IF_SSE5|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_CPUID[] = {
    {I_CPUID, 0, {0,0,0,0,0}, nasm_bytecodes+19445, IF_PENT},
    ITEMPLATE_END
};

static const struct itemplate instrux_CPU_READ[] = {
    {I_CPU_READ, 0, {0,0,0,0,0}, nasm_bytecodes+19449, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_CPU_WRITE[] = {
    {I_CPU_WRITE, 0, {0,0,0,0,0}, nasm_bytecodes+19453, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_CQO[] = {
    {I_CQO, 0, {0,0,0,0,0}, nasm_bytecodes+19457, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_CRC32[] = {
    {I_CRC32, 2, {REG32,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+5720, IF_SSE42},
    {I_CRC32, 2, {REG32,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+5703, IF_SSE42},
    {I_CRC32, 2, {REG32,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+5711, IF_SSE42},
    {I_CRC32, 2, {REG64,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+5719, IF_SSE42|IF_X64},
    {I_CRC32, 2, {REG64,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+5727, IF_SSE42|IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTDQ2PD[] = {
    {I_CVTDQ2PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15954, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTDQ2PS[] = {
    {I_CVTDQ2PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15960, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTPD2DQ[] = {
    {I_CVTPD2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15966, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTPD2PI[] = {
    {I_CVTPD2PI, 2, {MMXREG,RM_XMM,0,0,0}, nasm_bytecodes+15972, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTPD2PS[] = {
    {I_CVTPD2PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15978, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTPH2PS[] = {
    {I_CVTPH2PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8996, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTPI2PD[] = {
    {I_CVTPI2PD, 2, {XMMREG,RM_MMX,0,0,0}, nasm_bytecodes+15984, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTPI2PS[] = {
    {I_CVTPI2PS, 2, {XMMREG,RM_MMX,0,0,0}, nasm_bytecodes+15240, IF_KATMAI|IF_SSE|IF_MMX|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTPS2DQ[] = {
    {I_CVTPS2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15990, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTPS2PD[] = {
    {I_CVTPS2PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15996, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTPS2PH[] = {
    {I_CVTPS2PH, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+9003, IF_SSE5|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTPS2PI[] = {
    {I_CVTPS2PI, 2, {MMXREG,RM_XMM,0,0,0}, nasm_bytecodes+15246, IF_KATMAI|IF_SSE|IF_MMX|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTSD2SI[] = {
    {I_CVTSD2SI, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+7639, IF_WILLAMETTE|IF_SSE2|IF_SQ|IF_AR1},
    {I_CVTSD2SI, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+7639, IF_WILLAMETTE|IF_SSE2|IF_SQ|IF_AR1},
    {I_CVTSD2SI, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+7638, IF_X64|IF_SSE2|IF_SQ|IF_AR1},
    {I_CVTSD2SI, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+7638, IF_X64|IF_SSE2|IF_SQ|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTSD2SS[] = {
    {I_CVTSD2SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16002, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTSI2SD[] = {
    {I_CVTSI2SD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+7646, IF_WILLAMETTE|IF_SSE2|IF_SD|IF_AR1},
    {I_CVTSI2SD, 2, {XMMREG,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+7646, IF_WILLAMETTE|IF_SSE2|IF_SD|IF_AR1},
    {I_CVTSI2SD, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+7645, IF_X64|IF_SSE2|IF_SQ|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTSI2SS[] = {
    {I_CVTSI2SS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+7380, IF_KATMAI|IF_SSE|IF_SD|IF_AR1},
    {I_CVTSI2SS, 2, {XMMREG,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+7380, IF_KATMAI|IF_SSE|IF_SD|IF_AR1},
    {I_CVTSI2SS, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+7379, IF_X64|IF_SSE|IF_SQ|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTSS2SD[] = {
    {I_CVTSS2SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16008, IF_WILLAMETTE|IF_SSE2|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTSS2SI[] = {
    {I_CVTSS2SI, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+7387, IF_KATMAI|IF_SSE|IF_SD|IF_AR1},
    {I_CVTSS2SI, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+7387, IF_KATMAI|IF_SSE|IF_SD|IF_AR1},
    {I_CVTSS2SI, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+7386, IF_X64|IF_SSE|IF_SD|IF_AR1},
    {I_CVTSS2SI, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+7386, IF_X64|IF_SSE|IF_SD|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTTPD2DQ[] = {
    {I_CVTTPD2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16020, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTTPD2PI[] = {
    {I_CVTTPD2PI, 2, {MMXREG,RM_XMM,0,0,0}, nasm_bytecodes+16014, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTTPS2DQ[] = {
    {I_CVTTPS2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16026, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTTPS2PI[] = {
    {I_CVTTPS2PI, 2, {MMXREG,RM_XMM,0,0,0}, nasm_bytecodes+15252, IF_KATMAI|IF_SSE|IF_MMX|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTTSD2SI[] = {
    {I_CVTTSD2SI, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+7653, IF_WILLAMETTE|IF_SSE2|IF_SQ|IF_AR1},
    {I_CVTTSD2SI, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+7653, IF_WILLAMETTE|IF_SSE2|IF_SQ|IF_AR1},
    {I_CVTTSD2SI, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+7652, IF_X64|IF_SSE2|IF_SQ|IF_AR1},
    {I_CVTTSD2SI, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+7652, IF_X64|IF_SSE2|IF_SQ|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_CVTTSS2SI[] = {
    {I_CVTTSS2SI, 2, {REG32,RM_XMM,0,0,0}, nasm_bytecodes+7394, IF_KATMAI|IF_SSE|IF_SD|IF_AR1},
    {I_CVTTSS2SI, 2, {REG64,RM_XMM,0,0,0}, nasm_bytecodes+7393, IF_X64|IF_SSE|IF_SD|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_CWD[] = {
    {I_CWD, 0, {0,0,0,0,0}, nasm_bytecodes+19461, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_CWDE[] = {
    {I_CWDE, 0, {0,0,0,0,0}, nasm_bytecodes+19465, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_DAA[] = {
    {I_DAA, 0, {0,0,0,0,0}, nasm_bytecodes+20428, IF_8086|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_DAS[] = {
    {I_DAS, 0, {0,0,0,0,0}, nasm_bytecodes+20431, IF_8086|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_DB[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_DD[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_DEC[] = {
    {I_DEC, 1, {REG16,0,0,0,0}, nasm_bytecodes+19469, IF_8086|IF_NOLONG},
    {I_DEC, 1, {REG32,0,0,0,0}, nasm_bytecodes+19473, IF_386|IF_NOLONG},
    {I_DEC, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+19477, IF_8086},
    {I_DEC, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17790, IF_8086},
    {I_DEC, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17795, IF_386},
    {I_DEC, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17800, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_DIV[] = {
    {I_DIV, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+19481, IF_8086},
    {I_DIV, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17805, IF_8086},
    {I_DIV, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17810, IF_386},
    {I_DIV, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17815, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_DIVPD[] = {
    {I_DIVPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16032, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_DIVPS[] = {
    {I_DIVPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15258, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_DIVSD[] = {
    {I_DIVSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16038, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_DIVSS[] = {
    {I_DIVSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15264, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_DMINT[] = {
    {I_DMINT, 0, {0,0,0,0,0}, nasm_bytecodes+19485, IF_P6|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_DO[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_DPPD[] = {
    {I_DPPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5623, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_DPPS[] = {
    {I_DPPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5631, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_DQ[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_DT[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_DW[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_DY[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_EMMS[] = {
    {I_EMMS, 0, {0,0,0,0,0}, nasm_bytecodes+19489, IF_PENT|IF_MMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_ENTER[] = {
    {I_ENTER, 2, {IMMEDIATE,IMMEDIATE,0,0,0}, nasm_bytecodes+17820, IF_186},
    ITEMPLATE_END
};

static const struct itemplate instrux_EQU[] = {
    {I_EQU, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+5445, IF_8086},
    {I_EQU, 2, {IMMEDIATE|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+5445, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_EXTRACTPS[] = {
    {I_EXTRACTPS, 3, {RM_GPR|BITS32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+1, IF_SSE41},
    {I_EXTRACTPS, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+0, IF_SSE41|IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_EXTRQ[] = {
    {I_EXTRQ, 3, {XMMREG,IMMEDIATE,IMMEDIATE,0,0}, nasm_bytecodes+5591, IF_SSE4A|IF_AMD},
    {I_EXTRQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16272, IF_SSE4A|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_F2XM1[] = {
    {I_F2XM1, 0, {0,0,0,0,0}, nasm_bytecodes+19493, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FABS[] = {
    {I_FABS, 0, {0,0,0,0,0}, nasm_bytecodes+19497, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FADD[] = {
    {I_FADD, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19501, IF_8086|IF_FPU},
    {I_FADD, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19505, IF_8086|IF_FPU},
    {I_FADD, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+17825, IF_8086|IF_FPU},
    {I_FADD, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17830, IF_8086|IF_FPU},
    {I_FADD, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+17825, IF_8086|IF_FPU},
    {I_FADD, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17835, IF_8086|IF_FPU},
    {I_FADD, 0, {0,0,0,0,0}, nasm_bytecodes+19509, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FADDP[] = {
    {I_FADDP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17840, IF_8086|IF_FPU},
    {I_FADDP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+17840, IF_8086|IF_FPU},
    {I_FADDP, 0, {0,0,0,0,0}, nasm_bytecodes+19509, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FBLD[] = {
    {I_FBLD, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+19513, IF_8086|IF_FPU},
    {I_FBLD, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19513, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FBSTP[] = {
    {I_FBSTP, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+19517, IF_8086|IF_FPU},
    {I_FBSTP, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19517, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCHS[] = {
    {I_FCHS, 0, {0,0,0,0,0}, nasm_bytecodes+19521, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCLEX[] = {
    {I_FCLEX, 0, {0,0,0,0,0}, nasm_bytecodes+17845, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCMOVB[] = {
    {I_FCMOVB, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17850, IF_P6|IF_FPU},
    {I_FCMOVB, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17855, IF_P6|IF_FPU},
    {I_FCMOVB, 0, {0,0,0,0,0}, nasm_bytecodes+19525, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCMOVBE[] = {
    {I_FCMOVBE, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17860, IF_P6|IF_FPU},
    {I_FCMOVBE, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17865, IF_P6|IF_FPU},
    {I_FCMOVBE, 0, {0,0,0,0,0}, nasm_bytecodes+19529, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCMOVE[] = {
    {I_FCMOVE, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17870, IF_P6|IF_FPU},
    {I_FCMOVE, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17875, IF_P6|IF_FPU},
    {I_FCMOVE, 0, {0,0,0,0,0}, nasm_bytecodes+19533, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCMOVNB[] = {
    {I_FCMOVNB, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17880, IF_P6|IF_FPU},
    {I_FCMOVNB, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17885, IF_P6|IF_FPU},
    {I_FCMOVNB, 0, {0,0,0,0,0}, nasm_bytecodes+19537, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCMOVNBE[] = {
    {I_FCMOVNBE, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17890, IF_P6|IF_FPU},
    {I_FCMOVNBE, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17895, IF_P6|IF_FPU},
    {I_FCMOVNBE, 0, {0,0,0,0,0}, nasm_bytecodes+19541, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCMOVNE[] = {
    {I_FCMOVNE, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17900, IF_P6|IF_FPU},
    {I_FCMOVNE, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17905, IF_P6|IF_FPU},
    {I_FCMOVNE, 0, {0,0,0,0,0}, nasm_bytecodes+19545, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCMOVNU[] = {
    {I_FCMOVNU, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17910, IF_P6|IF_FPU},
    {I_FCMOVNU, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17915, IF_P6|IF_FPU},
    {I_FCMOVNU, 0, {0,0,0,0,0}, nasm_bytecodes+19549, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCMOVU[] = {
    {I_FCMOVU, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17920, IF_P6|IF_FPU},
    {I_FCMOVU, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17925, IF_P6|IF_FPU},
    {I_FCMOVU, 0, {0,0,0,0,0}, nasm_bytecodes+19553, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCOM[] = {
    {I_FCOM, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19557, IF_8086|IF_FPU},
    {I_FCOM, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19561, IF_8086|IF_FPU},
    {I_FCOM, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17930, IF_8086|IF_FPU},
    {I_FCOM, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17935, IF_8086|IF_FPU},
    {I_FCOM, 0, {0,0,0,0,0}, nasm_bytecodes+19565, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCOMI[] = {
    {I_FCOMI, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17940, IF_P6|IF_FPU},
    {I_FCOMI, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17945, IF_P6|IF_FPU},
    {I_FCOMI, 0, {0,0,0,0,0}, nasm_bytecodes+19569, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCOMIP[] = {
    {I_FCOMIP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17950, IF_P6|IF_FPU},
    {I_FCOMIP, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17955, IF_P6|IF_FPU},
    {I_FCOMIP, 0, {0,0,0,0,0}, nasm_bytecodes+19573, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCOMP[] = {
    {I_FCOMP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19577, IF_8086|IF_FPU},
    {I_FCOMP, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19581, IF_8086|IF_FPU},
    {I_FCOMP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17960, IF_8086|IF_FPU},
    {I_FCOMP, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17965, IF_8086|IF_FPU},
    {I_FCOMP, 0, {0,0,0,0,0}, nasm_bytecodes+19585, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCOMPP[] = {
    {I_FCOMPP, 0, {0,0,0,0,0}, nasm_bytecodes+19589, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FCOS[] = {
    {I_FCOS, 0, {0,0,0,0,0}, nasm_bytecodes+19593, IF_386|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FDECSTP[] = {
    {I_FDECSTP, 0, {0,0,0,0,0}, nasm_bytecodes+19597, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FDISI[] = {
    {I_FDISI, 0, {0,0,0,0,0}, nasm_bytecodes+17970, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FDIV[] = {
    {I_FDIV, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19601, IF_8086|IF_FPU},
    {I_FDIV, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19605, IF_8086|IF_FPU},
    {I_FDIV, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+17975, IF_8086|IF_FPU},
    {I_FDIV, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17980, IF_8086|IF_FPU},
    {I_FDIV, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+17975, IF_8086|IF_FPU},
    {I_FDIV, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17985, IF_8086|IF_FPU},
    {I_FDIV, 0, {0,0,0,0,0}, nasm_bytecodes+19609, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FDIVP[] = {
    {I_FDIVP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17990, IF_8086|IF_FPU},
    {I_FDIVP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+17990, IF_8086|IF_FPU},
    {I_FDIVP, 0, {0,0,0,0,0}, nasm_bytecodes+19609, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FDIVR[] = {
    {I_FDIVR, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19613, IF_8086|IF_FPU},
    {I_FDIVR, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19617, IF_8086|IF_FPU},
    {I_FDIVR, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+17995, IF_8086|IF_FPU},
    {I_FDIVR, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+17995, IF_8086|IF_FPU},
    {I_FDIVR, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18000, IF_8086|IF_FPU},
    {I_FDIVR, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18005, IF_8086|IF_FPU},
    {I_FDIVR, 0, {0,0,0,0,0}, nasm_bytecodes+19621, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FDIVRP[] = {
    {I_FDIVRP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18010, IF_8086|IF_FPU},
    {I_FDIVRP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18010, IF_8086|IF_FPU},
    {I_FDIVRP, 0, {0,0,0,0,0}, nasm_bytecodes+19621, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FEMMS[] = {
    {I_FEMMS, 0, {0,0,0,0,0}, nasm_bytecodes+19625, IF_PENT|IF_3DNOW},
    ITEMPLATE_END
};

static const struct itemplate instrux_FENI[] = {
    {I_FENI, 0, {0,0,0,0,0}, nasm_bytecodes+18015, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FFREE[] = {
    {I_FFREE, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18020, IF_8086|IF_FPU},
    {I_FFREE, 0, {0,0,0,0,0}, nasm_bytecodes+19629, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FFREEP[] = {
    {I_FFREEP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18025, IF_286|IF_FPU|IF_UNDOC},
    {I_FFREEP, 0, {0,0,0,0,0}, nasm_bytecodes+19633, IF_286|IF_FPU|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_FIADD[] = {
    {I_FIADD, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19637, IF_8086|IF_FPU},
    {I_FIADD, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19641, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FICOM[] = {
    {I_FICOM, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19645, IF_8086|IF_FPU},
    {I_FICOM, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19649, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FICOMP[] = {
    {I_FICOMP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19653, IF_8086|IF_FPU},
    {I_FICOMP, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19657, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FIDIV[] = {
    {I_FIDIV, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19661, IF_8086|IF_FPU},
    {I_FIDIV, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19665, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FIDIVR[] = {
    {I_FIDIVR, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19669, IF_8086|IF_FPU},
    {I_FIDIVR, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19673, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FILD[] = {
    {I_FILD, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19677, IF_8086|IF_FPU},
    {I_FILD, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19681, IF_8086|IF_FPU},
    {I_FILD, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19685, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FIMUL[] = {
    {I_FIMUL, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19689, IF_8086|IF_FPU},
    {I_FIMUL, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19693, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FINCSTP[] = {
    {I_FINCSTP, 0, {0,0,0,0,0}, nasm_bytecodes+19697, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FINIT[] = {
    {I_FINIT, 0, {0,0,0,0,0}, nasm_bytecodes+18030, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FIST[] = {
    {I_FIST, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19701, IF_8086|IF_FPU},
    {I_FIST, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19705, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FISTP[] = {
    {I_FISTP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19709, IF_8086|IF_FPU},
    {I_FISTP, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19713, IF_8086|IF_FPU},
    {I_FISTP, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19717, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FISTTP[] = {
    {I_FISTTP, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19721, IF_PRESCOTT|IF_FPU},
    {I_FISTTP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19725, IF_PRESCOTT|IF_FPU},
    {I_FISTTP, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19729, IF_PRESCOTT|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FISUB[] = {
    {I_FISUB, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19733, IF_8086|IF_FPU},
    {I_FISUB, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19737, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FISUBR[] = {
    {I_FISUBR, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19741, IF_8086|IF_FPU},
    {I_FISUBR, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19745, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FLD[] = {
    {I_FLD, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19749, IF_8086|IF_FPU},
    {I_FLD, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19753, IF_8086|IF_FPU},
    {I_FLD, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+19757, IF_8086|IF_FPU},
    {I_FLD, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18035, IF_8086|IF_FPU},
    {I_FLD, 0, {0,0,0,0,0}, nasm_bytecodes+19761, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FLD1[] = {
    {I_FLD1, 0, {0,0,0,0,0}, nasm_bytecodes+19765, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FLDCW[] = {
    {I_FLDCW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19769, IF_8086|IF_FPU|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_FLDENV[] = {
    {I_FLDENV, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19773, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FLDL2E[] = {
    {I_FLDL2E, 0, {0,0,0,0,0}, nasm_bytecodes+19777, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FLDL2T[] = {
    {I_FLDL2T, 0, {0,0,0,0,0}, nasm_bytecodes+19781, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FLDLG2[] = {
    {I_FLDLG2, 0, {0,0,0,0,0}, nasm_bytecodes+19785, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FLDLN2[] = {
    {I_FLDLN2, 0, {0,0,0,0,0}, nasm_bytecodes+19789, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FLDPI[] = {
    {I_FLDPI, 0, {0,0,0,0,0}, nasm_bytecodes+19793, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FLDZ[] = {
    {I_FLDZ, 0, {0,0,0,0,0}, nasm_bytecodes+19797, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FMADDPD[] = {
    {I_FMADDPD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8184, IF_SSE5|IF_AMD},
    {I_FMADDPD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8191, IF_SSE5|IF_AMD},
    {I_FMADDPD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8198, IF_SSE5|IF_AMD},
    {I_FMADDPD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8205, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FMADDPS[] = {
    {I_FMADDPS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8156, IF_SSE5|IF_AMD},
    {I_FMADDPS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8163, IF_SSE5|IF_AMD},
    {I_FMADDPS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8170, IF_SSE5|IF_AMD},
    {I_FMADDPS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8177, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FMADDSD[] = {
    {I_FMADDSD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8240, IF_SSE5|IF_AMD},
    {I_FMADDSD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8247, IF_SSE5|IF_AMD},
    {I_FMADDSD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8254, IF_SSE5|IF_AMD},
    {I_FMADDSD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8261, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FMADDSS[] = {
    {I_FMADDSS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8212, IF_SSE5|IF_AMD},
    {I_FMADDSS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8219, IF_SSE5|IF_AMD},
    {I_FMADDSS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8226, IF_SSE5|IF_AMD},
    {I_FMADDSS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8233, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FMSUBPD[] = {
    {I_FMSUBPD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8296, IF_SSE5|IF_AMD},
    {I_FMSUBPD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8303, IF_SSE5|IF_AMD},
    {I_FMSUBPD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8310, IF_SSE5|IF_AMD},
    {I_FMSUBPD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8317, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FMSUBPS[] = {
    {I_FMSUBPS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8268, IF_SSE5|IF_AMD},
    {I_FMSUBPS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8275, IF_SSE5|IF_AMD},
    {I_FMSUBPS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8282, IF_SSE5|IF_AMD},
    {I_FMSUBPS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8289, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FMSUBSD[] = {
    {I_FMSUBSD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8352, IF_SSE5|IF_AMD},
    {I_FMSUBSD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8359, IF_SSE5|IF_AMD},
    {I_FMSUBSD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8366, IF_SSE5|IF_AMD},
    {I_FMSUBSD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8373, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FMSUBSS[] = {
    {I_FMSUBSS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8324, IF_SSE5|IF_AMD},
    {I_FMSUBSS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8331, IF_SSE5|IF_AMD},
    {I_FMSUBSS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8338, IF_SSE5|IF_AMD},
    {I_FMSUBSS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8345, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FMUL[] = {
    {I_FMUL, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19801, IF_8086|IF_FPU},
    {I_FMUL, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19805, IF_8086|IF_FPU},
    {I_FMUL, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+18040, IF_8086|IF_FPU},
    {I_FMUL, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18040, IF_8086|IF_FPU},
    {I_FMUL, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18045, IF_8086|IF_FPU},
    {I_FMUL, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18050, IF_8086|IF_FPU},
    {I_FMUL, 0, {0,0,0,0,0}, nasm_bytecodes+19809, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FMULP[] = {
    {I_FMULP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18055, IF_8086|IF_FPU},
    {I_FMULP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18055, IF_8086|IF_FPU},
    {I_FMULP, 0, {0,0,0,0,0}, nasm_bytecodes+19809, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNCLEX[] = {
    {I_FNCLEX, 0, {0,0,0,0,0}, nasm_bytecodes+17846, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNDISI[] = {
    {I_FNDISI, 0, {0,0,0,0,0}, nasm_bytecodes+17971, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNENI[] = {
    {I_FNENI, 0, {0,0,0,0,0}, nasm_bytecodes+18016, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNINIT[] = {
    {I_FNINIT, 0, {0,0,0,0,0}, nasm_bytecodes+18031, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNMADDPD[] = {
    {I_FNMADDPD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8408, IF_SSE5|IF_AMD},
    {I_FNMADDPD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8415, IF_SSE5|IF_AMD},
    {I_FNMADDPD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8422, IF_SSE5|IF_AMD},
    {I_FNMADDPD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8429, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNMADDPS[] = {
    {I_FNMADDPS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8380, IF_SSE5|IF_AMD},
    {I_FNMADDPS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8387, IF_SSE5|IF_AMD},
    {I_FNMADDPS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8394, IF_SSE5|IF_AMD},
    {I_FNMADDPS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8401, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNMADDSD[] = {
    {I_FNMADDSD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8464, IF_SSE5|IF_AMD},
    {I_FNMADDSD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8471, IF_SSE5|IF_AMD},
    {I_FNMADDSD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8478, IF_SSE5|IF_AMD},
    {I_FNMADDSD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8485, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNMADDSS[] = {
    {I_FNMADDSS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8436, IF_SSE5|IF_AMD},
    {I_FNMADDSS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8443, IF_SSE5|IF_AMD},
    {I_FNMADDSS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8450, IF_SSE5|IF_AMD},
    {I_FNMADDSS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8457, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNMSUBPD[] = {
    {I_FNMSUBPD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8520, IF_SSE5|IF_AMD},
    {I_FNMSUBPD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8527, IF_SSE5|IF_AMD},
    {I_FNMSUBPD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8534, IF_SSE5|IF_AMD},
    {I_FNMSUBPD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8541, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNMSUBPS[] = {
    {I_FNMSUBPS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8492, IF_SSE5|IF_AMD},
    {I_FNMSUBPS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8499, IF_SSE5|IF_AMD},
    {I_FNMSUBPS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8506, IF_SSE5|IF_AMD},
    {I_FNMSUBPS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8513, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNMSUBSD[] = {
    {I_FNMSUBSD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8576, IF_SSE5|IF_AMD},
    {I_FNMSUBSD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8583, IF_SSE5|IF_AMD},
    {I_FNMSUBSD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8590, IF_SSE5|IF_AMD},
    {I_FNMSUBSD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8597, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNMSUBSS[] = {
    {I_FNMSUBSS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8548, IF_SSE5|IF_AMD},
    {I_FNMSUBSS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8555, IF_SSE5|IF_AMD},
    {I_FNMSUBSS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8562, IF_SSE5|IF_AMD},
    {I_FNMSUBSS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8569, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNOP[] = {
    {I_FNOP, 0, {0,0,0,0,0}, nasm_bytecodes+19813, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNSAVE[] = {
    {I_FNSAVE, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18061, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNSTCW[] = {
    {I_FNSTCW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18071, IF_8086|IF_FPU|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNSTENV[] = {
    {I_FNSTENV, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18076, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FNSTSW[] = {
    {I_FNSTSW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18086, IF_8086|IF_FPU|IF_SW},
    {I_FNSTSW, 1, {REG_AX,0,0,0,0}, nasm_bytecodes+18091, IF_286|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FPATAN[] = {
    {I_FPATAN, 0, {0,0,0,0,0}, nasm_bytecodes+19817, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FPREM[] = {
    {I_FPREM, 0, {0,0,0,0,0}, nasm_bytecodes+19821, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FPREM1[] = {
    {I_FPREM1, 0, {0,0,0,0,0}, nasm_bytecodes+19825, IF_386|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FPTAN[] = {
    {I_FPTAN, 0, {0,0,0,0,0}, nasm_bytecodes+19829, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FRCZPD[] = {
    {I_FRCZPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8975, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FRCZPS[] = {
    {I_FRCZPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8968, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FRCZSD[] = {
    {I_FRCZSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8989, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FRCZSS[] = {
    {I_FRCZSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8982, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_FRNDINT[] = {
    {I_FRNDINT, 0, {0,0,0,0,0}, nasm_bytecodes+19833, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FRSTOR[] = {
    {I_FRSTOR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19837, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSAVE[] = {
    {I_FSAVE, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18060, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSCALE[] = {
    {I_FSCALE, 0, {0,0,0,0,0}, nasm_bytecodes+19841, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSETPM[] = {
    {I_FSETPM, 0, {0,0,0,0,0}, nasm_bytecodes+19845, IF_286|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSIN[] = {
    {I_FSIN, 0, {0,0,0,0,0}, nasm_bytecodes+19849, IF_386|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSINCOS[] = {
    {I_FSINCOS, 0, {0,0,0,0,0}, nasm_bytecodes+19853, IF_386|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSQRT[] = {
    {I_FSQRT, 0, {0,0,0,0,0}, nasm_bytecodes+19857, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FST[] = {
    {I_FST, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19861, IF_8086|IF_FPU},
    {I_FST, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19865, IF_8086|IF_FPU},
    {I_FST, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18065, IF_8086|IF_FPU},
    {I_FST, 0, {0,0,0,0,0}, nasm_bytecodes+19869, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSTCW[] = {
    {I_FSTCW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18070, IF_8086|IF_FPU|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSTENV[] = {
    {I_FSTENV, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18075, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSTP[] = {
    {I_FSTP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19873, IF_8086|IF_FPU},
    {I_FSTP, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19877, IF_8086|IF_FPU},
    {I_FSTP, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+19881, IF_8086|IF_FPU},
    {I_FSTP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18080, IF_8086|IF_FPU},
    {I_FSTP, 0, {0,0,0,0,0}, nasm_bytecodes+19885, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSTSW[] = {
    {I_FSTSW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18085, IF_8086|IF_FPU|IF_SW},
    {I_FSTSW, 1, {REG_AX,0,0,0,0}, nasm_bytecodes+18090, IF_286|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSUB[] = {
    {I_FSUB, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19889, IF_8086|IF_FPU},
    {I_FSUB, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19893, IF_8086|IF_FPU},
    {I_FSUB, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+18095, IF_8086|IF_FPU},
    {I_FSUB, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18095, IF_8086|IF_FPU},
    {I_FSUB, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18100, IF_8086|IF_FPU},
    {I_FSUB, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18105, IF_8086|IF_FPU},
    {I_FSUB, 0, {0,0,0,0,0}, nasm_bytecodes+19897, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSUBP[] = {
    {I_FSUBP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18110, IF_8086|IF_FPU},
    {I_FSUBP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18110, IF_8086|IF_FPU},
    {I_FSUBP, 0, {0,0,0,0,0}, nasm_bytecodes+19897, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSUBR[] = {
    {I_FSUBR, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19901, IF_8086|IF_FPU},
    {I_FSUBR, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19905, IF_8086|IF_FPU},
    {I_FSUBR, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+18115, IF_8086|IF_FPU},
    {I_FSUBR, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18115, IF_8086|IF_FPU},
    {I_FSUBR, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18120, IF_8086|IF_FPU},
    {I_FSUBR, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18125, IF_8086|IF_FPU},
    {I_FSUBR, 0, {0,0,0,0,0}, nasm_bytecodes+19909, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FSUBRP[] = {
    {I_FSUBRP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18130, IF_8086|IF_FPU},
    {I_FSUBRP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18130, IF_8086|IF_FPU},
    {I_FSUBRP, 0, {0,0,0,0,0}, nasm_bytecodes+19909, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FTST[] = {
    {I_FTST, 0, {0,0,0,0,0}, nasm_bytecodes+19913, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FUCOM[] = {
    {I_FUCOM, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18135, IF_386|IF_FPU},
    {I_FUCOM, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18140, IF_386|IF_FPU},
    {I_FUCOM, 0, {0,0,0,0,0}, nasm_bytecodes+19917, IF_386|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FUCOMI[] = {
    {I_FUCOMI, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18145, IF_P6|IF_FPU},
    {I_FUCOMI, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18150, IF_P6|IF_FPU},
    {I_FUCOMI, 0, {0,0,0,0,0}, nasm_bytecodes+19921, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FUCOMIP[] = {
    {I_FUCOMIP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18155, IF_P6|IF_FPU},
    {I_FUCOMIP, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18160, IF_P6|IF_FPU},
    {I_FUCOMIP, 0, {0,0,0,0,0}, nasm_bytecodes+19925, IF_P6|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FUCOMP[] = {
    {I_FUCOMP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18165, IF_386|IF_FPU},
    {I_FUCOMP, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18170, IF_386|IF_FPU},
    {I_FUCOMP, 0, {0,0,0,0,0}, nasm_bytecodes+19929, IF_386|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FUCOMPP[] = {
    {I_FUCOMPP, 0, {0,0,0,0,0}, nasm_bytecodes+19933, IF_386|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FWAIT[] = {
    {I_FWAIT, 0, {0,0,0,0,0}, nasm_bytecodes+19919, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_FXAM[] = {
    {I_FXAM, 0, {0,0,0,0,0}, nasm_bytecodes+19937, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FXCH[] = {
    {I_FXCH, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18175, IF_8086|IF_FPU},
    {I_FXCH, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18175, IF_8086|IF_FPU},
    {I_FXCH, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18180, IF_8086|IF_FPU},
    {I_FXCH, 0, {0,0,0,0,0}, nasm_bytecodes+19941, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FXRSTOR[] = {
    {I_FXRSTOR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19300, IF_P6|IF_SSE|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FXSAVE[] = {
    {I_FXSAVE, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19305, IF_P6|IF_SSE|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FXTRACT[] = {
    {I_FXTRACT, 0, {0,0,0,0,0}, nasm_bytecodes+19945, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FYL2X[] = {
    {I_FYL2X, 0, {0,0,0,0,0}, nasm_bytecodes+19949, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_FYL2XP1[] = {
    {I_FYL2XP1, 0, {0,0,0,0,0}, nasm_bytecodes+19953, IF_8086|IF_FPU},
    ITEMPLATE_END
};

static const struct itemplate instrux_GETSEC[] = {
    {I_GETSEC, 0, {0,0,0,0,0}, nasm_bytecodes+20409, IF_KATMAI},
    ITEMPLATE_END
};

static const struct itemplate instrux_HADDPD[] = {
    {I_HADDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16212, IF_PRESCOTT|IF_SSE3|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_HADDPS[] = {
    {I_HADDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16218, IF_PRESCOTT|IF_SSE3|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP0[] = {
    {I_HINT_NOP0, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16356, IF_P6|IF_UNDOC},
    {I_HINT_NOP0, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16362, IF_P6|IF_UNDOC},
    {I_HINT_NOP0, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16368, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP1[] = {
    {I_HINT_NOP1, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16374, IF_P6|IF_UNDOC},
    {I_HINT_NOP1, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16380, IF_P6|IF_UNDOC},
    {I_HINT_NOP1, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16386, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP10[] = {
    {I_HINT_NOP10, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16536, IF_P6|IF_UNDOC},
    {I_HINT_NOP10, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16542, IF_P6|IF_UNDOC},
    {I_HINT_NOP10, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16548, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP11[] = {
    {I_HINT_NOP11, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16554, IF_P6|IF_UNDOC},
    {I_HINT_NOP11, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16560, IF_P6|IF_UNDOC},
    {I_HINT_NOP11, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16566, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP12[] = {
    {I_HINT_NOP12, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16572, IF_P6|IF_UNDOC},
    {I_HINT_NOP12, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16578, IF_P6|IF_UNDOC},
    {I_HINT_NOP12, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16584, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP13[] = {
    {I_HINT_NOP13, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16590, IF_P6|IF_UNDOC},
    {I_HINT_NOP13, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16596, IF_P6|IF_UNDOC},
    {I_HINT_NOP13, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16602, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP14[] = {
    {I_HINT_NOP14, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16608, IF_P6|IF_UNDOC},
    {I_HINT_NOP14, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16614, IF_P6|IF_UNDOC},
    {I_HINT_NOP14, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16620, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP15[] = {
    {I_HINT_NOP15, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16626, IF_P6|IF_UNDOC},
    {I_HINT_NOP15, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16632, IF_P6|IF_UNDOC},
    {I_HINT_NOP15, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16638, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP16[] = {
    {I_HINT_NOP16, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16644, IF_P6|IF_UNDOC},
    {I_HINT_NOP16, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16650, IF_P6|IF_UNDOC},
    {I_HINT_NOP16, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16656, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP17[] = {
    {I_HINT_NOP17, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16662, IF_P6|IF_UNDOC},
    {I_HINT_NOP17, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16668, IF_P6|IF_UNDOC},
    {I_HINT_NOP17, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16674, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP18[] = {
    {I_HINT_NOP18, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16680, IF_P6|IF_UNDOC},
    {I_HINT_NOP18, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16686, IF_P6|IF_UNDOC},
    {I_HINT_NOP18, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16692, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP19[] = {
    {I_HINT_NOP19, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16698, IF_P6|IF_UNDOC},
    {I_HINT_NOP19, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16704, IF_P6|IF_UNDOC},
    {I_HINT_NOP19, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16710, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP2[] = {
    {I_HINT_NOP2, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16392, IF_P6|IF_UNDOC},
    {I_HINT_NOP2, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16398, IF_P6|IF_UNDOC},
    {I_HINT_NOP2, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16404, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP20[] = {
    {I_HINT_NOP20, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16716, IF_P6|IF_UNDOC},
    {I_HINT_NOP20, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16722, IF_P6|IF_UNDOC},
    {I_HINT_NOP20, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16728, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP21[] = {
    {I_HINT_NOP21, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16734, IF_P6|IF_UNDOC},
    {I_HINT_NOP21, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16740, IF_P6|IF_UNDOC},
    {I_HINT_NOP21, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16746, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP22[] = {
    {I_HINT_NOP22, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16752, IF_P6|IF_UNDOC},
    {I_HINT_NOP22, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16758, IF_P6|IF_UNDOC},
    {I_HINT_NOP22, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16764, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP23[] = {
    {I_HINT_NOP23, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16770, IF_P6|IF_UNDOC},
    {I_HINT_NOP23, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16776, IF_P6|IF_UNDOC},
    {I_HINT_NOP23, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16782, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP24[] = {
    {I_HINT_NOP24, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16788, IF_P6|IF_UNDOC},
    {I_HINT_NOP24, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16794, IF_P6|IF_UNDOC},
    {I_HINT_NOP24, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16800, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP25[] = {
    {I_HINT_NOP25, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16806, IF_P6|IF_UNDOC},
    {I_HINT_NOP25, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16812, IF_P6|IF_UNDOC},
    {I_HINT_NOP25, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16818, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP26[] = {
    {I_HINT_NOP26, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16824, IF_P6|IF_UNDOC},
    {I_HINT_NOP26, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16830, IF_P6|IF_UNDOC},
    {I_HINT_NOP26, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16836, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP27[] = {
    {I_HINT_NOP27, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16842, IF_P6|IF_UNDOC},
    {I_HINT_NOP27, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16848, IF_P6|IF_UNDOC},
    {I_HINT_NOP27, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16854, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP28[] = {
    {I_HINT_NOP28, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16860, IF_P6|IF_UNDOC},
    {I_HINT_NOP28, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16866, IF_P6|IF_UNDOC},
    {I_HINT_NOP28, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16872, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP29[] = {
    {I_HINT_NOP29, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16878, IF_P6|IF_UNDOC},
    {I_HINT_NOP29, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16884, IF_P6|IF_UNDOC},
    {I_HINT_NOP29, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16890, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP3[] = {
    {I_HINT_NOP3, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16410, IF_P6|IF_UNDOC},
    {I_HINT_NOP3, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16416, IF_P6|IF_UNDOC},
    {I_HINT_NOP3, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16422, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP30[] = {
    {I_HINT_NOP30, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16896, IF_P6|IF_UNDOC},
    {I_HINT_NOP30, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16902, IF_P6|IF_UNDOC},
    {I_HINT_NOP30, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16908, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP31[] = {
    {I_HINT_NOP31, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16914, IF_P6|IF_UNDOC},
    {I_HINT_NOP31, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16920, IF_P6|IF_UNDOC},
    {I_HINT_NOP31, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16926, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP32[] = {
    {I_HINT_NOP32, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16932, IF_P6|IF_UNDOC},
    {I_HINT_NOP32, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16938, IF_P6|IF_UNDOC},
    {I_HINT_NOP32, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16944, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP33[] = {
    {I_HINT_NOP33, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16950, IF_P6|IF_UNDOC},
    {I_HINT_NOP33, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16956, IF_P6|IF_UNDOC},
    {I_HINT_NOP33, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16962, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP34[] = {
    {I_HINT_NOP34, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16968, IF_P6|IF_UNDOC},
    {I_HINT_NOP34, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16974, IF_P6|IF_UNDOC},
    {I_HINT_NOP34, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16980, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP35[] = {
    {I_HINT_NOP35, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16986, IF_P6|IF_UNDOC},
    {I_HINT_NOP35, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16992, IF_P6|IF_UNDOC},
    {I_HINT_NOP35, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16998, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP36[] = {
    {I_HINT_NOP36, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17004, IF_P6|IF_UNDOC},
    {I_HINT_NOP36, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17010, IF_P6|IF_UNDOC},
    {I_HINT_NOP36, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17016, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP37[] = {
    {I_HINT_NOP37, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17022, IF_P6|IF_UNDOC},
    {I_HINT_NOP37, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17028, IF_P6|IF_UNDOC},
    {I_HINT_NOP37, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17034, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP38[] = {
    {I_HINT_NOP38, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17040, IF_P6|IF_UNDOC},
    {I_HINT_NOP38, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17046, IF_P6|IF_UNDOC},
    {I_HINT_NOP38, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17052, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP39[] = {
    {I_HINT_NOP39, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17058, IF_P6|IF_UNDOC},
    {I_HINT_NOP39, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17064, IF_P6|IF_UNDOC},
    {I_HINT_NOP39, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17070, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP4[] = {
    {I_HINT_NOP4, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16428, IF_P6|IF_UNDOC},
    {I_HINT_NOP4, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16434, IF_P6|IF_UNDOC},
    {I_HINT_NOP4, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16440, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP40[] = {
    {I_HINT_NOP40, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17076, IF_P6|IF_UNDOC},
    {I_HINT_NOP40, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17082, IF_P6|IF_UNDOC},
    {I_HINT_NOP40, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17088, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP41[] = {
    {I_HINT_NOP41, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17094, IF_P6|IF_UNDOC},
    {I_HINT_NOP41, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17100, IF_P6|IF_UNDOC},
    {I_HINT_NOP41, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17106, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP42[] = {
    {I_HINT_NOP42, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17112, IF_P6|IF_UNDOC},
    {I_HINT_NOP42, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17118, IF_P6|IF_UNDOC},
    {I_HINT_NOP42, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17124, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP43[] = {
    {I_HINT_NOP43, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17130, IF_P6|IF_UNDOC},
    {I_HINT_NOP43, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17136, IF_P6|IF_UNDOC},
    {I_HINT_NOP43, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17142, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP44[] = {
    {I_HINT_NOP44, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17148, IF_P6|IF_UNDOC},
    {I_HINT_NOP44, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17154, IF_P6|IF_UNDOC},
    {I_HINT_NOP44, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17160, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP45[] = {
    {I_HINT_NOP45, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17166, IF_P6|IF_UNDOC},
    {I_HINT_NOP45, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17172, IF_P6|IF_UNDOC},
    {I_HINT_NOP45, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17178, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP46[] = {
    {I_HINT_NOP46, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17184, IF_P6|IF_UNDOC},
    {I_HINT_NOP46, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17190, IF_P6|IF_UNDOC},
    {I_HINT_NOP46, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17196, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP47[] = {
    {I_HINT_NOP47, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17202, IF_P6|IF_UNDOC},
    {I_HINT_NOP47, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17208, IF_P6|IF_UNDOC},
    {I_HINT_NOP47, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17214, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP48[] = {
    {I_HINT_NOP48, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17220, IF_P6|IF_UNDOC},
    {I_HINT_NOP48, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17226, IF_P6|IF_UNDOC},
    {I_HINT_NOP48, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17232, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP49[] = {
    {I_HINT_NOP49, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17238, IF_P6|IF_UNDOC},
    {I_HINT_NOP49, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17244, IF_P6|IF_UNDOC},
    {I_HINT_NOP49, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17250, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP5[] = {
    {I_HINT_NOP5, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16446, IF_P6|IF_UNDOC},
    {I_HINT_NOP5, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16452, IF_P6|IF_UNDOC},
    {I_HINT_NOP5, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16458, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP50[] = {
    {I_HINT_NOP50, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17256, IF_P6|IF_UNDOC},
    {I_HINT_NOP50, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17262, IF_P6|IF_UNDOC},
    {I_HINT_NOP50, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17268, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP51[] = {
    {I_HINT_NOP51, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17274, IF_P6|IF_UNDOC},
    {I_HINT_NOP51, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17280, IF_P6|IF_UNDOC},
    {I_HINT_NOP51, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17286, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP52[] = {
    {I_HINT_NOP52, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17292, IF_P6|IF_UNDOC},
    {I_HINT_NOP52, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17298, IF_P6|IF_UNDOC},
    {I_HINT_NOP52, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17304, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP53[] = {
    {I_HINT_NOP53, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17310, IF_P6|IF_UNDOC},
    {I_HINT_NOP53, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17316, IF_P6|IF_UNDOC},
    {I_HINT_NOP53, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17322, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP54[] = {
    {I_HINT_NOP54, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17328, IF_P6|IF_UNDOC},
    {I_HINT_NOP54, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17334, IF_P6|IF_UNDOC},
    {I_HINT_NOP54, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17340, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP55[] = {
    {I_HINT_NOP55, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17346, IF_P6|IF_UNDOC},
    {I_HINT_NOP55, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17352, IF_P6|IF_UNDOC},
    {I_HINT_NOP55, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17358, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP56[] = {
    {I_HINT_NOP56, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+14718, IF_P6|IF_UNDOC},
    {I_HINT_NOP56, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+14724, IF_P6|IF_UNDOC},
    {I_HINT_NOP56, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+14730, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP57[] = {
    {I_HINT_NOP57, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17364, IF_P6|IF_UNDOC},
    {I_HINT_NOP57, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17370, IF_P6|IF_UNDOC},
    {I_HINT_NOP57, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17376, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP58[] = {
    {I_HINT_NOP58, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17382, IF_P6|IF_UNDOC},
    {I_HINT_NOP58, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17388, IF_P6|IF_UNDOC},
    {I_HINT_NOP58, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17394, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP59[] = {
    {I_HINT_NOP59, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17400, IF_P6|IF_UNDOC},
    {I_HINT_NOP59, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17406, IF_P6|IF_UNDOC},
    {I_HINT_NOP59, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17412, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP6[] = {
    {I_HINT_NOP6, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16464, IF_P6|IF_UNDOC},
    {I_HINT_NOP6, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16470, IF_P6|IF_UNDOC},
    {I_HINT_NOP6, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16476, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP60[] = {
    {I_HINT_NOP60, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17418, IF_P6|IF_UNDOC},
    {I_HINT_NOP60, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17424, IF_P6|IF_UNDOC},
    {I_HINT_NOP60, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17430, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP61[] = {
    {I_HINT_NOP61, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17436, IF_P6|IF_UNDOC},
    {I_HINT_NOP61, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17442, IF_P6|IF_UNDOC},
    {I_HINT_NOP61, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17448, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP62[] = {
    {I_HINT_NOP62, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17454, IF_P6|IF_UNDOC},
    {I_HINT_NOP62, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17460, IF_P6|IF_UNDOC},
    {I_HINT_NOP62, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17466, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP63[] = {
    {I_HINT_NOP63, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17472, IF_P6|IF_UNDOC},
    {I_HINT_NOP63, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17478, IF_P6|IF_UNDOC},
    {I_HINT_NOP63, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17484, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP7[] = {
    {I_HINT_NOP7, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16482, IF_P6|IF_UNDOC},
    {I_HINT_NOP7, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16488, IF_P6|IF_UNDOC},
    {I_HINT_NOP7, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16494, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP8[] = {
    {I_HINT_NOP8, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16500, IF_P6|IF_UNDOC},
    {I_HINT_NOP8, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16506, IF_P6|IF_UNDOC},
    {I_HINT_NOP8, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16512, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HINT_NOP9[] = {
    {I_HINT_NOP9, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16518, IF_P6|IF_UNDOC},
    {I_HINT_NOP9, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16524, IF_P6|IF_UNDOC},
    {I_HINT_NOP9, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16530, IF_X64|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_HLT[] = {
    {I_HLT, 0, {0,0,0,0,0}, nasm_bytecodes+20434, IF_8086|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_HSUBPD[] = {
    {I_HSUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16224, IF_PRESCOTT|IF_SSE3|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_HSUBPS[] = {
    {I_HSUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16230, IF_PRESCOTT|IF_SSE3|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_IBTS[] = {
    {I_IBTS, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14298, IF_386|IF_SW|IF_UNDOC},
    {I_IBTS, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14298, IF_386|IF_UNDOC},
    {I_IBTS, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14304, IF_386|IF_SD|IF_UNDOC},
    {I_IBTS, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14304, IF_386|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_ICEBP[] = {
    {I_ICEBP, 0, {0,0,0,0,0}, nasm_bytecodes+20437, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_IDIV[] = {
    {I_IDIV, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+19957, IF_8086},
    {I_IDIV, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18185, IF_8086},
    {I_IDIV, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18190, IF_386},
    {I_IDIV, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18195, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_IMUL[] = {
    {I_IMUL, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+19961, IF_8086},
    {I_IMUL, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18200, IF_8086},
    {I_IMUL, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18205, IF_386},
    {I_IMUL, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18210, IF_X64},
    {I_IMUL, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14316, IF_386|IF_SM},
    {I_IMUL, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14316, IF_386},
    {I_IMUL, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14322, IF_386|IF_SM},
    {I_IMUL, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14322, IF_386},
    {I_IMUL, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+14328, IF_X64|IF_SM},
    {I_IMUL, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14328, IF_X64},
    {I_IMUL, 3, {REG16,MEMORY,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14334, IF_186|IF_SM},
    {I_IMUL, 3, {REG16,MEMORY,SBYTE16,0,0}, nasm_bytecodes+14334, IF_186|IF_SM},
    {I_IMUL, 3, {REG16,MEMORY,IMMEDIATE|BITS16,0,0}, nasm_bytecodes+14340, IF_186|IF_SM},
    {I_IMUL, 3, {REG16,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+14346, IF_186|IF_SM},
    {I_IMUL, 3, {REG16,REG16,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14334, IF_186},
    {I_IMUL, 3, {REG16,REG16,SBYTE32,0,0}, nasm_bytecodes+14334, IF_186|IF_SM},
    {I_IMUL, 3, {REG16,REG16,IMMEDIATE|BITS16,0,0}, nasm_bytecodes+14340, IF_186},
    {I_IMUL, 3, {REG16,REG16,IMMEDIATE,0,0}, nasm_bytecodes+14346, IF_186|IF_SM},
    {I_IMUL, 3, {REG32,MEMORY,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14352, IF_386|IF_SM},
    {I_IMUL, 3, {REG32,MEMORY,SBYTE64,0,0}, nasm_bytecodes+14352, IF_386|IF_SM},
    {I_IMUL, 3, {REG32,MEMORY,IMMEDIATE|BITS32,0,0}, nasm_bytecodes+14358, IF_386|IF_SM},
    {I_IMUL, 3, {REG32,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+14364, IF_386|IF_SM},
    {I_IMUL, 3, {REG32,REG32,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14352, IF_386},
    {I_IMUL, 3, {REG32,REG32,SBYTE16,0,0}, nasm_bytecodes+14352, IF_386|IF_SM},
    {I_IMUL, 3, {REG32,REG32,IMMEDIATE|BITS32,0,0}, nasm_bytecodes+14358, IF_386},
    {I_IMUL, 3, {REG32,REG32,IMMEDIATE,0,0}, nasm_bytecodes+14364, IF_386|IF_SM},
    {I_IMUL, 3, {REG64,MEMORY,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14370, IF_X64|IF_SM},
    {I_IMUL, 3, {REG64,MEMORY,SBYTE32,0,0}, nasm_bytecodes+14370, IF_X64|IF_SM},
    {I_IMUL, 3, {REG64,MEMORY,IMMEDIATE|BITS32,0,0}, nasm_bytecodes+14376, IF_X64|IF_SM},
    {I_IMUL, 3, {REG64,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+14382, IF_X64|IF_SM},
    {I_IMUL, 3, {REG64,REG64,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14370, IF_X64},
    {I_IMUL, 3, {REG64,REG64,SBYTE64,0,0}, nasm_bytecodes+14370, IF_X64|IF_SM},
    {I_IMUL, 3, {REG64,REG64,IMMEDIATE|BITS32,0,0}, nasm_bytecodes+14376, IF_X64},
    {I_IMUL, 3, {REG64,REG64,IMMEDIATE,0,0}, nasm_bytecodes+14382, IF_X64|IF_SM},
    {I_IMUL, 2, {REG16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14388, IF_186},
    {I_IMUL, 2, {REG16,SBYTE16,0,0,0}, nasm_bytecodes+14388, IF_186|IF_SM},
    {I_IMUL, 2, {REG16,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14394, IF_186},
    {I_IMUL, 2, {REG16,IMMEDIATE,0,0,0}, nasm_bytecodes+14400, IF_186|IF_SM},
    {I_IMUL, 2, {REG32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14406, IF_386},
    {I_IMUL, 2, {REG32,SBYTE32,0,0,0}, nasm_bytecodes+14406, IF_386|IF_SM},
    {I_IMUL, 2, {REG32,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14412, IF_386},
    {I_IMUL, 2, {REG32,IMMEDIATE,0,0,0}, nasm_bytecodes+14418, IF_386|IF_SM},
    {I_IMUL, 2, {REG64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14424, IF_X64},
    {I_IMUL, 2, {REG64,SBYTE64,0,0,0}, nasm_bytecodes+14424, IF_X64|IF_SM},
    {I_IMUL, 2, {REG64,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14430, IF_X64},
    {I_IMUL, 2, {REG64,IMMEDIATE,0,0,0}, nasm_bytecodes+14436, IF_X64|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_IN[] = {
    {I_IN, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+19965, IF_8086|IF_SB},
    {I_IN, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+18215, IF_8086|IF_SB},
    {I_IN, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+18220, IF_386|IF_SB},
    {I_IN, 2, {REG_AL,REG_DX,0,0,0}, nasm_bytecodes+20440, IF_8086},
    {I_IN, 2, {REG_AX,REG_DX,0,0,0}, nasm_bytecodes+19969, IF_8086},
    {I_IN, 2, {REG_EAX,REG_DX,0,0,0}, nasm_bytecodes+19973, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_INC[] = {
    {I_INC, 1, {REG16,0,0,0,0}, nasm_bytecodes+19977, IF_8086|IF_NOLONG},
    {I_INC, 1, {REG32,0,0,0,0}, nasm_bytecodes+19981, IF_386|IF_NOLONG},
    {I_INC, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+19985, IF_8086},
    {I_INC, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18225, IF_8086},
    {I_INC, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18230, IF_386},
    {I_INC, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18235, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_INCBIN[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_INSB[] = {
    {I_INSB, 0, {0,0,0,0,0}, nasm_bytecodes+20443, IF_186},
    ITEMPLATE_END
};

static const struct itemplate instrux_INSD[] = {
    {I_INSD, 0, {0,0,0,0,0}, nasm_bytecodes+19989, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_INSERTPS[] = {
    {I_INSERTPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5639, IF_SSE41|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_INSERTQ[] = {
    {I_INSERTQ, 4, {XMMREG,XMMREG,IMMEDIATE,IMMEDIATE,0}, nasm_bytecodes+5599, IF_SSE4A|IF_AMD},
    {I_INSERTQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16278, IF_SSE4A|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_INSW[] = {
    {I_INSW, 0, {0,0,0,0,0}, nasm_bytecodes+19993, IF_186},
    ITEMPLATE_END
};

static const struct itemplate instrux_INT[] = {
    {I_INT, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+19997, IF_8086|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_INT01[] = {
    {I_INT01, 0, {0,0,0,0,0}, nasm_bytecodes+20437, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_INT03[] = {
    {I_INT03, 0, {0,0,0,0,0}, nasm_bytecodes+20446, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_INT1[] = {
    {I_INT1, 0, {0,0,0,0,0}, nasm_bytecodes+20437, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_INT3[] = {
    {I_INT3, 0, {0,0,0,0,0}, nasm_bytecodes+20446, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_INTO[] = {
    {I_INTO, 0, {0,0,0,0,0}, nasm_bytecodes+20449, IF_8086|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_INVD[] = {
    {I_INVD, 0, {0,0,0,0,0}, nasm_bytecodes+20001, IF_486|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_INVEPT[] = {
    {I_INVEPT, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+5560, IF_VMX|IF_SO|IF_NOLONG},
    {I_INVEPT, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+5559, IF_VMX|IF_SO|IF_LONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_INVLPG[] = {
    {I_INVLPG, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18240, IF_486|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_INVLPGA[] = {
    {I_INVLPGA, 2, {REG_AX,REG_ECX,0,0,0}, nasm_bytecodes+14442, IF_X86_64|IF_AMD|IF_NOLONG},
    {I_INVLPGA, 2, {REG_EAX,REG_ECX,0,0,0}, nasm_bytecodes+14448, IF_X86_64|IF_AMD},
    {I_INVLPGA, 2, {REG_RAX,REG_ECX,0,0,0}, nasm_bytecodes+6819, IF_X64|IF_AMD},
    {I_INVLPGA, 0, {0,0,0,0,0}, nasm_bytecodes+14449, IF_X86_64|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_INVVPID[] = {
    {I_INVVPID, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+5568, IF_VMX|IF_SO|IF_NOLONG},
    {I_INVVPID, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+5567, IF_VMX|IF_SO|IF_LONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_IRET[] = {
    {I_IRET, 0, {0,0,0,0,0}, nasm_bytecodes+20005, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_IRETD[] = {
    {I_IRETD, 0, {0,0,0,0,0}, nasm_bytecodes+20009, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_IRETQ[] = {
    {I_IRETQ, 0, {0,0,0,0,0}, nasm_bytecodes+20013, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_IRETW[] = {
    {I_IRETW, 0, {0,0,0,0,0}, nasm_bytecodes+20017, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_JCXZ[] = {
    {I_JCXZ, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18245, IF_8086|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_JECXZ[] = {
    {I_JECXZ, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18250, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_JMP[] = {
    {I_JMP, 1, {IMMEDIATE|SHORT,0,0,0,0}, nasm_bytecodes+18256, IF_8086},
    {I_JMP, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18255, IF_8086},
    {I_JMP, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18260, IF_8086},
    {I_JMP, 1, {IMMEDIATE|NEAR,0,0,0,0}, nasm_bytecodes+18260, IF_8086},
    {I_JMP, 1, {IMMEDIATE|FAR,0,0,0,0}, nasm_bytecodes+14454, IF_8086|IF_NOLONG},
    {I_JMP, 1, {IMMEDIATE|BITS16,0,0,0,0}, nasm_bytecodes+18265, IF_8086},
    {I_JMP, 1, {IMMEDIATE|BITS16|NEAR,0,0,0,0}, nasm_bytecodes+18265, IF_8086},
    {I_JMP, 1, {IMMEDIATE|BITS16|FAR,0,0,0,0}, nasm_bytecodes+14460, IF_8086|IF_NOLONG},
    {I_JMP, 1, {IMMEDIATE|BITS32,0,0,0,0}, nasm_bytecodes+18270, IF_386},
    {I_JMP, 1, {IMMEDIATE|BITS32|NEAR,0,0,0,0}, nasm_bytecodes+18270, IF_386},
    {I_JMP, 1, {IMMEDIATE|BITS32|FAR,0,0,0,0}, nasm_bytecodes+14466, IF_386|IF_NOLONG},
    {I_JMP, 2, {IMMEDIATE|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14472, IF_8086|IF_NOLONG},
    {I_JMP, 2, {IMMEDIATE|BITS16|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14478, IF_8086|IF_NOLONG},
    {I_JMP, 2, {IMMEDIATE|COLON,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14478, IF_8086|IF_NOLONG},
    {I_JMP, 2, {IMMEDIATE|BITS32|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14484, IF_386|IF_NOLONG},
    {I_JMP, 2, {IMMEDIATE|COLON,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14484, IF_386|IF_NOLONG},
    {I_JMP, 1, {MEMORY|FAR,0,0,0,0}, nasm_bytecodes+18275, IF_8086|IF_NOLONG},
    {I_JMP, 1, {MEMORY|FAR,0,0,0,0}, nasm_bytecodes+18280, IF_X64},
    {I_JMP, 1, {MEMORY|BITS16|FAR,0,0,0,0}, nasm_bytecodes+18285, IF_8086},
    {I_JMP, 1, {MEMORY|BITS32|FAR,0,0,0,0}, nasm_bytecodes+18290, IF_386},
    {I_JMP, 1, {MEMORY|BITS64|FAR,0,0,0,0}, nasm_bytecodes+18280, IF_X64},
    {I_JMP, 1, {MEMORY|NEAR,0,0,0,0}, nasm_bytecodes+18295, IF_8086},
    {I_JMP, 1, {MEMORY|BITS16|NEAR,0,0,0,0}, nasm_bytecodes+18300, IF_8086},
    {I_JMP, 1, {MEMORY|BITS32|NEAR,0,0,0,0}, nasm_bytecodes+18305, IF_386|IF_NOLONG},
    {I_JMP, 1, {MEMORY|BITS64|NEAR,0,0,0,0}, nasm_bytecodes+18310, IF_X64},
    {I_JMP, 1, {REG16,0,0,0,0}, nasm_bytecodes+18300, IF_8086},
    {I_JMP, 1, {REG32,0,0,0,0}, nasm_bytecodes+18305, IF_386|IF_NOLONG},
    {I_JMP, 1, {REG64,0,0,0,0}, nasm_bytecodes+18310, IF_X64},
    {I_JMP, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18295, IF_8086},
    {I_JMP, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+18300, IF_8086},
    {I_JMP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+18305, IF_386|IF_NOLONG},
    {I_JMP, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+18310, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_JMPE[] = {
    {I_JMPE, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+14490, IF_IA64},
    {I_JMPE, 1, {IMMEDIATE|BITS16,0,0,0,0}, nasm_bytecodes+14496, IF_IA64},
    {I_JMPE, 1, {IMMEDIATE|BITS32,0,0,0,0}, nasm_bytecodes+14502, IF_IA64},
    {I_JMPE, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+14508, IF_IA64},
    {I_JMPE, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+14514, IF_IA64},
    ITEMPLATE_END
};

static const struct itemplate instrux_JRCXZ[] = {
    {I_JRCXZ, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18251, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_LAHF[] = {
    {I_LAHF, 0, {0,0,0,0,0}, nasm_bytecodes+20452, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_LAR[] = {
    {I_LAR, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14520, IF_286|IF_PROT|IF_SW},
    {I_LAR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14520, IF_286|IF_PROT},
    {I_LAR, 2, {REG16,REG32,0,0,0}, nasm_bytecodes+14520, IF_386|IF_PROT},
    {I_LAR, 2, {REG16,REG64,0,0,0}, nasm_bytecodes+6826, IF_X64|IF_PROT},
    {I_LAR, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14526, IF_386|IF_PROT|IF_SW},
    {I_LAR, 2, {REG32,REG16,0,0,0}, nasm_bytecodes+14526, IF_386|IF_PROT},
    {I_LAR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14526, IF_386|IF_PROT},
    {I_LAR, 2, {REG32,REG64,0,0,0}, nasm_bytecodes+6833, IF_X64|IF_PROT},
    {I_LAR, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+14532, IF_X64|IF_PROT|IF_SW},
    {I_LAR, 2, {REG64,REG16,0,0,0}, nasm_bytecodes+14532, IF_X64|IF_PROT},
    {I_LAR, 2, {REG64,REG32,0,0,0}, nasm_bytecodes+14532, IF_X64|IF_PROT},
    {I_LAR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14532, IF_X64|IF_PROT},
    ITEMPLATE_END
};

static const struct itemplate instrux_LDDQU[] = {
    {I_LDDQU, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16236, IF_PRESCOTT|IF_SSE3|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_LDMXCSR[] = {
    {I_LDMXCSR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19290, IF_KATMAI|IF_SSE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_LDS[] = {
    {I_LDS, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18315, IF_8086|IF_NOLONG},
    {I_LDS, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18320, IF_386|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_LEA[] = {
    {I_LEA, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18325, IF_8086},
    {I_LEA, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18330, IF_386},
    {I_LEA, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+18335, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_LEAVE[] = {
    {I_LEAVE, 0, {0,0,0,0,0}, nasm_bytecodes+18572, IF_186},
    ITEMPLATE_END
};

static const struct itemplate instrux_LES[] = {
    {I_LES, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18340, IF_8086|IF_NOLONG},
    {I_LES, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18345, IF_386|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_LFENCE[] = {
    {I_LFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+18350, IF_X64|IF_AMD},
    {I_LFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+18350, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_LFS[] = {
    {I_LFS, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14538, IF_386},
    {I_LFS, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14544, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_LGDT[] = {
    {I_LGDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18355, IF_286|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_LGS[] = {
    {I_LGS, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14550, IF_386},
    {I_LGS, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14556, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_LIDT[] = {
    {I_LIDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18360, IF_286|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_LLDT[] = {
    {I_LLDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18365, IF_286|IF_PROT|IF_PRIV},
    {I_LLDT, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+18365, IF_286|IF_PROT|IF_PRIV},
    {I_LLDT, 1, {REG16,0,0,0,0}, nasm_bytecodes+18365, IF_286|IF_PROT|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_LMSW[] = {
    {I_LMSW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18370, IF_286|IF_PRIV},
    {I_LMSW, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+18370, IF_286|IF_PRIV},
    {I_LMSW, 1, {REG16,0,0,0,0}, nasm_bytecodes+18370, IF_286|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_LOADALL[] = {
    {I_LOADALL, 0, {0,0,0,0,0}, nasm_bytecodes+20021, IF_386|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_LOADALL286[] = {
    {I_LOADALL286, 0, {0,0,0,0,0}, nasm_bytecodes+20025, IF_286|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_LODSB[] = {
    {I_LODSB, 0, {0,0,0,0,0}, nasm_bytecodes+20455, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_LODSD[] = {
    {I_LODSD, 0, {0,0,0,0,0}, nasm_bytecodes+20029, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_LODSQ[] = {
    {I_LODSQ, 0, {0,0,0,0,0}, nasm_bytecodes+20033, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_LODSW[] = {
    {I_LODSW, 0, {0,0,0,0,0}, nasm_bytecodes+20037, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_LOOP[] = {
    {I_LOOP, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18375, IF_8086},
    {I_LOOP, 2, {IMMEDIATE,REG_CX,0,0,0}, nasm_bytecodes+18380, IF_8086|IF_NOLONG},
    {I_LOOP, 2, {IMMEDIATE,REG_ECX,0,0,0}, nasm_bytecodes+18385, IF_386},
    {I_LOOP, 2, {IMMEDIATE,REG_RCX,0,0,0}, nasm_bytecodes+18390, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_LOOPE[] = {
    {I_LOOPE, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18395, IF_8086},
    {I_LOOPE, 2, {IMMEDIATE,REG_CX,0,0,0}, nasm_bytecodes+18400, IF_8086|IF_NOLONG},
    {I_LOOPE, 2, {IMMEDIATE,REG_ECX,0,0,0}, nasm_bytecodes+18405, IF_386},
    {I_LOOPE, 2, {IMMEDIATE,REG_RCX,0,0,0}, nasm_bytecodes+18410, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_LOOPNE[] = {
    {I_LOOPNE, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18415, IF_8086},
    {I_LOOPNE, 2, {IMMEDIATE,REG_CX,0,0,0}, nasm_bytecodes+18420, IF_8086|IF_NOLONG},
    {I_LOOPNE, 2, {IMMEDIATE,REG_ECX,0,0,0}, nasm_bytecodes+18425, IF_386},
    {I_LOOPNE, 2, {IMMEDIATE,REG_RCX,0,0,0}, nasm_bytecodes+18430, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_LOOPNZ[] = {
    {I_LOOPNZ, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18415, IF_8086},
    {I_LOOPNZ, 2, {IMMEDIATE,REG_CX,0,0,0}, nasm_bytecodes+18420, IF_8086|IF_NOLONG},
    {I_LOOPNZ, 2, {IMMEDIATE,REG_ECX,0,0,0}, nasm_bytecodes+18425, IF_386},
    {I_LOOPNZ, 2, {IMMEDIATE,REG_RCX,0,0,0}, nasm_bytecodes+18430, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_LOOPZ[] = {
    {I_LOOPZ, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18395, IF_8086},
    {I_LOOPZ, 2, {IMMEDIATE,REG_CX,0,0,0}, nasm_bytecodes+18400, IF_8086|IF_NOLONG},
    {I_LOOPZ, 2, {IMMEDIATE,REG_ECX,0,0,0}, nasm_bytecodes+18405, IF_386},
    {I_LOOPZ, 2, {IMMEDIATE,REG_RCX,0,0,0}, nasm_bytecodes+18410, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_LSL[] = {
    {I_LSL, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14562, IF_286|IF_PROT|IF_SW},
    {I_LSL, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14562, IF_286|IF_PROT},
    {I_LSL, 2, {REG16,REG32,0,0,0}, nasm_bytecodes+14562, IF_386|IF_PROT},
    {I_LSL, 2, {REG16,REG64,0,0,0}, nasm_bytecodes+6840, IF_X64|IF_PROT},
    {I_LSL, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14568, IF_386|IF_PROT|IF_SW},
    {I_LSL, 2, {REG32,REG16,0,0,0}, nasm_bytecodes+14568, IF_386|IF_PROT},
    {I_LSL, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14568, IF_386|IF_PROT},
    {I_LSL, 2, {REG32,REG64,0,0,0}, nasm_bytecodes+6847, IF_X64|IF_PROT},
    {I_LSL, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+14574, IF_X64|IF_PROT|IF_SW},
    {I_LSL, 2, {REG64,REG16,0,0,0}, nasm_bytecodes+14574, IF_X64|IF_PROT},
    {I_LSL, 2, {REG64,REG32,0,0,0}, nasm_bytecodes+14574, IF_X64|IF_PROT},
    {I_LSL, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14574, IF_X64|IF_PROT},
    ITEMPLATE_END
};

static const struct itemplate instrux_LSS[] = {
    {I_LSS, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14580, IF_386},
    {I_LSS, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14586, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_LTR[] = {
    {I_LTR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18435, IF_286|IF_PROT|IF_PRIV},
    {I_LTR, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+18435, IF_286|IF_PROT|IF_PRIV},
    {I_LTR, 1, {REG16,0,0,0,0}, nasm_bytecodes+18435, IF_286|IF_PROT|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_LZCNT[] = {
    {I_LZCNT, 2, {REG16,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+7897, IF_P6|IF_AMD},
    {I_LZCNT, 2, {REG32,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+7904, IF_P6|IF_AMD},
    {I_LZCNT, 2, {REG64,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+7911, IF_X64|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_MASKMOVDQU[] = {
    {I_MASKMOVDQU, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15492, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MASKMOVQ[] = {
    {I_MASKMOVQ, 2, {MMXREG,MMXREG,0,0,0}, nasm_bytecodes+15474, IF_KATMAI|IF_MMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_MAXPD[] = {
    {I_MAXPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16044, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_MAXPS[] = {
    {I_MAXPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15270, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MAXSD[] = {
    {I_MAXSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16050, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MAXSS[] = {
    {I_MAXSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15276, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MFENCE[] = {
    {I_MFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+18440, IF_X64|IF_AMD},
    {I_MFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+18440, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MINPD[] = {
    {I_MINPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16056, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_MINPS[] = {
    {I_MINPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15282, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MINSD[] = {
    {I_MINSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16062, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MINSS[] = {
    {I_MINSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15288, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MONITOR[] = {
    {I_MONITOR, 0, {0,0,0,0,0}, nasm_bytecodes+18445, IF_PRESCOTT},
    {I_MONITOR, 3, {REG_EAX,REG_ECX,REG_EDX,0,0}, nasm_bytecodes+18445, IF_PRESCOTT},
    ITEMPLATE_END
};

static const struct itemplate instrux_MONTMUL[] = {
    {I_MONTMUL, 0, {0,0,0,0,0}, nasm_bytecodes+16338, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOV[] = {
    {I_MOV, 2, {MEMORY,REG_SREG,0,0,0}, nasm_bytecodes+18456, IF_8086|IF_SM},
    {I_MOV, 2, {REG16,REG_SREG,0,0,0}, nasm_bytecodes+18450, IF_8086},
    {I_MOV, 2, {REG32,REG_SREG,0,0,0}, nasm_bytecodes+18455, IF_386},
    {I_MOV, 2, {REG_SREG,MEMORY,0,0,0}, nasm_bytecodes+20041, IF_8086|IF_SM},
    {I_MOV, 2, {REG_SREG,REG16,0,0,0}, nasm_bytecodes+20041, IF_8086},
    {I_MOV, 2, {REG_SREG,REG32,0,0,0}, nasm_bytecodes+20041, IF_386},
    {I_MOV, 2, {REG_AL,MEM_OFFS,0,0,0}, nasm_bytecodes+20045, IF_8086|IF_SM},
    {I_MOV, 2, {REG_AX,MEM_OFFS,0,0,0}, nasm_bytecodes+18460, IF_8086|IF_SM},
    {I_MOV, 2, {REG_EAX,MEM_OFFS,0,0,0}, nasm_bytecodes+18465, IF_386|IF_SM},
    {I_MOV, 2, {REG_RAX,MEM_OFFS,0,0,0}, nasm_bytecodes+18470, IF_X64|IF_SM},
    {I_MOV, 2, {MEM_OFFS,REG_AL,0,0,0}, nasm_bytecodes+20049, IF_8086|IF_SM},
    {I_MOV, 2, {MEM_OFFS,REG_AX,0,0,0}, nasm_bytecodes+18475, IF_8086|IF_SM},
    {I_MOV, 2, {MEM_OFFS,REG_EAX,0,0,0}, nasm_bytecodes+18480, IF_386|IF_SM},
    {I_MOV, 2, {MEM_OFFS,REG_RAX,0,0,0}, nasm_bytecodes+18485, IF_X64|IF_SM},
    {I_MOV, 2, {REG32,REG_CREG,0,0,0}, nasm_bytecodes+14592, IF_386|IF_PRIV|IF_NOLONG},
    {I_MOV, 2, {REG64,REG_CREG,0,0,0}, nasm_bytecodes+14598, IF_X64|IF_PRIV},
    {I_MOV, 2, {REG_CREG,REG32,0,0,0}, nasm_bytecodes+14604, IF_386|IF_PRIV|IF_NOLONG},
    {I_MOV, 2, {REG_CREG,REG64,0,0,0}, nasm_bytecodes+14610, IF_X64|IF_PRIV},
    {I_MOV, 2, {REG32,REG_DREG,0,0,0}, nasm_bytecodes+14617, IF_386|IF_PRIV|IF_NOLONG},
    {I_MOV, 2, {REG64,REG_DREG,0,0,0}, nasm_bytecodes+14616, IF_X64|IF_PRIV},
    {I_MOV, 2, {REG_DREG,REG32,0,0,0}, nasm_bytecodes+14623, IF_386|IF_PRIV|IF_NOLONG},
    {I_MOV, 2, {REG_DREG,REG64,0,0,0}, nasm_bytecodes+14622, IF_X64|IF_PRIV},
    {I_MOV, 2, {REG32,REG_TREG,0,0,0}, nasm_bytecodes+18490, IF_386|IF_NOLONG},
    {I_MOV, 2, {REG_TREG,REG32,0,0,0}, nasm_bytecodes+18495, IF_386|IF_NOLONG},
    {I_MOV, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20053, IF_8086|IF_SM},
    {I_MOV, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20053, IF_8086},
    {I_MOV, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+18500, IF_8086|IF_SM},
    {I_MOV, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18500, IF_8086},
    {I_MOV, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+18505, IF_386|IF_SM},
    {I_MOV, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18505, IF_386},
    {I_MOV, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+18510, IF_X64|IF_SM},
    {I_MOV, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18510, IF_X64},
    {I_MOV, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+20057, IF_8086|IF_SM},
    {I_MOV, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20057, IF_8086},
    {I_MOV, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18515, IF_8086|IF_SM},
    {I_MOV, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18515, IF_8086},
    {I_MOV, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18520, IF_386|IF_SM},
    {I_MOV, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18520, IF_386},
    {I_MOV, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+18525, IF_X64|IF_SM},
    {I_MOV, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18525, IF_X64},
    {I_MOV, 2, {REG8,IMMEDIATE,0,0,0}, nasm_bytecodes+20061, IF_8086|IF_SM},
    {I_MOV, 2, {REG16,IMMEDIATE,0,0,0}, nasm_bytecodes+18530, IF_8086|IF_SM},
    {I_MOV, 2, {REG32,IMMEDIATE,0,0,0}, nasm_bytecodes+18535, IF_386|IF_SM},
    {I_MOV, 2, {REG64,IMMEDIATE,0,0,0}, nasm_bytecodes+18540, IF_X64|IF_SM},
    {I_MOV, 2, {REG64,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14628, IF_X64},
    {I_MOV, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18545, IF_8086|IF_SM},
    {I_MOV, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14634, IF_8086|IF_SM},
    {I_MOV, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14640, IF_386|IF_SM},
    {I_MOV, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14628, IF_X64|IF_SM},
    {I_MOV, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+18545, IF_8086|IF_SM},
    {I_MOV, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14634, IF_8086|IF_SM},
    {I_MOV, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14640, IF_386|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVAPD[] = {
    {I_MOVAPD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16068, IF_WILLAMETTE|IF_SSE2},
    {I_MOVAPD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16074, IF_WILLAMETTE|IF_SSE2},
    {I_MOVAPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16074, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_MOVAPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16068, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVAPS[] = {
    {I_MOVAPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15294, IF_KATMAI|IF_SSE},
    {I_MOVAPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15300, IF_KATMAI|IF_SSE},
    {I_MOVAPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15294, IF_KATMAI|IF_SSE},
    {I_MOVAPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15300, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVBE[] = {
    {I_MOVBE, 2, {REG16,MEMORY|BITS16,0,0,0}, nasm_bytecodes+9115, IF_NEHALEM|IF_SM},
    {I_MOVBE, 2, {REG32,MEMORY|BITS32,0,0,0}, nasm_bytecodes+9122, IF_NEHALEM|IF_SM},
    {I_MOVBE, 2, {REG64,MEMORY|BITS64,0,0,0}, nasm_bytecodes+9129, IF_NEHALEM|IF_SM},
    {I_MOVBE, 2, {MEMORY|BITS16,REG16,0,0,0}, nasm_bytecodes+9136, IF_NEHALEM|IF_SM},
    {I_MOVBE, 2, {MEMORY|BITS32,REG32,0,0,0}, nasm_bytecodes+9143, IF_NEHALEM|IF_SM},
    {I_MOVBE, 2, {MEMORY|BITS64,REG64,0,0,0}, nasm_bytecodes+9150, IF_NEHALEM|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVD[] = {
    {I_MOVD, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+14646, IF_PENT|IF_MMX|IF_SD},
    {I_MOVD, 2, {MMXREG,REG32,0,0,0}, nasm_bytecodes+14646, IF_PENT|IF_MMX},
    {I_MOVD, 2, {MEMORY,MMXREG,0,0,0}, nasm_bytecodes+14652, IF_PENT|IF_MMX|IF_SD},
    {I_MOVD, 2, {REG32,MMXREG,0,0,0}, nasm_bytecodes+14652, IF_PENT|IF_MMX},
    {I_MOVD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+6854, IF_X64|IF_SD},
    {I_MOVD, 2, {XMMREG,REG32,0,0,0}, nasm_bytecodes+6854, IF_X64},
    {I_MOVD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+6861, IF_X64|IF_SD},
    {I_MOVD, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+6861, IF_X64|IF_SSE},
    {I_MOVD, 2, {XMMREG,REG32,0,0,0}, nasm_bytecodes+15510, IF_WILLAMETTE|IF_SSE2},
    {I_MOVD, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+15516, IF_WILLAMETTE|IF_SSE2},
    {I_MOVD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15516, IF_WILLAMETTE|IF_SSE2|IF_SD},
    {I_MOVD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15510, IF_WILLAMETTE|IF_SSE2|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVDDUP[] = {
    {I_MOVDDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16242, IF_PRESCOTT|IF_SSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVDQ2Q[] = {
    {I_MOVDQ2Q, 2, {MMXREG,XMMREG,0,0,0}, nasm_bytecodes+15546, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVDQA[] = {
    {I_MOVDQA, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15522, IF_WILLAMETTE|IF_SSE2},
    {I_MOVDQA, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15528, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_MOVDQA, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15522, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_MOVDQA, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15528, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVDQU[] = {
    {I_MOVDQU, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15534, IF_WILLAMETTE|IF_SSE2},
    {I_MOVDQU, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15540, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_MOVDQU, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15534, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_MOVDQU, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15540, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVHLPS[] = {
    {I_MOVHLPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15126, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVHPD[] = {
    {I_MOVHPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16080, IF_WILLAMETTE|IF_SSE2},
    {I_MOVHPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16086, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVHPS[] = {
    {I_MOVHPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15306, IF_KATMAI|IF_SSE},
    {I_MOVHPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15312, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVLHPS[] = {
    {I_MOVLHPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15306, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVLPD[] = {
    {I_MOVLPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16092, IF_WILLAMETTE|IF_SSE2},
    {I_MOVLPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16098, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVLPS[] = {
    {I_MOVLPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15126, IF_KATMAI|IF_SSE},
    {I_MOVLPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15318, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVMSKPD[] = {
    {I_MOVMSKPD, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+16104, IF_WILLAMETTE|IF_SSE2},
    {I_MOVMSKPD, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+7659, IF_X64|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVMSKPS[] = {
    {I_MOVMSKPS, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+15324, IF_KATMAI|IF_SSE},
    {I_MOVMSKPS, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+7400, IF_X64|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVNTDQ[] = {
    {I_MOVNTDQ, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15498, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVNTDQA[] = {
    {I_MOVNTDQA, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+7932, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVNTI[] = {
    {I_MOVNTI, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+7485, IF_WILLAMETTE|IF_SD},
    {I_MOVNTI, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+7484, IF_X64|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVNTPD[] = {
    {I_MOVNTPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15504, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVNTPS[] = {
    {I_MOVNTPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15330, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVNTQ[] = {
    {I_MOVNTQ, 2, {MEMORY,MMXREG,0,0,0}, nasm_bytecodes+15480, IF_KATMAI|IF_MMX|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVNTSD[] = {
    {I_MOVNTSD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16284, IF_SSE4A|IF_AMD|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVNTSS[] = {
    {I_MOVNTSS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16290, IF_SSE4A|IF_AMD|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVQ[] = {
    {I_MOVQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6868, IF_PENT|IF_MMX|IF_SQ},
    {I_MOVQ, 2, {RM_MMX,MMXREG,0,0,0}, nasm_bytecodes+6875, IF_PENT|IF_MMX|IF_SQ},
    {I_MOVQ, 2, {MMXREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+14646, IF_X64|IF_MMX},
    {I_MOVQ, 2, {RM_GPR|BITS64,MMXREG,0,0,0}, nasm_bytecodes+14652, IF_X64|IF_MMX},
    {I_MOVQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15552, IF_WILLAMETTE|IF_SSE2},
    {I_MOVQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15558, IF_WILLAMETTE|IF_SSE2},
    {I_MOVQ, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15558, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    {I_MOVQ, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15552, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    {I_MOVQ, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+7491, IF_X64|IF_SSE2},
    {I_MOVQ, 2, {RM_GPR|BITS64,XMMREG,0,0,0}, nasm_bytecodes+7498, IF_X64|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVQ2DQ[] = {
    {I_MOVQ2DQ, 2, {XMMREG,MMXREG,0,0,0}, nasm_bytecodes+15564, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVSB[] = {
    {I_MOVSB, 0, {0,0,0,0,0}, nasm_bytecodes+5164, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVSD[] = {
    {I_MOVSD, 0, {0,0,0,0,0}, nasm_bytecodes+20065, IF_386},
    {I_MOVSD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16110, IF_WILLAMETTE|IF_SSE2},
    {I_MOVSD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16116, IF_WILLAMETTE|IF_SSE2},
    {I_MOVSD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16116, IF_WILLAMETTE|IF_SSE2},
    {I_MOVSD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16110, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVSHDUP[] = {
    {I_MOVSHDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16248, IF_PRESCOTT|IF_SSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVSLDUP[] = {
    {I_MOVSLDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16254, IF_PRESCOTT|IF_SSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVSQ[] = {
    {I_MOVSQ, 0, {0,0,0,0,0}, nasm_bytecodes+20069, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVSS[] = {
    {I_MOVSS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15336, IF_KATMAI|IF_SSE},
    {I_MOVSS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15342, IF_KATMAI|IF_SSE},
    {I_MOVSS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15336, IF_KATMAI|IF_SSE},
    {I_MOVSS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15342, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVSW[] = {
    {I_MOVSW, 0, {0,0,0,0,0}, nasm_bytecodes+20073, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVSX[] = {
    {I_MOVSX, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14658, IF_386|IF_SB},
    {I_MOVSX, 2, {REG16,REG8,0,0,0}, nasm_bytecodes+14658, IF_386},
    {I_MOVSX, 2, {REG32,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+14664, IF_386},
    {I_MOVSX, 2, {REG32,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+14670, IF_386},
    {I_MOVSX, 2, {REG64,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+14676, IF_X64},
    {I_MOVSX, 2, {REG64,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+14682, IF_X64},
    {I_MOVSX, 2, {REG64,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+18550, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVSXD[] = {
    {I_MOVSXD, 2, {REG64,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+18550, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVUPD[] = {
    {I_MOVUPD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16122, IF_WILLAMETTE|IF_SSE2},
    {I_MOVUPD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16128, IF_WILLAMETTE|IF_SSE2},
    {I_MOVUPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16128, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_MOVUPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16122, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVUPS[] = {
    {I_MOVUPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15348, IF_KATMAI|IF_SSE},
    {I_MOVUPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15354, IF_KATMAI|IF_SSE},
    {I_MOVUPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15348, IF_KATMAI|IF_SSE},
    {I_MOVUPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15354, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MOVZX[] = {
    {I_MOVZX, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14688, IF_386|IF_SB},
    {I_MOVZX, 2, {REG16,REG8,0,0,0}, nasm_bytecodes+14688, IF_386},
    {I_MOVZX, 2, {REG32,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+14694, IF_386},
    {I_MOVZX, 2, {REG32,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+14700, IF_386},
    {I_MOVZX, 2, {REG64,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+14706, IF_X64},
    {I_MOVZX, 2, {REG64,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+14712, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_MPSADBW[] = {
    {I_MPSADBW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5647, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_MUL[] = {
    {I_MUL, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+20077, IF_8086},
    {I_MUL, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18555, IF_8086},
    {I_MUL, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18560, IF_386},
    {I_MUL, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18565, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_MULPD[] = {
    {I_MULPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16134, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_MULPS[] = {
    {I_MULPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15360, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MULSD[] = {
    {I_MULSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16140, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_MULSS[] = {
    {I_MULSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15366, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_MWAIT[] = {
    {I_MWAIT, 0, {0,0,0,0,0}, nasm_bytecodes+18570, IF_PRESCOTT},
    {I_MWAIT, 2, {REG_EAX,REG_ECX,0,0,0}, nasm_bytecodes+18570, IF_PRESCOTT},
    ITEMPLATE_END
};

static const struct itemplate instrux_NEG[] = {
    {I_NEG, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+20081, IF_8086},
    {I_NEG, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18575, IF_8086},
    {I_NEG, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18580, IF_386},
    {I_NEG, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18585, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_NOP[] = {
    {I_NOP, 0, {0,0,0,0,0}, nasm_bytecodes+20085, IF_8086},
    {I_NOP, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+14718, IF_P6},
    {I_NOP, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+14724, IF_P6},
    {I_NOP, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+14730, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_NOT[] = {
    {I_NOT, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+20089, IF_8086},
    {I_NOT, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18590, IF_8086},
    {I_NOT, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18595, IF_386},
    {I_NOT, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18600, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_OR[] = {
    {I_OR, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20093, IF_8086|IF_SM},
    {I_OR, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20093, IF_8086},
    {I_OR, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+18605, IF_8086|IF_SM},
    {I_OR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18605, IF_8086},
    {I_OR, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+18610, IF_386|IF_SM},
    {I_OR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18610, IF_386},
    {I_OR, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+18615, IF_X64|IF_SM},
    {I_OR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18615, IF_X64},
    {I_OR, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+11855, IF_8086|IF_SM},
    {I_OR, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+11855, IF_8086},
    {I_OR, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18620, IF_8086|IF_SM},
    {I_OR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18620, IF_8086},
    {I_OR, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18625, IF_386|IF_SM},
    {I_OR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18625, IF_386},
    {I_OR, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+18630, IF_X64|IF_SM},
    {I_OR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18630, IF_X64},
    {I_OR, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14736, IF_8086},
    {I_OR, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14742, IF_386},
    {I_OR, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14748, IF_X64},
    {I_OR, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+20097, IF_8086|IF_SM},
    {I_OR, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+14736, IF_8086|IF_SM},
    {I_OR, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+18635, IF_8086|IF_SM},
    {I_OR, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+14742, IF_386|IF_SM},
    {I_OR, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+18640, IF_386|IF_SM},
    {I_OR, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+14748, IF_X64|IF_SM},
    {I_OR, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+18645, IF_X64|IF_SM},
    {I_OR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18650, IF_8086|IF_SM},
    {I_OR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14754, IF_8086|IF_SM},
    {I_OR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14760, IF_386|IF_SM},
    {I_OR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14766, IF_X64|IF_SM},
    {I_OR, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+18650, IF_8086|IF_SM},
    {I_OR, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14754, IF_8086|IF_SM},
    {I_OR, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14760, IF_386|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_ORPD[] = {
    {I_ORPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16146, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_ORPS[] = {
    {I_ORPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15372, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_OUT[] = {
    {I_OUT, 2, {IMMEDIATE,REG_AL,0,0,0}, nasm_bytecodes+20101, IF_8086|IF_SB},
    {I_OUT, 2, {IMMEDIATE,REG_AX,0,0,0}, nasm_bytecodes+18655, IF_8086|IF_SB},
    {I_OUT, 2, {IMMEDIATE,REG_EAX,0,0,0}, nasm_bytecodes+18660, IF_386|IF_SB},
    {I_OUT, 2, {REG_DX,REG_AL,0,0,0}, nasm_bytecodes+20458, IF_8086},
    {I_OUT, 2, {REG_DX,REG_AX,0,0,0}, nasm_bytecodes+20105, IF_8086},
    {I_OUT, 2, {REG_DX,REG_EAX,0,0,0}, nasm_bytecodes+20109, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_OUTSB[] = {
    {I_OUTSB, 0, {0,0,0,0,0}, nasm_bytecodes+20461, IF_186},
    ITEMPLATE_END
};

static const struct itemplate instrux_OUTSD[] = {
    {I_OUTSD, 0, {0,0,0,0,0}, nasm_bytecodes+20113, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_OUTSW[] = {
    {I_OUTSW, 0, {0,0,0,0,0}, nasm_bytecodes+20117, IF_186},
    ITEMPLATE_END
};

static const struct itemplate instrux_PABSB[] = {
    {I_PABSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7687, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PABSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7694, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PABSD[] = {
    {I_PABSD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7715, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PABSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7722, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PABSW[] = {
    {I_PABSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7701, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PABSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7708, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PACKSSDW[] = {
    {I_PACKSSDW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6882, IF_PENT|IF_MMX|IF_SQ},
    {I_PACKSSDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15576, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PACKSSWB[] = {
    {I_PACKSSWB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6889, IF_PENT|IF_MMX|IF_SQ},
    {I_PACKSSWB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15570, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PACKUSDW[] = {
    {I_PACKUSDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7939, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PACKUSWB[] = {
    {I_PACKUSWB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6896, IF_PENT|IF_MMX|IF_SQ},
    {I_PACKUSWB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15582, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PADDB[] = {
    {I_PADDB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6903, IF_PENT|IF_MMX|IF_SQ},
    {I_PADDB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15588, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PADDD[] = {
    {I_PADDD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6910, IF_PENT|IF_MMX|IF_SQ},
    {I_PADDD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15600, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PADDQ[] = {
    {I_PADDQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+15606, IF_WILLAMETTE|IF_MMX|IF_SQ},
    {I_PADDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15612, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PADDSB[] = {
    {I_PADDSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6917, IF_PENT|IF_MMX|IF_SQ},
    {I_PADDSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15618, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PADDSIW[] = {
    {I_PADDSIW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14772, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PADDSW[] = {
    {I_PADDSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6924, IF_PENT|IF_MMX|IF_SQ},
    {I_PADDSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15624, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PADDUSB[] = {
    {I_PADDUSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6931, IF_PENT|IF_MMX|IF_SQ},
    {I_PADDUSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15630, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PADDUSW[] = {
    {I_PADDUSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6938, IF_PENT|IF_MMX|IF_SQ},
    {I_PADDUSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15636, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PADDW[] = {
    {I_PADDW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6945, IF_PENT|IF_MMX|IF_SQ},
    {I_PADDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15594, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PALIGNR[] = {
    {I_PALIGNR, 3, {MMXREG,RM_MMX,IMMEDIATE,0,0}, nasm_bytecodes+5575, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PALIGNR, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5583, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PAND[] = {
    {I_PAND, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6952, IF_PENT|IF_MMX|IF_SQ},
    {I_PAND, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15642, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PANDN[] = {
    {I_PANDN, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6959, IF_PENT|IF_MMX|IF_SQ},
    {I_PANDN, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15648, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PAUSE[] = {
    {I_PAUSE, 0, {0,0,0,0,0}, nasm_bytecodes+18665, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_PAVEB[] = {
    {I_PAVEB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14778, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PAVGB[] = {
    {I_PAVGB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7414, IF_KATMAI|IF_MMX|IF_SQ},
    {I_PAVGB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15654, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PAVGUSB[] = {
    {I_PAVGUSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5103, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PAVGW[] = {
    {I_PAVGW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7421, IF_KATMAI|IF_MMX|IF_SQ},
    {I_PAVGW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15660, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PBLENDVB[] = {
    {I_PBLENDVB, 3, {XMMREG,RM_XMM,XMM0,0,0}, nasm_bytecodes+7946, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PBLENDW[] = {
    {I_PBLENDW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5655, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCLMULHQHQDQ[] = {
    {I_PCLMULHQHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5022, IF_SSE|IF_WESTMERE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCLMULHQLQDQ[] = {
    {I_PCLMULHQLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5004, IF_SSE|IF_WESTMERE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCLMULLQHQDQ[] = {
    {I_PCLMULLQHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5013, IF_SSE|IF_WESTMERE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCLMULLQLQDQ[] = {
    {I_PCLMULLQLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4995, IF_SSE|IF_WESTMERE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCLMULQDQ[] = {
    {I_PCLMULQDQ, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6711, IF_SSE|IF_WESTMERE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMOV[] = {
    {I_PCMOV, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8660, IF_SSE5|IF_AMD},
    {I_PCMOV, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8667, IF_SSE5|IF_AMD},
    {I_PCMOV, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8674, IF_SSE5|IF_AMD},
    {I_PCMOV, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8681, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPEQB[] = {
    {I_PCMPEQB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6966, IF_PENT|IF_MMX|IF_SQ},
    {I_PCMPEQB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15666, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPEQD[] = {
    {I_PCMPEQD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6973, IF_PENT|IF_MMX|IF_SQ},
    {I_PCMPEQD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15678, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPEQQ[] = {
    {I_PCMPEQQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7953, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPEQW[] = {
    {I_PCMPEQW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6980, IF_PENT|IF_MMX|IF_SQ},
    {I_PCMPEQW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15672, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPESTRI[] = {
    {I_PCMPESTRI, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5735, IF_SSE42},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPESTRM[] = {
    {I_PCMPESTRM, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5743, IF_SSE42},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPGTB[] = {
    {I_PCMPGTB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6987, IF_PENT|IF_MMX|IF_SQ},
    {I_PCMPGTB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15684, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPGTD[] = {
    {I_PCMPGTD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6994, IF_PENT|IF_MMX|IF_SQ},
    {I_PCMPGTD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15696, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPGTQ[] = {
    {I_PCMPGTQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8128, IF_SSE42},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPGTW[] = {
    {I_PCMPGTW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7001, IF_PENT|IF_MMX|IF_SQ},
    {I_PCMPGTW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15690, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPISTRI[] = {
    {I_PCMPISTRI, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5751, IF_SSE42},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCMPISTRM[] = {
    {I_PCMPISTRM, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5759, IF_SSE42},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMB[] = {
    {I_PCOMB, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5799, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMD[] = {
    {I_PCOMD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5815, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMEQB[] = {
    {I_PCOMEQB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+657, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMEQD[] = {
    {I_PCOMEQD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+801, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMEQQ[] = {
    {I_PCOMEQQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+873, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMEQUB[] = {
    {I_PCOMEQUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+945, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMEQUD[] = {
    {I_PCOMEQUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1089, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMEQUQ[] = {
    {I_PCOMEQUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1161, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMEQUW[] = {
    {I_PCOMEQUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1017, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMEQW[] = {
    {I_PCOMEQW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+729, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMFALSEB[] = {
    {I_PCOMFALSEB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+675, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMFALSED[] = {
    {I_PCOMFALSED, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+819, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMFALSEQ[] = {
    {I_PCOMFALSEQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+891, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMFALSEUB[] = {
    {I_PCOMFALSEUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+963, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMFALSEUD[] = {
    {I_PCOMFALSEUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1107, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMFALSEUQ[] = {
    {I_PCOMFALSEUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1179, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMFALSEUW[] = {
    {I_PCOMFALSEUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1035, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMFALSEW[] = {
    {I_PCOMFALSEW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+747, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGEB[] = {
    {I_PCOMGEB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+648, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGED[] = {
    {I_PCOMGED, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+792, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGEQ[] = {
    {I_PCOMGEQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+864, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGEUB[] = {
    {I_PCOMGEUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+936, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGEUD[] = {
    {I_PCOMGEUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1080, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGEUQ[] = {
    {I_PCOMGEUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1152, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGEUW[] = {
    {I_PCOMGEUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1008, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGEW[] = {
    {I_PCOMGEW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+720, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGTB[] = {
    {I_PCOMGTB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+639, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGTD[] = {
    {I_PCOMGTD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+783, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGTQ[] = {
    {I_PCOMGTQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+855, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGTUB[] = {
    {I_PCOMGTUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+927, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGTUD[] = {
    {I_PCOMGTUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1071, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGTUQ[] = {
    {I_PCOMGTUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1143, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGTUW[] = {
    {I_PCOMGTUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+999, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMGTW[] = {
    {I_PCOMGTW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+711, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLEB[] = {
    {I_PCOMLEB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+630, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLED[] = {
    {I_PCOMLED, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+774, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLEQ[] = {
    {I_PCOMLEQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+846, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLEUB[] = {
    {I_PCOMLEUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+918, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLEUD[] = {
    {I_PCOMLEUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1062, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLEUQ[] = {
    {I_PCOMLEUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1134, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLEUW[] = {
    {I_PCOMLEUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+990, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLEW[] = {
    {I_PCOMLEW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+702, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLTB[] = {
    {I_PCOMLTB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+621, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLTD[] = {
    {I_PCOMLTD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+765, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLTQ[] = {
    {I_PCOMLTQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+837, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLTUB[] = {
    {I_PCOMLTUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+909, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLTUD[] = {
    {I_PCOMLTUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1053, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLTUQ[] = {
    {I_PCOMLTUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1125, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLTUW[] = {
    {I_PCOMLTUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+981, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMLTW[] = {
    {I_PCOMLTW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+693, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMNEQB[] = {
    {I_PCOMNEQB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+666, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMNEQD[] = {
    {I_PCOMNEQD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+810, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMNEQQ[] = {
    {I_PCOMNEQQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+882, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMNEQUB[] = {
    {I_PCOMNEQUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+954, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMNEQUD[] = {
    {I_PCOMNEQUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1098, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMNEQUQ[] = {
    {I_PCOMNEQUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1170, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMNEQUW[] = {
    {I_PCOMNEQUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1026, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMNEQW[] = {
    {I_PCOMNEQW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+738, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMQ[] = {
    {I_PCOMQ, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5823, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMTRUEB[] = {
    {I_PCOMTRUEB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+684, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMTRUED[] = {
    {I_PCOMTRUED, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+828, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMTRUEQ[] = {
    {I_PCOMTRUEQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+900, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMTRUEUB[] = {
    {I_PCOMTRUEUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+972, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMTRUEUD[] = {
    {I_PCOMTRUEUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1116, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMTRUEUQ[] = {
    {I_PCOMTRUEUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1188, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMTRUEUW[] = {
    {I_PCOMTRUEUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1044, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMTRUEW[] = {
    {I_PCOMTRUEW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+756, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMUB[] = {
    {I_PCOMUB, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5831, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMUD[] = {
    {I_PCOMUD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5847, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMUQ[] = {
    {I_PCOMUQ, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5855, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMUW[] = {
    {I_PCOMUW, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5839, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PCOMW[] = {
    {I_PCOMW, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5807, IF_SSE5|IF_AMD|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PDISTIB[] = {
    {I_PDISTIB, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+15943, IF_PENT|IF_MMX|IF_SM|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PERMPD[] = {
    {I_PERMPD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8632, IF_SSE5|IF_AMD},
    {I_PERMPD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8639, IF_SSE5|IF_AMD},
    {I_PERMPD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8646, IF_SSE5|IF_AMD},
    {I_PERMPD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8653, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PERMPS[] = {
    {I_PERMPS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8604, IF_SSE5|IF_AMD},
    {I_PERMPS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8611, IF_SSE5|IF_AMD},
    {I_PERMPS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8618, IF_SSE5|IF_AMD},
    {I_PERMPS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8625, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PEXTRB[] = {
    {I_PEXTRB, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+10, IF_SSE41},
    {I_PEXTRB, 3, {MEMORY|BITS8,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+10, IF_SSE41},
    {I_PEXTRB, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+9, IF_SSE41|IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_PEXTRD[] = {
    {I_PEXTRD, 3, {RM_GPR|BITS32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+19, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PEXTRQ[] = {
    {I_PEXTRQ, 3, {RM_GPR|BITS64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+18, IF_SSE41|IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_PEXTRW[] = {
    {I_PEXTRW, 3, {REG32,MMXREG,IMMEDIATE,0,0}, nasm_bytecodes+7428, IF_KATMAI|IF_MMX|IF_SB|IF_AR2},
    {I_PEXTRW, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7505, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    {I_PEXTRW, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+28, IF_SSE41},
    {I_PEXTRW, 3, {MEMORY|BITS16,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+28, IF_SSE41},
    {I_PEXTRW, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+27, IF_SSE41|IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_PF2ID[] = {
    {I_PF2ID, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5111, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PF2IW[] = {
    {I_PF2IW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5391, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFACC[] = {
    {I_PFACC, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5119, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFADD[] = {
    {I_PFADD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5127, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFCMPEQ[] = {
    {I_PFCMPEQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5135, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFCMPGE[] = {
    {I_PFCMPGE, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5143, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFCMPGT[] = {
    {I_PFCMPGT, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5151, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFMAX[] = {
    {I_PFMAX, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5159, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFMIN[] = {
    {I_PFMIN, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5167, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFMUL[] = {
    {I_PFMUL, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5175, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFNACC[] = {
    {I_PFNACC, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5399, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFPNACC[] = {
    {I_PFPNACC, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5407, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFRCP[] = {
    {I_PFRCP, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5183, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFRCPIT1[] = {
    {I_PFRCPIT1, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5191, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFRCPIT2[] = {
    {I_PFRCPIT2, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5199, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFRCPV[] = {
    {I_PFRCPV, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5895, IF_PENT|IF_3DNOW|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFRSQIT1[] = {
    {I_PFRSQIT1, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5207, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFRSQRT[] = {
    {I_PFRSQRT, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5215, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFRSQRTV[] = {
    {I_PFRSQRTV, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5903, IF_PENT|IF_3DNOW|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFSUB[] = {
    {I_PFSUB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5223, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PFSUBR[] = {
    {I_PFSUBR, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5231, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDBD[] = {
    {I_PHADDBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9017, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDBQ[] = {
    {I_PHADDBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9024, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDBW[] = {
    {I_PHADDBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9010, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDD[] = {
    {I_PHADDD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7743, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PHADDD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7750, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDDQ[] = {
    {I_PHADDDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9045, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDSW[] = {
    {I_PHADDSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7757, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PHADDSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7764, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDUBD[] = {
    {I_PHADDUBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9059, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDUBQ[] = {
    {I_PHADDUBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9066, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDUBW[] = {
    {I_PHADDUBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9052, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDUDQ[] = {
    {I_PHADDUDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9087, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDUWD[] = {
    {I_PHADDUWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9073, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDUWQ[] = {
    {I_PHADDUWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9080, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDW[] = {
    {I_PHADDW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7729, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PHADDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7736, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDWD[] = {
    {I_PHADDWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9031, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHADDWQ[] = {
    {I_PHADDWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9038, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHMINPOSUW[] = {
    {I_PHMINPOSUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7960, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHSUBBW[] = {
    {I_PHSUBBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9094, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHSUBD[] = {
    {I_PHSUBD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7785, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PHSUBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7792, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHSUBDQ[] = {
    {I_PHSUBDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9108, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHSUBSW[] = {
    {I_PHSUBSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7799, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PHSUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7806, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHSUBW[] = {
    {I_PHSUBW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7771, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PHSUBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7778, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PHSUBWD[] = {
    {I_PHSUBWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9101, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PI2FD[] = {
    {I_PI2FD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5239, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PI2FW[] = {
    {I_PI2FW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5415, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PINSRB[] = {
    {I_PINSRB, 3, {XMMREG,REG32,IMMEDIATE,0,0}, nasm_bytecodes+5663, IF_SSE41},
    {I_PINSRB, 3, {XMMREG,MEMORY|BITS8,IMMEDIATE,0,0}, nasm_bytecodes+5663, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PINSRD[] = {
    {I_PINSRD, 3, {XMMREG,RM_GPR|BITS32,IMMEDIATE,0,0}, nasm_bytecodes+37, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PINSRQ[] = {
    {I_PINSRQ, 3, {XMMREG,RM_GPR|BITS64,IMMEDIATE,0,0}, nasm_bytecodes+36, IF_SSE41|IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_PINSRW[] = {
    {I_PINSRW, 3, {MMXREG,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7435, IF_KATMAI|IF_MMX|IF_SB|IF_AR2},
    {I_PINSRW, 3, {MMXREG,REG32,IMMEDIATE,0,0}, nasm_bytecodes+7435, IF_KATMAI|IF_MMX|IF_SB|IF_AR2},
    {I_PINSRW, 3, {MMXREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7435, IF_KATMAI|IF_MMX|IF_SB|IF_AR2},
    {I_PINSRW, 3, {MMXREG,MEMORY|BITS16,IMMEDIATE,0,0}, nasm_bytecodes+7435, IF_KATMAI|IF_MMX|IF_SB|IF_AR2},
    {I_PINSRW, 3, {XMMREG,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7512, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    {I_PINSRW, 3, {XMMREG,REG32,IMMEDIATE,0,0}, nasm_bytecodes+7512, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    {I_PINSRW, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7512, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    {I_PINSRW, 3, {XMMREG,MEMORY|BITS16,IMMEDIATE,0,0}, nasm_bytecodes+7512, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMACHRIW[] = {
    {I_PMACHRIW, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+16039, IF_PENT|IF_MMX|IF_SM|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMACSDD[] = {
    {I_PMACSDD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8751, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMACSDQH[] = {
    {I_PMACSDQH, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8779, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMACSDQL[] = {
    {I_PMACSDQL, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8765, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMACSSDD[] = {
    {I_PMACSSDD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8744, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMACSSDQH[] = {
    {I_PMACSSDQH, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8772, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMACSSDQL[] = {
    {I_PMACSSDQL, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8758, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMACSSWD[] = {
    {I_PMACSSWD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8730, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMACSSWW[] = {
    {I_PMACSSWW, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8716, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMACSWD[] = {
    {I_PMACSWD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8737, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMACSWW[] = {
    {I_PMACSWW, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8723, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMADCSSWD[] = {
    {I_PMADCSSWD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8786, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMADCSWD[] = {
    {I_PMADCSWD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8793, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMADDUBSW[] = {
    {I_PMADDUBSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7813, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PMADDUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7820, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMADDWD[] = {
    {I_PMADDWD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7008, IF_PENT|IF_MMX|IF_SQ},
    {I_PMADDWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15702, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMAGW[] = {
    {I_PMAGW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14784, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMAXSB[] = {
    {I_PMAXSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7967, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMAXSD[] = {
    {I_PMAXSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7974, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMAXSW[] = {
    {I_PMAXSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7442, IF_KATMAI|IF_MMX|IF_SQ},
    {I_PMAXSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15708, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMAXUB[] = {
    {I_PMAXUB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7449, IF_KATMAI|IF_MMX|IF_SQ},
    {I_PMAXUB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15714, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMAXUD[] = {
    {I_PMAXUD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7981, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMAXUW[] = {
    {I_PMAXUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7988, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMINSB[] = {
    {I_PMINSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7995, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMINSD[] = {
    {I_PMINSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8002, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMINSW[] = {
    {I_PMINSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7456, IF_KATMAI|IF_MMX|IF_SQ},
    {I_PMINSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15720, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMINUB[] = {
    {I_PMINUB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7463, IF_KATMAI|IF_MMX|IF_SQ},
    {I_PMINUB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15726, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMINUD[] = {
    {I_PMINUD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8009, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMINUW[] = {
    {I_PMINUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8016, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVMSKB[] = {
    {I_PMOVMSKB, 2, {REG32,MMXREG,0,0,0}, nasm_bytecodes+15486, IF_KATMAI|IF_MMX},
    {I_PMOVMSKB, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+15732, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVSXBD[] = {
    {I_PMOVSXBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8030, IF_SSE41|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVSXBQ[] = {
    {I_PMOVSXBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8037, IF_SSE41|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVSXBW[] = {
    {I_PMOVSXBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8023, IF_SSE41|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVSXDQ[] = {
    {I_PMOVSXDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8058, IF_SSE41|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVSXWD[] = {
    {I_PMOVSXWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8044, IF_SSE41|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVSXWQ[] = {
    {I_PMOVSXWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8051, IF_SSE41|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVZXBD[] = {
    {I_PMOVZXBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8072, IF_SSE41|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVZXBQ[] = {
    {I_PMOVZXBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8079, IF_SSE41|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVZXBW[] = {
    {I_PMOVZXBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8065, IF_SSE41|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVZXDQ[] = {
    {I_PMOVZXDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8100, IF_SSE41|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVZXWD[] = {
    {I_PMOVZXWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8086, IF_SSE41|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMOVZXWQ[] = {
    {I_PMOVZXWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8093, IF_SSE41|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMULDQ[] = {
    {I_PMULDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8107, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMULHRIW[] = {
    {I_PMULHRIW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14790, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMULHRSW[] = {
    {I_PMULHRSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7827, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PMULHRSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7834, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMULHRWA[] = {
    {I_PMULHRWA, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5247, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMULHRWC[] = {
    {I_PMULHRWC, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14796, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMULHUW[] = {
    {I_PMULHUW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7470, IF_KATMAI|IF_MMX|IF_SQ},
    {I_PMULHUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15738, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMULHW[] = {
    {I_PMULHW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7015, IF_PENT|IF_MMX|IF_SQ},
    {I_PMULHW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15744, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMULLD[] = {
    {I_PMULLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8114, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMULLW[] = {
    {I_PMULLW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7022, IF_PENT|IF_MMX|IF_SQ},
    {I_PMULLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15750, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMULUDQ[] = {
    {I_PMULUDQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7519, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_PMULUDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15756, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMVGEZB[] = {
    {I_PMVGEZB, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+16171, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMVLZB[] = {
    {I_PMVLZB, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+16027, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMVNZB[] = {
    {I_PMVNZB, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+16009, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PMVZB[] = {
    {I_PMVZB, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+15931, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_POP[] = {
    {I_POP, 1, {REG16,0,0,0,0}, nasm_bytecodes+20121, IF_8086},
    {I_POP, 1, {REG32,0,0,0,0}, nasm_bytecodes+20125, IF_386|IF_NOLONG},
    {I_POP, 1, {REG64,0,0,0,0}, nasm_bytecodes+20129, IF_X64},
    {I_POP, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18670, IF_8086},
    {I_POP, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18675, IF_386|IF_NOLONG},
    {I_POP, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18680, IF_X64},
    {I_POP, 1, {REG_CS,0,0,0,0}, nasm_bytecodes+4398, IF_8086|IF_UNDOC},
    {I_POP, 1, {REG_DESS,0,0,0,0}, nasm_bytecodes+19939, IF_8086|IF_NOLONG},
    {I_POP, 1, {REG_FSGS,0,0,0,0}, nasm_bytecodes+20133, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_POPA[] = {
    {I_POPA, 0, {0,0,0,0,0}, nasm_bytecodes+20137, IF_186|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_POPAD[] = {
    {I_POPAD, 0, {0,0,0,0,0}, nasm_bytecodes+20141, IF_386|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_POPAW[] = {
    {I_POPAW, 0, {0,0,0,0,0}, nasm_bytecodes+20145, IF_186|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_POPCNT[] = {
    {I_POPCNT, 2, {REG16,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+8135, IF_NEHALEM|IF_SW},
    {I_POPCNT, 2, {REG32,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+8142, IF_NEHALEM|IF_SD},
    {I_POPCNT, 2, {REG64,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+8149, IF_NEHALEM|IF_SQ|IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_POPF[] = {
    {I_POPF, 0, {0,0,0,0,0}, nasm_bytecodes+20149, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_POPFD[] = {
    {I_POPFD, 0, {0,0,0,0,0}, nasm_bytecodes+20153, IF_386|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_POPFQ[] = {
    {I_POPFQ, 0, {0,0,0,0,0}, nasm_bytecodes+20153, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_POPFW[] = {
    {I_POPFW, 0, {0,0,0,0,0}, nasm_bytecodes+20157, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_POR[] = {
    {I_POR, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7029, IF_PENT|IF_MMX|IF_SQ},
    {I_POR, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15762, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PPERM[] = {
    {I_PPERM, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8688, IF_SSE5|IF_AMD},
    {I_PPERM, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8695, IF_SSE5|IF_AMD},
    {I_PPERM, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8702, IF_SSE5|IF_AMD},
    {I_PPERM, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8709, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PREFETCH[] = {
    {I_PREFETCH, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18685, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PREFETCHNTA[] = {
    {I_PREFETCHNTA, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16369, IF_KATMAI},
    ITEMPLATE_END
};

static const struct itemplate instrux_PREFETCHT0[] = {
    {I_PREFETCHT0, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16387, IF_KATMAI},
    ITEMPLATE_END
};

static const struct itemplate instrux_PREFETCHT1[] = {
    {I_PREFETCHT1, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16405, IF_KATMAI},
    ITEMPLATE_END
};

static const struct itemplate instrux_PREFETCHT2[] = {
    {I_PREFETCHT2, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16423, IF_KATMAI},
    ITEMPLATE_END
};

static const struct itemplate instrux_PREFETCHW[] = {
    {I_PREFETCHW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18690, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PROTB[] = {
    {I_PROTB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8800, IF_SSE5|IF_AMD},
    {I_PROTB, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8807, IF_SSE5|IF_AMD},
    {I_PROTB, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5863, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PROTD[] = {
    {I_PROTD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8828, IF_SSE5|IF_AMD},
    {I_PROTD, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8835, IF_SSE5|IF_AMD},
    {I_PROTD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5879, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PROTQ[] = {
    {I_PROTQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8842, IF_SSE5|IF_AMD},
    {I_PROTQ, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8849, IF_SSE5|IF_AMD},
    {I_PROTQ, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5887, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PROTW[] = {
    {I_PROTW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8814, IF_SSE5|IF_AMD},
    {I_PROTW, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8821, IF_SSE5|IF_AMD},
    {I_PROTW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5871, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSADBW[] = {
    {I_PSADBW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7477, IF_KATMAI|IF_MMX|IF_SQ},
    {I_PSADBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15768, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHAB[] = {
    {I_PSHAB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8912, IF_SSE5|IF_AMD},
    {I_PSHAB, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8919, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHAD[] = {
    {I_PSHAD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8940, IF_SSE5|IF_AMD},
    {I_PSHAD, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8947, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHAQ[] = {
    {I_PSHAQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8954, IF_SSE5|IF_AMD},
    {I_PSHAQ, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8961, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHAW[] = {
    {I_PSHAW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8926, IF_SSE5|IF_AMD},
    {I_PSHAW, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8933, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHLB[] = {
    {I_PSHLB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8856, IF_SSE5|IF_AMD},
    {I_PSHLB, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8863, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHLD[] = {
    {I_PSHLD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8884, IF_SSE5|IF_AMD},
    {I_PSHLD, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8891, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHLQ[] = {
    {I_PSHLQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8898, IF_SSE5|IF_AMD},
    {I_PSHLQ, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8905, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHLW[] = {
    {I_PSHLW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8870, IF_SSE5|IF_AMD},
    {I_PSHLW, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8877, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHUFB[] = {
    {I_PSHUFB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7841, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PSHUFB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7848, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHUFD[] = {
    {I_PSHUFD, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7526, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    {I_PSHUFD, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7526, IF_WILLAMETTE|IF_SSE2|IF_SM2|IF_SB|IF_AR2},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHUFHW[] = {
    {I_PSHUFHW, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7533, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    {I_PSHUFHW, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7533, IF_WILLAMETTE|IF_SSE2|IF_SM2|IF_SB|IF_AR2},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHUFLW[] = {
    {I_PSHUFLW, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7540, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    {I_PSHUFLW, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7540, IF_WILLAMETTE|IF_SSE2|IF_SM2|IF_SB|IF_AR2},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSHUFW[] = {
    {I_PSHUFW, 3, {MMXREG,RM_MMX,IMMEDIATE,0,0}, nasm_bytecodes+5383, IF_KATMAI|IF_MMX|IF_SM2|IF_SB|IF_AR2},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSIGNB[] = {
    {I_PSIGNB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7855, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PSIGNB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7862, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSIGND[] = {
    {I_PSIGND, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7883, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PSIGND, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7890, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSIGNW[] = {
    {I_PSIGNW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7869, IF_SSSE3|IF_MMX|IF_SQ},
    {I_PSIGNW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7876, IF_SSSE3},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSLLD[] = {
    {I_PSLLD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7036, IF_PENT|IF_MMX|IF_SQ},
    {I_PSLLD, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7043, IF_PENT|IF_MMX},
    {I_PSLLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15780, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_PSLLD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7561, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSLLDQ[] = {
    {I_PSLLDQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7547, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSLLQ[] = {
    {I_PSLLQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7050, IF_PENT|IF_MMX|IF_SQ},
    {I_PSLLQ, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7057, IF_PENT|IF_MMX},
    {I_PSLLQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15786, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_PSLLQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7568, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSLLW[] = {
    {I_PSLLW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7064, IF_PENT|IF_MMX|IF_SQ},
    {I_PSLLW, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7071, IF_PENT|IF_MMX},
    {I_PSLLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15774, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_PSLLW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7554, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSRAD[] = {
    {I_PSRAD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7078, IF_PENT|IF_MMX|IF_SQ},
    {I_PSRAD, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7085, IF_PENT|IF_MMX},
    {I_PSRAD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15798, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_PSRAD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7582, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSRAW[] = {
    {I_PSRAW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7092, IF_PENT|IF_MMX|IF_SQ},
    {I_PSRAW, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7099, IF_PENT|IF_MMX},
    {I_PSRAW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15792, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_PSRAW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7575, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSRLD[] = {
    {I_PSRLD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7106, IF_PENT|IF_MMX|IF_SQ},
    {I_PSRLD, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7113, IF_PENT|IF_MMX},
    {I_PSRLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15810, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_PSRLD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7603, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSRLDQ[] = {
    {I_PSRLDQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7589, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSRLQ[] = {
    {I_PSRLQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7120, IF_PENT|IF_MMX|IF_SQ},
    {I_PSRLQ, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7127, IF_PENT|IF_MMX},
    {I_PSRLQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15816, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_PSRLQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7610, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSRLW[] = {
    {I_PSRLW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7134, IF_PENT|IF_MMX|IF_SQ},
    {I_PSRLW, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7141, IF_PENT|IF_MMX},
    {I_PSRLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15804, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_PSRLW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7596, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSUBB[] = {
    {I_PSUBB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7148, IF_PENT|IF_MMX|IF_SQ},
    {I_PSUBB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15822, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSUBD[] = {
    {I_PSUBD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7155, IF_PENT|IF_MMX|IF_SQ},
    {I_PSUBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15834, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSUBQ[] = {
    {I_PSUBQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7617, IF_WILLAMETTE|IF_SSE2|IF_SO},
    {I_PSUBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15840, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSUBSB[] = {
    {I_PSUBSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7162, IF_PENT|IF_MMX|IF_SQ},
    {I_PSUBSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15846, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSUBSIW[] = {
    {I_PSUBSIW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14802, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSUBSW[] = {
    {I_PSUBSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7169, IF_PENT|IF_MMX|IF_SQ},
    {I_PSUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15852, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSUBUSB[] = {
    {I_PSUBUSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7176, IF_PENT|IF_MMX|IF_SQ},
    {I_PSUBUSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15858, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSUBUSW[] = {
    {I_PSUBUSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7183, IF_PENT|IF_MMX|IF_SQ},
    {I_PSUBUSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15864, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSUBW[] = {
    {I_PSUBW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7190, IF_PENT|IF_MMX|IF_SQ},
    {I_PSUBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15828, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PSWAPD[] = {
    {I_PSWAPD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5423, IF_PENT|IF_3DNOW|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PTEST[] = {
    {I_PTEST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8121, IF_SSE41},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUNPCKHBW[] = {
    {I_PUNPCKHBW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7197, IF_PENT|IF_MMX|IF_SQ},
    {I_PUNPCKHBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15870, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUNPCKHDQ[] = {
    {I_PUNPCKHDQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7204, IF_PENT|IF_MMX|IF_SQ},
    {I_PUNPCKHDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15882, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUNPCKHQDQ[] = {
    {I_PUNPCKHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15888, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUNPCKHWD[] = {
    {I_PUNPCKHWD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7211, IF_PENT|IF_MMX|IF_SQ},
    {I_PUNPCKHWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15876, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUNPCKLBW[] = {
    {I_PUNPCKLBW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7218, IF_PENT|IF_MMX|IF_SQ},
    {I_PUNPCKLBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15894, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUNPCKLDQ[] = {
    {I_PUNPCKLDQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7225, IF_PENT|IF_MMX|IF_SQ},
    {I_PUNPCKLDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15906, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUNPCKLQDQ[] = {
    {I_PUNPCKLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15912, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUNPCKLWD[] = {
    {I_PUNPCKLWD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7232, IF_PENT|IF_MMX|IF_SQ},
    {I_PUNPCKLWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15900, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUSH[] = {
    {I_PUSH, 1, {REG16,0,0,0,0}, nasm_bytecodes+20161, IF_8086},
    {I_PUSH, 1, {REG32,0,0,0,0}, nasm_bytecodes+20165, IF_386|IF_NOLONG},
    {I_PUSH, 1, {REG64,0,0,0,0}, nasm_bytecodes+20169, IF_X64},
    {I_PUSH, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18695, IF_8086},
    {I_PUSH, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18700, IF_386|IF_NOLONG},
    {I_PUSH, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18705, IF_X64},
    {I_PUSH, 1, {REG_CS,0,0,0,0}, nasm_bytecodes+19915, IF_8086|IF_NOLONG},
    {I_PUSH, 1, {REG_DESS,0,0,0,0}, nasm_bytecodes+19915, IF_8086|IF_NOLONG},
    {I_PUSH, 1, {REG_FSGS,0,0,0,0}, nasm_bytecodes+20173, IF_386},
    {I_PUSH, 1, {IMMEDIATE|BITS8,0,0,0,0}, nasm_bytecodes+20177, IF_186},
    {I_PUSH, 1, {IMMEDIATE|BITS16,0,0,0,0}, nasm_bytecodes+18710, IF_186|IF_AR0|IF_SZ},
    {I_PUSH, 1, {IMMEDIATE|BITS32,0,0,0,0}, nasm_bytecodes+18715, IF_386|IF_NOLONG|IF_AR0|IF_SZ},
    {I_PUSH, 1, {IMMEDIATE|BITS32,0,0,0,0}, nasm_bytecodes+18715, IF_386|IF_NOLONG|IF_SD},
    {I_PUSH, 1, {IMMEDIATE|BITS64,0,0,0,0}, nasm_bytecodes+18720, IF_X64|IF_AR0|IF_SZ},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUSHA[] = {
    {I_PUSHA, 0, {0,0,0,0,0}, nasm_bytecodes+20181, IF_186|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUSHAD[] = {
    {I_PUSHAD, 0, {0,0,0,0,0}, nasm_bytecodes+20185, IF_386|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUSHAW[] = {
    {I_PUSHAW, 0, {0,0,0,0,0}, nasm_bytecodes+20189, IF_186|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUSHF[] = {
    {I_PUSHF, 0, {0,0,0,0,0}, nasm_bytecodes+20193, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUSHFD[] = {
    {I_PUSHFD, 0, {0,0,0,0,0}, nasm_bytecodes+20197, IF_386|IF_NOLONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUSHFQ[] = {
    {I_PUSHFQ, 0, {0,0,0,0,0}, nasm_bytecodes+20197, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_PUSHFW[] = {
    {I_PUSHFW, 0, {0,0,0,0,0}, nasm_bytecodes+20201, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_PXOR[] = {
    {I_PXOR, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7239, IF_PENT|IF_MMX|IF_SQ},
    {I_PXOR, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15918, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_RCL[] = {
    {I_RCL, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20205, IF_8086},
    {I_RCL, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20209, IF_8086},
    {I_RCL, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18725, IF_186|IF_SB},
    {I_RCL, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18730, IF_8086},
    {I_RCL, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18735, IF_8086},
    {I_RCL, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14808, IF_186|IF_SB},
    {I_RCL, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18740, IF_386},
    {I_RCL, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18745, IF_386},
    {I_RCL, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14814, IF_386|IF_SB},
    {I_RCL, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18750, IF_X64},
    {I_RCL, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18755, IF_X64},
    {I_RCL, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14820, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_RCPPS[] = {
    {I_RCPPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15378, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_RCPSS[] = {
    {I_RCPSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15384, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_RCR[] = {
    {I_RCR, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20213, IF_8086},
    {I_RCR, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20217, IF_8086},
    {I_RCR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18760, IF_186|IF_SB},
    {I_RCR, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18765, IF_8086},
    {I_RCR, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18770, IF_8086},
    {I_RCR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14826, IF_186|IF_SB},
    {I_RCR, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18775, IF_386},
    {I_RCR, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18780, IF_386},
    {I_RCR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14832, IF_386|IF_SB},
    {I_RCR, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18785, IF_X64},
    {I_RCR, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18790, IF_X64},
    {I_RCR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14838, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_RDM[] = {
    {I_RDM, 0, {0,0,0,0,0}, nasm_bytecodes+19409, IF_P6|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_RDMSR[] = {
    {I_RDMSR, 0, {0,0,0,0,0}, nasm_bytecodes+20221, IF_PENT|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_RDPMC[] = {
    {I_RDPMC, 0, {0,0,0,0,0}, nasm_bytecodes+20225, IF_P6},
    ITEMPLATE_END
};

static const struct itemplate instrux_RDSHR[] = {
    {I_RDSHR, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+14844, IF_P6|IF_CYRIX|IF_SMM},
    ITEMPLATE_END
};

static const struct itemplate instrux_RDTSC[] = {
    {I_RDTSC, 0, {0,0,0,0,0}, nasm_bytecodes+20229, IF_PENT},
    ITEMPLATE_END
};

static const struct itemplate instrux_RDTSCP[] = {
    {I_RDTSCP, 0, {0,0,0,0,0}, nasm_bytecodes+18795, IF_X86_64},
    ITEMPLATE_END
};

static const struct itemplate instrux_RESB[] = {
    {I_RESB, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+19523, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_RESD[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_RESO[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_RESQ[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_REST[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_RESW[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_RESY[] = {
    ITEMPLATE_END
};

static const struct itemplate instrux_RET[] = {
    {I_RET, 0, {0,0,0,0,0}, nasm_bytecodes+19342, IF_8086},
    {I_RET, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+20233, IF_8086|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_RETF[] = {
    {I_RETF, 0, {0,0,0,0,0}, nasm_bytecodes+20464, IF_8086},
    {I_RETF, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+20237, IF_8086|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_RETN[] = {
    {I_RETN, 0, {0,0,0,0,0}, nasm_bytecodes+19342, IF_8086},
    {I_RETN, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+20233, IF_8086|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_ROL[] = {
    {I_ROL, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20241, IF_8086},
    {I_ROL, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20245, IF_8086},
    {I_ROL, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18800, IF_186|IF_SB},
    {I_ROL, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18805, IF_8086},
    {I_ROL, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18810, IF_8086},
    {I_ROL, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14850, IF_186|IF_SB},
    {I_ROL, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18815, IF_386},
    {I_ROL, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18820, IF_386},
    {I_ROL, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14856, IF_386|IF_SB},
    {I_ROL, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18825, IF_X64},
    {I_ROL, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18830, IF_X64},
    {I_ROL, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14862, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_ROR[] = {
    {I_ROR, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20249, IF_8086},
    {I_ROR, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20253, IF_8086},
    {I_ROR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18835, IF_186|IF_SB},
    {I_ROR, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18840, IF_8086},
    {I_ROR, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18845, IF_8086},
    {I_ROR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14868, IF_186|IF_SB},
    {I_ROR, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18850, IF_386},
    {I_ROR, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18855, IF_386},
    {I_ROR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14874, IF_386|IF_SB},
    {I_ROR, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18860, IF_X64},
    {I_ROR, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18865, IF_X64},
    {I_ROR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14880, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_ROUNDPD[] = {
    {I_ROUNDPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5671, IF_SSE41},
    {I_ROUNDPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5679, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_ROUNDPS[] = {
    {I_ROUNDPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5679, IF_SSE41},
    {I_ROUNDPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5679, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_ROUNDSD[] = {
    {I_ROUNDSD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5687, IF_SSE41},
    {I_ROUNDSD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5679, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_ROUNDSS[] = {
    {I_ROUNDSS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5695, IF_SSE41},
    {I_ROUNDSS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5679, IF_SSE5|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_RSDC[] = {
    {I_RSDC, 2, {REG_SREG,MEMORY|BITS80,0,0,0}, nasm_bytecodes+16279, IF_486|IF_CYRIX|IF_SMM},
    ITEMPLATE_END
};

static const struct itemplate instrux_RSLDT[] = {
    {I_RSLDT, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+18870, IF_486|IF_CYRIX|IF_SMM},
    ITEMPLATE_END
};

static const struct itemplate instrux_RSM[] = {
    {I_RSM, 0, {0,0,0,0,0}, nasm_bytecodes+20257, IF_PENT|IF_SMM},
    ITEMPLATE_END
};

static const struct itemplate instrux_RSQRTPS[] = {
    {I_RSQRTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15390, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_RSQRTSS[] = {
    {I_RSQRTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15396, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_RSTS[] = {
    {I_RSTS, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+18875, IF_486|IF_CYRIX|IF_SMM},
    ITEMPLATE_END
};

static const struct itemplate instrux_SAHF[] = {
    {I_SAHF, 0, {0,0,0,0,0}, nasm_bytecodes+5132, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_SAL[] = {
    {I_SAL, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20261, IF_8086},
    {I_SAL, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20265, IF_8086},
    {I_SAL, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18880, IF_186|IF_SB},
    {I_SAL, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18885, IF_8086},
    {I_SAL, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18890, IF_8086},
    {I_SAL, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14886, IF_186|IF_SB},
    {I_SAL, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18895, IF_386},
    {I_SAL, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18900, IF_386},
    {I_SAL, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14892, IF_386|IF_SB},
    {I_SAL, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18905, IF_X64},
    {I_SAL, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18910, IF_X64},
    {I_SAL, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14898, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_SALC[] = {
    {I_SALC, 0, {0,0,0,0,0}, nasm_bytecodes+20467, IF_8086|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_SAR[] = {
    {I_SAR, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20269, IF_8086},
    {I_SAR, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20273, IF_8086},
    {I_SAR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18915, IF_186|IF_SB},
    {I_SAR, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18920, IF_8086},
    {I_SAR, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18925, IF_8086},
    {I_SAR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14904, IF_186|IF_SB},
    {I_SAR, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18930, IF_386},
    {I_SAR, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18935, IF_386},
    {I_SAR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14910, IF_386|IF_SB},
    {I_SAR, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18940, IF_X64},
    {I_SAR, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18945, IF_X64},
    {I_SAR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14916, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_SBB[] = {
    {I_SBB, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20277, IF_8086|IF_SM},
    {I_SBB, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20277, IF_8086},
    {I_SBB, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+18950, IF_8086|IF_SM},
    {I_SBB, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18950, IF_8086},
    {I_SBB, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+18955, IF_386|IF_SM},
    {I_SBB, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18955, IF_386},
    {I_SBB, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+18960, IF_X64|IF_SM},
    {I_SBB, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18960, IF_X64},
    {I_SBB, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+9559, IF_8086|IF_SM},
    {I_SBB, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+9559, IF_8086},
    {I_SBB, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18965, IF_8086|IF_SM},
    {I_SBB, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18965, IF_8086},
    {I_SBB, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18970, IF_386|IF_SM},
    {I_SBB, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18970, IF_386},
    {I_SBB, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+18975, IF_X64|IF_SM},
    {I_SBB, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18975, IF_X64},
    {I_SBB, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14922, IF_8086},
    {I_SBB, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14928, IF_386},
    {I_SBB, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14934, IF_X64},
    {I_SBB, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+20281, IF_8086|IF_SM},
    {I_SBB, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+14922, IF_8086|IF_SM},
    {I_SBB, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+18980, IF_8086|IF_SM},
    {I_SBB, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+14928, IF_386|IF_SM},
    {I_SBB, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+18985, IF_386|IF_SM},
    {I_SBB, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+14934, IF_X64|IF_SM},
    {I_SBB, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+18990, IF_X64|IF_SM},
    {I_SBB, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18995, IF_8086|IF_SM},
    {I_SBB, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14940, IF_8086|IF_SM},
    {I_SBB, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14946, IF_386|IF_SM},
    {I_SBB, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14952, IF_X64|IF_SM},
    {I_SBB, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+18995, IF_8086|IF_SM},
    {I_SBB, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14940, IF_8086|IF_SM},
    {I_SBB, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14946, IF_386|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_SCASB[] = {
    {I_SCASB, 0, {0,0,0,0,0}, nasm_bytecodes+20285, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_SCASD[] = {
    {I_SCASD, 0, {0,0,0,0,0}, nasm_bytecodes+19000, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_SCASQ[] = {
    {I_SCASQ, 0, {0,0,0,0,0}, nasm_bytecodes+19005, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_SCASW[] = {
    {I_SCASW, 0, {0,0,0,0,0}, nasm_bytecodes+19010, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_SFENCE[] = {
    {I_SFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+19015, IF_X64|IF_AMD},
    {I_SFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+19015, IF_KATMAI},
    ITEMPLATE_END
};

static const struct itemplate instrux_SGDT[] = {
    {I_SGDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19020, IF_286},
    ITEMPLATE_END
};

static const struct itemplate instrux_SHL[] = {
    {I_SHL, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20261, IF_8086},
    {I_SHL, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20265, IF_8086},
    {I_SHL, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18880, IF_186|IF_SB},
    {I_SHL, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18885, IF_8086},
    {I_SHL, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18890, IF_8086},
    {I_SHL, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14886, IF_186|IF_SB},
    {I_SHL, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18895, IF_386},
    {I_SHL, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18900, IF_386},
    {I_SHL, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14892, IF_386|IF_SB},
    {I_SHL, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18905, IF_X64},
    {I_SHL, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18910, IF_X64},
    {I_SHL, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14898, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_SHLD[] = {
    {I_SHLD, 3, {MEMORY,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7246, IF_386|IF_SM2|IF_SB|IF_AR2},
    {I_SHLD, 3, {REG16,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7246, IF_386|IF_SM2|IF_SB|IF_AR2},
    {I_SHLD, 3, {MEMORY,REG32,IMMEDIATE,0,0}, nasm_bytecodes+7253, IF_386|IF_SM2|IF_SB|IF_AR2},
    {I_SHLD, 3, {REG32,REG32,IMMEDIATE,0,0}, nasm_bytecodes+7253, IF_386|IF_SM2|IF_SB|IF_AR2},
    {I_SHLD, 3, {MEMORY,REG64,IMMEDIATE,0,0}, nasm_bytecodes+7260, IF_X64|IF_SM2|IF_SB|IF_AR2},
    {I_SHLD, 3, {REG64,REG64,IMMEDIATE,0,0}, nasm_bytecodes+7260, IF_X64|IF_SM2|IF_SB|IF_AR2},
    {I_SHLD, 3, {MEMORY,REG16,REG_CL,0,0}, nasm_bytecodes+14958, IF_386|IF_SM},
    {I_SHLD, 3, {REG16,REG16,REG_CL,0,0}, nasm_bytecodes+14958, IF_386},
    {I_SHLD, 3, {MEMORY,REG32,REG_CL,0,0}, nasm_bytecodes+14964, IF_386|IF_SM},
    {I_SHLD, 3, {REG32,REG32,REG_CL,0,0}, nasm_bytecodes+14964, IF_386},
    {I_SHLD, 3, {MEMORY,REG64,REG_CL,0,0}, nasm_bytecodes+14970, IF_X64|IF_SM},
    {I_SHLD, 3, {REG64,REG64,REG_CL,0,0}, nasm_bytecodes+14970, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_SHR[] = {
    {I_SHR, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20289, IF_8086},
    {I_SHR, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20293, IF_8086},
    {I_SHR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+19025, IF_186|IF_SB},
    {I_SHR, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+19030, IF_8086},
    {I_SHR, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+19035, IF_8086},
    {I_SHR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14976, IF_186|IF_SB},
    {I_SHR, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+19040, IF_386},
    {I_SHR, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+19045, IF_386},
    {I_SHR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14982, IF_386|IF_SB},
    {I_SHR, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+19050, IF_X64},
    {I_SHR, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+19055, IF_X64},
    {I_SHR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14988, IF_X64|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_SHRD[] = {
    {I_SHRD, 3, {MEMORY,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7267, IF_386|IF_SM2|IF_SB|IF_AR2},
    {I_SHRD, 3, {REG16,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7267, IF_386|IF_SM2|IF_SB|IF_AR2},
    {I_SHRD, 3, {MEMORY,REG32,IMMEDIATE,0,0}, nasm_bytecodes+7274, IF_386|IF_SM2|IF_SB|IF_AR2},
    {I_SHRD, 3, {REG32,REG32,IMMEDIATE,0,0}, nasm_bytecodes+7274, IF_386|IF_SM2|IF_SB|IF_AR2},
    {I_SHRD, 3, {MEMORY,REG64,IMMEDIATE,0,0}, nasm_bytecodes+7281, IF_X64|IF_SM2|IF_SB|IF_AR2},
    {I_SHRD, 3, {REG64,REG64,IMMEDIATE,0,0}, nasm_bytecodes+7281, IF_X64|IF_SM2|IF_SB|IF_AR2},
    {I_SHRD, 3, {MEMORY,REG16,REG_CL,0,0}, nasm_bytecodes+14994, IF_386|IF_SM},
    {I_SHRD, 3, {REG16,REG16,REG_CL,0,0}, nasm_bytecodes+14994, IF_386},
    {I_SHRD, 3, {MEMORY,REG32,REG_CL,0,0}, nasm_bytecodes+15000, IF_386|IF_SM},
    {I_SHRD, 3, {REG32,REG32,REG_CL,0,0}, nasm_bytecodes+15000, IF_386},
    {I_SHRD, 3, {MEMORY,REG64,REG_CL,0,0}, nasm_bytecodes+15006, IF_X64|IF_SM},
    {I_SHRD, 3, {REG64,REG64,REG_CL,0,0}, nasm_bytecodes+15006, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_SHUFPD[] = {
    {I_SHUFPD, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7666, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    {I_SHUFPD, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7666, IF_WILLAMETTE|IF_SSE2|IF_SM|IF_SB|IF_AR2},
    ITEMPLATE_END
};

static const struct itemplate instrux_SHUFPS[] = {
    {I_SHUFPS, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7407, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    {I_SHUFPS, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7407, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    ITEMPLATE_END
};

static const struct itemplate instrux_SIDT[] = {
    {I_SIDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19060, IF_286},
    ITEMPLATE_END
};

static const struct itemplate instrux_SKINIT[] = {
    {I_SKINIT, 0, {0,0,0,0,0}, nasm_bytecodes+19065, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_SLDT[] = {
    {I_SLDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15031, IF_286},
    {I_SLDT, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+15031, IF_286},
    {I_SLDT, 1, {REG16,0,0,0,0}, nasm_bytecodes+15012, IF_286},
    {I_SLDT, 1, {REG32,0,0,0,0}, nasm_bytecodes+15018, IF_386},
    {I_SLDT, 1, {REG64,0,0,0,0}, nasm_bytecodes+15024, IF_X64},
    {I_SLDT, 1, {REG64,0,0,0,0}, nasm_bytecodes+15030, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_SMI[] = {
    {I_SMI, 0, {0,0,0,0,0}, nasm_bytecodes+20437, IF_386|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_SMINT[] = {
    {I_SMINT, 0, {0,0,0,0,0}, nasm_bytecodes+20297, IF_P6|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_SMINTOLD[] = {
    {I_SMINTOLD, 0, {0,0,0,0,0}, nasm_bytecodes+20301, IF_486|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_SMSW[] = {
    {I_SMSW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15043, IF_286},
    {I_SMSW, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+15043, IF_286},
    {I_SMSW, 1, {REG16,0,0,0,0}, nasm_bytecodes+15036, IF_286},
    {I_SMSW, 1, {REG32,0,0,0,0}, nasm_bytecodes+15042, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_SQRTPD[] = {
    {I_SQRTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16152, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_SQRTPS[] = {
    {I_SQRTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15402, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_SQRTSD[] = {
    {I_SQRTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16158, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_SQRTSS[] = {
    {I_SQRTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15408, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_STC[] = {
    {I_STC, 0, {0,0,0,0,0}, nasm_bytecodes+18797, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_STD[] = {
    {I_STD, 0, {0,0,0,0,0}, nasm_bytecodes+20470, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_STGI[] = {
    {I_STGI, 0, {0,0,0,0,0}, nasm_bytecodes+19070, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_STI[] = {
    {I_STI, 0, {0,0,0,0,0}, nasm_bytecodes+20473, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_STMXCSR[] = {
    {I_STMXCSR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19295, IF_KATMAI|IF_SSE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_STOSB[] = {
    {I_STOSB, 0, {0,0,0,0,0}, nasm_bytecodes+5236, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_STOSD[] = {
    {I_STOSD, 0, {0,0,0,0,0}, nasm_bytecodes+20305, IF_386},
    ITEMPLATE_END
};

static const struct itemplate instrux_STOSQ[] = {
    {I_STOSQ, 0, {0,0,0,0,0}, nasm_bytecodes+20309, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_STOSW[] = {
    {I_STOSW, 0, {0,0,0,0,0}, nasm_bytecodes+20313, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_STR[] = {
    {I_STR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15061, IF_286|IF_PROT},
    {I_STR, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+15061, IF_286|IF_PROT},
    {I_STR, 1, {REG16,0,0,0,0}, nasm_bytecodes+15048, IF_286|IF_PROT},
    {I_STR, 1, {REG32,0,0,0,0}, nasm_bytecodes+15054, IF_386|IF_PROT},
    {I_STR, 1, {REG64,0,0,0,0}, nasm_bytecodes+15060, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_SUB[] = {
    {I_SUB, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20317, IF_8086|IF_SM},
    {I_SUB, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20317, IF_8086},
    {I_SUB, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+19075, IF_8086|IF_SM},
    {I_SUB, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19075, IF_8086},
    {I_SUB, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+19080, IF_386|IF_SM},
    {I_SUB, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19080, IF_386},
    {I_SUB, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+19085, IF_X64|IF_SM},
    {I_SUB, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19085, IF_X64},
    {I_SUB, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+10574, IF_8086|IF_SM},
    {I_SUB, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+10574, IF_8086},
    {I_SUB, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+19090, IF_8086|IF_SM},
    {I_SUB, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19090, IF_8086},
    {I_SUB, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+19095, IF_386|IF_SM},
    {I_SUB, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19095, IF_386},
    {I_SUB, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+19100, IF_X64|IF_SM},
    {I_SUB, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19100, IF_X64},
    {I_SUB, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15066, IF_8086},
    {I_SUB, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15072, IF_386},
    {I_SUB, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15078, IF_X64},
    {I_SUB, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+20321, IF_8086|IF_SM},
    {I_SUB, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+15066, IF_8086|IF_SM},
    {I_SUB, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+19105, IF_8086|IF_SM},
    {I_SUB, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+15072, IF_386|IF_SM},
    {I_SUB, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19110, IF_386|IF_SM},
    {I_SUB, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+15078, IF_X64|IF_SM},
    {I_SUB, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19115, IF_X64|IF_SM},
    {I_SUB, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+19120, IF_8086|IF_SM},
    {I_SUB, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+15084, IF_8086|IF_SM},
    {I_SUB, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+15090, IF_386|IF_SM},
    {I_SUB, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+15096, IF_X64|IF_SM},
    {I_SUB, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+19120, IF_8086|IF_SM},
    {I_SUB, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+15084, IF_8086|IF_SM},
    {I_SUB, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+15090, IF_386|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_SUBPD[] = {
    {I_SUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16164, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_SUBPS[] = {
    {I_SUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15414, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_SUBSD[] = {
    {I_SUBSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16170, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_SUBSS[] = {
    {I_SUBSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15420, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_SVDC[] = {
    {I_SVDC, 2, {MEMORY|BITS80,REG_SREG,0,0,0}, nasm_bytecodes+7675, IF_486|IF_CYRIX|IF_SMM},
    ITEMPLATE_END
};

static const struct itemplate instrux_SVLDT[] = {
    {I_SVLDT, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+19125, IF_486|IF_CYRIX|IF_SMM},
    ITEMPLATE_END
};

static const struct itemplate instrux_SVTS[] = {
    {I_SVTS, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+19130, IF_486|IF_CYRIX|IF_SMM},
    ITEMPLATE_END
};

static const struct itemplate instrux_SWAPGS[] = {
    {I_SWAPGS, 0, {0,0,0,0,0}, nasm_bytecodes+19135, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_SYSCALL[] = {
    {I_SYSCALL, 0, {0,0,0,0,0}, nasm_bytecodes+20025, IF_P6|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_SYSENTER[] = {
    {I_SYSENTER, 0, {0,0,0,0,0}, nasm_bytecodes+20325, IF_P6},
    ITEMPLATE_END
};

static const struct itemplate instrux_SYSEXIT[] = {
    {I_SYSEXIT, 0, {0,0,0,0,0}, nasm_bytecodes+20329, IF_P6|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_SYSRET[] = {
    {I_SYSRET, 0, {0,0,0,0,0}, nasm_bytecodes+20021, IF_P6|IF_PRIV|IF_AMD},
    ITEMPLATE_END
};

static const struct itemplate instrux_TEST[] = {
    {I_TEST, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20333, IF_8086|IF_SM},
    {I_TEST, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20333, IF_8086},
    {I_TEST, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+19140, IF_8086|IF_SM},
    {I_TEST, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19140, IF_8086},
    {I_TEST, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+19145, IF_386|IF_SM},
    {I_TEST, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19145, IF_386},
    {I_TEST, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+19150, IF_X64|IF_SM},
    {I_TEST, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19150, IF_X64},
    {I_TEST, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+20337, IF_8086|IF_SM},
    {I_TEST, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+19155, IF_8086|IF_SM},
    {I_TEST, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+19160, IF_386|IF_SM},
    {I_TEST, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+19165, IF_X64|IF_SM},
    {I_TEST, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+20341, IF_8086|IF_SM},
    {I_TEST, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+19170, IF_8086|IF_SM},
    {I_TEST, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19175, IF_386|IF_SM},
    {I_TEST, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19180, IF_X64|IF_SM},
    {I_TEST, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+19185, IF_8086|IF_SM},
    {I_TEST, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+15102, IF_8086|IF_SM},
    {I_TEST, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+15108, IF_386|IF_SM},
    {I_TEST, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+15114, IF_X64|IF_SM},
    {I_TEST, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+19185, IF_8086|IF_SM},
    {I_TEST, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+15102, IF_8086|IF_SM},
    {I_TEST, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+15108, IF_386|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_UCOMISD[] = {
    {I_UCOMISD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16176, IF_WILLAMETTE|IF_SSE2},
    ITEMPLATE_END
};

static const struct itemplate instrux_UCOMISS[] = {
    {I_UCOMISS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15426, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_UD0[] = {
    {I_UD0, 0, {0,0,0,0,0}, nasm_bytecodes+20345, IF_186|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_UD1[] = {
    {I_UD1, 0, {0,0,0,0,0}, nasm_bytecodes+20349, IF_186|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_UD2[] = {
    {I_UD2, 0, {0,0,0,0,0}, nasm_bytecodes+20353, IF_186},
    ITEMPLATE_END
};

static const struct itemplate instrux_UD2A[] = {
    {I_UD2A, 0, {0,0,0,0,0}, nasm_bytecodes+20353, IF_186},
    ITEMPLATE_END
};

static const struct itemplate instrux_UD2B[] = {
    {I_UD2B, 0, {0,0,0,0,0}, nasm_bytecodes+20349, IF_186|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_UMOV[] = {
    {I_UMOV, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+15120, IF_386|IF_UNDOC|IF_SM},
    {I_UMOV, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+15120, IF_386|IF_UNDOC},
    {I_UMOV, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+7288, IF_386|IF_UNDOC|IF_SM},
    {I_UMOV, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+7288, IF_386|IF_UNDOC},
    {I_UMOV, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+7295, IF_386|IF_UNDOC|IF_SM},
    {I_UMOV, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+7295, IF_386|IF_UNDOC},
    {I_UMOV, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+15126, IF_386|IF_UNDOC|IF_SM},
    {I_UMOV, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+15126, IF_386|IF_UNDOC},
    {I_UMOV, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+7302, IF_386|IF_UNDOC|IF_SM},
    {I_UMOV, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+7302, IF_386|IF_UNDOC},
    {I_UMOV, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+7309, IF_386|IF_UNDOC|IF_SM},
    {I_UMOV, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+7309, IF_386|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_UNPCKHPD[] = {
    {I_UNPCKHPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16182, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_UNPCKHPS[] = {
    {I_UNPCKHPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15432, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_UNPCKLPD[] = {
    {I_UNPCKLPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16188, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_UNPCKLPS[] = {
    {I_UNPCKLPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15438, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VADDPD[] = {
    {I_VADDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9255, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VADDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9262, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VADDPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9269, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VADDPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9276, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VADDPS[] = {
    {I_VADDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9283, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VADDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9290, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VADDPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9297, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VADDPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9304, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VADDSD[] = {
    {I_VADDSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9311, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VADDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9318, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VADDSS[] = {
    {I_VADDSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9325, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VADDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9332, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VADDSUBPD[] = {
    {I_VADDSUBPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9339, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VADDSUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9346, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VADDSUBPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9353, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VADDSUBPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9360, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VADDSUBPS[] = {
    {I_VADDSUBPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9367, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VADDSUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9374, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VADDSUBPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9381, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VADDSUBPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9388, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VAESDEC[] = {
    {I_VAESDEC, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9220, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VAESDEC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9227, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VAESDECLAST[] = {
    {I_VAESDECLAST, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9234, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VAESDECLAST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9241, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VAESENC[] = {
    {I_VAESENC, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9192, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VAESENC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9199, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VAESENCLAST[] = {
    {I_VAESENCLAST, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9206, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VAESENCLAST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9213, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VAESIMC[] = {
    {I_VAESIMC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9248, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VAESKEYGENASSIST[] = {
    {I_VAESKEYGENASSIST, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5919, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VANDNPD[] = {
    {I_VANDNPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9451, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VANDNPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9458, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VANDNPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9465, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VANDNPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9472, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VANDNPS[] = {
    {I_VANDNPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9479, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VANDNPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9486, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VANDNPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9493, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VANDNPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9500, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VANDPD[] = {
    {I_VANDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9395, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VANDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9402, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VANDPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9409, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VANDPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9416, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VANDPS[] = {
    {I_VANDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9423, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VANDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9430, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VANDPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9437, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VANDPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9444, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VBLENDPD[] = {
    {I_VBLENDPD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5927, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VBLENDPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5935, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VBLENDPD, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+5943, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VBLENDPD, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+5951, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VBLENDPS[] = {
    {I_VBLENDPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5959, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VBLENDPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5967, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VBLENDPS, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+5975, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VBLENDPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+5983, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VBLENDVPD[] = {
    {I_VBLENDVPD, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+1197, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VBLENDVPD, 3, {XMMREG,RM_XMM,XMM0,0,0}, nasm_bytecodes+9507, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VBLENDVPD, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+1206, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VBLENDVPD, 3, {YMMREG,RM_YMM,YMM0,0,0}, nasm_bytecodes+9514, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VBLENDVPD, 3, {YMMREG,RM_YMM,YMM0,0,0}, nasm_bytecodes+9528, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VBLENDVPS[] = {
    {I_VBLENDVPS, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+1215, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VBLENDVPS, 3, {XMMREG,RM_XMM,XMM0,0,0}, nasm_bytecodes+9521, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VBLENDVPS, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+1224, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VBROADCASTF128[] = {
    {I_VBROADCASTF128, 2, {YMMREG,MEMORY,0,0,0}, nasm_bytecodes+9556, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VBROADCASTSD[] = {
    {I_VBROADCASTSD, 2, {YMMREG,MEMORY,0,0,0}, nasm_bytecodes+9549, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VBROADCASTSS[] = {
    {I_VBROADCASTSS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+9535, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VBROADCASTSS, 2, {YMMREG,MEMORY,0,0,0}, nasm_bytecodes+9542, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQPD[] = {
    {I_VCMPEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1233, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1242, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1251, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPEQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1260, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQPS[] = {
    {I_VCMPEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2385, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2394, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2403, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPEQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2412, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQSD[] = {
    {I_VCMPEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3537, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPEQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3546, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQSS[] = {
    {I_VCMPEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4113, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPEQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4122, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_OSPD[] = {
    {I_VCMPEQ_OSPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1809, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_OSPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1818, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_OSPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1827, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPEQ_OSPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1836, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_OSPS[] = {
    {I_VCMPEQ_OSPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2961, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_OSPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2970, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_OSPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2979, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPEQ_OSPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2988, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_OSSD[] = {
    {I_VCMPEQ_OSSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3825, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPEQ_OSSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3834, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_OSSS[] = {
    {I_VCMPEQ_OSSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4401, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPEQ_OSSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4410, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_UQPD[] = {
    {I_VCMPEQ_UQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1521, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_UQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1530, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_UQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1539, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPEQ_UQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1548, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_UQPS[] = {
    {I_VCMPEQ_UQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2673, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_UQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2682, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_UQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2691, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPEQ_UQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2700, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_UQSD[] = {
    {I_VCMPEQ_UQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3681, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPEQ_UQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3690, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_UQSS[] = {
    {I_VCMPEQ_UQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4257, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPEQ_UQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4266, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_USPD[] = {
    {I_VCMPEQ_USPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2097, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_USPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2106, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_USPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2115, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPEQ_USPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2124, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_USPS[] = {
    {I_VCMPEQ_USPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3249, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_USPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3258, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPEQ_USPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3267, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPEQ_USPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3276, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_USSD[] = {
    {I_VCMPEQ_USSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3969, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPEQ_USSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3978, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPEQ_USSS[] = {
    {I_VCMPEQ_USSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4545, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPEQ_USSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4554, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPFALSEPD[] = {
    {I_VCMPFALSEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1629, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPFALSEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1638, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPFALSEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1647, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPFALSEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1656, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPFALSEPS[] = {
    {I_VCMPFALSEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2781, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPFALSEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2790, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPFALSEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2799, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPFALSEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2808, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPFALSESD[] = {
    {I_VCMPFALSESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3735, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPFALSESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3744, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPFALSESS[] = {
    {I_VCMPFALSESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4311, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPFALSESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4320, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPFALSE_OSPD[] = {
    {I_VCMPFALSE_OSPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2205, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPFALSE_OSPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2214, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPFALSE_OSPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2223, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPFALSE_OSPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2232, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPFALSE_OSPS[] = {
    {I_VCMPFALSE_OSPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3357, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPFALSE_OSPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3366, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPFALSE_OSPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3375, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPFALSE_OSPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3384, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPFALSE_OSSD[] = {
    {I_VCMPFALSE_OSSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4023, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPFALSE_OSSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4032, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPFALSE_OSSS[] = {
    {I_VCMPFALSE_OSSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4599, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPFALSE_OSSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4608, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGEPD[] = {
    {I_VCMPGEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1701, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1710, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1719, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPGEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1728, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGEPS[] = {
    {I_VCMPGEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2853, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2862, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2871, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPGEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2880, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGESD[] = {
    {I_VCMPGESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3771, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPGESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3780, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGESS[] = {
    {I_VCMPGESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4347, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPGESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4356, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGE_OQPD[] = {
    {I_VCMPGE_OQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2277, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGE_OQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2286, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGE_OQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2295, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPGE_OQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2304, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGE_OQPS[] = {
    {I_VCMPGE_OQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3429, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGE_OQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3438, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGE_OQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3447, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPGE_OQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3456, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGE_OQSD[] = {
    {I_VCMPGE_OQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4059, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPGE_OQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4068, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGE_OQSS[] = {
    {I_VCMPGE_OQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4635, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPGE_OQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4644, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGTPD[] = {
    {I_VCMPGTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1737, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1746, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGTPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1755, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPGTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1764, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGTPS[] = {
    {I_VCMPGTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2889, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2898, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGTPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2907, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPGTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2916, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGTSD[] = {
    {I_VCMPGTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3789, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPGTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3798, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGTSS[] = {
    {I_VCMPGTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4365, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPGTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4374, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGT_OQPD[] = {
    {I_VCMPGT_OQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2313, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGT_OQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2322, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGT_OQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2331, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPGT_OQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2340, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGT_OQPS[] = {
    {I_VCMPGT_OQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3465, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGT_OQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3474, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPGT_OQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3483, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPGT_OQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3492, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGT_OQSD[] = {
    {I_VCMPGT_OQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4077, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPGT_OQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4086, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPGT_OQSS[] = {
    {I_VCMPGT_OQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4653, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPGT_OQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4662, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLEPD[] = {
    {I_VCMPLEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1305, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1314, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1323, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPLEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1332, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLEPS[] = {
    {I_VCMPLEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2457, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2466, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2475, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPLEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2484, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLESD[] = {
    {I_VCMPLESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3573, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPLESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3582, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLESS[] = {
    {I_VCMPLESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4149, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPLESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4158, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLE_OQPD[] = {
    {I_VCMPLE_OQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1881, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLE_OQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1890, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLE_OQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1899, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPLE_OQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1908, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLE_OQPS[] = {
    {I_VCMPLE_OQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3033, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLE_OQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3042, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLE_OQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3051, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPLE_OQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3060, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLE_OQSD[] = {
    {I_VCMPLE_OQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3861, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPLE_OQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3870, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLE_OQSS[] = {
    {I_VCMPLE_OQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4437, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPLE_OQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4446, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLTPD[] = {
    {I_VCMPLTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1269, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1278, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLTPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1287, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPLTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1296, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLTPS[] = {
    {I_VCMPLTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2421, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2430, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLTPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2439, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPLTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2448, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLTSD[] = {
    {I_VCMPLTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3555, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPLTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3564, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLTSS[] = {
    {I_VCMPLTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4131, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPLTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4140, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLT_OQPD[] = {
    {I_VCMPLT_OQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1845, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLT_OQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1854, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLT_OQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1863, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPLT_OQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1872, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLT_OQPS[] = {
    {I_VCMPLT_OQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2997, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLT_OQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3006, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPLT_OQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3015, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPLT_OQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3024, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLT_OQSD[] = {
    {I_VCMPLT_OQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3843, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPLT_OQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3852, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPLT_OQSS[] = {
    {I_VCMPLT_OQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4419, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPLT_OQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4428, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQPD[] = {
    {I_VCMPNEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1377, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1386, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1395, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNEQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1404, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQPS[] = {
    {I_VCMPNEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2529, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2538, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2547, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNEQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2556, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQSD[] = {
    {I_VCMPNEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3609, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNEQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3618, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQSS[] = {
    {I_VCMPNEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4185, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNEQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4194, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_OQPD[] = {
    {I_VCMPNEQ_OQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1665, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_OQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1674, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_OQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1683, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNEQ_OQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1692, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_OQPS[] = {
    {I_VCMPNEQ_OQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2817, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_OQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2826, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_OQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2835, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNEQ_OQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2844, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_OQSD[] = {
    {I_VCMPNEQ_OQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3753, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNEQ_OQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3762, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_OQSS[] = {
    {I_VCMPNEQ_OQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4329, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNEQ_OQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4338, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_OSPD[] = {
    {I_VCMPNEQ_OSPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2241, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_OSPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2250, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_OSPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2259, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNEQ_OSPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2268, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_OSPS[] = {
    {I_VCMPNEQ_OSPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3393, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_OSPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3402, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_OSPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3411, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNEQ_OSPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3420, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_OSSD[] = {
    {I_VCMPNEQ_OSSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4041, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNEQ_OSSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4050, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_OSSS[] = {
    {I_VCMPNEQ_OSSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4617, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNEQ_OSSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4626, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_USPD[] = {
    {I_VCMPNEQ_USPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1953, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_USPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1962, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_USPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1971, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNEQ_USPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1980, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_USPS[] = {
    {I_VCMPNEQ_USPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3105, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_USPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3114, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNEQ_USPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3123, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNEQ_USPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3132, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_USSD[] = {
    {I_VCMPNEQ_USSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3897, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNEQ_USSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3906, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNEQ_USSS[] = {
    {I_VCMPNEQ_USSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4473, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNEQ_USSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4482, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGEPD[] = {
    {I_VCMPNGEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1557, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1566, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1575, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNGEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1584, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGEPS[] = {
    {I_VCMPNGEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2709, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2718, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2727, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNGEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2736, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGESD[] = {
    {I_VCMPNGESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3699, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNGESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3708, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGESS[] = {
    {I_VCMPNGESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4275, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNGESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4284, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGE_UQPD[] = {
    {I_VCMPNGE_UQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2133, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGE_UQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2142, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGE_UQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2151, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNGE_UQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2160, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGE_UQPS[] = {
    {I_VCMPNGE_UQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3285, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGE_UQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3294, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGE_UQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3303, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNGE_UQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3312, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGE_UQSD[] = {
    {I_VCMPNGE_UQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3987, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNGE_UQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3996, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGE_UQSS[] = {
    {I_VCMPNGE_UQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4563, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNGE_UQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4572, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGTPD[] = {
    {I_VCMPNGTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1593, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1602, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGTPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1611, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNGTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1620, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGTPS[] = {
    {I_VCMPNGTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2745, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2754, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGTPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2763, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNGTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2772, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGTSD[] = {
    {I_VCMPNGTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3717, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNGTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3726, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGTSS[] = {
    {I_VCMPNGTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4293, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNGTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4302, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGT_UQPD[] = {
    {I_VCMPNGT_UQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2169, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGT_UQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2178, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGT_UQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2187, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNGT_UQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2196, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGT_UQPS[] = {
    {I_VCMPNGT_UQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3321, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGT_UQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3330, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNGT_UQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3339, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNGT_UQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3348, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGT_UQSD[] = {
    {I_VCMPNGT_UQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4005, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNGT_UQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4014, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNGT_UQSS[] = {
    {I_VCMPNGT_UQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4581, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNGT_UQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4590, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLEPD[] = {
    {I_VCMPNLEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1449, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1458, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1467, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNLEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1476, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLEPS[] = {
    {I_VCMPNLEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2601, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2610, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2619, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNLEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2628, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLESD[] = {
    {I_VCMPNLESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3645, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNLESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3654, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLESS[] = {
    {I_VCMPNLESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4221, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNLESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4230, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLE_UQPD[] = {
    {I_VCMPNLE_UQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2025, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLE_UQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2034, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLE_UQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2043, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNLE_UQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2052, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLE_UQPS[] = {
    {I_VCMPNLE_UQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3177, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLE_UQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3186, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLE_UQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3195, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNLE_UQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3204, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLE_UQSD[] = {
    {I_VCMPNLE_UQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3933, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNLE_UQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3942, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLE_UQSS[] = {
    {I_VCMPNLE_UQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4509, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNLE_UQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4518, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLTPD[] = {
    {I_VCMPNLTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1413, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1422, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLTPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1431, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNLTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1440, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLTPS[] = {
    {I_VCMPNLTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2565, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2574, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLTPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2583, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNLTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2592, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLTSD[] = {
    {I_VCMPNLTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3627, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNLTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3636, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLTSS[] = {
    {I_VCMPNLTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4203, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNLTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4212, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLT_UQPD[] = {
    {I_VCMPNLT_UQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1989, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLT_UQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1998, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLT_UQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2007, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNLT_UQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2016, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLT_UQPS[] = {
    {I_VCMPNLT_UQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3141, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLT_UQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3150, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPNLT_UQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3159, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPNLT_UQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3168, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLT_UQSD[] = {
    {I_VCMPNLT_UQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3915, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPNLT_UQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3924, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPNLT_UQSS[] = {
    {I_VCMPNLT_UQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4491, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPNLT_UQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4500, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPORDPD[] = {
    {I_VCMPORDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1485, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPORDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1494, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPORDPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1503, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPORDPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1512, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPORDPS[] = {
    {I_VCMPORDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2637, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPORDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2646, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPORDPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2655, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPORDPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2664, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPORDSD[] = {
    {I_VCMPORDSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3663, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPORDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3672, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPORDSS[] = {
    {I_VCMPORDSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4239, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPORDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4248, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPORD_SPD[] = {
    {I_VCMPORD_SPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2061, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPORD_SPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2070, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPORD_SPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2079, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPORD_SPS[] = {
    {I_VCMPORD_SPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3213, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPORD_SPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3222, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPORD_SPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3231, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPORD_SSD[] = {
    {I_VCMPORD_SSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3951, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPORD_SSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3960, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPORD_SSS[] = {
    {I_VCMPORD_SSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4527, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPORD_SSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4536, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPORS_SPD[] = {
    {I_VCMPORS_SPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2088, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPORS_SPS[] = {
    {I_VCMPORS_SPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3240, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPPD[] = {
    {I_VCMPPD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5991, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5999, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPPD, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6007, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPPD, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6015, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPPS[] = {
    {I_VCMPPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6023, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6031, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPPS, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6039, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6047, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPSD[] = {
    {I_VCMPSD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6055, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPSD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6063, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPSS[] = {
    {I_VCMPSS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6071, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPSS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6079, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPTRUEPD[] = {
    {I_VCMPTRUEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1773, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPTRUEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1782, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPTRUEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1791, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPTRUEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1800, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPTRUEPS[] = {
    {I_VCMPTRUEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2925, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPTRUEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2934, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPTRUEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2943, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPTRUEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2952, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPTRUESD[] = {
    {I_VCMPTRUESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3807, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPTRUESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3816, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPTRUESS[] = {
    {I_VCMPTRUESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4383, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPTRUESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4392, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPTRUE_USPD[] = {
    {I_VCMPTRUE_USPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2349, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPTRUE_USPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2358, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPTRUE_USPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2367, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPTRUE_USPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2376, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPTRUE_USPS[] = {
    {I_VCMPTRUE_USPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3501, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPTRUE_USPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3510, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPTRUE_USPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3519, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPTRUE_USPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3528, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPTRUE_USSD[] = {
    {I_VCMPTRUE_USSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4095, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPTRUE_USSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4104, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPTRUE_USSS[] = {
    {I_VCMPTRUE_USSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4671, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPTRUE_USSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4680, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPUNORDPD[] = {
    {I_VCMPUNORDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1341, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPUNORDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1350, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPUNORDPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1359, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPUNORDPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1368, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPUNORDPS[] = {
    {I_VCMPUNORDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2493, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPUNORDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2502, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPUNORDPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2511, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPUNORDPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2520, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPUNORDSD[] = {
    {I_VCMPUNORDSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3591, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPUNORDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3600, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPUNORDSS[] = {
    {I_VCMPUNORDSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4167, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPUNORDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4176, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPUNORD_SPD[] = {
    {I_VCMPUNORD_SPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1917, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPUNORD_SPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1926, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPUNORD_SPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1935, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPUNORD_SPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1944, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPUNORD_SPS[] = {
    {I_VCMPUNORD_SPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3069, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPUNORD_SPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3078, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCMPUNORD_SPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3087, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VCMPUNORD_SPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3096, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPUNORD_SSD[] = {
    {I_VCMPUNORD_SSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3879, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCMPUNORD_SSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3888, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCMPUNORD_SSS[] = {
    {I_VCMPUNORD_SSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4455, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCMPUNORD_SSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4464, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCOMISD[] = {
    {I_VCOMISD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9563, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCOMISS[] = {
    {I_VCOMISS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9570, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTDQ2PD[] = {
    {I_VCVTDQ2PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9577, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCVTDQ2PD, 2, {YMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9584, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTDQ2PS[] = {
    {I_VCVTDQ2PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9591, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCVTDQ2PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9598, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTPD2DQ[] = {
    {I_VCVTPD2DQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+9605, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTPD2DQ, 2, {XMMREG,MEMORY|BITS128,0,0,0}, nasm_bytecodes+9605, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTPD2DQ, 2, {XMMREG,YMMREG,0,0,0}, nasm_bytecodes+9612, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTPD2DQ, 2, {XMMREG,MEMORY|BITS256,0,0,0}, nasm_bytecodes+9612, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTPD2PS[] = {
    {I_VCVTPD2PS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+9619, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTPD2PS, 2, {XMMREG,MEMORY|BITS128,0,0,0}, nasm_bytecodes+9619, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTPD2PS, 2, {XMMREG,YMMREG,0,0,0}, nasm_bytecodes+9626, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTPD2PS, 2, {XMMREG,MEMORY|BITS256,0,0,0}, nasm_bytecodes+9626, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTPS2DQ[] = {
    {I_VCVTPS2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9633, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCVTPS2DQ, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9640, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTPS2PD[] = {
    {I_VCVTPS2PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9647, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCVTPS2PD, 2, {YMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9654, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTSD2SI[] = {
    {I_VCVTSD2SI, 2, {REG32,RM_XMM,0,0,0}, nasm_bytecodes+9661, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCVTSD2SI, 2, {REG64,RM_XMM,0,0,0}, nasm_bytecodes+9668, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTSD2SS[] = {
    {I_VCVTSD2SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9675, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCVTSD2SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9682, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTSI2SD[] = {
    {I_VCVTSI2SD, 3, {XMMREG,XMMREG,RM_GPR|BITS32,0,0}, nasm_bytecodes+9689, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTSI2SD, 2, {XMMREG,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+9696, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTSI2SD, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+9689, IF_AVX|IF_SANDYBRIDGE|IF_SD|IF_AR2},
    {I_VCVTSI2SD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+9696, IF_AVX|IF_SANDYBRIDGE|IF_SD|IF_AR2},
    {I_VCVTSI2SD, 3, {XMMREG,XMMREG,RM_GPR|BITS64,0,0}, nasm_bytecodes+9703, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    {I_VCVTSI2SD, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+9710, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTSI2SS[] = {
    {I_VCVTSI2SS, 3, {XMMREG,XMMREG,RM_GPR|BITS32,0,0}, nasm_bytecodes+9717, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTSI2SS, 2, {XMMREG,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+9724, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTSI2SS, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+9717, IF_AVX|IF_SANDYBRIDGE|IF_SD|IF_AR2},
    {I_VCVTSI2SS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+9724, IF_AVX|IF_SANDYBRIDGE|IF_SD|IF_AR2},
    {I_VCVTSI2SS, 3, {XMMREG,XMMREG,RM_GPR|BITS64,0,0}, nasm_bytecodes+9731, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    {I_VCVTSI2SS, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+9738, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTSS2SD[] = {
    {I_VCVTSS2SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9745, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCVTSS2SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9752, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTSS2SI[] = {
    {I_VCVTSS2SI, 2, {REG32,RM_XMM,0,0,0}, nasm_bytecodes+9759, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCVTSS2SI, 2, {REG64,RM_XMM,0,0,0}, nasm_bytecodes+9766, IF_AVX|IF_SANDYBRIDGE|IF_SD|IF_LONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTTPD2DQ[] = {
    {I_VCVTTPD2DQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+9773, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTTPD2DQ, 2, {XMMREG,MEMORY|BITS128,0,0,0}, nasm_bytecodes+9773, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTTPD2DQ, 2, {XMMREG,YMMREG,0,0,0}, nasm_bytecodes+9780, IF_AVX|IF_SANDYBRIDGE},
    {I_VCVTTPD2DQ, 2, {XMMREG,MEMORY|BITS256,0,0,0}, nasm_bytecodes+9780, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTTPS2DQ[] = {
    {I_VCVTTPS2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9787, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VCVTTPS2DQ, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9794, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTTSD2SI[] = {
    {I_VCVTTSD2SI, 2, {REG32,RM_XMM,0,0,0}, nasm_bytecodes+9801, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VCVTTSD2SI, 2, {REG64,RM_XMM,0,0,0}, nasm_bytecodes+9808, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_VCVTTSS2SI[] = {
    {I_VCVTTSS2SI, 2, {REG32,RM_XMM,0,0,0}, nasm_bytecodes+9815, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VCVTTSS2SI, 2, {REG64,RM_XMM,0,0,0}, nasm_bytecodes+9822, IF_AVX|IF_SANDYBRIDGE|IF_SD|IF_LONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_VDIVPD[] = {
    {I_VDIVPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9829, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VDIVPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9836, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VDIVPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9843, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VDIVPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9850, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VDIVPS[] = {
    {I_VDIVPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9857, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VDIVPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9864, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VDIVPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9871, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VDIVPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9878, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VDIVSD[] = {
    {I_VDIVSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9885, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VDIVSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9892, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VDIVSS[] = {
    {I_VDIVSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9899, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VDIVSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9906, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VDPPD[] = {
    {I_VDPPD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6087, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VDPPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6095, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VDPPS[] = {
    {I_VDPPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6103, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VDPPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6111, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VDPPS, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6119, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VDPPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6127, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VERR[] = {
    {I_VERR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19190, IF_286|IF_PROT},
    {I_VERR, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19190, IF_286|IF_PROT},
    {I_VERR, 1, {REG16,0,0,0,0}, nasm_bytecodes+19190, IF_286|IF_PROT},
    ITEMPLATE_END
};

static const struct itemplate instrux_VERW[] = {
    {I_VERW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19195, IF_286|IF_PROT},
    {I_VERW, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19195, IF_286|IF_PROT},
    {I_VERW, 1, {REG16,0,0,0,0}, nasm_bytecodes+19195, IF_286|IF_PROT},
    ITEMPLATE_END
};

static const struct itemplate instrux_VEXTRACTF128[] = {
    {I_VEXTRACTF128, 3, {RM_XMM,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6135, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VEXTRACTPS[] = {
    {I_VEXTRACTPS, 3, {RM_GPR|BITS32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6143, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD123PD[] = {
    {I_VFMADD123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12720, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12727, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12734, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12741, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD123PS[] = {
    {I_VFMADD123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12692, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12699, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12706, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12713, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD123SD[] = {
    {I_VFMADD123SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13686, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMADD123SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13693, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD123SS[] = {
    {I_VFMADD123SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13672, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMADD123SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13679, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD132PD[] = {
    {I_VFMADD132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12664, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12671, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12678, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12685, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD132PS[] = {
    {I_VFMADD132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12636, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12643, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12650, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12657, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD132SD[] = {
    {I_VFMADD132SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13658, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMADD132SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13665, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD132SS[] = {
    {I_VFMADD132SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13644, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMADD132SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13651, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD213PD[] = {
    {I_VFMADD213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12720, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12727, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12734, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12741, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD213PS[] = {
    {I_VFMADD213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12692, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12699, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12706, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12713, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD213SD[] = {
    {I_VFMADD213SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13686, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMADD213SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13693, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD213SS[] = {
    {I_VFMADD213SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13672, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMADD213SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13679, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD231PD[] = {
    {I_VFMADD231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12776, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12783, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12790, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12797, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD231PS[] = {
    {I_VFMADD231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12748, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12755, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12762, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12769, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD231SD[] = {
    {I_VFMADD231SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13714, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMADD231SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13721, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD231SS[] = {
    {I_VFMADD231SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13700, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMADD231SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13707, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD312PD[] = {
    {I_VFMADD312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12664, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12671, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12678, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12685, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD312PS[] = {
    {I_VFMADD312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12636, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12643, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12650, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12657, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD312SD[] = {
    {I_VFMADD312SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13658, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMADD312SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13665, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD312SS[] = {
    {I_VFMADD312SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13644, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMADD312SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13651, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD321PD[] = {
    {I_VFMADD321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12776, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12783, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12790, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12797, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD321PS[] = {
    {I_VFMADD321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12748, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12755, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADD321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12762, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADD321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12769, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD321SD[] = {
    {I_VFMADD321SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13714, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMADD321SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13721, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADD321SS[] = {
    {I_VFMADD321SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13700, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMADD321SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13707, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB123PD[] = {
    {I_VFMADDSUB123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12888, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12895, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12902, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12909, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB123PS[] = {
    {I_VFMADDSUB123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12860, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12867, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12874, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12881, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB132PD[] = {
    {I_VFMADDSUB132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12832, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12839, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12846, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12853, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB132PS[] = {
    {I_VFMADDSUB132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12804, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12811, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12818, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12825, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB213PD[] = {
    {I_VFMADDSUB213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12888, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12895, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12902, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12909, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB213PS[] = {
    {I_VFMADDSUB213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12860, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12867, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12874, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12881, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB231PD[] = {
    {I_VFMADDSUB231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12944, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12951, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12958, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12965, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB231PS[] = {
    {I_VFMADDSUB231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12916, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12923, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12930, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12937, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB312PD[] = {
    {I_VFMADDSUB312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12832, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12839, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12846, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12853, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB312PS[] = {
    {I_VFMADDSUB312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12804, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12811, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12818, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12825, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB321PD[] = {
    {I_VFMADDSUB321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12944, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12951, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12958, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12965, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMADDSUB321PS[] = {
    {I_VFMADDSUB321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12916, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12923, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMADDSUB321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12930, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMADDSUB321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12937, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB123PD[] = {
    {I_VFMSUB123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13056, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13063, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13070, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13077, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB123PS[] = {
    {I_VFMSUB123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13028, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13035, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13042, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13049, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB123SD[] = {
    {I_VFMSUB123SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13770, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMSUB123SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13777, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB123SS[] = {
    {I_VFMSUB123SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13756, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMSUB123SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13763, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB132PD[] = {
    {I_VFMSUB132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13000, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13007, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13014, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13021, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB132PS[] = {
    {I_VFMSUB132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12972, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12979, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12986, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12993, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB132SD[] = {
    {I_VFMSUB132SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13742, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMSUB132SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13749, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB132SS[] = {
    {I_VFMSUB132SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13728, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMSUB132SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13735, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB213PD[] = {
    {I_VFMSUB213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13056, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13063, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13070, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13077, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB213PS[] = {
    {I_VFMSUB213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13028, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13035, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13042, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13049, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB213SD[] = {
    {I_VFMSUB213SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13770, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMSUB213SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13777, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB213SS[] = {
    {I_VFMSUB213SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13756, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMSUB213SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13763, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB231PD[] = {
    {I_VFMSUB231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13112, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13119, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13126, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13133, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB231PS[] = {
    {I_VFMSUB231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13084, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13091, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13098, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13105, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB231SD[] = {
    {I_VFMSUB231SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13798, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMSUB231SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13805, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB231SS[] = {
    {I_VFMSUB231SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13784, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMSUB231SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13791, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB312PD[] = {
    {I_VFMSUB312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13000, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13007, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13014, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13021, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB312PS[] = {
    {I_VFMSUB312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12972, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12979, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12986, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12993, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB312SD[] = {
    {I_VFMSUB312SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13742, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMSUB312SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13749, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB312SS[] = {
    {I_VFMSUB312SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13728, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMSUB312SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13735, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB321PD[] = {
    {I_VFMSUB321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13112, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13119, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13126, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13133, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB321PS[] = {
    {I_VFMSUB321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13084, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13091, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUB321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13098, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUB321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13105, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB321SD[] = {
    {I_VFMSUB321SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13798, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFMSUB321SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13805, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUB321SS[] = {
    {I_VFMSUB321SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13784, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFMSUB321SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13791, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD123PD[] = {
    {I_VFMSUBADD123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13224, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13231, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13238, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13245, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD123PS[] = {
    {I_VFMSUBADD123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13196, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13203, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13210, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13217, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD132PD[] = {
    {I_VFMSUBADD132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13168, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13175, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13182, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13189, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD132PS[] = {
    {I_VFMSUBADD132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13140, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13147, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13154, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13161, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD213PD[] = {
    {I_VFMSUBADD213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13224, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13231, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13238, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13245, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD213PS[] = {
    {I_VFMSUBADD213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13196, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13203, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13210, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13217, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD231PD[] = {
    {I_VFMSUBADD231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13280, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13287, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13294, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13301, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD231PS[] = {
    {I_VFMSUBADD231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13252, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13259, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13266, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13273, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD312PD[] = {
    {I_VFMSUBADD312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13168, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13175, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13182, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13189, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD312PS[] = {
    {I_VFMSUBADD312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13140, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13147, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13154, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13161, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD321PD[] = {
    {I_VFMSUBADD321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13280, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13287, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13294, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13301, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFMSUBADD321PS[] = {
    {I_VFMSUBADD321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13252, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13259, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFMSUBADD321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13266, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFMSUBADD321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13273, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD123PD[] = {
    {I_VFNMADD123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13392, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13399, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13406, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13413, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD123PS[] = {
    {I_VFNMADD123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13364, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13371, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13378, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13385, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD123SD[] = {
    {I_VFNMADD123SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13854, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMADD123SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13861, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD123SS[] = {
    {I_VFNMADD123SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13840, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMADD123SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13847, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD132PD[] = {
    {I_VFNMADD132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13336, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13343, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13350, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13357, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD132PS[] = {
    {I_VFNMADD132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13308, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13315, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13322, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13329, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD132SD[] = {
    {I_VFNMADD132SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13826, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMADD132SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13833, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD132SS[] = {
    {I_VFNMADD132SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13812, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMADD132SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13819, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD213PD[] = {
    {I_VFNMADD213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13392, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13399, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13406, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13413, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD213PS[] = {
    {I_VFNMADD213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13364, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13371, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13378, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13385, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD213SD[] = {
    {I_VFNMADD213SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13854, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMADD213SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13861, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD213SS[] = {
    {I_VFNMADD213SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13840, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMADD213SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13847, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD231PD[] = {
    {I_VFNMADD231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13448, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13455, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13462, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13469, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD231PS[] = {
    {I_VFNMADD231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13420, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13427, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13434, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13441, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD231SD[] = {
    {I_VFNMADD231SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13882, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMADD231SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13889, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD231SS[] = {
    {I_VFNMADD231SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13868, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMADD231SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13875, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD312PD[] = {
    {I_VFNMADD312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13336, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13343, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13350, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13357, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD312PS[] = {
    {I_VFNMADD312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13308, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13315, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13322, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13329, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD312SD[] = {
    {I_VFNMADD312SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13826, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMADD312SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13833, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD312SS[] = {
    {I_VFNMADD312SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13812, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMADD312SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13819, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD321PD[] = {
    {I_VFNMADD321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13448, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13455, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13462, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13469, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD321PS[] = {
    {I_VFNMADD321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13420, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13427, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMADD321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13434, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMADD321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13441, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD321SD[] = {
    {I_VFNMADD321SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13882, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMADD321SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13889, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMADD321SS[] = {
    {I_VFNMADD321SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13868, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMADD321SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13875, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB123PD[] = {
    {I_VFNMSUB123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13560, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13567, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13574, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13581, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB123PS[] = {
    {I_VFNMSUB123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13532, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13539, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13546, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13553, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB123SD[] = {
    {I_VFNMSUB123SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13938, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMSUB123SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13945, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB123SS[] = {
    {I_VFNMSUB123SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13924, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMSUB123SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13931, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB132PD[] = {
    {I_VFNMSUB132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13504, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13511, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13518, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13525, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB132PS[] = {
    {I_VFNMSUB132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13476, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13483, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13490, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13497, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB132SD[] = {
    {I_VFNMSUB132SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13910, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMSUB132SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13917, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB132SS[] = {
    {I_VFNMSUB132SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13896, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMSUB132SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13903, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB213PD[] = {
    {I_VFNMSUB213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13560, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13567, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13574, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13581, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB213PS[] = {
    {I_VFNMSUB213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13532, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13539, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13546, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13553, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB213SD[] = {
    {I_VFNMSUB213SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13938, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMSUB213SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13945, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB213SS[] = {
    {I_VFNMSUB213SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13924, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMSUB213SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13931, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB231PD[] = {
    {I_VFNMSUB231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13616, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13623, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13630, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13637, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB231PS[] = {
    {I_VFNMSUB231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13588, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13595, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13602, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13609, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB231SD[] = {
    {I_VFNMSUB231SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13966, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMSUB231SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13973, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB231SS[] = {
    {I_VFNMSUB231SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13952, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMSUB231SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13959, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB312PD[] = {
    {I_VFNMSUB312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13504, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13511, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13518, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13525, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB312PS[] = {
    {I_VFNMSUB312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13476, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13483, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13490, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13497, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB312SD[] = {
    {I_VFNMSUB312SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13910, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMSUB312SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13917, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB312SS[] = {
    {I_VFNMSUB312SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13896, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMSUB312SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13903, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB321PD[] = {
    {I_VFNMSUB321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13616, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13623, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13630, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13637, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB321PS[] = {
    {I_VFNMSUB321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13588, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13595, IF_FMA|IF_FUTURE|IF_SO},
    {I_VFNMSUB321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13602, IF_FMA|IF_FUTURE|IF_SY},
    {I_VFNMSUB321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13609, IF_FMA|IF_FUTURE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB321SD[] = {
    {I_VFNMSUB321SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13966, IF_FMA|IF_FUTURE|IF_SQ},
    {I_VFNMSUB321SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13973, IF_FMA|IF_FUTURE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VFNMSUB321SS[] = {
    {I_VFNMSUB321SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13952, IF_FMA|IF_FUTURE|IF_SD},
    {I_VFNMSUB321SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13959, IF_FMA|IF_FUTURE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VHADDPD[] = {
    {I_VHADDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9913, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VHADDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9920, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VHADDPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9927, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VHADDPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9934, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VHADDPS[] = {
    {I_VHADDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9941, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VHADDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9948, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VHADDPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9955, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VHADDPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9962, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VHSUBPD[] = {
    {I_VHSUBPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9969, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VHSUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9976, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VHSUBPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9983, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VHSUBPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9990, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VHSUBPS[] = {
    {I_VHSUBPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9997, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VHSUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10004, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VHSUBPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10011, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VHSUBPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10018, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VINSERTF128[] = {
    {I_VINSERTF128, 4, {YMMREG,YMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6151, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VINSERTPS[] = {
    {I_VINSERTPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6159, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VINSERTPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6167, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VLDDQU[] = {
    {I_VLDDQU, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10025, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VLDDQU, 2, {YMMREG,MEMORY,0,0,0}, nasm_bytecodes+10032, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VLDMXCSR[] = {
    {I_VLDMXCSR, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+10039, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VLDQQU[] = {
    {I_VLDQQU, 2, {YMMREG,MEMORY,0,0,0}, nasm_bytecodes+10032, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMASKMOVDQU[] = {
    {I_VMASKMOVDQU, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10046, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMASKMOVPD[] = {
    {I_VMASKMOVPD, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10081, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMASKMOVPD, 3, {YMMREG,YMMREG,MEMORY,0,0}, nasm_bytecodes+10088, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMASKMOVPD, 3, {MEMORY,XMMREG,XMMREG,0,0}, nasm_bytecodes+10095, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMASKMOVPD, 3, {MEMORY,YMMREG,YMMREG,0,0}, nasm_bytecodes+10102, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMASKMOVPS[] = {
    {I_VMASKMOVPS, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10053, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMASKMOVPS, 3, {YMMREG,YMMREG,MEMORY,0,0}, nasm_bytecodes+10060, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMASKMOVPS, 3, {MEMORY,XMMREG,XMMREG,0,0}, nasm_bytecodes+10067, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMASKMOVPS, 3, {MEMORY,XMMREG,XMMREG,0,0}, nasm_bytecodes+10074, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMAXPD[] = {
    {I_VMAXPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10109, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMAXPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10116, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMAXPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10123, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMAXPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10130, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMAXPS[] = {
    {I_VMAXPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10137, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMAXPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10144, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMAXPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10151, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMAXPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10158, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMAXSD[] = {
    {I_VMAXSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10165, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMAXSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10172, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMAXSS[] = {
    {I_VMAXSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10179, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VMAXSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10186, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMCALL[] = {
    {I_VMCALL, 0, {0,0,0,0,0}, nasm_bytecodes+19315, IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMCLEAR[] = {
    {I_VMCLEAR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16260, IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMINPD[] = {
    {I_VMINPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10193, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMINPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10200, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMINPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10207, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMINPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10214, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMINPS[] = {
    {I_VMINPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10221, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMINPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10228, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMINPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10235, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMINPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10242, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMINSD[] = {
    {I_VMINSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10249, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMINSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10256, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMINSS[] = {
    {I_VMINSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10263, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VMINSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10270, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMLAUNCH[] = {
    {I_VMLAUNCH, 0, {0,0,0,0,0}, nasm_bytecodes+19320, IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMLOAD[] = {
    {I_VMLOAD, 0, {0,0,0,0,0}, nasm_bytecodes+19325, IF_X64|IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMMCALL[] = {
    {I_VMMCALL, 0, {0,0,0,0,0}, nasm_bytecodes+19330, IF_X64|IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVAPD[] = {
    {I_VMOVAPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10277, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVAPD, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10284, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVAPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10291, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMOVAPD, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10298, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVAPS[] = {
    {I_VMOVAPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10305, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVAPS, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10312, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVAPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10319, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMOVAPS, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10326, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVD[] = {
    {I_VMOVD, 2, {XMMREG,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+10347, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VMOVD, 2, {RM_GPR|BITS32,XMMREG,0,0,0}, nasm_bytecodes+10361, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVDDUP[] = {
    {I_VMOVDDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10375, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVDDUP, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10382, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVDQA[] = {
    {I_VMOVDQA, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10389, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVDQA, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10396, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVDQA, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10403, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMOVDQA, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10410, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVDQU[] = {
    {I_VMOVDQU, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10417, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVDQU, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10424, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVDQU, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10431, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMOVDQU, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10438, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVHLPS[] = {
    {I_VMOVHLPS, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10445, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVHLPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10452, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVHPD[] = {
    {I_VMOVHPD, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10459, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVHPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10466, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVHPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10473, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVHPS[] = {
    {I_VMOVHPS, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10480, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVHPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10487, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVHPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10494, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVLHPS[] = {
    {I_VMOVLHPS, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10480, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVLHPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10487, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVLPD[] = {
    {I_VMOVLPD, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10501, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVLPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10508, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVLPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10515, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVLPS[] = {
    {I_VMOVLPS, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10445, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVLPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10452, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVLPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10522, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVMSKPD[] = {
    {I_VMOVMSKPD, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+10529, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    {I_VMOVMSKPD, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+10529, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVMSKPD, 2, {REG64,YMMREG,0,0,0}, nasm_bytecodes+10536, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    {I_VMOVMSKPD, 2, {REG32,YMMREG,0,0,0}, nasm_bytecodes+10536, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVMSKPS[] = {
    {I_VMOVMSKPS, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+10543, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    {I_VMOVMSKPS, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+10543, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVMSKPS, 2, {REG64,YMMREG,0,0,0}, nasm_bytecodes+10550, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    {I_VMOVMSKPS, 2, {REG32,YMMREG,0,0,0}, nasm_bytecodes+10550, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVNTDQ[] = {
    {I_VMOVNTDQ, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10557, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVNTDQ, 2, {MEMORY,YMMREG,0,0,0}, nasm_bytecodes+10564, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVNTDQA[] = {
    {I_VMOVNTDQA, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10571, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVNTPD[] = {
    {I_VMOVNTPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10578, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVNTPD, 2, {MEMORY,YMMREG,0,0,0}, nasm_bytecodes+10585, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVNTPS[] = {
    {I_VMOVNTPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10592, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVNTPS, 2, {MEMORY,YMMREG,0,0,0}, nasm_bytecodes+10599, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVNTQQ[] = {
    {I_VMOVNTQQ, 2, {MEMORY,YMMREG,0,0,0}, nasm_bytecodes+10564, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVQ[] = {
    {I_VMOVQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10333, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVQ, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10340, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVQ, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+10354, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    {I_VMOVQ, 2, {RM_GPR|BITS64,XMMREG,0,0,0}, nasm_bytecodes+10368, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVQQA[] = {
    {I_VMOVQQA, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10403, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMOVQQA, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10410, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVQQU[] = {
    {I_VMOVQQU, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10431, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMOVQQU, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10438, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVSD[] = {
    {I_VMOVSD, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10606, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVSD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10613, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVSD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10620, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVSD, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10627, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVSD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10634, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVSD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10641, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVSHDUP[] = {
    {I_VMOVSHDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10648, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVSHDUP, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10655, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVSLDUP[] = {
    {I_VMOVSLDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10662, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVSLDUP, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10669, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVSS[] = {
    {I_VMOVSS, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10676, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVSS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10683, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVSS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10690, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMOVSS, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10697, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVSS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10704, IF_AVX|IF_SANDYBRIDGE},
    {I_VMOVSS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10711, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVUPD[] = {
    {I_VMOVUPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10718, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVUPD, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10725, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVUPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10732, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMOVUPD, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10739, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMOVUPS[] = {
    {I_VMOVUPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10746, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVUPS, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10753, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMOVUPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10760, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMOVUPS, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10767, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMPSADBW[] = {
    {I_VMPSADBW, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6175, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMPSADBW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6183, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMPTRLD[] = {
    {I_VMPTRLD, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16267, IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMPTRST[] = {
    {I_VMPTRST, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19335, IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMREAD[] = {
    {I_VMREAD, 2, {RM_GPR|BITS32,REG32,0,0,0}, nasm_bytecodes+7674, IF_VMX|IF_NOLONG|IF_SD},
    {I_VMREAD, 2, {RM_GPR|BITS64,REG64,0,0,0}, nasm_bytecodes+7673, IF_X64|IF_VMX|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMRESUME[] = {
    {I_VMRESUME, 0, {0,0,0,0,0}, nasm_bytecodes+19340, IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMRUN[] = {
    {I_VMRUN, 0, {0,0,0,0,0}, nasm_bytecodes+19345, IF_X64|IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMSAVE[] = {
    {I_VMSAVE, 0, {0,0,0,0,0}, nasm_bytecodes+19350, IF_X64|IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMULPD[] = {
    {I_VMULPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10774, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMULPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10781, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMULPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10788, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMULPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10795, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMULPS[] = {
    {I_VMULPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10802, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMULPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10809, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VMULPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10816, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VMULPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10823, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMULSD[] = {
    {I_VMULSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10830, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VMULSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10837, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMULSS[] = {
    {I_VMULSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10844, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VMULSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10851, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMWRITE[] = {
    {I_VMWRITE, 2, {REG32,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+7681, IF_VMX|IF_NOLONG|IF_SD},
    {I_VMWRITE, 2, {REG64,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+7680, IF_X64|IF_VMX|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMXOFF[] = {
    {I_VMXOFF, 0, {0,0,0,0,0}, nasm_bytecodes+19355, IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VMXON[] = {
    {I_VMXON, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16266, IF_VMX},
    ITEMPLATE_END
};

static const struct itemplate instrux_VORPD[] = {
    {I_VORPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10858, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VORPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10865, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VORPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10872, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VORPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10879, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VORPS[] = {
    {I_VORPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10886, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VORPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10893, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VORPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10900, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VORPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10907, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPABSB[] = {
    {I_VPABSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10914, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPABSD[] = {
    {I_VPABSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10928, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPABSW[] = {
    {I_VPABSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10921, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPACKSSDW[] = {
    {I_VPACKSSDW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10949, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPACKSSDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10956, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPACKSSWB[] = {
    {I_VPACKSSWB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10935, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPACKSSWB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10942, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPACKUSDW[] = {
    {I_VPACKUSDW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10977, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPACKUSDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10984, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPACKUSWB[] = {
    {I_VPACKUSWB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10963, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPACKUSWB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10970, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPADDB[] = {
    {I_VPADDB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10991, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPADDB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10998, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPADDD[] = {
    {I_VPADDD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11019, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPADDD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11026, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPADDQ[] = {
    {I_VPADDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11033, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPADDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11040, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPADDSB[] = {
    {I_VPADDSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11047, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPADDSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11054, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPADDSW[] = {
    {I_VPADDSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11061, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPADDSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11068, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPADDUSB[] = {
    {I_VPADDUSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11075, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPADDUSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11082, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPADDUSW[] = {
    {I_VPADDUSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11089, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPADDUSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11096, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPADDW[] = {
    {I_VPADDW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11005, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPADDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11012, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPALIGNR[] = {
    {I_VPALIGNR, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6191, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPALIGNR, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6199, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPAND[] = {
    {I_VPAND, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11103, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPAND, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11110, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPANDN[] = {
    {I_VPANDN, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11117, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPANDN, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11124, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPAVGB[] = {
    {I_VPAVGB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11131, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPAVGB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11138, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPAVGW[] = {
    {I_VPAVGW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11145, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPAVGW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11152, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPBLENDVB[] = {
    {I_VPBLENDVB, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4689, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPBLENDVB, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+4698, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPBLENDW[] = {
    {I_VPBLENDW, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6207, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPBLENDW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6215, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCLMULHQHQDQ[] = {
    {I_VPCLMULHQHQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+5085, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCLMULHQHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5094, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCLMULHQLQDQ[] = {
    {I_VPCLMULHQLQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+5049, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCLMULHQLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5058, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCLMULLQHQDQ[] = {
    {I_VPCLMULLQHQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+5067, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCLMULLQHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5076, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCLMULLQLQDQ[] = {
    {I_VPCLMULLQLQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+5031, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCLMULLQLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5040, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCLMULQDQ[] = {
    {I_VPCLMULQDQ, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6719, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCLMULQDQ, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6727, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPEQB[] = {
    {I_VPCMPEQB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11159, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCMPEQB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11166, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPEQD[] = {
    {I_VPCMPEQD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11187, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCMPEQD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11194, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPEQQ[] = {
    {I_VPCMPEQQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11201, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCMPEQQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11208, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPEQW[] = {
    {I_VPCMPEQW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11173, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCMPEQW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11180, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPESTRI[] = {
    {I_VPCMPESTRI, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6223, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPESTRM[] = {
    {I_VPCMPESTRM, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6231, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPGTB[] = {
    {I_VPCMPGTB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11215, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCMPGTB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11222, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPGTD[] = {
    {I_VPCMPGTD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11243, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCMPGTD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11250, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPGTQ[] = {
    {I_VPCMPGTQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11257, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCMPGTQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11264, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPGTW[] = {
    {I_VPCMPGTW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11229, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPCMPGTW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11236, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPISTRI[] = {
    {I_VPCMPISTRI, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6239, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPCMPISTRM[] = {
    {I_VPCMPISTRM, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6247, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPERM2F128[] = {
    {I_VPERM2F128, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6287, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPERMIL2PD[] = {
    {I_VPERMIL2PD, 5, {XMMREG,XMMREG,RM_XMM,XMMREG,IMMEDIATE}, nasm_bytecodes+4815, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMIL2PD, 5, {XMMREG,XMMREG,XMMREG,RM_XMM,IMMEDIATE}, nasm_bytecodes+4824, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMIL2PD, 5, {YMMREG,YMMREG,RM_YMM,YMMREG,IMMEDIATE}, nasm_bytecodes+4833, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VPERMIL2PD, 5, {YMMREG,YMMREG,YMMREG,RM_YMM,IMMEDIATE}, nasm_bytecodes+4842, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPERMIL2PS[] = {
    {I_VPERMIL2PS, 5, {XMMREG,XMMREG,RM_XMM,XMMREG,IMMEDIATE}, nasm_bytecodes+4959, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMIL2PS, 5, {XMMREG,XMMREG,XMMREG,RM_XMM,IMMEDIATE}, nasm_bytecodes+4968, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMIL2PS, 5, {YMMREG,YMMREG,RM_YMM,YMMREG,IMMEDIATE}, nasm_bytecodes+4977, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VPERMIL2PS, 5, {YMMREG,YMMREG,YMMREG,RM_YMM,IMMEDIATE}, nasm_bytecodes+4986, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPERMILMO2PD[] = {
    {I_VPERMILMO2PD, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4743, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILMO2PD, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4752, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILMO2PD, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4761, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VPERMILMO2PD, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4770, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPERMILMO2PS[] = {
    {I_VPERMILMO2PS, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4887, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILMO2PS, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4896, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILMO2PS, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4905, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VPERMILMO2PS, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4914, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPERMILMZ2PD[] = {
    {I_VPERMILMZ2PD, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4779, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILMZ2PD, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4788, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILMZ2PD, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4797, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VPERMILMZ2PD, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4806, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPERMILMZ2PS[] = {
    {I_VPERMILMZ2PS, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4923, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILMZ2PS, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4932, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILMZ2PS, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4941, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VPERMILMZ2PS, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4950, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPERMILPD[] = {
    {I_VPERMILPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11271, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+11278, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VPERMILPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6255, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILPD, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6263, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPERMILPS[] = {
    {I_VPERMILPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11285, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+11292, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VPERMILPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6271, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6279, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPERMILTD2PD[] = {
    {I_VPERMILTD2PD, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4707, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILTD2PD, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4716, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILTD2PD, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4725, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VPERMILTD2PD, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4734, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPERMILTD2PS[] = {
    {I_VPERMILTD2PS, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4851, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILTD2PS, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4860, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPERMILTD2PS, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4869, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VPERMILTD2PS, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4878, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPEXTRB[] = {
    {I_VPEXTRB, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6295, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    {I_VPEXTRB, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6295, IF_AVX|IF_SANDYBRIDGE},
    {I_VPEXTRB, 3, {MEMORY,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6295, IF_AVX|IF_SANDYBRIDGE|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPEXTRD[] = {
    {I_VPEXTRD, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6319, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    {I_VPEXTRD, 3, {RM_GPR|BITS32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6319, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPEXTRQ[] = {
    {I_VPEXTRQ, 3, {RM_GPR|BITS64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6327, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPEXTRW[] = {
    {I_VPEXTRW, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6303, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    {I_VPEXTRW, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6303, IF_AVX|IF_SANDYBRIDGE},
    {I_VPEXTRW, 3, {MEMORY,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6303, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    {I_VPEXTRW, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6311, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    {I_VPEXTRW, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6311, IF_AVX|IF_SANDYBRIDGE},
    {I_VPEXTRW, 3, {MEMORY,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6311, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPHADDD[] = {
    {I_VPHADDD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11313, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPHADDD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11320, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPHADDSW[] = {
    {I_VPHADDSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11327, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPHADDSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11334, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPHADDW[] = {
    {I_VPHADDW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11299, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPHADDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11306, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPHMINPOSUW[] = {
    {I_VPHMINPOSUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11341, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPHSUBD[] = {
    {I_VPHSUBD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11362, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPHSUBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11369, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPHSUBSW[] = {
    {I_VPHSUBSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11376, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPHSUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11383, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPHSUBW[] = {
    {I_VPHSUBW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11348, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPHSUBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11355, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPINSRB[] = {
    {I_VPINSRB, 4, {XMMREG,XMMREG,REG32,IMMEDIATE,0}, nasm_bytecodes+6335, IF_AVX|IF_SANDYBRIDGE},
    {I_VPINSRB, 3, {XMMREG,REG32,IMMEDIATE,0,0}, nasm_bytecodes+6343, IF_AVX|IF_SANDYBRIDGE},
    {I_VPINSRB, 4, {XMMREG,XMMREG,MEMORY,IMMEDIATE,0}, nasm_bytecodes+6335, IF_AVX|IF_SANDYBRIDGE|IF_SB},
    {I_VPINSRB, 4, {XMMREG,REG32,MEMORY,IMMEDIATE,0}, nasm_bytecodes+6343, IF_AVX|IF_SANDYBRIDGE|IF_SB},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPINSRD[] = {
    {I_VPINSRD, 4, {XMMREG,XMMREG,RM_GPR|BITS32,IMMEDIATE,0}, nasm_bytecodes+6367, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VPINSRD, 3, {XMMREG,RM_GPR|BITS32,IMMEDIATE,0,0}, nasm_bytecodes+6375, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPINSRQ[] = {
    {I_VPINSRQ, 4, {XMMREG,XMMREG,RM_GPR|BITS64,IMMEDIATE,0}, nasm_bytecodes+6383, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    {I_VPINSRQ, 3, {XMMREG,RM_GPR|BITS64,IMMEDIATE,0,0}, nasm_bytecodes+6391, IF_AVX|IF_SANDYBRIDGE|IF_SD|IF_LONG},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPINSRW[] = {
    {I_VPINSRW, 4, {XMMREG,XMMREG,REG32,IMMEDIATE,0}, nasm_bytecodes+6351, IF_AVX|IF_SANDYBRIDGE},
    {I_VPINSRW, 3, {XMMREG,REG32,IMMEDIATE,0,0}, nasm_bytecodes+6359, IF_AVX|IF_SANDYBRIDGE},
    {I_VPINSRW, 4, {XMMREG,XMMREG,MEMORY,IMMEDIATE,0}, nasm_bytecodes+6351, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    {I_VPINSRW, 4, {XMMREG,REG32,MEMORY,IMMEDIATE,0}, nasm_bytecodes+6359, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMADDUBSW[] = {
    {I_VPMADDUBSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11404, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMADDUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11411, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMADDWD[] = {
    {I_VPMADDWD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11390, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMADDWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11397, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMAXSB[] = {
    {I_VPMAXSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11418, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMAXSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11425, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMAXSD[] = {
    {I_VPMAXSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11446, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMAXSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11453, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMAXSW[] = {
    {I_VPMAXSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11432, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMAXSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11439, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMAXUB[] = {
    {I_VPMAXUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11460, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMAXUB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11467, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMAXUD[] = {
    {I_VPMAXUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11488, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMAXUD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11495, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMAXUW[] = {
    {I_VPMAXUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11474, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMAXUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11481, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMINSB[] = {
    {I_VPMINSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11502, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMINSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11509, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMINSD[] = {
    {I_VPMINSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11530, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMINSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11537, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMINSW[] = {
    {I_VPMINSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11516, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMINSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11523, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMINUB[] = {
    {I_VPMINUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11544, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMINUB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11551, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMINUD[] = {
    {I_VPMINUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11572, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMINUD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11579, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMINUW[] = {
    {I_VPMINUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11558, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMINUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11565, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVMSKB[] = {
    {I_VPMOVMSKB, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+11586, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    {I_VPMOVMSKB, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+11586, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVSXBD[] = {
    {I_VPMOVSXBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11600, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVSXBQ[] = {
    {I_VPMOVSXBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11607, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVSXBW[] = {
    {I_VPMOVSXBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11593, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVSXDQ[] = {
    {I_VPMOVSXDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11628, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVSXWD[] = {
    {I_VPMOVSXWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11614, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVSXWQ[] = {
    {I_VPMOVSXWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11621, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVZXBD[] = {
    {I_VPMOVZXBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11642, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVZXBQ[] = {
    {I_VPMOVZXBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11649, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVZXBW[] = {
    {I_VPMOVZXBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11635, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVZXDQ[] = {
    {I_VPMOVZXDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11670, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVZXWD[] = {
    {I_VPMOVZXWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11656, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMOVZXWQ[] = {
    {I_VPMOVZXWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11663, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMULDQ[] = {
    {I_VPMULDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11761, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMULDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11768, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMULHRSW[] = {
    {I_VPMULHRSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11691, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMULHRSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11698, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMULHUW[] = {
    {I_VPMULHUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11677, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMULHUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11684, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMULHW[] = {
    {I_VPMULHW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11705, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMULHW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11712, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMULLD[] = {
    {I_VPMULLD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11733, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMULLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11740, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMULLW[] = {
    {I_VPMULLW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11719, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMULLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11726, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPMULUDQ[] = {
    {I_VPMULUDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11747, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPMULUDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11754, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPOR[] = {
    {I_VPOR, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11775, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPOR, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11782, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSADBW[] = {
    {I_VPSADBW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11789, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSADBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11796, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSHUFB[] = {
    {I_VPSHUFB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11803, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSHUFB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11810, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSHUFD[] = {
    {I_VPSHUFD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6399, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSHUFHW[] = {
    {I_VPSHUFHW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6407, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSHUFLW[] = {
    {I_VPSHUFLW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6415, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSIGNB[] = {
    {I_VPSIGNB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11817, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSIGNB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11824, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSIGND[] = {
    {I_VPSIGND, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11845, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSIGND, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11852, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSIGNW[] = {
    {I_VPSIGNW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11831, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSIGNW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11838, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSLLD[] = {
    {I_VPSLLD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11873, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSLLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11880, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSLLD, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6471, IF_AVX|IF_SANDYBRIDGE},
    {I_VPSLLD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6479, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSLLDQ[] = {
    {I_VPSLLDQ, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6423, IF_AVX|IF_SANDYBRIDGE},
    {I_VPSLLDQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6431, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSLLQ[] = {
    {I_VPSLLQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11887, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSLLQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11894, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSLLQ, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6487, IF_AVX|IF_SANDYBRIDGE},
    {I_VPSLLQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6495, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSLLW[] = {
    {I_VPSLLW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11859, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSLLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11866, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSLLW, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6455, IF_AVX|IF_SANDYBRIDGE},
    {I_VPSLLW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6463, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSRAD[] = {
    {I_VPSRAD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11915, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSRAD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11922, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSRAD, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6519, IF_AVX|IF_SANDYBRIDGE},
    {I_VPSRAD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6527, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSRAW[] = {
    {I_VPSRAW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11901, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSRAW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11908, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSRAW, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6503, IF_AVX|IF_SANDYBRIDGE},
    {I_VPSRAW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6511, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSRLD[] = {
    {I_VPSRLD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11943, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSRLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11950, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSRLD, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6551, IF_AVX|IF_SANDYBRIDGE},
    {I_VPSRLD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6559, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSRLDQ[] = {
    {I_VPSRLDQ, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6439, IF_AVX|IF_SANDYBRIDGE},
    {I_VPSRLDQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6447, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSRLQ[] = {
    {I_VPSRLQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11957, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSRLQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11964, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSRLQ, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6567, IF_AVX|IF_SANDYBRIDGE},
    {I_VPSRLQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6575, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSRLW[] = {
    {I_VPSRLW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11929, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSRLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11936, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSRLW, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6535, IF_AVX|IF_SANDYBRIDGE},
    {I_VPSRLW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6543, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSUBB[] = {
    {I_VPSUBB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11985, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSUBB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11992, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSUBD[] = {
    {I_VPSUBD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12013, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSUBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12020, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSUBQ[] = {
    {I_VPSUBQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12027, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSUBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12034, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSUBSB[] = {
    {I_VPSUBSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12041, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSUBSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12048, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSUBSW[] = {
    {I_VPSUBSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12055, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12062, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSUBUSB[] = {
    {I_VPSUBUSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12069, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSUBUSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12076, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSUBUSW[] = {
    {I_VPSUBUSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12083, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSUBUSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12090, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPSUBW[] = {
    {I_VPSUBW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11999, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPSUBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12006, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPTEST[] = {
    {I_VPTEST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11971, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPTEST, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+11978, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPUNPCKHBW[] = {
    {I_VPUNPCKHBW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12097, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPUNPCKHBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12104, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPUNPCKHDQ[] = {
    {I_VPUNPCKHDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12125, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPUNPCKHDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12132, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPUNPCKHQDQ[] = {
    {I_VPUNPCKHQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12139, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPUNPCKHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12146, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPUNPCKHWD[] = {
    {I_VPUNPCKHWD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12111, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPUNPCKHWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12118, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPUNPCKLBW[] = {
    {I_VPUNPCKLBW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12153, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPUNPCKLBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12160, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPUNPCKLDQ[] = {
    {I_VPUNPCKLDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12181, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPUNPCKLDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12188, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPUNPCKLQDQ[] = {
    {I_VPUNPCKLQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12195, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPUNPCKLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12202, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPUNPCKLWD[] = {
    {I_VPUNPCKLWD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12167, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPUNPCKLWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12174, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VPXOR[] = {
    {I_VPXOR, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12209, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VPXOR, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12216, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_VRCPPS[] = {
    {I_VRCPPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12223, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VRCPPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12230, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VRCPSS[] = {
    {I_VRCPSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12237, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VRCPSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12244, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VROUNDPD[] = {
    {I_VROUNDPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6583, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VROUNDPD, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6591, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VROUNDPS[] = {
    {I_VROUNDPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6599, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VROUNDPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6607, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VROUNDSD[] = {
    {I_VROUNDSD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6615, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VROUNDSD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6623, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VROUNDSS[] = {
    {I_VROUNDSS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6631, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VROUNDSS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6639, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VRSQRTPS[] = {
    {I_VRSQRTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12251, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VRSQRTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12258, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VRSQRTSS[] = {
    {I_VRSQRTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12265, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VRSQRTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12272, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VSHUFPD[] = {
    {I_VSHUFPD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6647, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VSHUFPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6655, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VSHUFPD, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6663, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VSHUFPD, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6671, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VSHUFPS[] = {
    {I_VSHUFPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6679, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VSHUFPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6687, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VSHUFPS, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6695, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VSHUFPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6703, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VSQRTPD[] = {
    {I_VSQRTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12279, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VSQRTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12286, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VSQRTPS[] = {
    {I_VSQRTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12293, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VSQRTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12300, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VSQRTSD[] = {
    {I_VSQRTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12307, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VSQRTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12314, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VSQRTSS[] = {
    {I_VSQRTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12321, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VSQRTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12328, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VSTMXCSR[] = {
    {I_VSTMXCSR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+12335, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VSUBPD[] = {
    {I_VSUBPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12342, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VSUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12349, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VSUBPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12356, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VSUBPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12363, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VSUBPS[] = {
    {I_VSUBPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12370, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VSUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12377, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VSUBPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12384, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VSUBPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12391, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VSUBSD[] = {
    {I_VSUBSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12398, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    {I_VSUBSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12405, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VSUBSS[] = {
    {I_VSUBSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12412, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    {I_VSUBSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12419, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VTESTPD[] = {
    {I_VTESTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12440, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VTESTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12447, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VTESTPS[] = {
    {I_VTESTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12426, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VTESTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12433, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VUCOMISD[] = {
    {I_VUCOMISD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12454, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    ITEMPLATE_END
};

static const struct itemplate instrux_VUCOMISS[] = {
    {I_VUCOMISS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12461, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    ITEMPLATE_END
};

static const struct itemplate instrux_VUNPCKHPD[] = {
    {I_VUNPCKHPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12468, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VUNPCKHPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12475, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VUNPCKHPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12482, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VUNPCKHPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12489, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VUNPCKHPS[] = {
    {I_VUNPCKHPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12496, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VUNPCKHPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12503, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VUNPCKHPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12510, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VUNPCKHPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12517, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VUNPCKLPD[] = {
    {I_VUNPCKLPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12524, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VUNPCKLPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12531, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VUNPCKLPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12538, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VUNPCKLPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12545, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VUNPCKLPS[] = {
    {I_VUNPCKLPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12552, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VUNPCKLPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12559, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VUNPCKLPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12566, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VUNPCKLPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12573, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VXORPD[] = {
    {I_VXORPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12580, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VXORPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12587, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VXORPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12594, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VXORPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12601, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VXORPS[] = {
    {I_VXORPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12608, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VXORPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12615, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    {I_VXORPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12622, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    {I_VXORPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12629, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    ITEMPLATE_END
};

static const struct itemplate instrux_VZEROALL[] = {
    {I_VZEROALL, 0, {0,0,0,0,0}, nasm_bytecodes+16296, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_VZEROUPPER[] = {
    {I_VZEROUPPER, 0, {0,0,0,0,0}, nasm_bytecodes+16302, IF_AVX|IF_SANDYBRIDGE},
    ITEMPLATE_END
};

static const struct itemplate instrux_WBINVD[] = {
    {I_WBINVD, 0, {0,0,0,0,0}, nasm_bytecodes+20357, IF_486|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_WRMSR[] = {
    {I_WRMSR, 0, {0,0,0,0,0}, nasm_bytecodes+20361, IF_PENT|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_WRSHR[] = {
    {I_WRSHR, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+15132, IF_P6|IF_CYRIX|IF_SMM},
    ITEMPLATE_END
};

static const struct itemplate instrux_XADD[] = {
    {I_XADD, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+19200, IF_486|IF_SM},
    {I_XADD, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+19200, IF_486},
    {I_XADD, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+15138, IF_486|IF_SM},
    {I_XADD, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+15138, IF_486},
    {I_XADD, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+15144, IF_486|IF_SM},
    {I_XADD, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+15144, IF_486},
    {I_XADD, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+15150, IF_X64|IF_SM},
    {I_XADD, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+15150, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_XBTS[] = {
    {I_XBTS, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+15156, IF_386|IF_SW|IF_UNDOC},
    {I_XBTS, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+15156, IF_386|IF_UNDOC},
    {I_XBTS, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+15162, IF_386|IF_SD|IF_UNDOC},
    {I_XBTS, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+15162, IF_386|IF_UNDOC},
    ITEMPLATE_END
};

static const struct itemplate instrux_XCHG[] = {
    {I_XCHG, 2, {REG_AX,REG16,0,0,0}, nasm_bytecodes+20365, IF_8086},
    {I_XCHG, 2, {REG_EAX,REG32NA,0,0,0}, nasm_bytecodes+20369, IF_386},
    {I_XCHG, 2, {REG_RAX,REG64,0,0,0}, nasm_bytecodes+20373, IF_X64},
    {I_XCHG, 2, {REG16,REG_AX,0,0,0}, nasm_bytecodes+20377, IF_8086},
    {I_XCHG, 2, {REG32NA,REG_EAX,0,0,0}, nasm_bytecodes+20381, IF_386},
    {I_XCHG, 2, {REG64,REG_RAX,0,0,0}, nasm_bytecodes+20385, IF_X64},
    {I_XCHG, 2, {REG_EAX,REG_EAX,0,0,0}, nasm_bytecodes+20389, IF_386|IF_NOLONG},
    {I_XCHG, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+20393, IF_8086|IF_SM},
    {I_XCHG, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20393, IF_8086},
    {I_XCHG, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+19205, IF_8086|IF_SM},
    {I_XCHG, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19205, IF_8086},
    {I_XCHG, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+19210, IF_386|IF_SM},
    {I_XCHG, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19210, IF_386},
    {I_XCHG, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+19215, IF_X64|IF_SM},
    {I_XCHG, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19215, IF_X64},
    {I_XCHG, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20397, IF_8086|IF_SM},
    {I_XCHG, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20397, IF_8086},
    {I_XCHG, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+19220, IF_8086|IF_SM},
    {I_XCHG, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19220, IF_8086},
    {I_XCHG, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+19225, IF_386|IF_SM},
    {I_XCHG, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19225, IF_386},
    {I_XCHG, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+19230, IF_X64|IF_SM},
    {I_XCHG, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19230, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_XCRYPTCBC[] = {
    {I_XCRYPTCBC, 0, {0,0,0,0,0}, nasm_bytecodes+16314, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_XCRYPTCFB[] = {
    {I_XCRYPTCFB, 0, {0,0,0,0,0}, nasm_bytecodes+16326, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_XCRYPTCTR[] = {
    {I_XCRYPTCTR, 0, {0,0,0,0,0}, nasm_bytecodes+16320, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_XCRYPTECB[] = {
    {I_XCRYPTECB, 0, {0,0,0,0,0}, nasm_bytecodes+16308, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_XCRYPTOFB[] = {
    {I_XCRYPTOFB, 0, {0,0,0,0,0}, nasm_bytecodes+16332, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_XGETBV[] = {
    {I_XGETBV, 0, {0,0,0,0,0}, nasm_bytecodes+15450, IF_NEHALEM},
    ITEMPLATE_END
};

static const struct itemplate instrux_XLAT[] = {
    {I_XLAT, 0, {0,0,0,0,0}, nasm_bytecodes+20476, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_XLATB[] = {
    {I_XLATB, 0, {0,0,0,0,0}, nasm_bytecodes+20476, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_XOR[] = {
    {I_XOR, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20401, IF_8086|IF_SM},
    {I_XOR, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20401, IF_8086},
    {I_XOR, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+19235, IF_8086|IF_SM},
    {I_XOR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19235, IF_8086},
    {I_XOR, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+19240, IF_386|IF_SM},
    {I_XOR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19240, IF_386},
    {I_XOR, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+19245, IF_X64|IF_SM},
    {I_XOR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19245, IF_X64},
    {I_XOR, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+11652, IF_8086|IF_SM},
    {I_XOR, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+11652, IF_8086},
    {I_XOR, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+19250, IF_8086|IF_SM},
    {I_XOR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19250, IF_8086},
    {I_XOR, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+19255, IF_386|IF_SM},
    {I_XOR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19255, IF_386},
    {I_XOR, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+19260, IF_X64|IF_SM},
    {I_XOR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19260, IF_X64},
    {I_XOR, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15168, IF_8086},
    {I_XOR, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15174, IF_386},
    {I_XOR, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15180, IF_X64},
    {I_XOR, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+20405, IF_8086|IF_SM},
    {I_XOR, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+15168, IF_8086|IF_SM},
    {I_XOR, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+19265, IF_8086|IF_SM},
    {I_XOR, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+15174, IF_386|IF_SM},
    {I_XOR, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19270, IF_386|IF_SM},
    {I_XOR, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+15180, IF_X64|IF_SM},
    {I_XOR, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19275, IF_X64|IF_SM},
    {I_XOR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+19280, IF_8086|IF_SM},
    {I_XOR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+15186, IF_8086|IF_SM},
    {I_XOR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+15192, IF_386|IF_SM},
    {I_XOR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+15198, IF_X64|IF_SM},
    {I_XOR, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+19280, IF_8086|IF_SM},
    {I_XOR, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+15186, IF_8086|IF_SM},
    {I_XOR, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+15192, IF_386|IF_SM},
    ITEMPLATE_END
};

static const struct itemplate instrux_XORPD[] = {
    {I_XORPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16194, IF_WILLAMETTE|IF_SSE2|IF_SO},
    ITEMPLATE_END
};

static const struct itemplate instrux_XORPS[] = {
    {I_XORPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15444, IF_KATMAI|IF_SSE},
    ITEMPLATE_END
};

static const struct itemplate instrux_XRSTOR[] = {
    {I_XRSTOR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15468, IF_NEHALEM},
    ITEMPLATE_END
};

static const struct itemplate instrux_XSAVE[] = {
    {I_XSAVE, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15462, IF_NEHALEM},
    ITEMPLATE_END
};

static const struct itemplate instrux_XSETBV[] = {
    {I_XSETBV, 0, {0,0,0,0,0}, nasm_bytecodes+15456, IF_NEHALEM|IF_PRIV},
    ITEMPLATE_END
};

static const struct itemplate instrux_XSHA1[] = {
    {I_XSHA1, 0, {0,0,0,0,0}, nasm_bytecodes+16344, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_XSHA256[] = {
    {I_XSHA256, 0, {0,0,0,0,0}, nasm_bytecodes+16350, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_XSTORE[] = {
    {I_XSTORE, 0, {0,0,0,0,0}, nasm_bytecodes+19360, IF_PENT|IF_CYRIX},
    ITEMPLATE_END
};

static const struct itemplate instrux_CMOVcc[] = {
    {I_CMOVcc, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+7316, IF_P6|IF_SM},
    {I_CMOVcc, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+7316, IF_P6},
    {I_CMOVcc, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+7323, IF_P6|IF_SM},
    {I_CMOVcc, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+7323, IF_P6},
    {I_CMOVcc, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+7330, IF_X64|IF_SM},
    {I_CMOVcc, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+7330, IF_X64},
    ITEMPLATE_END
};

static const struct itemplate instrux_Jcc[] = {
    {I_Jcc, 1, {IMMEDIATE|NEAR,0,0,0,0}, nasm_bytecodes+7337, IF_386},
    {I_Jcc, 1, {IMMEDIATE|BITS16|NEAR,0,0,0,0}, nasm_bytecodes+7344, IF_386},
    {I_Jcc, 1, {IMMEDIATE|BITS32|NEAR,0,0,0,0}, nasm_bytecodes+7351, IF_386},
    {I_Jcc, 1, {IMMEDIATE|SHORT,0,0,0,0}, nasm_bytecodes+19286, IF_8086},
    {I_Jcc, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+19285, IF_8086},
    {I_Jcc, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+7352, IF_386},
    {I_Jcc, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+7358, IF_8086},
    {I_Jcc, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+19286, IF_8086},
    ITEMPLATE_END
};

static const struct itemplate instrux_SETcc[] = {
    {I_SETcc, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15204, IF_386|IF_SB},
    {I_SETcc, 1, {REG8,0,0,0,0}, nasm_bytecodes+15204, IF_386},
    ITEMPLATE_END
};

const struct itemplate * const nasm_instructions[] = {
    instrux_AAA,
    instrux_AAD,
    instrux_AAM,
    instrux_AAS,
    instrux_ADC,
    instrux_ADD,
    instrux_ADDPD,
    instrux_ADDPS,
    instrux_ADDSD,
    instrux_ADDSS,
    instrux_ADDSUBPD,
    instrux_ADDSUBPS,
    instrux_AESDEC,
    instrux_AESDECLAST,
    instrux_AESENC,
    instrux_AESENCLAST,
    instrux_AESIMC,
    instrux_AESKEYGENASSIST,
    instrux_AND,
    instrux_ANDNPD,
    instrux_ANDNPS,
    instrux_ANDPD,
    instrux_ANDPS,
    instrux_ARPL,
    instrux_BB0_RESET,
    instrux_BB1_RESET,
    instrux_BLENDPD,
    instrux_BLENDPS,
    instrux_BLENDVPD,
    instrux_BLENDVPS,
    instrux_BOUND,
    instrux_BSF,
    instrux_BSR,
    instrux_BSWAP,
    instrux_BT,
    instrux_BTC,
    instrux_BTR,
    instrux_BTS,
    instrux_CALL,
    instrux_CBW,
    instrux_CDQ,
    instrux_CDQE,
    instrux_CLC,
    instrux_CLD,
    instrux_CLFLUSH,
    instrux_CLGI,
    instrux_CLI,
    instrux_CLTS,
    instrux_CMC,
    instrux_CMP,
    instrux_CMPEQPD,
    instrux_CMPEQPS,
    instrux_CMPEQSD,
    instrux_CMPEQSS,
    instrux_CMPLEPD,
    instrux_CMPLEPS,
    instrux_CMPLESD,
    instrux_CMPLESS,
    instrux_CMPLTPD,
    instrux_CMPLTPS,
    instrux_CMPLTSD,
    instrux_CMPLTSS,
    instrux_CMPNEQPD,
    instrux_CMPNEQPS,
    instrux_CMPNEQSD,
    instrux_CMPNEQSS,
    instrux_CMPNLEPD,
    instrux_CMPNLEPS,
    instrux_CMPNLESD,
    instrux_CMPNLESS,
    instrux_CMPNLTPD,
    instrux_CMPNLTPS,
    instrux_CMPNLTSD,
    instrux_CMPNLTSS,
    instrux_CMPORDPD,
    instrux_CMPORDPS,
    instrux_CMPORDSD,
    instrux_CMPORDSS,
    instrux_CMPPD,
    instrux_CMPPS,
    instrux_CMPSB,
    instrux_CMPSD,
    instrux_CMPSQ,
    instrux_CMPSS,
    instrux_CMPSW,
    instrux_CMPUNORDPD,
    instrux_CMPUNORDPS,
    instrux_CMPUNORDSD,
    instrux_CMPUNORDSS,
    instrux_CMPXCHG,
    instrux_CMPXCHG16B,
    instrux_CMPXCHG486,
    instrux_CMPXCHG8B,
    instrux_COMEQPD,
    instrux_COMEQPS,
    instrux_COMEQSD,
    instrux_COMEQSS,
    instrux_COMFALSEPD,
    instrux_COMFALSEPS,
    instrux_COMFALSESD,
    instrux_COMFALSESS,
    instrux_COMISD,
    instrux_COMISS,
    instrux_COMLEPD,
    instrux_COMLEPS,
    instrux_COMLESD,
    instrux_COMLESS,
    instrux_COMLTPD,
    instrux_COMLTPS,
    instrux_COMLTSD,
    instrux_COMLTSS,
    instrux_COMNEQPD,
    instrux_COMNEQPS,
    instrux_COMNEQSD,
    instrux_COMNEQSS,
    instrux_COMNLEPD,
    instrux_COMNLEPS,
    instrux_COMNLESD,
    instrux_COMNLESS,
    instrux_COMNLTPD,
    instrux_COMNLTPS,
    instrux_COMNLTSD,
    instrux_COMNLTSS,
    instrux_COMORDPD,
    instrux_COMORDPS,
    instrux_COMORDSD,
    instrux_COMORDSS,
    instrux_COMPD,
    instrux_COMPS,
    instrux_COMSD,
    instrux_COMSS,
    instrux_COMTRUEPD,
    instrux_COMTRUEPS,
    instrux_COMTRUESD,
    instrux_COMTRUESS,
    instrux_COMUEQPD,
    instrux_COMUEQPS,
    instrux_COMUEQSD,
    instrux_COMUEQSS,
    instrux_COMULEPD,
    instrux_COMULEPS,
    instrux_COMULESD,
    instrux_COMULESS,
    instrux_COMULTPD,
    instrux_COMULTPS,
    instrux_COMULTSD,
    instrux_COMULTSS,
    instrux_COMUNEQPD,
    instrux_COMUNEQPS,
    instrux_COMUNEQSD,
    instrux_COMUNEQSS,
    instrux_COMUNLEPD,
    instrux_COMUNLEPS,
    instrux_COMUNLESD,
    instrux_COMUNLESS,
    instrux_COMUNLTPD,
    instrux_COMUNLTPS,
    instrux_COMUNLTSD,
    instrux_COMUNLTSS,
    instrux_COMUNORDPD,
    instrux_COMUNORDPS,
    instrux_COMUNORDSD,
    instrux_COMUNORDSS,
    instrux_CPUID,
    instrux_CPU_READ,
    instrux_CPU_WRITE,
    instrux_CQO,
    instrux_CRC32,
    instrux_CVTDQ2PD,
    instrux_CVTDQ2PS,
    instrux_CVTPD2DQ,
    instrux_CVTPD2PI,
    instrux_CVTPD2PS,
    instrux_CVTPH2PS,
    instrux_CVTPI2PD,
    instrux_CVTPI2PS,
    instrux_CVTPS2DQ,
    instrux_CVTPS2PD,
    instrux_CVTPS2PH,
    instrux_CVTPS2PI,
    instrux_CVTSD2SI,
    instrux_CVTSD2SS,
    instrux_CVTSI2SD,
    instrux_CVTSI2SS,
    instrux_CVTSS2SD,
    instrux_CVTSS2SI,
    instrux_CVTTPD2DQ,
    instrux_CVTTPD2PI,
    instrux_CVTTPS2DQ,
    instrux_CVTTPS2PI,
    instrux_CVTTSD2SI,
    instrux_CVTTSS2SI,
    instrux_CWD,
    instrux_CWDE,
    instrux_DAA,
    instrux_DAS,
    instrux_DB,
    instrux_DD,
    instrux_DEC,
    instrux_DIV,
    instrux_DIVPD,
    instrux_DIVPS,
    instrux_DIVSD,
    instrux_DIVSS,
    instrux_DMINT,
    instrux_DO,
    instrux_DPPD,
    instrux_DPPS,
    instrux_DQ,
    instrux_DT,
    instrux_DW,
    instrux_DY,
    instrux_EMMS,
    instrux_ENTER,
    instrux_EQU,
    instrux_EXTRACTPS,
    instrux_EXTRQ,
    instrux_F2XM1,
    instrux_FABS,
    instrux_FADD,
    instrux_FADDP,
    instrux_FBLD,
    instrux_FBSTP,
    instrux_FCHS,
    instrux_FCLEX,
    instrux_FCMOVB,
    instrux_FCMOVBE,
    instrux_FCMOVE,
    instrux_FCMOVNB,
    instrux_FCMOVNBE,
    instrux_FCMOVNE,
    instrux_FCMOVNU,
    instrux_FCMOVU,
    instrux_FCOM,
    instrux_FCOMI,
    instrux_FCOMIP,
    instrux_FCOMP,
    instrux_FCOMPP,
    instrux_FCOS,
    instrux_FDECSTP,
    instrux_FDISI,
    instrux_FDIV,
    instrux_FDIVP,
    instrux_FDIVR,
    instrux_FDIVRP,
    instrux_FEMMS,
    instrux_FENI,
    instrux_FFREE,
    instrux_FFREEP,
    instrux_FIADD,
    instrux_FICOM,
    instrux_FICOMP,
    instrux_FIDIV,
    instrux_FIDIVR,
    instrux_FILD,
    instrux_FIMUL,
    instrux_FINCSTP,
    instrux_FINIT,
    instrux_FIST,
    instrux_FISTP,
    instrux_FISTTP,
    instrux_FISUB,
    instrux_FISUBR,
    instrux_FLD,
    instrux_FLD1,
    instrux_FLDCW,
    instrux_FLDENV,
    instrux_FLDL2E,
    instrux_FLDL2T,
    instrux_FLDLG2,
    instrux_FLDLN2,
    instrux_FLDPI,
    instrux_FLDZ,
    instrux_FMADDPD,
    instrux_FMADDPS,
    instrux_FMADDSD,
    instrux_FMADDSS,
    instrux_FMSUBPD,
    instrux_FMSUBPS,
    instrux_FMSUBSD,
    instrux_FMSUBSS,
    instrux_FMUL,
    instrux_FMULP,
    instrux_FNCLEX,
    instrux_FNDISI,
    instrux_FNENI,
    instrux_FNINIT,
    instrux_FNMADDPD,
    instrux_FNMADDPS,
    instrux_FNMADDSD,
    instrux_FNMADDSS,
    instrux_FNMSUBPD,
    instrux_FNMSUBPS,
    instrux_FNMSUBSD,
    instrux_FNMSUBSS,
    instrux_FNOP,
    instrux_FNSAVE,
    instrux_FNSTCW,
    instrux_FNSTENV,
    instrux_FNSTSW,
    instrux_FPATAN,
    instrux_FPREM,
    instrux_FPREM1,
    instrux_FPTAN,
    instrux_FRCZPD,
    instrux_FRCZPS,
    instrux_FRCZSD,
    instrux_FRCZSS,
    instrux_FRNDINT,
    instrux_FRSTOR,
    instrux_FSAVE,
    instrux_FSCALE,
    instrux_FSETPM,
    instrux_FSIN,
    instrux_FSINCOS,
    instrux_FSQRT,
    instrux_FST,
    instrux_FSTCW,
    instrux_FSTENV,
    instrux_FSTP,
    instrux_FSTSW,
    instrux_FSUB,
    instrux_FSUBP,
    instrux_FSUBR,
    instrux_FSUBRP,
    instrux_FTST,
    instrux_FUCOM,
    instrux_FUCOMI,
    instrux_FUCOMIP,
    instrux_FUCOMP,
    instrux_FUCOMPP,
    instrux_FWAIT,
    instrux_FXAM,
    instrux_FXCH,
    instrux_FXRSTOR,
    instrux_FXSAVE,
    instrux_FXTRACT,
    instrux_FYL2X,
    instrux_FYL2XP1,
    instrux_GETSEC,
    instrux_HADDPD,
    instrux_HADDPS,
    instrux_HINT_NOP0,
    instrux_HINT_NOP1,
    instrux_HINT_NOP10,
    instrux_HINT_NOP11,
    instrux_HINT_NOP12,
    instrux_HINT_NOP13,
    instrux_HINT_NOP14,
    instrux_HINT_NOP15,
    instrux_HINT_NOP16,
    instrux_HINT_NOP17,
    instrux_HINT_NOP18,
    instrux_HINT_NOP19,
    instrux_HINT_NOP2,
    instrux_HINT_NOP20,
    instrux_HINT_NOP21,
    instrux_HINT_NOP22,
    instrux_HINT_NOP23,
    instrux_HINT_NOP24,
    instrux_HINT_NOP25,
    instrux_HINT_NOP26,
    instrux_HINT_NOP27,
    instrux_HINT_NOP28,
    instrux_HINT_NOP29,
    instrux_HINT_NOP3,
    instrux_HINT_NOP30,
    instrux_HINT_NOP31,
    instrux_HINT_NOP32,
    instrux_HINT_NOP33,
    instrux_HINT_NOP34,
    instrux_HINT_NOP35,
    instrux_HINT_NOP36,
    instrux_HINT_NOP37,
    instrux_HINT_NOP38,
    instrux_HINT_NOP39,
    instrux_HINT_NOP4,
    instrux_HINT_NOP40,
    instrux_HINT_NOP41,
    instrux_HINT_NOP42,
    instrux_HINT_NOP43,
    instrux_HINT_NOP44,
    instrux_HINT_NOP45,
    instrux_HINT_NOP46,
    instrux_HINT_NOP47,
    instrux_HINT_NOP48,
    instrux_HINT_NOP49,
    instrux_HINT_NOP5,
    instrux_HINT_NOP50,
    instrux_HINT_NOP51,
    instrux_HINT_NOP52,
    instrux_HINT_NOP53,
    instrux_HINT_NOP54,
    instrux_HINT_NOP55,
    instrux_HINT_NOP56,
    instrux_HINT_NOP57,
    instrux_HINT_NOP58,
    instrux_HINT_NOP59,
    instrux_HINT_NOP6,
    instrux_HINT_NOP60,
    instrux_HINT_NOP61,
    instrux_HINT_NOP62,
    instrux_HINT_NOP63,
    instrux_HINT_NOP7,
    instrux_HINT_NOP8,
    instrux_HINT_NOP9,
    instrux_HLT,
    instrux_HSUBPD,
    instrux_HSUBPS,
    instrux_IBTS,
    instrux_ICEBP,
    instrux_IDIV,
    instrux_IMUL,
    instrux_IN,
    instrux_INC,
    instrux_INCBIN,
    instrux_INSB,
    instrux_INSD,
    instrux_INSERTPS,
    instrux_INSERTQ,
    instrux_INSW,
    instrux_INT,
    instrux_INT01,
    instrux_INT03,
    instrux_INT1,
    instrux_INT3,
    instrux_INTO,
    instrux_INVD,
    instrux_INVEPT,
    instrux_INVLPG,
    instrux_INVLPGA,
    instrux_INVVPID,
    instrux_IRET,
    instrux_IRETD,
    instrux_IRETQ,
    instrux_IRETW,
    instrux_JCXZ,
    instrux_JECXZ,
    instrux_JMP,
    instrux_JMPE,
    instrux_JRCXZ,
    instrux_LAHF,
    instrux_LAR,
    instrux_LDDQU,
    instrux_LDMXCSR,
    instrux_LDS,
    instrux_LEA,
    instrux_LEAVE,
    instrux_LES,
    instrux_LFENCE,
    instrux_LFS,
    instrux_LGDT,
    instrux_LGS,
    instrux_LIDT,
    instrux_LLDT,
    instrux_LMSW,
    instrux_LOADALL,
    instrux_LOADALL286,
    instrux_LODSB,
    instrux_LODSD,
    instrux_LODSQ,
    instrux_LODSW,
    instrux_LOOP,
    instrux_LOOPE,
    instrux_LOOPNE,
    instrux_LOOPNZ,
    instrux_LOOPZ,
    instrux_LSL,
    instrux_LSS,
    instrux_LTR,
    instrux_LZCNT,
    instrux_MASKMOVDQU,
    instrux_MASKMOVQ,
    instrux_MAXPD,
    instrux_MAXPS,
    instrux_MAXSD,
    instrux_MAXSS,
    instrux_MFENCE,
    instrux_MINPD,
    instrux_MINPS,
    instrux_MINSD,
    instrux_MINSS,
    instrux_MONITOR,
    instrux_MONTMUL,
    instrux_MOV,
    instrux_MOVAPD,
    instrux_MOVAPS,
    instrux_MOVBE,
    instrux_MOVD,
    instrux_MOVDDUP,
    instrux_MOVDQ2Q,
    instrux_MOVDQA,
    instrux_MOVDQU,
    instrux_MOVHLPS,
    instrux_MOVHPD,
    instrux_MOVHPS,
    instrux_MOVLHPS,
    instrux_MOVLPD,
    instrux_MOVLPS,
    instrux_MOVMSKPD,
    instrux_MOVMSKPS,
    instrux_MOVNTDQ,
    instrux_MOVNTDQA,
    instrux_MOVNTI,
    instrux_MOVNTPD,
    instrux_MOVNTPS,
    instrux_MOVNTQ,
    instrux_MOVNTSD,
    instrux_MOVNTSS,
    instrux_MOVQ,
    instrux_MOVQ2DQ,
    instrux_MOVSB,
    instrux_MOVSD,
    instrux_MOVSHDUP,
    instrux_MOVSLDUP,
    instrux_MOVSQ,
    instrux_MOVSS,
    instrux_MOVSW,
    instrux_MOVSX,
    instrux_MOVSXD,
    instrux_MOVUPD,
    instrux_MOVUPS,
    instrux_MOVZX,
    instrux_MPSADBW,
    instrux_MUL,
    instrux_MULPD,
    instrux_MULPS,
    instrux_MULSD,
    instrux_MULSS,
    instrux_MWAIT,
    instrux_NEG,
    instrux_NOP,
    instrux_NOT,
    instrux_OR,
    instrux_ORPD,
    instrux_ORPS,
    instrux_OUT,
    instrux_OUTSB,
    instrux_OUTSD,
    instrux_OUTSW,
    instrux_PABSB,
    instrux_PABSD,
    instrux_PABSW,
    instrux_PACKSSDW,
    instrux_PACKSSWB,
    instrux_PACKUSDW,
    instrux_PACKUSWB,
    instrux_PADDB,
    instrux_PADDD,
    instrux_PADDQ,
    instrux_PADDSB,
    instrux_PADDSIW,
    instrux_PADDSW,
    instrux_PADDUSB,
    instrux_PADDUSW,
    instrux_PADDW,
    instrux_PALIGNR,
    instrux_PAND,
    instrux_PANDN,
    instrux_PAUSE,
    instrux_PAVEB,
    instrux_PAVGB,
    instrux_PAVGUSB,
    instrux_PAVGW,
    instrux_PBLENDVB,
    instrux_PBLENDW,
    instrux_PCLMULHQHQDQ,
    instrux_PCLMULHQLQDQ,
    instrux_PCLMULLQHQDQ,
    instrux_PCLMULLQLQDQ,
    instrux_PCLMULQDQ,
    instrux_PCMOV,
    instrux_PCMPEQB,
    instrux_PCMPEQD,
    instrux_PCMPEQQ,
    instrux_PCMPEQW,
    instrux_PCMPESTRI,
    instrux_PCMPESTRM,
    instrux_PCMPGTB,
    instrux_PCMPGTD,
    instrux_PCMPGTQ,
    instrux_PCMPGTW,
    instrux_PCMPISTRI,
    instrux_PCMPISTRM,
    instrux_PCOMB,
    instrux_PCOMD,
    instrux_PCOMEQB,
    instrux_PCOMEQD,
    instrux_PCOMEQQ,
    instrux_PCOMEQUB,
    instrux_PCOMEQUD,
    instrux_PCOMEQUQ,
    instrux_PCOMEQUW,
    instrux_PCOMEQW,
    instrux_PCOMFALSEB,
    instrux_PCOMFALSED,
    instrux_PCOMFALSEQ,
    instrux_PCOMFALSEUB,
    instrux_PCOMFALSEUD,
    instrux_PCOMFALSEUQ,
    instrux_PCOMFALSEUW,
    instrux_PCOMFALSEW,
    instrux_PCOMGEB,
    instrux_PCOMGED,
    instrux_PCOMGEQ,
    instrux_PCOMGEUB,
    instrux_PCOMGEUD,
    instrux_PCOMGEUQ,
    instrux_PCOMGEUW,
    instrux_PCOMGEW,
    instrux_PCOMGTB,
    instrux_PCOMGTD,
    instrux_PCOMGTQ,
    instrux_PCOMGTUB,
    instrux_PCOMGTUD,
    instrux_PCOMGTUQ,
    instrux_PCOMGTUW,
    instrux_PCOMGTW,
    instrux_PCOMLEB,
    instrux_PCOMLED,
    instrux_PCOMLEQ,
    instrux_PCOMLEUB,
    instrux_PCOMLEUD,
    instrux_PCOMLEUQ,
    instrux_PCOMLEUW,
    instrux_PCOMLEW,
    instrux_PCOMLTB,
    instrux_PCOMLTD,
    instrux_PCOMLTQ,
    instrux_PCOMLTUB,
    instrux_PCOMLTUD,
    instrux_PCOMLTUQ,
    instrux_PCOMLTUW,
    instrux_PCOMLTW,
    instrux_PCOMNEQB,
    instrux_PCOMNEQD,
    instrux_PCOMNEQQ,
    instrux_PCOMNEQUB,
    instrux_PCOMNEQUD,
    instrux_PCOMNEQUQ,
    instrux_PCOMNEQUW,
    instrux_PCOMNEQW,
    instrux_PCOMQ,
    instrux_PCOMTRUEB,
    instrux_PCOMTRUED,
    instrux_PCOMTRUEQ,
    instrux_PCOMTRUEUB,
    instrux_PCOMTRUEUD,
    instrux_PCOMTRUEUQ,
    instrux_PCOMTRUEUW,
    instrux_PCOMTRUEW,
    instrux_PCOMUB,
    instrux_PCOMUD,
    instrux_PCOMUQ,
    instrux_PCOMUW,
    instrux_PCOMW,
    instrux_PDISTIB,
    instrux_PERMPD,
    instrux_PERMPS,
    instrux_PEXTRB,
    instrux_PEXTRD,
    instrux_PEXTRQ,
    instrux_PEXTRW,
    instrux_PF2ID,
    instrux_PF2IW,
    instrux_PFACC,
    instrux_PFADD,
    instrux_PFCMPEQ,
    instrux_PFCMPGE,
    instrux_PFCMPGT,
    instrux_PFMAX,
    instrux_PFMIN,
    instrux_PFMUL,
    instrux_PFNACC,
    instrux_PFPNACC,
    instrux_PFRCP,
    instrux_PFRCPIT1,
    instrux_PFRCPIT2,
    instrux_PFRCPV,
    instrux_PFRSQIT1,
    instrux_PFRSQRT,
    instrux_PFRSQRTV,
    instrux_PFSUB,
    instrux_PFSUBR,
    instrux_PHADDBD,
    instrux_PHADDBQ,
    instrux_PHADDBW,
    instrux_PHADDD,
    instrux_PHADDDQ,
    instrux_PHADDSW,
    instrux_PHADDUBD,
    instrux_PHADDUBQ,
    instrux_PHADDUBW,
    instrux_PHADDUDQ,
    instrux_PHADDUWD,
    instrux_PHADDUWQ,
    instrux_PHADDW,
    instrux_PHADDWD,
    instrux_PHADDWQ,
    instrux_PHMINPOSUW,
    instrux_PHSUBBW,
    instrux_PHSUBD,
    instrux_PHSUBDQ,
    instrux_PHSUBSW,
    instrux_PHSUBW,
    instrux_PHSUBWD,
    instrux_PI2FD,
    instrux_PI2FW,
    instrux_PINSRB,
    instrux_PINSRD,
    instrux_PINSRQ,
    instrux_PINSRW,
    instrux_PMACHRIW,
    instrux_PMACSDD,
    instrux_PMACSDQH,
    instrux_PMACSDQL,
    instrux_PMACSSDD,
    instrux_PMACSSDQH,
    instrux_PMACSSDQL,
    instrux_PMACSSWD,
    instrux_PMACSSWW,
    instrux_PMACSWD,
    instrux_PMACSWW,
    instrux_PMADCSSWD,
    instrux_PMADCSWD,
    instrux_PMADDUBSW,
    instrux_PMADDWD,
    instrux_PMAGW,
    instrux_PMAXSB,
    instrux_PMAXSD,
    instrux_PMAXSW,
    instrux_PMAXUB,
    instrux_PMAXUD,
    instrux_PMAXUW,
    instrux_PMINSB,
    instrux_PMINSD,
    instrux_PMINSW,
    instrux_PMINUB,
    instrux_PMINUD,
    instrux_PMINUW,
    instrux_PMOVMSKB,
    instrux_PMOVSXBD,
    instrux_PMOVSXBQ,
    instrux_PMOVSXBW,
    instrux_PMOVSXDQ,
    instrux_PMOVSXWD,
    instrux_PMOVSXWQ,
    instrux_PMOVZXBD,
    instrux_PMOVZXBQ,
    instrux_PMOVZXBW,
    instrux_PMOVZXDQ,
    instrux_PMOVZXWD,
    instrux_PMOVZXWQ,
    instrux_PMULDQ,
    instrux_PMULHRIW,
    instrux_PMULHRSW,
    instrux_PMULHRWA,
    instrux_PMULHRWC,
    instrux_PMULHUW,
    instrux_PMULHW,
    instrux_PMULLD,
    instrux_PMULLW,
    instrux_PMULUDQ,
    instrux_PMVGEZB,
    instrux_PMVLZB,
    instrux_PMVNZB,
    instrux_PMVZB,
    instrux_POP,
    instrux_POPA,
    instrux_POPAD,
    instrux_POPAW,
    instrux_POPCNT,
    instrux_POPF,
    instrux_POPFD,
    instrux_POPFQ,
    instrux_POPFW,
    instrux_POR,
    instrux_PPERM,
    instrux_PREFETCH,
    instrux_PREFETCHNTA,
    instrux_PREFETCHT0,
    instrux_PREFETCHT1,
    instrux_PREFETCHT2,
    instrux_PREFETCHW,
    instrux_PROTB,
    instrux_PROTD,
    instrux_PROTQ,
    instrux_PROTW,
    instrux_PSADBW,
    instrux_PSHAB,
    instrux_PSHAD,
    instrux_PSHAQ,
    instrux_PSHAW,
    instrux_PSHLB,
    instrux_PSHLD,
    instrux_PSHLQ,
    instrux_PSHLW,
    instrux_PSHUFB,
    instrux_PSHUFD,
    instrux_PSHUFHW,
    instrux_PSHUFLW,
    instrux_PSHUFW,
    instrux_PSIGNB,
    instrux_PSIGND,
    instrux_PSIGNW,
    instrux_PSLLD,
    instrux_PSLLDQ,
    instrux_PSLLQ,
    instrux_PSLLW,
    instrux_PSRAD,
    instrux_PSRAW,
    instrux_PSRLD,
    instrux_PSRLDQ,
    instrux_PSRLQ,
    instrux_PSRLW,
    instrux_PSUBB,
    instrux_PSUBD,
    instrux_PSUBQ,
    instrux_PSUBSB,
    instrux_PSUBSIW,
    instrux_PSUBSW,
    instrux_PSUBUSB,
    instrux_PSUBUSW,
    instrux_PSUBW,
    instrux_PSWAPD,
    instrux_PTEST,
    instrux_PUNPCKHBW,
    instrux_PUNPCKHDQ,
    instrux_PUNPCKHQDQ,
    instrux_PUNPCKHWD,
    instrux_PUNPCKLBW,
    instrux_PUNPCKLDQ,
    instrux_PUNPCKLQDQ,
    instrux_PUNPCKLWD,
    instrux_PUSH,
    instrux_PUSHA,
    instrux_PUSHAD,
    instrux_PUSHAW,
    instrux_PUSHF,
    instrux_PUSHFD,
    instrux_PUSHFQ,
    instrux_PUSHFW,
    instrux_PXOR,
    instrux_RCL,
    instrux_RCPPS,
    instrux_RCPSS,
    instrux_RCR,
    instrux_RDM,
    instrux_RDMSR,
    instrux_RDPMC,
    instrux_RDSHR,
    instrux_RDTSC,
    instrux_RDTSCP,
    instrux_RESB,
    instrux_RESD,
    instrux_RESO,
    instrux_RESQ,
    instrux_REST,
    instrux_RESW,
    instrux_RESY,
    instrux_RET,
    instrux_RETF,
    instrux_RETN,
    instrux_ROL,
    instrux_ROR,
    instrux_ROUNDPD,
    instrux_ROUNDPS,
    instrux_ROUNDSD,
    instrux_ROUNDSS,
    instrux_RSDC,
    instrux_RSLDT,
    instrux_RSM,
    instrux_RSQRTPS,
    instrux_RSQRTSS,
    instrux_RSTS,
    instrux_SAHF,
    instrux_SAL,
    instrux_SALC,
    instrux_SAR,
    instrux_SBB,
    instrux_SCASB,
    instrux_SCASD,
    instrux_SCASQ,
    instrux_SCASW,
    instrux_SFENCE,
    instrux_SGDT,
    instrux_SHL,
    instrux_SHLD,
    instrux_SHR,
    instrux_SHRD,
    instrux_SHUFPD,
    instrux_SHUFPS,
    instrux_SIDT,
    instrux_SKINIT,
    instrux_SLDT,
    instrux_SMI,
    instrux_SMINT,
    instrux_SMINTOLD,
    instrux_SMSW,
    instrux_SQRTPD,
    instrux_SQRTPS,
    instrux_SQRTSD,
    instrux_SQRTSS,
    instrux_STC,
    instrux_STD,
    instrux_STGI,
    instrux_STI,
    instrux_STMXCSR,
    instrux_STOSB,
    instrux_STOSD,
    instrux_STOSQ,
    instrux_STOSW,
    instrux_STR,
    instrux_SUB,
    instrux_SUBPD,
    instrux_SUBPS,
    instrux_SUBSD,
    instrux_SUBSS,
    instrux_SVDC,
    instrux_SVLDT,
    instrux_SVTS,
    instrux_SWAPGS,
    instrux_SYSCALL,
    instrux_SYSENTER,
    instrux_SYSEXIT,
    instrux_SYSRET,
    instrux_TEST,
    instrux_UCOMISD,
    instrux_UCOMISS,
    instrux_UD0,
    instrux_UD1,
    instrux_UD2,
    instrux_UD2A,
    instrux_UD2B,
    instrux_UMOV,
    instrux_UNPCKHPD,
    instrux_UNPCKHPS,
    instrux_UNPCKLPD,
    instrux_UNPCKLPS,
    instrux_VADDPD,
    instrux_VADDPS,
    instrux_VADDSD,
    instrux_VADDSS,
    instrux_VADDSUBPD,
    instrux_VADDSUBPS,
    instrux_VAESDEC,
    instrux_VAESDECLAST,
    instrux_VAESENC,
    instrux_VAESENCLAST,
    instrux_VAESIMC,
    instrux_VAESKEYGENASSIST,
    instrux_VANDNPD,
    instrux_VANDNPS,
    instrux_VANDPD,
    instrux_VANDPS,
    instrux_VBLENDPD,
    instrux_VBLENDPS,
    instrux_VBLENDVPD,
    instrux_VBLENDVPS,
    instrux_VBROADCASTF128,
    instrux_VBROADCASTSD,
    instrux_VBROADCASTSS,
    instrux_VCMPEQPD,
    instrux_VCMPEQPS,
    instrux_VCMPEQSD,
    instrux_VCMPEQSS,
    instrux_VCMPEQ_OSPD,
    instrux_VCMPEQ_OSPS,
    instrux_VCMPEQ_OSSD,
    instrux_VCMPEQ_OSSS,
    instrux_VCMPEQ_UQPD,
    instrux_VCMPEQ_UQPS,
    instrux_VCMPEQ_UQSD,
    instrux_VCMPEQ_UQSS,
    instrux_VCMPEQ_USPD,
    instrux_VCMPEQ_USPS,
    instrux_VCMPEQ_USSD,
    instrux_VCMPEQ_USSS,
    instrux_VCMPFALSEPD,
    instrux_VCMPFALSEPS,
    instrux_VCMPFALSESD,
    instrux_VCMPFALSESS,
    instrux_VCMPFALSE_OSPD,
    instrux_VCMPFALSE_OSPS,
    instrux_VCMPFALSE_OSSD,
    instrux_VCMPFALSE_OSSS,
    instrux_VCMPGEPD,
    instrux_VCMPGEPS,
    instrux_VCMPGESD,
    instrux_VCMPGESS,
    instrux_VCMPGE_OQPD,
    instrux_VCMPGE_OQPS,
    instrux_VCMPGE_OQSD,
    instrux_VCMPGE_OQSS,
    instrux_VCMPGTPD,
    instrux_VCMPGTPS,
    instrux_VCMPGTSD,
    instrux_VCMPGTSS,
    instrux_VCMPGT_OQPD,
    instrux_VCMPGT_OQPS,
    instrux_VCMPGT_OQSD,
    instrux_VCMPGT_OQSS,
    instrux_VCMPLEPD,
    instrux_VCMPLEPS,
    instrux_VCMPLESD,
    instrux_VCMPLESS,
    instrux_VCMPLE_OQPD,
    instrux_VCMPLE_OQPS,
    instrux_VCMPLE_OQSD,
    instrux_VCMPLE_OQSS,
    instrux_VCMPLTPD,
    instrux_VCMPLTPS,
    instrux_VCMPLTSD,
    instrux_VCMPLTSS,
    instrux_VCMPLT_OQPD,
    instrux_VCMPLT_OQPS,
    instrux_VCMPLT_OQSD,
    instrux_VCMPLT_OQSS,
    instrux_VCMPNEQPD,
    instrux_VCMPNEQPS,
    instrux_VCMPNEQSD,
    instrux_VCMPNEQSS,
    instrux_VCMPNEQ_OQPD,
    instrux_VCMPNEQ_OQPS,
    instrux_VCMPNEQ_OQSD,
    instrux_VCMPNEQ_OQSS,
    instrux_VCMPNEQ_OSPD,
    instrux_VCMPNEQ_OSPS,
    instrux_VCMPNEQ_OSSD,
    instrux_VCMPNEQ_OSSS,
    instrux_VCMPNEQ_USPD,
    instrux_VCMPNEQ_USPS,
    instrux_VCMPNEQ_USSD,
    instrux_VCMPNEQ_USSS,
    instrux_VCMPNGEPD,
    instrux_VCMPNGEPS,
    instrux_VCMPNGESD,
    instrux_VCMPNGESS,
    instrux_VCMPNGE_UQPD,
    instrux_VCMPNGE_UQPS,
    instrux_VCMPNGE_UQSD,
    instrux_VCMPNGE_UQSS,
    instrux_VCMPNGTPD,
    instrux_VCMPNGTPS,
    instrux_VCMPNGTSD,
    instrux_VCMPNGTSS,
    instrux_VCMPNGT_UQPD,
    instrux_VCMPNGT_UQPS,
    instrux_VCMPNGT_UQSD,
    instrux_VCMPNGT_UQSS,
    instrux_VCMPNLEPD,
    instrux_VCMPNLEPS,
    instrux_VCMPNLESD,
    instrux_VCMPNLESS,
    instrux_VCMPNLE_UQPD,
    instrux_VCMPNLE_UQPS,
    instrux_VCMPNLE_UQSD,
    instrux_VCMPNLE_UQSS,
    instrux_VCMPNLTPD,
    instrux_VCMPNLTPS,
    instrux_VCMPNLTSD,
    instrux_VCMPNLTSS,
    instrux_VCMPNLT_UQPD,
    instrux_VCMPNLT_UQPS,
    instrux_VCMPNLT_UQSD,
    instrux_VCMPNLT_UQSS,
    instrux_VCMPORDPD,
    instrux_VCMPORDPS,
    instrux_VCMPORDSD,
    instrux_VCMPORDSS,
    instrux_VCMPORD_SPD,
    instrux_VCMPORD_SPS,
    instrux_VCMPORD_SSD,
    instrux_VCMPORD_SSS,
    instrux_VCMPORS_SPD,
    instrux_VCMPORS_SPS,
    instrux_VCMPPD,
    instrux_VCMPPS,
    instrux_VCMPSD,
    instrux_VCMPSS,
    instrux_VCMPTRUEPD,
    instrux_VCMPTRUEPS,
    instrux_VCMPTRUESD,
    instrux_VCMPTRUESS,
    instrux_VCMPTRUE_USPD,
    instrux_VCMPTRUE_USPS,
    instrux_VCMPTRUE_USSD,
    instrux_VCMPTRUE_USSS,
    instrux_VCMPUNORDPD,
    instrux_VCMPUNORDPS,
    instrux_VCMPUNORDSD,
    instrux_VCMPUNORDSS,
    instrux_VCMPUNORD_SPD,
    instrux_VCMPUNORD_SPS,
    instrux_VCMPUNORD_SSD,
    instrux_VCMPUNORD_SSS,
    instrux_VCOMISD,
    instrux_VCOMISS,
    instrux_VCVTDQ2PD,
    instrux_VCVTDQ2PS,
    instrux_VCVTPD2DQ,
    instrux_VCVTPD2PS,
    instrux_VCVTPS2DQ,
    instrux_VCVTPS2PD,
    instrux_VCVTSD2SI,
    instrux_VCVTSD2SS,
    instrux_VCVTSI2SD,
    instrux_VCVTSI2SS,
    instrux_VCVTSS2SD,
    instrux_VCVTSS2SI,
    instrux_VCVTTPD2DQ,
    instrux_VCVTTPS2DQ,
    instrux_VCVTTSD2SI,
    instrux_VCVTTSS2SI,
    instrux_VDIVPD,
    instrux_VDIVPS,
    instrux_VDIVSD,
    instrux_VDIVSS,
    instrux_VDPPD,
    instrux_VDPPS,
    instrux_VERR,
    instrux_VERW,
    instrux_VEXTRACTF128,
    instrux_VEXTRACTPS,
    instrux_VFMADD123PD,
    instrux_VFMADD123PS,
    instrux_VFMADD123SD,
    instrux_VFMADD123SS,
    instrux_VFMADD132PD,
    instrux_VFMADD132PS,
    instrux_VFMADD132SD,
    instrux_VFMADD132SS,
    instrux_VFMADD213PD,
    instrux_VFMADD213PS,
    instrux_VFMADD213SD,
    instrux_VFMADD213SS,
    instrux_VFMADD231PD,
    instrux_VFMADD231PS,
    instrux_VFMADD231SD,
    instrux_VFMADD231SS,
    instrux_VFMADD312PD,
    instrux_VFMADD312PS,
    instrux_VFMADD312SD,
    instrux_VFMADD312SS,
    instrux_VFMADD321PD,
    instrux_VFMADD321PS,
    instrux_VFMADD321SD,
    instrux_VFMADD321SS,
    instrux_VFMADDSUB123PD,
    instrux_VFMADDSUB123PS,
    instrux_VFMADDSUB132PD,
    instrux_VFMADDSUB132PS,
    instrux_VFMADDSUB213PD,
    instrux_VFMADDSUB213PS,
    instrux_VFMADDSUB231PD,
    instrux_VFMADDSUB231PS,
    instrux_VFMADDSUB312PD,
    instrux_VFMADDSUB312PS,
    instrux_VFMADDSUB321PD,
    instrux_VFMADDSUB321PS,
    instrux_VFMSUB123PD,
    instrux_VFMSUB123PS,
    instrux_VFMSUB123SD,
    instrux_VFMSUB123SS,
    instrux_VFMSUB132PD,
    instrux_VFMSUB132PS,
    instrux_VFMSUB132SD,
    instrux_VFMSUB132SS,
    instrux_VFMSUB213PD,
    instrux_VFMSUB213PS,
    instrux_VFMSUB213SD,
    instrux_VFMSUB213SS,
    instrux_VFMSUB231PD,
    instrux_VFMSUB231PS,
    instrux_VFMSUB231SD,
    instrux_VFMSUB231SS,
    instrux_VFMSUB312PD,
    instrux_VFMSUB312PS,
    instrux_VFMSUB312SD,
    instrux_VFMSUB312SS,
    instrux_VFMSUB321PD,
    instrux_VFMSUB321PS,
    instrux_VFMSUB321SD,
    instrux_VFMSUB321SS,
    instrux_VFMSUBADD123PD,
    instrux_VFMSUBADD123PS,
    instrux_VFMSUBADD132PD,
    instrux_VFMSUBADD132PS,
    instrux_VFMSUBADD213PD,
    instrux_VFMSUBADD213PS,
    instrux_VFMSUBADD231PD,
    instrux_VFMSUBADD231PS,
    instrux_VFMSUBADD312PD,
    instrux_VFMSUBADD312PS,
    instrux_VFMSUBADD321PD,
    instrux_VFMSUBADD321PS,
    instrux_VFNMADD123PD,
    instrux_VFNMADD123PS,
    instrux_VFNMADD123SD,
    instrux_VFNMADD123SS,
    instrux_VFNMADD132PD,
    instrux_VFNMADD132PS,
    instrux_VFNMADD132SD,
    instrux_VFNMADD132SS,
    instrux_VFNMADD213PD,
    instrux_VFNMADD213PS,
    instrux_VFNMADD213SD,
    instrux_VFNMADD213SS,
    instrux_VFNMADD231PD,
    instrux_VFNMADD231PS,
    instrux_VFNMADD231SD,
    instrux_VFNMADD231SS,
    instrux_VFNMADD312PD,
    instrux_VFNMADD312PS,
    instrux_VFNMADD312SD,
    instrux_VFNMADD312SS,
    instrux_VFNMADD321PD,
    instrux_VFNMADD321PS,
    instrux_VFNMADD321SD,
    instrux_VFNMADD321SS,
    instrux_VFNMSUB123PD,
    instrux_VFNMSUB123PS,
    instrux_VFNMSUB123SD,
    instrux_VFNMSUB123SS,
    instrux_VFNMSUB132PD,
    instrux_VFNMSUB132PS,
    instrux_VFNMSUB132SD,
    instrux_VFNMSUB132SS,
    instrux_VFNMSUB213PD,
    instrux_VFNMSUB213PS,
    instrux_VFNMSUB213SD,
    instrux_VFNMSUB213SS,
    instrux_VFNMSUB231PD,
    instrux_VFNMSUB231PS,
    instrux_VFNMSUB231SD,
    instrux_VFNMSUB231SS,
    instrux_VFNMSUB312PD,
    instrux_VFNMSUB312PS,
    instrux_VFNMSUB312SD,
    instrux_VFNMSUB312SS,
    instrux_VFNMSUB321PD,
    instrux_VFNMSUB321PS,
    instrux_VFNMSUB321SD,
    instrux_VFNMSUB321SS,
    instrux_VHADDPD,
    instrux_VHADDPS,
    instrux_VHSUBPD,
    instrux_VHSUBPS,
    instrux_VINSERTF128,
    instrux_VINSERTPS,
    instrux_VLDDQU,
    instrux_VLDMXCSR,
    instrux_VLDQQU,
    instrux_VMASKMOVDQU,
    instrux_VMASKMOVPD,
    instrux_VMASKMOVPS,
    instrux_VMAXPD,
    instrux_VMAXPS,
    instrux_VMAXSD,
    instrux_VMAXSS,
    instrux_VMCALL,
    instrux_VMCLEAR,
    instrux_VMINPD,
    instrux_VMINPS,
    instrux_VMINSD,
    instrux_VMINSS,
    instrux_VMLAUNCH,
    instrux_VMLOAD,
    instrux_VMMCALL,
    instrux_VMOVAPD,
    instrux_VMOVAPS,
    instrux_VMOVD,
    instrux_VMOVDDUP,
    instrux_VMOVDQA,
    instrux_VMOVDQU,
    instrux_VMOVHLPS,
    instrux_VMOVHPD,
    instrux_VMOVHPS,
    instrux_VMOVLHPS,
    instrux_VMOVLPD,
    instrux_VMOVLPS,
    instrux_VMOVMSKPD,
    instrux_VMOVMSKPS,
    instrux_VMOVNTDQ,
    instrux_VMOVNTDQA,
    instrux_VMOVNTPD,
    instrux_VMOVNTPS,
    instrux_VMOVNTQQ,
    instrux_VMOVQ,
    instrux_VMOVQQA,
    instrux_VMOVQQU,
    instrux_VMOVSD,
    instrux_VMOVSHDUP,
    instrux_VMOVSLDUP,
    instrux_VMOVSS,
    instrux_VMOVUPD,
    instrux_VMOVUPS,
    instrux_VMPSADBW,
    instrux_VMPTRLD,
    instrux_VMPTRST,
    instrux_VMREAD,
    instrux_VMRESUME,
    instrux_VMRUN,
    instrux_VMSAVE,
    instrux_VMULPD,
    instrux_VMULPS,
    instrux_VMULSD,
    instrux_VMULSS,
    instrux_VMWRITE,
    instrux_VMXOFF,
    instrux_VMXON,
    instrux_VORPD,
    instrux_VORPS,
    instrux_VPABSB,
    instrux_VPABSD,
    instrux_VPABSW,
    instrux_VPACKSSDW,
    instrux_VPACKSSWB,
    instrux_VPACKUSDW,
    instrux_VPACKUSWB,
    instrux_VPADDB,
    instrux_VPADDD,
    instrux_VPADDQ,
    instrux_VPADDSB,
    instrux_VPADDSW,
    instrux_VPADDUSB,
    instrux_VPADDUSW,
    instrux_VPADDW,
    instrux_VPALIGNR,
    instrux_VPAND,
    instrux_VPANDN,
    instrux_VPAVGB,
    instrux_VPAVGW,
    instrux_VPBLENDVB,
    instrux_VPBLENDW,
    instrux_VPCLMULHQHQDQ,
    instrux_VPCLMULHQLQDQ,
    instrux_VPCLMULLQHQDQ,
    instrux_VPCLMULLQLQDQ,
    instrux_VPCLMULQDQ,
    instrux_VPCMPEQB,
    instrux_VPCMPEQD,
    instrux_VPCMPEQQ,
    instrux_VPCMPEQW,
    instrux_VPCMPESTRI,
    instrux_VPCMPESTRM,
    instrux_VPCMPGTB,
    instrux_VPCMPGTD,
    instrux_VPCMPGTQ,
    instrux_VPCMPGTW,
    instrux_VPCMPISTRI,
    instrux_VPCMPISTRM,
    instrux_VPERM2F128,
    instrux_VPERMIL2PD,
    instrux_VPERMIL2PS,
    instrux_VPERMILMO2PD,
    instrux_VPERMILMO2PS,
    instrux_VPERMILMZ2PD,
    instrux_VPERMILMZ2PS,
    instrux_VPERMILPD,
    instrux_VPERMILPS,
    instrux_VPERMILTD2PD,
    instrux_VPERMILTD2PS,
    instrux_VPEXTRB,
    instrux_VPEXTRD,
    instrux_VPEXTRQ,
    instrux_VPEXTRW,
    instrux_VPHADDD,
    instrux_VPHADDSW,
    instrux_VPHADDW,
    instrux_VPHMINPOSUW,
    instrux_VPHSUBD,
    instrux_VPHSUBSW,
    instrux_VPHSUBW,
    instrux_VPINSRB,
    instrux_VPINSRD,
    instrux_VPINSRQ,
    instrux_VPINSRW,
    instrux_VPMADDUBSW,
    instrux_VPMADDWD,
    instrux_VPMAXSB,
    instrux_VPMAXSD,
    instrux_VPMAXSW,
    instrux_VPMAXUB,
    instrux_VPMAXUD,
    instrux_VPMAXUW,
    instrux_VPMINSB,
    instrux_VPMINSD,
    instrux_VPMINSW,
    instrux_VPMINUB,
    instrux_VPMINUD,
    instrux_VPMINUW,
    instrux_VPMOVMSKB,
    instrux_VPMOVSXBD,
    instrux_VPMOVSXBQ,
    instrux_VPMOVSXBW,
    instrux_VPMOVSXDQ,
    instrux_VPMOVSXWD,
    instrux_VPMOVSXWQ,
    instrux_VPMOVZXBD,
    instrux_VPMOVZXBQ,
    instrux_VPMOVZXBW,
    instrux_VPMOVZXDQ,
    instrux_VPMOVZXWD,
    instrux_VPMOVZXWQ,
    instrux_VPMULDQ,
    instrux_VPMULHRSW,
    instrux_VPMULHUW,
    instrux_VPMULHW,
    instrux_VPMULLD,
    instrux_VPMULLW,
    instrux_VPMULUDQ,
    instrux_VPOR,
    instrux_VPSADBW,
    instrux_VPSHUFB,
    instrux_VPSHUFD,
    instrux_VPSHUFHW,
    instrux_VPSHUFLW,
    instrux_VPSIGNB,
    instrux_VPSIGND,
    instrux_VPSIGNW,
    instrux_VPSLLD,
    instrux_VPSLLDQ,
    instrux_VPSLLQ,
    instrux_VPSLLW,
    instrux_VPSRAD,
    instrux_VPSRAW,
    instrux_VPSRLD,
    instrux_VPSRLDQ,
    instrux_VPSRLQ,
    instrux_VPSRLW,
    instrux_VPSUBB,
    instrux_VPSUBD,
    instrux_VPSUBQ,
    instrux_VPSUBSB,
    instrux_VPSUBSW,
    instrux_VPSUBUSB,
    instrux_VPSUBUSW,
    instrux_VPSUBW,
    instrux_VPTEST,
    instrux_VPUNPCKHBW,
    instrux_VPUNPCKHDQ,
    instrux_VPUNPCKHQDQ,
    instrux_VPUNPCKHWD,
    instrux_VPUNPCKLBW,
    instrux_VPUNPCKLDQ,
    instrux_VPUNPCKLQDQ,
    instrux_VPUNPCKLWD,
    instrux_VPXOR,
    instrux_VRCPPS,
    instrux_VRCPSS,
    instrux_VROUNDPD,
    instrux_VROUNDPS,
    instrux_VROUNDSD,
    instrux_VROUNDSS,
    instrux_VRSQRTPS,
    instrux_VRSQRTSS,
    instrux_VSHUFPD,
    instrux_VSHUFPS,
    instrux_VSQRTPD,
    instrux_VSQRTPS,
    instrux_VSQRTSD,
    instrux_VSQRTSS,
    instrux_VSTMXCSR,
    instrux_VSUBPD,
    instrux_VSUBPS,
    instrux_VSUBSD,
    instrux_VSUBSS,
    instrux_VTESTPD,
    instrux_VTESTPS,
    instrux_VUCOMISD,
    instrux_VUCOMISS,
    instrux_VUNPCKHPD,
    instrux_VUNPCKHPS,
    instrux_VUNPCKLPD,
    instrux_VUNPCKLPS,
    instrux_VXORPD,
    instrux_VXORPS,
    instrux_VZEROALL,
    instrux_VZEROUPPER,
    instrux_WBINVD,
    instrux_WRMSR,
    instrux_WRSHR,
    instrux_XADD,
    instrux_XBTS,
    instrux_XCHG,
    instrux_XCRYPTCBC,
    instrux_XCRYPTCFB,
    instrux_XCRYPTCTR,
    instrux_XCRYPTECB,
    instrux_XCRYPTOFB,
    instrux_XGETBV,
    instrux_XLAT,
    instrux_XLATB,
    instrux_XOR,
    instrux_XORPD,
    instrux_XORPS,
    instrux_XRSTOR,
    instrux_XSAVE,
    instrux_XSETBV,
    instrux_XSHA1,
    instrux_XSHA256,
    instrux_XSTORE,
    instrux_CMOVcc,
    instrux_Jcc,
    instrux_SETcc,
};
