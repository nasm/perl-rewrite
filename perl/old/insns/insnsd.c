/* This file auto-generated from insns.dat by insns.pl - don't edit it */

#include "nasm.h"
#include "insns.h"

static const struct itemplate instrux[] = {
    /*    0 */ {I_RESB, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+19523, IF_8086},
    /*    1 */ {I_AAA, 0, {0,0,0,0,0}, nasm_bytecodes+20413, IF_8086|IF_NOLONG},
    /*    2 */ {I_AAD, 0, {0,0,0,0,0}, nasm_bytecodes+19365, IF_8086|IF_NOLONG},
    /*    3 */ {I_AAD, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+19369, IF_8086|IF_SB|IF_NOLONG},
    /*    4 */ {I_AAM, 0, {0,0,0,0,0}, nasm_bytecodes+19373, IF_8086|IF_NOLONG},
    /*    5 */ {I_AAM, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+19377, IF_8086|IF_SB|IF_NOLONG},
    /*    6 */ {I_AAS, 0, {0,0,0,0,0}, nasm_bytecodes+20416, IF_8086|IF_NOLONG},
    /*    7 */ {I_ADC, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+19381, IF_8086|IF_SM},
    /*    8 */ {I_ADC, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+19381, IF_8086},
    /*    9 */ {I_ADC, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+17490, IF_8086|IF_SM},
    /*   10 */ {I_ADC, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17490, IF_8086},
    /*   11 */ {I_ADC, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+17495, IF_386|IF_SM},
    /*   12 */ {I_ADC, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17495, IF_386},
    /*   13 */ {I_ADC, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+17500, IF_X64|IF_SM},
    /*   14 */ {I_ADC, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17500, IF_X64},
    /*   15 */ {I_ADC, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+10672, IF_8086|IF_SM},
    /*   16 */ {I_ADC, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+10672, IF_8086},
    /*   17 */ {I_ADC, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+17505, IF_8086|IF_SM},
    /*   18 */ {I_ADC, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17505, IF_8086},
    /*   19 */ {I_ADC, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+17510, IF_386|IF_SM},
    /*   20 */ {I_ADC, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17510, IF_386},
    /*   21 */ {I_ADC, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+17515, IF_X64|IF_SM},
    /*   22 */ {I_ADC, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17515, IF_X64},
    /*   23 */ {I_ADC, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+13980, IF_8086},
    /*   24 */ {I_ADC, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+13986, IF_386},
    /*   25 */ {I_ADC, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+13992, IF_X64},
    /*   26 */ {I_ADC, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+19385, IF_8086|IF_SM},
    /*   27 */ {I_ADC, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+13980, IF_8086|IF_SM},
    /*   28 */ {I_ADC, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+17520, IF_8086|IF_SM},
    /*   29 */ {I_ADC, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+13986, IF_386|IF_SM},
    /*   30 */ {I_ADC, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17525, IF_386|IF_SM},
    /*   31 */ {I_ADC, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+13992, IF_X64|IF_SM},
    /*   32 */ {I_ADC, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17530, IF_X64|IF_SM},
    /*   33 */ {I_ADC, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+17535, IF_8086|IF_SM},
    /*   34 */ {I_ADC, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+13998, IF_8086|IF_SM},
    /*   35 */ {I_ADC, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14004, IF_386|IF_SM},
    /*   36 */ {I_ADC, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14010, IF_X64|IF_SM},
    /*   37 */ {I_ADC, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+17535, IF_8086|IF_SM},
    /*   38 */ {I_ADC, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+13998, IF_8086|IF_SM},
    /*   39 */ {I_ADC, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14004, IF_386|IF_SM},
    /*   40 */ {I_ADD, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+19389, IF_8086|IF_SM},
    /*   41 */ {I_ADD, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+19389, IF_8086},
    /*   42 */ {I_ADD, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+17540, IF_8086|IF_SM},
    /*   43 */ {I_ADD, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17540, IF_8086},
    /*   44 */ {I_ADD, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+17545, IF_386|IF_SM},
    /*   45 */ {I_ADD, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17545, IF_386},
    /*   46 */ {I_ADD, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+17550, IF_X64|IF_SM},
    /*   47 */ {I_ADD, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17550, IF_X64},
    /*   48 */ {I_ADD, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+11323, IF_8086|IF_SM},
    /*   49 */ {I_ADD, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+11323, IF_8086},
    /*   50 */ {I_ADD, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+17555, IF_8086|IF_SM},
    /*   51 */ {I_ADD, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17555, IF_8086},
    /*   52 */ {I_ADD, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+17560, IF_386|IF_SM},
    /*   53 */ {I_ADD, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17560, IF_386},
    /*   54 */ {I_ADD, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+17565, IF_X64|IF_SM},
    /*   55 */ {I_ADD, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17565, IF_X64},
    /*   56 */ {I_ADD, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14016, IF_8086},
    /*   57 */ {I_ADD, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14022, IF_386},
    /*   58 */ {I_ADD, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14028, IF_X64},
    /*   59 */ {I_ADD, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+19393, IF_8086|IF_SM},
    /*   60 */ {I_ADD, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+14016, IF_8086|IF_SM},
    /*   61 */ {I_ADD, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+17570, IF_8086|IF_SM},
    /*   62 */ {I_ADD, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+14022, IF_386|IF_SM},
    /*   63 */ {I_ADD, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17575, IF_386|IF_SM},
    /*   64 */ {I_ADD, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+14028, IF_X64|IF_SM},
    /*   65 */ {I_ADD, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17580, IF_X64|IF_SM},
    /*   66 */ {I_ADD, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+17585, IF_8086|IF_SM},
    /*   67 */ {I_ADD, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14034, IF_8086|IF_SM},
    /*   68 */ {I_ADD, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14040, IF_386|IF_SM},
    /*   69 */ {I_ADD, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14046, IF_X64|IF_SM},
    /*   70 */ {I_ADD, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+17585, IF_8086|IF_SM},
    /*   71 */ {I_ADD, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14034, IF_8086|IF_SM},
    /*   72 */ {I_ADD, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14040, IF_386|IF_SM},
    /*   73 */ {I_AND, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+19397, IF_8086|IF_SM},
    /*   74 */ {I_AND, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+19397, IF_8086},
    /*   75 */ {I_AND, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+17590, IF_8086|IF_SM},
    /*   76 */ {I_AND, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17590, IF_8086},
    /*   77 */ {I_AND, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+17595, IF_386|IF_SM},
    /*   78 */ {I_AND, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17595, IF_386},
    /*   79 */ {I_AND, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+17600, IF_X64|IF_SM},
    /*   80 */ {I_AND, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17600, IF_X64},
    /*   81 */ {I_AND, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+11610, IF_8086|IF_SM},
    /*   82 */ {I_AND, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+11610, IF_8086},
    /*   83 */ {I_AND, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+17605, IF_8086|IF_SM},
    /*   84 */ {I_AND, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17605, IF_8086},
    /*   85 */ {I_AND, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+17610, IF_386|IF_SM},
    /*   86 */ {I_AND, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17610, IF_386},
    /*   87 */ {I_AND, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+17615, IF_X64|IF_SM},
    /*   88 */ {I_AND, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17615, IF_X64},
    /*   89 */ {I_AND, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14052, IF_8086},
    /*   90 */ {I_AND, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14058, IF_386},
    /*   91 */ {I_AND, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14064, IF_X64},
    /*   92 */ {I_AND, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+19401, IF_8086|IF_SM},
    /*   93 */ {I_AND, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+14052, IF_8086|IF_SM},
    /*   94 */ {I_AND, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+17620, IF_8086|IF_SM},
    /*   95 */ {I_AND, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+14058, IF_386|IF_SM},
    /*   96 */ {I_AND, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17625, IF_386|IF_SM},
    /*   97 */ {I_AND, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+14064, IF_X64|IF_SM},
    /*   98 */ {I_AND, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17630, IF_X64|IF_SM},
    /*   99 */ {I_AND, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+17635, IF_8086|IF_SM},
    /*  100 */ {I_AND, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14070, IF_8086|IF_SM},
    /*  101 */ {I_AND, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14076, IF_386|IF_SM},
    /*  102 */ {I_AND, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14082, IF_X64|IF_SM},
    /*  103 */ {I_AND, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+17635, IF_8086|IF_SM},
    /*  104 */ {I_AND, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14070, IF_8086|IF_SM},
    /*  105 */ {I_AND, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14076, IF_386|IF_SM},
    /*  106 */ {I_ARPL, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+19405, IF_286|IF_PROT|IF_SM|IF_NOLONG},
    /*  107 */ {I_ARPL, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19405, IF_286|IF_PROT|IF_NOLONG},
    /*  108 */ {I_BOUND, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+17640, IF_186|IF_NOLONG},
    /*  109 */ {I_BOUND, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+17645, IF_386|IF_NOLONG},
    /*  110 */ {I_BSF, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14088, IF_386|IF_SM},
    /*  111 */ {I_BSF, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14088, IF_386},
    /*  112 */ {I_BSF, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14094, IF_386|IF_SM},
    /*  113 */ {I_BSF, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14094, IF_386},
    /*  114 */ {I_BSF, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+14100, IF_X64|IF_SM},
    /*  115 */ {I_BSF, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14100, IF_X64},
    /*  116 */ {I_BSR, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14106, IF_386|IF_SM},
    /*  117 */ {I_BSR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14106, IF_386},
    /*  118 */ {I_BSR, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14112, IF_386|IF_SM},
    /*  119 */ {I_BSR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14112, IF_386},
    /*  120 */ {I_BSR, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+14118, IF_X64|IF_SM},
    /*  121 */ {I_BSR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14118, IF_X64},
    /*  122 */ {I_BSWAP, 1, {REG32,0,0,0,0}, nasm_bytecodes+14124, IF_486},
    /*  123 */ {I_BSWAP, 1, {REG64,0,0,0,0}, nasm_bytecodes+14130, IF_X64},
    /*  124 */ {I_BT, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14136, IF_386|IF_SM},
    /*  125 */ {I_BT, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14136, IF_386},
    /*  126 */ {I_BT, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14142, IF_386|IF_SM},
    /*  127 */ {I_BT, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14142, IF_386},
    /*  128 */ {I_BT, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+14148, IF_X64|IF_SM},
    /*  129 */ {I_BT, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14148, IF_X64},
    /*  130 */ {I_BT, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+6735, IF_386|IF_SB},
    /*  131 */ {I_BT, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+6742, IF_386|IF_SB},
    /*  132 */ {I_BT, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+6749, IF_X64|IF_SB},
    /*  133 */ {I_BTC, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14154, IF_386|IF_SM},
    /*  134 */ {I_BTC, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14154, IF_386},
    /*  135 */ {I_BTC, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14160, IF_386|IF_SM},
    /*  136 */ {I_BTC, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14160, IF_386},
    /*  137 */ {I_BTC, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+14166, IF_X64|IF_SM},
    /*  138 */ {I_BTC, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14166, IF_X64},
    /*  139 */ {I_BTC, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+6756, IF_386|IF_SB},
    /*  140 */ {I_BTC, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+6763, IF_386|IF_SB},
    /*  141 */ {I_BTC, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+6770, IF_X64|IF_SB},
    /*  142 */ {I_BTR, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14172, IF_386|IF_SM},
    /*  143 */ {I_BTR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14172, IF_386},
    /*  144 */ {I_BTR, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14178, IF_386|IF_SM},
    /*  145 */ {I_BTR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14178, IF_386},
    /*  146 */ {I_BTR, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+14184, IF_X64|IF_SM},
    /*  147 */ {I_BTR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14184, IF_X64},
    /*  148 */ {I_BTR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+6777, IF_386|IF_SB},
    /*  149 */ {I_BTR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+6784, IF_386|IF_SB},
    /*  150 */ {I_BTR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+6791, IF_X64|IF_SB},
    /*  151 */ {I_BTS, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14190, IF_386|IF_SM},
    /*  152 */ {I_BTS, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14190, IF_386},
    /*  153 */ {I_BTS, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14196, IF_386|IF_SM},
    /*  154 */ {I_BTS, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14196, IF_386},
    /*  155 */ {I_BTS, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+14202, IF_X64|IF_SM},
    /*  156 */ {I_BTS, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14202, IF_X64},
    /*  157 */ {I_BTS, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+6798, IF_386|IF_SB},
    /*  158 */ {I_BTS, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+6805, IF_386|IF_SB},
    /*  159 */ {I_BTS, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+6812, IF_X64|IF_SB},
    /*  160 */ {I_CALL, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+17650, IF_8086},
    /*  161 */ {I_CALL, 1, {IMMEDIATE|NEAR,0,0,0,0}, nasm_bytecodes+17650, IF_8086},
    /*  162 */ {I_CALL, 1, {IMMEDIATE|BITS16,0,0,0,0}, nasm_bytecodes+17655, IF_8086},
    /*  163 */ {I_CALL, 1, {IMMEDIATE|BITS16|NEAR,0,0,0,0}, nasm_bytecodes+17655, IF_8086},
    /*  164 */ {I_CALL, 1, {IMMEDIATE|BITS32,0,0,0,0}, nasm_bytecodes+17660, IF_386},
    /*  165 */ {I_CALL, 1, {IMMEDIATE|BITS32|NEAR,0,0,0,0}, nasm_bytecodes+17660, IF_386},
    /*  166 */ {I_CALL, 2, {IMMEDIATE|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14226, IF_8086|IF_NOLONG},
    /*  167 */ {I_CALL, 2, {IMMEDIATE|BITS16|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14232, IF_8086|IF_NOLONG},
    /*  168 */ {I_CALL, 2, {IMMEDIATE|COLON,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14232, IF_8086|IF_NOLONG},
    /*  169 */ {I_CALL, 2, {IMMEDIATE|BITS32|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14238, IF_386|IF_NOLONG},
    /*  170 */ {I_CALL, 2, {IMMEDIATE|COLON,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14238, IF_386|IF_NOLONG},
    /*  171 */ {I_CALL, 1, {MEMORY|FAR,0,0,0,0}, nasm_bytecodes+17665, IF_8086|IF_NOLONG},
    /*  172 */ {I_CALL, 1, {MEMORY|FAR,0,0,0,0}, nasm_bytecodes+17670, IF_X64},
    /*  173 */ {I_CALL, 1, {MEMORY|BITS16|FAR,0,0,0,0}, nasm_bytecodes+17675, IF_8086},
    /*  174 */ {I_CALL, 1, {MEMORY|BITS32|FAR,0,0,0,0}, nasm_bytecodes+17680, IF_386},
    /*  175 */ {I_CALL, 1, {MEMORY|BITS64|FAR,0,0,0,0}, nasm_bytecodes+17670, IF_X64},
    /*  176 */ {I_CALL, 1, {MEMORY|NEAR,0,0,0,0}, nasm_bytecodes+17685, IF_8086},
    /*  177 */ {I_CALL, 1, {MEMORY|BITS16|NEAR,0,0,0,0}, nasm_bytecodes+17690, IF_8086},
    /*  178 */ {I_CALL, 1, {MEMORY|BITS32|NEAR,0,0,0,0}, nasm_bytecodes+17695, IF_386|IF_NOLONG},
    /*  179 */ {I_CALL, 1, {MEMORY|BITS64|NEAR,0,0,0,0}, nasm_bytecodes+17700, IF_X64},
    /*  180 */ {I_CALL, 1, {REG16,0,0,0,0}, nasm_bytecodes+17690, IF_8086},
    /*  181 */ {I_CALL, 1, {REG32,0,0,0,0}, nasm_bytecodes+17695, IF_386|IF_NOLONG},
    /*  182 */ {I_CALL, 1, {REG64,0,0,0,0}, nasm_bytecodes+17705, IF_X64},
    /*  183 */ {I_CALL, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+17685, IF_8086},
    /*  184 */ {I_CALL, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+17690, IF_8086},
    /*  185 */ {I_CALL, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+17695, IF_386|IF_NOLONG},
    /*  186 */ {I_CALL, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+17705, IF_X64},
    /*  187 */ {I_CBW, 0, {0,0,0,0,0}, nasm_bytecodes+19417, IF_8086},
    /*  188 */ {I_CDQ, 0, {0,0,0,0,0}, nasm_bytecodes+19421, IF_386},
    /*  189 */ {I_CDQE, 0, {0,0,0,0,0}, nasm_bytecodes+19425, IF_X64},
    /*  190 */ {I_CLC, 0, {0,0,0,0,0}, nasm_bytecodes+19137, IF_8086},
    /*  191 */ {I_CLD, 0, {0,0,0,0,0}, nasm_bytecodes+20419, IF_8086},
    /*  192 */ {I_CLGI, 0, {0,0,0,0,0}, nasm_bytecodes+17710, IF_X64|IF_AMD},
    /*  193 */ {I_CLI, 0, {0,0,0,0,0}, nasm_bytecodes+20422, IF_8086},
    /*  194 */ {I_CLTS, 0, {0,0,0,0,0}, nasm_bytecodes+19429, IF_286|IF_PRIV},
    /*  195 */ {I_CMC, 0, {0,0,0,0,0}, nasm_bytecodes+20425, IF_8086},
    /*  196 */ {I_CMP, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+19433, IF_8086|IF_SM},
    /*  197 */ {I_CMP, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+19433, IF_8086},
    /*  198 */ {I_CMP, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+17715, IF_8086|IF_SM},
    /*  199 */ {I_CMP, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17715, IF_8086},
    /*  200 */ {I_CMP, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+17720, IF_386|IF_SM},
    /*  201 */ {I_CMP, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17720, IF_386},
    /*  202 */ {I_CMP, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+17725, IF_X64|IF_SM},
    /*  203 */ {I_CMP, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17725, IF_X64},
    /*  204 */ {I_CMP, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+11568, IF_8086|IF_SM},
    /*  205 */ {I_CMP, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+11568, IF_8086},
    /*  206 */ {I_CMP, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+17730, IF_8086|IF_SM},
    /*  207 */ {I_CMP, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+17730, IF_8086},
    /*  208 */ {I_CMP, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+17735, IF_386|IF_SM},
    /*  209 */ {I_CMP, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+17735, IF_386},
    /*  210 */ {I_CMP, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+17740, IF_X64|IF_SM},
    /*  211 */ {I_CMP, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+17740, IF_X64},
    /*  212 */ {I_CMP, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14244, IF_8086},
    /*  213 */ {I_CMP, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14250, IF_386},
    /*  214 */ {I_CMP, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14256, IF_X64},
    /*  215 */ {I_CMP, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+19437, IF_8086|IF_SM},
    /*  216 */ {I_CMP, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+14244, IF_8086|IF_SM},
    /*  217 */ {I_CMP, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+17745, IF_8086|IF_SM},
    /*  218 */ {I_CMP, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+14250, IF_386|IF_SM},
    /*  219 */ {I_CMP, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17750, IF_386|IF_SM},
    /*  220 */ {I_CMP, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+14256, IF_X64|IF_SM},
    /*  221 */ {I_CMP, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+17755, IF_X64|IF_SM},
    /*  222 */ {I_CMP, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+17760, IF_8086|IF_SM},
    /*  223 */ {I_CMP, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14262, IF_8086|IF_SM},
    /*  224 */ {I_CMP, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14268, IF_386|IF_SM},
    /*  225 */ {I_CMP, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14274, IF_X64|IF_SM},
    /*  226 */ {I_CMP, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+17760, IF_8086|IF_SM},
    /*  227 */ {I_CMP, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14262, IF_8086|IF_SM},
    /*  228 */ {I_CMP, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14268, IF_386|IF_SM},
    /*  229 */ {I_CMPSB, 0, {0,0,0,0,0}, nasm_bytecodes+19441, IF_8086},
    /*  230 */ {I_CMPSD, 0, {0,0,0,0,0}, nasm_bytecodes+17765, IF_386},
    /*  231 */ {I_CMPSQ, 0, {0,0,0,0,0}, nasm_bytecodes+17770, IF_X64},
    /*  232 */ {I_CMPSW, 0, {0,0,0,0,0}, nasm_bytecodes+17775, IF_8086},
    /*  233 */ {I_CMPXCHG, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+17780, IF_PENT|IF_SM},
    /*  234 */ {I_CMPXCHG, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+17780, IF_PENT},
    /*  235 */ {I_CMPXCHG, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+14280, IF_PENT|IF_SM},
    /*  236 */ {I_CMPXCHG, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14280, IF_PENT},
    /*  237 */ {I_CMPXCHG, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+14286, IF_PENT|IF_SM},
    /*  238 */ {I_CMPXCHG, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14286, IF_PENT},
    /*  239 */ {I_CMPXCHG, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+14292, IF_X64|IF_SM},
    /*  240 */ {I_CMPXCHG, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14292, IF_X64},
    /*  241 */ {I_CMPXCHG8B, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+14311, IF_PENT},
    /*  242 */ {I_CMPXCHG16B, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+14310, IF_X64},
    /*  243 */ {I_CPUID, 0, {0,0,0,0,0}, nasm_bytecodes+19445, IF_PENT},
    /*  244 */ {I_CPU_READ, 0, {0,0,0,0,0}, nasm_bytecodes+19449, IF_PENT|IF_CYRIX},
    /*  245 */ {I_CPU_WRITE, 0, {0,0,0,0,0}, nasm_bytecodes+19453, IF_PENT|IF_CYRIX},
    /*  246 */ {I_CQO, 0, {0,0,0,0,0}, nasm_bytecodes+19457, IF_X64},
    /*  247 */ {I_CWD, 0, {0,0,0,0,0}, nasm_bytecodes+19461, IF_8086},
    /*  248 */ {I_CWDE, 0, {0,0,0,0,0}, nasm_bytecodes+19465, IF_386},
    /*  249 */ {I_DAA, 0, {0,0,0,0,0}, nasm_bytecodes+20428, IF_8086|IF_NOLONG},
    /*  250 */ {I_DAS, 0, {0,0,0,0,0}, nasm_bytecodes+20431, IF_8086|IF_NOLONG},
    /*  251 */ {I_DEC, 1, {REG16,0,0,0,0}, nasm_bytecodes+19469, IF_8086|IF_NOLONG},
    /*  252 */ {I_DEC, 1, {REG32,0,0,0,0}, nasm_bytecodes+19473, IF_386|IF_NOLONG},
    /*  253 */ {I_DEC, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+19477, IF_8086},
    /*  254 */ {I_DEC, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17790, IF_8086},
    /*  255 */ {I_DEC, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17795, IF_386},
    /*  256 */ {I_DEC, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17800, IF_X64},
    /*  257 */ {I_DIV, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+19481, IF_8086},
    /*  258 */ {I_DIV, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17805, IF_8086},
    /*  259 */ {I_DIV, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17810, IF_386},
    /*  260 */ {I_DIV, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17815, IF_X64},
    /*  261 */ {I_DMINT, 0, {0,0,0,0,0}, nasm_bytecodes+19485, IF_P6|IF_CYRIX},
    /*  262 */ {I_EMMS, 0, {0,0,0,0,0}, nasm_bytecodes+19489, IF_PENT|IF_MMX},
    /*  263 */ {I_ENTER, 2, {IMMEDIATE,IMMEDIATE,0,0,0}, nasm_bytecodes+17820, IF_186},
    /*  264 */ {I_EQU, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+5445, IF_8086},
    /*  265 */ {I_EQU, 2, {IMMEDIATE|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+5445, IF_8086},
    /*  266 */ {I_F2XM1, 0, {0,0,0,0,0}, nasm_bytecodes+19493, IF_8086|IF_FPU},
    /*  267 */ {I_FABS, 0, {0,0,0,0,0}, nasm_bytecodes+19497, IF_8086|IF_FPU},
    /*  268 */ {I_FADD, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19501, IF_8086|IF_FPU},
    /*  269 */ {I_FADD, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19505, IF_8086|IF_FPU},
    /*  270 */ {I_FADD, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+17825, IF_8086|IF_FPU},
    /*  271 */ {I_FADD, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17830, IF_8086|IF_FPU},
    /*  272 */ {I_FADD, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+17825, IF_8086|IF_FPU},
    /*  273 */ {I_FADD, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17835, IF_8086|IF_FPU},
    /*  274 */ {I_FADDP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17840, IF_8086|IF_FPU},
    /*  275 */ {I_FADDP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+17840, IF_8086|IF_FPU},
    /*  276 */ {I_FBLD, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+19513, IF_8086|IF_FPU},
    /*  277 */ {I_FBLD, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19513, IF_8086|IF_FPU},
    /*  278 */ {I_FBSTP, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+19517, IF_8086|IF_FPU},
    /*  279 */ {I_FBSTP, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19517, IF_8086|IF_FPU},
    /*  280 */ {I_FCHS, 0, {0,0,0,0,0}, nasm_bytecodes+19521, IF_8086|IF_FPU},
    /*  281 */ {I_FCLEX, 0, {0,0,0,0,0}, nasm_bytecodes+17845, IF_8086|IF_FPU},
    /*  282 */ {I_FCMOVB, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17850, IF_P6|IF_FPU},
    /*  283 */ {I_FCMOVB, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17855, IF_P6|IF_FPU},
    /*  284 */ {I_FCMOVBE, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17860, IF_P6|IF_FPU},
    /*  285 */ {I_FCMOVBE, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17865, IF_P6|IF_FPU},
    /*  286 */ {I_FCMOVE, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17870, IF_P6|IF_FPU},
    /*  287 */ {I_FCMOVE, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17875, IF_P6|IF_FPU},
    /*  288 */ {I_FCMOVNB, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17880, IF_P6|IF_FPU},
    /*  289 */ {I_FCMOVNB, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17885, IF_P6|IF_FPU},
    /*  290 */ {I_FCMOVNBE, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17890, IF_P6|IF_FPU},
    /*  291 */ {I_FCMOVNBE, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17895, IF_P6|IF_FPU},
    /*  292 */ {I_FCMOVNE, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17900, IF_P6|IF_FPU},
    /*  293 */ {I_FCMOVNE, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17905, IF_P6|IF_FPU},
    /*  294 */ {I_FCMOVNU, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17910, IF_P6|IF_FPU},
    /*  295 */ {I_FCMOVNU, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17915, IF_P6|IF_FPU},
    /*  296 */ {I_FCMOVU, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17920, IF_P6|IF_FPU},
    /*  297 */ {I_FCMOVU, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17925, IF_P6|IF_FPU},
    /*  298 */ {I_FCOM, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19557, IF_8086|IF_FPU},
    /*  299 */ {I_FCOM, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19561, IF_8086|IF_FPU},
    /*  300 */ {I_FCOM, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17930, IF_8086|IF_FPU},
    /*  301 */ {I_FCOM, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17935, IF_8086|IF_FPU},
    /*  302 */ {I_FCOMI, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17940, IF_P6|IF_FPU},
    /*  303 */ {I_FCOMI, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17945, IF_P6|IF_FPU},
    /*  304 */ {I_FCOMIP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17950, IF_P6|IF_FPU},
    /*  305 */ {I_FCOMIP, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17955, IF_P6|IF_FPU},
    /*  306 */ {I_FCOMP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19577, IF_8086|IF_FPU},
    /*  307 */ {I_FCOMP, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19581, IF_8086|IF_FPU},
    /*  308 */ {I_FCOMP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17960, IF_8086|IF_FPU},
    /*  309 */ {I_FCOMP, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17965, IF_8086|IF_FPU},
    /*  310 */ {I_FCOMPP, 0, {0,0,0,0,0}, nasm_bytecodes+19589, IF_8086|IF_FPU},
    /*  311 */ {I_FCOS, 0, {0,0,0,0,0}, nasm_bytecodes+19593, IF_386|IF_FPU},
    /*  312 */ {I_FDECSTP, 0, {0,0,0,0,0}, nasm_bytecodes+19597, IF_8086|IF_FPU},
    /*  313 */ {I_FDISI, 0, {0,0,0,0,0}, nasm_bytecodes+17970, IF_8086|IF_FPU},
    /*  314 */ {I_FDIV, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19601, IF_8086|IF_FPU},
    /*  315 */ {I_FDIV, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19605, IF_8086|IF_FPU},
    /*  316 */ {I_FDIV, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+17975, IF_8086|IF_FPU},
    /*  317 */ {I_FDIV, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17980, IF_8086|IF_FPU},
    /*  318 */ {I_FDIV, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+17975, IF_8086|IF_FPU},
    /*  319 */ {I_FDIV, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+17985, IF_8086|IF_FPU},
    /*  320 */ {I_FDIVP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+17990, IF_8086|IF_FPU},
    /*  321 */ {I_FDIVP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+17990, IF_8086|IF_FPU},
    /*  322 */ {I_FDIVR, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19613, IF_8086|IF_FPU},
    /*  323 */ {I_FDIVR, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19617, IF_8086|IF_FPU},
    /*  324 */ {I_FDIVR, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+17995, IF_8086|IF_FPU},
    /*  325 */ {I_FDIVR, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+17995, IF_8086|IF_FPU},
    /*  326 */ {I_FDIVR, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18000, IF_8086|IF_FPU},
    /*  327 */ {I_FDIVR, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18005, IF_8086|IF_FPU},
    /*  328 */ {I_FDIVRP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18010, IF_8086|IF_FPU},
    /*  329 */ {I_FDIVRP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18010, IF_8086|IF_FPU},
    /*  330 */ {I_FEMMS, 0, {0,0,0,0,0}, nasm_bytecodes+19625, IF_PENT|IF_3DNOW},
    /*  331 */ {I_FENI, 0, {0,0,0,0,0}, nasm_bytecodes+18015, IF_8086|IF_FPU},
    /*  332 */ {I_FFREE, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18020, IF_8086|IF_FPU},
    /*  333 */ {I_FFREE, 0, {0,0,0,0,0}, nasm_bytecodes+19629, IF_8086|IF_FPU},
    /*  334 */ {I_FFREEP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18025, IF_286|IF_FPU|IF_UNDOC},
    /*  335 */ {I_FFREEP, 0, {0,0,0,0,0}, nasm_bytecodes+19633, IF_286|IF_FPU|IF_UNDOC},
    /*  336 */ {I_FIADD, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19637, IF_8086|IF_FPU},
    /*  337 */ {I_FIADD, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19641, IF_8086|IF_FPU},
    /*  338 */ {I_FICOM, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19645, IF_8086|IF_FPU},
    /*  339 */ {I_FICOM, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19649, IF_8086|IF_FPU},
    /*  340 */ {I_FICOMP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19653, IF_8086|IF_FPU},
    /*  341 */ {I_FICOMP, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19657, IF_8086|IF_FPU},
    /*  342 */ {I_FIDIV, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19661, IF_8086|IF_FPU},
    /*  343 */ {I_FIDIV, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19665, IF_8086|IF_FPU},
    /*  344 */ {I_FIDIVR, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19669, IF_8086|IF_FPU},
    /*  345 */ {I_FIDIVR, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19673, IF_8086|IF_FPU},
    /*  346 */ {I_FILD, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19677, IF_8086|IF_FPU},
    /*  347 */ {I_FILD, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19681, IF_8086|IF_FPU},
    /*  348 */ {I_FILD, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19685, IF_8086|IF_FPU},
    /*  349 */ {I_FIMUL, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19689, IF_8086|IF_FPU},
    /*  350 */ {I_FIMUL, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19693, IF_8086|IF_FPU},
    /*  351 */ {I_FINCSTP, 0, {0,0,0,0,0}, nasm_bytecodes+19697, IF_8086|IF_FPU},
    /*  352 */ {I_FINIT, 0, {0,0,0,0,0}, nasm_bytecodes+18030, IF_8086|IF_FPU},
    /*  353 */ {I_FIST, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19701, IF_8086|IF_FPU},
    /*  354 */ {I_FIST, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19705, IF_8086|IF_FPU},
    /*  355 */ {I_FISTP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19709, IF_8086|IF_FPU},
    /*  356 */ {I_FISTP, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19713, IF_8086|IF_FPU},
    /*  357 */ {I_FISTP, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19717, IF_8086|IF_FPU},
    /*  358 */ {I_FISTTP, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19721, IF_PRESCOTT|IF_FPU},
    /*  359 */ {I_FISTTP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19725, IF_PRESCOTT|IF_FPU},
    /*  360 */ {I_FISTTP, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19729, IF_PRESCOTT|IF_FPU},
    /*  361 */ {I_FISUB, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19733, IF_8086|IF_FPU},
    /*  362 */ {I_FISUB, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19737, IF_8086|IF_FPU},
    /*  363 */ {I_FISUBR, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19741, IF_8086|IF_FPU},
    /*  364 */ {I_FISUBR, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19745, IF_8086|IF_FPU},
    /*  365 */ {I_FLD, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19749, IF_8086|IF_FPU},
    /*  366 */ {I_FLD, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19753, IF_8086|IF_FPU},
    /*  367 */ {I_FLD, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+19757, IF_8086|IF_FPU},
    /*  368 */ {I_FLD, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18035, IF_8086|IF_FPU},
    /*  369 */ {I_FLD1, 0, {0,0,0,0,0}, nasm_bytecodes+19765, IF_8086|IF_FPU},
    /*  370 */ {I_FLDCW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19769, IF_8086|IF_FPU|IF_SW},
    /*  371 */ {I_FLDENV, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19773, IF_8086|IF_FPU},
    /*  372 */ {I_FLDL2E, 0, {0,0,0,0,0}, nasm_bytecodes+19777, IF_8086|IF_FPU},
    /*  373 */ {I_FLDL2T, 0, {0,0,0,0,0}, nasm_bytecodes+19781, IF_8086|IF_FPU},
    /*  374 */ {I_FLDLG2, 0, {0,0,0,0,0}, nasm_bytecodes+19785, IF_8086|IF_FPU},
    /*  375 */ {I_FLDLN2, 0, {0,0,0,0,0}, nasm_bytecodes+19789, IF_8086|IF_FPU},
    /*  376 */ {I_FLDPI, 0, {0,0,0,0,0}, nasm_bytecodes+19793, IF_8086|IF_FPU},
    /*  377 */ {I_FLDZ, 0, {0,0,0,0,0}, nasm_bytecodes+19797, IF_8086|IF_FPU},
    /*  378 */ {I_FMUL, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19801, IF_8086|IF_FPU},
    /*  379 */ {I_FMUL, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19805, IF_8086|IF_FPU},
    /*  380 */ {I_FMUL, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+18040, IF_8086|IF_FPU},
    /*  381 */ {I_FMUL, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18040, IF_8086|IF_FPU},
    /*  382 */ {I_FMUL, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18045, IF_8086|IF_FPU},
    /*  383 */ {I_FMUL, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18050, IF_8086|IF_FPU},
    /*  384 */ {I_FMULP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18055, IF_8086|IF_FPU},
    /*  385 */ {I_FMULP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18055, IF_8086|IF_FPU},
    /*  386 */ {I_FNCLEX, 0, {0,0,0,0,0}, nasm_bytecodes+17846, IF_8086|IF_FPU},
    /*  387 */ {I_FNDISI, 0, {0,0,0,0,0}, nasm_bytecodes+17971, IF_8086|IF_FPU},
    /*  388 */ {I_FNENI, 0, {0,0,0,0,0}, nasm_bytecodes+18016, IF_8086|IF_FPU},
    /*  389 */ {I_FNINIT, 0, {0,0,0,0,0}, nasm_bytecodes+18031, IF_8086|IF_FPU},
    /*  390 */ {I_FNOP, 0, {0,0,0,0,0}, nasm_bytecodes+19813, IF_8086|IF_FPU},
    /*  391 */ {I_FNSAVE, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18061, IF_8086|IF_FPU},
    /*  392 */ {I_FNSTCW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18071, IF_8086|IF_FPU|IF_SW},
    /*  393 */ {I_FNSTENV, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18076, IF_8086|IF_FPU},
    /*  394 */ {I_FNSTSW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18086, IF_8086|IF_FPU|IF_SW},
    /*  395 */ {I_FNSTSW, 1, {REG_AX,0,0,0,0}, nasm_bytecodes+18091, IF_286|IF_FPU},
    /*  396 */ {I_FPATAN, 0, {0,0,0,0,0}, nasm_bytecodes+19817, IF_8086|IF_FPU},
    /*  397 */ {I_FPREM, 0, {0,0,0,0,0}, nasm_bytecodes+19821, IF_8086|IF_FPU},
    /*  398 */ {I_FPREM1, 0, {0,0,0,0,0}, nasm_bytecodes+19825, IF_386|IF_FPU},
    /*  399 */ {I_FPTAN, 0, {0,0,0,0,0}, nasm_bytecodes+19829, IF_8086|IF_FPU},
    /*  400 */ {I_FRNDINT, 0, {0,0,0,0,0}, nasm_bytecodes+19833, IF_8086|IF_FPU},
    /*  401 */ {I_FRSTOR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19837, IF_8086|IF_FPU},
    /*  402 */ {I_FSAVE, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18060, IF_8086|IF_FPU},
    /*  403 */ {I_FSCALE, 0, {0,0,0,0,0}, nasm_bytecodes+19841, IF_8086|IF_FPU},
    /*  404 */ {I_FSETPM, 0, {0,0,0,0,0}, nasm_bytecodes+19845, IF_286|IF_FPU},
    /*  405 */ {I_FSIN, 0, {0,0,0,0,0}, nasm_bytecodes+19849, IF_386|IF_FPU},
    /*  406 */ {I_FSINCOS, 0, {0,0,0,0,0}, nasm_bytecodes+19853, IF_386|IF_FPU},
    /*  407 */ {I_FSQRT, 0, {0,0,0,0,0}, nasm_bytecodes+19857, IF_8086|IF_FPU},
    /*  408 */ {I_FST, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19861, IF_8086|IF_FPU},
    /*  409 */ {I_FST, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19865, IF_8086|IF_FPU},
    /*  410 */ {I_FST, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18065, IF_8086|IF_FPU},
    /*  411 */ {I_FSTCW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18070, IF_8086|IF_FPU|IF_SW},
    /*  412 */ {I_FSTENV, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18075, IF_8086|IF_FPU},
    /*  413 */ {I_FSTP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19873, IF_8086|IF_FPU},
    /*  414 */ {I_FSTP, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19877, IF_8086|IF_FPU},
    /*  415 */ {I_FSTP, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+19881, IF_8086|IF_FPU},
    /*  416 */ {I_FSTP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18080, IF_8086|IF_FPU},
    /*  417 */ {I_FSTSW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18085, IF_8086|IF_FPU|IF_SW},
    /*  418 */ {I_FSTSW, 1, {REG_AX,0,0,0,0}, nasm_bytecodes+18090, IF_286|IF_FPU},
    /*  419 */ {I_FSUB, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19889, IF_8086|IF_FPU},
    /*  420 */ {I_FSUB, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19893, IF_8086|IF_FPU},
    /*  421 */ {I_FSUB, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+18095, IF_8086|IF_FPU},
    /*  422 */ {I_FSUB, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18095, IF_8086|IF_FPU},
    /*  423 */ {I_FSUB, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18100, IF_8086|IF_FPU},
    /*  424 */ {I_FSUB, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18105, IF_8086|IF_FPU},
    /*  425 */ {I_FSUBP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18110, IF_8086|IF_FPU},
    /*  426 */ {I_FSUBP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18110, IF_8086|IF_FPU},
    /*  427 */ {I_FSUBR, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+19901, IF_8086|IF_FPU},
    /*  428 */ {I_FSUBR, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+19905, IF_8086|IF_FPU},
    /*  429 */ {I_FSUBR, 1, {FPUREG|TO,0,0,0,0}, nasm_bytecodes+18115, IF_8086|IF_FPU},
    /*  430 */ {I_FSUBR, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18115, IF_8086|IF_FPU},
    /*  431 */ {I_FSUBR, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18120, IF_8086|IF_FPU},
    /*  432 */ {I_FSUBR, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18125, IF_8086|IF_FPU},
    /*  433 */ {I_FSUBRP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18130, IF_8086|IF_FPU},
    /*  434 */ {I_FSUBRP, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18130, IF_8086|IF_FPU},
    /*  435 */ {I_FTST, 0, {0,0,0,0,0}, nasm_bytecodes+19913, IF_8086|IF_FPU},
    /*  436 */ {I_FUCOM, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18135, IF_386|IF_FPU},
    /*  437 */ {I_FUCOM, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18140, IF_386|IF_FPU},
    /*  438 */ {I_FUCOMI, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18145, IF_P6|IF_FPU},
    /*  439 */ {I_FUCOMI, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18150, IF_P6|IF_FPU},
    /*  440 */ {I_FUCOMIP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18155, IF_P6|IF_FPU},
    /*  441 */ {I_FUCOMIP, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18160, IF_P6|IF_FPU},
    /*  442 */ {I_FUCOMP, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18165, IF_386|IF_FPU},
    /*  443 */ {I_FUCOMP, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18170, IF_386|IF_FPU},
    /*  444 */ {I_FUCOMPP, 0, {0,0,0,0,0}, nasm_bytecodes+19933, IF_386|IF_FPU},
    /*  445 */ {I_FXAM, 0, {0,0,0,0,0}, nasm_bytecodes+19937, IF_8086|IF_FPU},
    /*  446 */ {I_FXCH, 1, {FPUREG,0,0,0,0}, nasm_bytecodes+18175, IF_8086|IF_FPU},
    /*  447 */ {I_FXCH, 2, {FPUREG,FPU0,0,0,0}, nasm_bytecodes+18175, IF_8086|IF_FPU},
    /*  448 */ {I_FXCH, 2, {FPU0,FPUREG,0,0,0}, nasm_bytecodes+18180, IF_8086|IF_FPU},
    /*  449 */ {I_FXTRACT, 0, {0,0,0,0,0}, nasm_bytecodes+19945, IF_8086|IF_FPU},
    /*  450 */ {I_FYL2X, 0, {0,0,0,0,0}, nasm_bytecodes+19949, IF_8086|IF_FPU},
    /*  451 */ {I_FYL2XP1, 0, {0,0,0,0,0}, nasm_bytecodes+19953, IF_8086|IF_FPU},
    /*  452 */ {I_HLT, 0, {0,0,0,0,0}, nasm_bytecodes+20434, IF_8086|IF_PRIV},
    /*  453 */ {I_IDIV, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+19957, IF_8086},
    /*  454 */ {I_IDIV, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18185, IF_8086},
    /*  455 */ {I_IDIV, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18190, IF_386},
    /*  456 */ {I_IDIV, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18195, IF_X64},
    /*  457 */ {I_IMUL, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+19961, IF_8086},
    /*  458 */ {I_IMUL, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18200, IF_8086},
    /*  459 */ {I_IMUL, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18205, IF_386},
    /*  460 */ {I_IMUL, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18210, IF_X64},
    /*  461 */ {I_IMUL, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14316, IF_386|IF_SM},
    /*  462 */ {I_IMUL, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14316, IF_386},
    /*  463 */ {I_IMUL, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14322, IF_386|IF_SM},
    /*  464 */ {I_IMUL, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14322, IF_386},
    /*  465 */ {I_IMUL, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+14328, IF_X64|IF_SM},
    /*  466 */ {I_IMUL, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14328, IF_X64},
    /*  467 */ {I_IMUL, 3, {REG16,MEMORY,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14334, IF_186|IF_SM},
    /*  468 */ {I_IMUL, 3, {REG16,MEMORY,IMMEDIATE|BITS16,0,0}, nasm_bytecodes+14340, IF_186|IF_SM},
    /*  469 */ {I_IMUL, 3, {REG16,REG16,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14334, IF_186},
    /*  470 */ {I_IMUL, 3, {REG16,REG16,IMMEDIATE|BITS16,0,0}, nasm_bytecodes+14340, IF_186},
    /*  471 */ {I_IMUL, 3, {REG32,MEMORY,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14352, IF_386|IF_SM},
    /*  472 */ {I_IMUL, 3, {REG32,MEMORY,IMMEDIATE|BITS32,0,0}, nasm_bytecodes+14358, IF_386|IF_SM},
    /*  473 */ {I_IMUL, 3, {REG32,REG32,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14352, IF_386},
    /*  474 */ {I_IMUL, 3, {REG32,REG32,IMMEDIATE|BITS32,0,0}, nasm_bytecodes+14358, IF_386},
    /*  475 */ {I_IMUL, 3, {REG64,MEMORY,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14370, IF_X64|IF_SM},
    /*  476 */ {I_IMUL, 3, {REG64,MEMORY,IMMEDIATE|BITS32,0,0}, nasm_bytecodes+14376, IF_X64|IF_SM},
    /*  477 */ {I_IMUL, 3, {REG64,REG64,IMMEDIATE|BITS8,0,0}, nasm_bytecodes+14370, IF_X64},
    /*  478 */ {I_IMUL, 3, {REG64,REG64,IMMEDIATE|BITS32,0,0}, nasm_bytecodes+14376, IF_X64},
    /*  479 */ {I_IMUL, 2, {REG16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14388, IF_186},
    /*  480 */ {I_IMUL, 2, {REG16,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14394, IF_186},
    /*  481 */ {I_IMUL, 2, {REG32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14406, IF_386},
    /*  482 */ {I_IMUL, 2, {REG32,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14412, IF_386},
    /*  483 */ {I_IMUL, 2, {REG64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14424, IF_X64},
    /*  484 */ {I_IMUL, 2, {REG64,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14430, IF_X64},
    /*  485 */ {I_IN, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+19965, IF_8086|IF_SB},
    /*  486 */ {I_IN, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+18215, IF_8086|IF_SB},
    /*  487 */ {I_IN, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+18220, IF_386|IF_SB},
    /*  488 */ {I_IN, 2, {REG_AL,REG_DX,0,0,0}, nasm_bytecodes+20440, IF_8086},
    /*  489 */ {I_IN, 2, {REG_AX,REG_DX,0,0,0}, nasm_bytecodes+19969, IF_8086},
    /*  490 */ {I_IN, 2, {REG_EAX,REG_DX,0,0,0}, nasm_bytecodes+19973, IF_386},
    /*  491 */ {I_INC, 1, {REG16,0,0,0,0}, nasm_bytecodes+19977, IF_8086|IF_NOLONG},
    /*  492 */ {I_INC, 1, {REG32,0,0,0,0}, nasm_bytecodes+19981, IF_386|IF_NOLONG},
    /*  493 */ {I_INC, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+19985, IF_8086},
    /*  494 */ {I_INC, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18225, IF_8086},
    /*  495 */ {I_INC, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18230, IF_386},
    /*  496 */ {I_INC, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18235, IF_X64},
    /*  497 */ {I_INSB, 0, {0,0,0,0,0}, nasm_bytecodes+20443, IF_186},
    /*  498 */ {I_INSD, 0, {0,0,0,0,0}, nasm_bytecodes+19989, IF_386},
    /*  499 */ {I_INSW, 0, {0,0,0,0,0}, nasm_bytecodes+19993, IF_186},
    /*  500 */ {I_INT, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+19997, IF_8086|IF_SB},
    /*  501 */ {I_INT1, 0, {0,0,0,0,0}, nasm_bytecodes+20437, IF_386},
    /*  502 */ {I_INT3, 0, {0,0,0,0,0}, nasm_bytecodes+20446, IF_8086},
    /*  503 */ {I_INTO, 0, {0,0,0,0,0}, nasm_bytecodes+20449, IF_8086|IF_NOLONG},
    /*  504 */ {I_INVD, 0, {0,0,0,0,0}, nasm_bytecodes+20001, IF_486|IF_PRIV},
    /*  505 */ {I_INVLPG, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18240, IF_486|IF_PRIV},
    /*  506 */ {I_INVLPGA, 2, {REG_AX,REG_ECX,0,0,0}, nasm_bytecodes+14442, IF_X86_64|IF_AMD|IF_NOLONG},
    /*  507 */ {I_INVLPGA, 2, {REG_EAX,REG_ECX,0,0,0}, nasm_bytecodes+14448, IF_X86_64|IF_AMD},
    /*  508 */ {I_INVLPGA, 2, {REG_RAX,REG_ECX,0,0,0}, nasm_bytecodes+6819, IF_X64|IF_AMD},
    /*  509 */ {I_INVLPGA, 0, {0,0,0,0,0}, nasm_bytecodes+14449, IF_X86_64|IF_AMD},
    /*  510 */ {I_IRET, 0, {0,0,0,0,0}, nasm_bytecodes+20005, IF_8086},
    /*  511 */ {I_IRETD, 0, {0,0,0,0,0}, nasm_bytecodes+20009, IF_386},
    /*  512 */ {I_IRETQ, 0, {0,0,0,0,0}, nasm_bytecodes+20013, IF_X64},
    /*  513 */ {I_IRETW, 0, {0,0,0,0,0}, nasm_bytecodes+20017, IF_8086},
    /*  514 */ {I_JCXZ, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18245, IF_8086|IF_NOLONG},
    /*  515 */ {I_JECXZ, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18250, IF_386},
    /*  516 */ {I_JMP, 1, {IMMEDIATE|SHORT,0,0,0,0}, nasm_bytecodes+18256, IF_8086},
    /*  517 */ {I_JMP, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18260, IF_8086},
    /*  518 */ {I_JMP, 1, {IMMEDIATE|BITS16,0,0,0,0}, nasm_bytecodes+18265, IF_8086},
    /*  519 */ {I_JMP, 1, {IMMEDIATE|BITS32,0,0,0,0}, nasm_bytecodes+18270, IF_386},
    /*  520 */ {I_JMP, 2, {IMMEDIATE|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14472, IF_8086|IF_NOLONG},
    /*  521 */ {I_JMP, 2, {IMMEDIATE|BITS16|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14478, IF_8086|IF_NOLONG},
    /*  522 */ {I_JMP, 2, {IMMEDIATE|COLON,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14478, IF_8086|IF_NOLONG},
    /*  523 */ {I_JMP, 2, {IMMEDIATE|BITS32|COLON,IMMEDIATE,0,0,0}, nasm_bytecodes+14484, IF_386|IF_NOLONG},
    /*  524 */ {I_JMP, 2, {IMMEDIATE|COLON,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14484, IF_386|IF_NOLONG},
    /*  525 */ {I_JMP, 1, {MEMORY|FAR,0,0,0,0}, nasm_bytecodes+18275, IF_8086|IF_NOLONG},
    /*  526 */ {I_JMP, 1, {MEMORY|FAR,0,0,0,0}, nasm_bytecodes+18280, IF_X64},
    /*  527 */ {I_JMP, 1, {MEMORY|BITS16|FAR,0,0,0,0}, nasm_bytecodes+18285, IF_8086},
    /*  528 */ {I_JMP, 1, {MEMORY|BITS32|FAR,0,0,0,0}, nasm_bytecodes+18290, IF_386},
    /*  529 */ {I_JMP, 1, {MEMORY|BITS64|FAR,0,0,0,0}, nasm_bytecodes+18280, IF_X64},
    /*  530 */ {I_JMP, 1, {MEMORY|NEAR,0,0,0,0}, nasm_bytecodes+18295, IF_8086},
    /*  531 */ {I_JMP, 1, {MEMORY|BITS16|NEAR,0,0,0,0}, nasm_bytecodes+18300, IF_8086},
    /*  532 */ {I_JMP, 1, {MEMORY|BITS32|NEAR,0,0,0,0}, nasm_bytecodes+18305, IF_386|IF_NOLONG},
    /*  533 */ {I_JMP, 1, {MEMORY|BITS64|NEAR,0,0,0,0}, nasm_bytecodes+18310, IF_X64},
    /*  534 */ {I_JMP, 1, {REG16,0,0,0,0}, nasm_bytecodes+18300, IF_8086},
    /*  535 */ {I_JMP, 1, {REG32,0,0,0,0}, nasm_bytecodes+18305, IF_386|IF_NOLONG},
    /*  536 */ {I_JMP, 1, {REG64,0,0,0,0}, nasm_bytecodes+18310, IF_X64},
    /*  537 */ {I_JMP, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18295, IF_8086},
    /*  538 */ {I_JMP, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+18300, IF_8086},
    /*  539 */ {I_JMP, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+18305, IF_386|IF_NOLONG},
    /*  540 */ {I_JMP, 1, {MEMORY|BITS64,0,0,0,0}, nasm_bytecodes+18310, IF_X64},
    /*  541 */ {I_JMPE, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+14490, IF_IA64},
    /*  542 */ {I_JMPE, 1, {IMMEDIATE|BITS16,0,0,0,0}, nasm_bytecodes+14496, IF_IA64},
    /*  543 */ {I_JMPE, 1, {IMMEDIATE|BITS32,0,0,0,0}, nasm_bytecodes+14502, IF_IA64},
    /*  544 */ {I_JMPE, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+14508, IF_IA64},
    /*  545 */ {I_JMPE, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+14514, IF_IA64},
    /*  546 */ {I_JRCXZ, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18251, IF_X64},
    /*  547 */ {I_LAHF, 0, {0,0,0,0,0}, nasm_bytecodes+20452, IF_8086},
    /*  548 */ {I_LAR, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14520, IF_286|IF_PROT|IF_SW},
    /*  549 */ {I_LAR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14520, IF_286|IF_PROT},
    /*  550 */ {I_LAR, 2, {REG16,REG32,0,0,0}, nasm_bytecodes+14520, IF_386|IF_PROT},
    /*  551 */ {I_LAR, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14526, IF_386|IF_PROT|IF_SW},
    /*  552 */ {I_LAR, 2, {REG32,REG16,0,0,0}, nasm_bytecodes+14526, IF_386|IF_PROT},
    /*  553 */ {I_LAR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14526, IF_386|IF_PROT},
    /*  554 */ {I_LAR, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+14532, IF_X64|IF_PROT|IF_SW},
    /*  555 */ {I_LAR, 2, {REG64,REG16,0,0,0}, nasm_bytecodes+14532, IF_X64|IF_PROT},
    /*  556 */ {I_LAR, 2, {REG64,REG32,0,0,0}, nasm_bytecodes+14532, IF_X64|IF_PROT},
    /*  557 */ {I_LAR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14532, IF_X64|IF_PROT},
    /*  558 */ {I_LDS, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18315, IF_8086|IF_NOLONG},
    /*  559 */ {I_LDS, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18320, IF_386|IF_NOLONG},
    /*  560 */ {I_LEA, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18325, IF_8086},
    /*  561 */ {I_LEA, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18330, IF_386},
    /*  562 */ {I_LEA, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+18335, IF_X64},
    /*  563 */ {I_LEAVE, 0, {0,0,0,0,0}, nasm_bytecodes+18572, IF_186},
    /*  564 */ {I_LES, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18340, IF_8086|IF_NOLONG},
    /*  565 */ {I_LES, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18345, IF_386|IF_NOLONG},
    /*  566 */ {I_LFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+18350, IF_X64|IF_AMD},
    /*  567 */ {I_LFS, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14538, IF_386},
    /*  568 */ {I_LFS, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14544, IF_386},
    /*  569 */ {I_LGDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18355, IF_286|IF_PRIV},
    /*  570 */ {I_LGS, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14550, IF_386},
    /*  571 */ {I_LGS, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14556, IF_386},
    /*  572 */ {I_LIDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18360, IF_286|IF_PRIV},
    /*  573 */ {I_LLDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18365, IF_286|IF_PROT|IF_PRIV},
    /*  574 */ {I_LLDT, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+18365, IF_286|IF_PROT|IF_PRIV},
    /*  575 */ {I_LLDT, 1, {REG16,0,0,0,0}, nasm_bytecodes+18365, IF_286|IF_PROT|IF_PRIV},
    /*  576 */ {I_LMSW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18370, IF_286|IF_PRIV},
    /*  577 */ {I_LMSW, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+18370, IF_286|IF_PRIV},
    /*  578 */ {I_LMSW, 1, {REG16,0,0,0,0}, nasm_bytecodes+18370, IF_286|IF_PRIV},
    /*  579 */ {I_LOADALL, 0, {0,0,0,0,0}, nasm_bytecodes+20021, IF_386|IF_UNDOC},
    /*  580 */ {I_LOADALL286, 0, {0,0,0,0,0}, nasm_bytecodes+20025, IF_286|IF_UNDOC},
    /*  581 */ {I_LODSB, 0, {0,0,0,0,0}, nasm_bytecodes+20455, IF_8086},
    /*  582 */ {I_LODSD, 0, {0,0,0,0,0}, nasm_bytecodes+20029, IF_386},
    /*  583 */ {I_LODSQ, 0, {0,0,0,0,0}, nasm_bytecodes+20033, IF_X64},
    /*  584 */ {I_LODSW, 0, {0,0,0,0,0}, nasm_bytecodes+20037, IF_8086},
    /*  585 */ {I_LOOP, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18375, IF_8086},
    /*  586 */ {I_LOOP, 2, {IMMEDIATE,REG_CX,0,0,0}, nasm_bytecodes+18380, IF_8086|IF_NOLONG},
    /*  587 */ {I_LOOP, 2, {IMMEDIATE,REG_ECX,0,0,0}, nasm_bytecodes+18385, IF_386},
    /*  588 */ {I_LOOP, 2, {IMMEDIATE,REG_RCX,0,0,0}, nasm_bytecodes+18390, IF_X64},
    /*  589 */ {I_LOOPE, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18395, IF_8086},
    /*  590 */ {I_LOOPE, 2, {IMMEDIATE,REG_CX,0,0,0}, nasm_bytecodes+18400, IF_8086|IF_NOLONG},
    /*  591 */ {I_LOOPE, 2, {IMMEDIATE,REG_ECX,0,0,0}, nasm_bytecodes+18405, IF_386},
    /*  592 */ {I_LOOPE, 2, {IMMEDIATE,REG_RCX,0,0,0}, nasm_bytecodes+18410, IF_X64},
    /*  593 */ {I_LOOPNE, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18415, IF_8086},
    /*  594 */ {I_LOOPNE, 2, {IMMEDIATE,REG_CX,0,0,0}, nasm_bytecodes+18420, IF_8086|IF_NOLONG},
    /*  595 */ {I_LOOPNE, 2, {IMMEDIATE,REG_ECX,0,0,0}, nasm_bytecodes+18425, IF_386},
    /*  596 */ {I_LOOPNE, 2, {IMMEDIATE,REG_RCX,0,0,0}, nasm_bytecodes+18430, IF_X64},
    /*  597 */ {I_LOOPNZ, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18415, IF_8086},
    /*  598 */ {I_LOOPNZ, 2, {IMMEDIATE,REG_CX,0,0,0}, nasm_bytecodes+18420, IF_8086|IF_NOLONG},
    /*  599 */ {I_LOOPNZ, 2, {IMMEDIATE,REG_ECX,0,0,0}, nasm_bytecodes+18425, IF_386},
    /*  600 */ {I_LOOPNZ, 2, {IMMEDIATE,REG_RCX,0,0,0}, nasm_bytecodes+18430, IF_X64},
    /*  601 */ {I_LOOPZ, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+18395, IF_8086},
    /*  602 */ {I_LOOPZ, 2, {IMMEDIATE,REG_CX,0,0,0}, nasm_bytecodes+18400, IF_8086|IF_NOLONG},
    /*  603 */ {I_LOOPZ, 2, {IMMEDIATE,REG_ECX,0,0,0}, nasm_bytecodes+18405, IF_386},
    /*  604 */ {I_LOOPZ, 2, {IMMEDIATE,REG_RCX,0,0,0}, nasm_bytecodes+18410, IF_X64},
    /*  605 */ {I_LSL, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14562, IF_286|IF_PROT|IF_SW},
    /*  606 */ {I_LSL, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+14562, IF_286|IF_PROT},
    /*  607 */ {I_LSL, 2, {REG16,REG32,0,0,0}, nasm_bytecodes+14562, IF_386|IF_PROT},
    /*  608 */ {I_LSL, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14568, IF_386|IF_PROT|IF_SW},
    /*  609 */ {I_LSL, 2, {REG32,REG16,0,0,0}, nasm_bytecodes+14568, IF_386|IF_PROT},
    /*  610 */ {I_LSL, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+14568, IF_386|IF_PROT},
    /*  611 */ {I_LSL, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+14574, IF_X64|IF_PROT|IF_SW},
    /*  612 */ {I_LSL, 2, {REG64,REG16,0,0,0}, nasm_bytecodes+14574, IF_X64|IF_PROT},
    /*  613 */ {I_LSL, 2, {REG64,REG32,0,0,0}, nasm_bytecodes+14574, IF_X64|IF_PROT},
    /*  614 */ {I_LSL, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+14574, IF_X64|IF_PROT},
    /*  615 */ {I_LSS, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14580, IF_386},
    /*  616 */ {I_LSS, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+14586, IF_386},
    /*  617 */ {I_LTR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18435, IF_286|IF_PROT|IF_PRIV},
    /*  618 */ {I_LTR, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+18435, IF_286|IF_PROT|IF_PRIV},
    /*  619 */ {I_LTR, 1, {REG16,0,0,0,0}, nasm_bytecodes+18435, IF_286|IF_PROT|IF_PRIV},
    /*  620 */ {I_MFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+18440, IF_X64|IF_AMD},
    /*  621 */ {I_MONITOR, 0, {0,0,0,0,0}, nasm_bytecodes+18445, IF_PRESCOTT},
    /*  622 */ {I_MOV, 2, {MEMORY,REG_SREG,0,0,0}, nasm_bytecodes+18456, IF_8086|IF_SM},
    /*  623 */ {I_MOV, 2, {REG16,REG_SREG,0,0,0}, nasm_bytecodes+18450, IF_8086},
    /*  624 */ {I_MOV, 2, {REG32,REG_SREG,0,0,0}, nasm_bytecodes+18455, IF_386},
    /*  625 */ {I_MOV, 2, {REG_SREG,MEMORY,0,0,0}, nasm_bytecodes+20041, IF_8086|IF_SM},
    /*  626 */ {I_MOV, 2, {REG_SREG,REG16,0,0,0}, nasm_bytecodes+20041, IF_8086},
    /*  627 */ {I_MOV, 2, {REG_SREG,REG32,0,0,0}, nasm_bytecodes+20041, IF_386},
    /*  628 */ {I_MOV, 2, {REG_AL,MEM_OFFS,0,0,0}, nasm_bytecodes+20045, IF_8086|IF_SM},
    /*  629 */ {I_MOV, 2, {REG_AX,MEM_OFFS,0,0,0}, nasm_bytecodes+18460, IF_8086|IF_SM},
    /*  630 */ {I_MOV, 2, {REG_EAX,MEM_OFFS,0,0,0}, nasm_bytecodes+18465, IF_386|IF_SM},
    /*  631 */ {I_MOV, 2, {REG_RAX,MEM_OFFS,0,0,0}, nasm_bytecodes+18470, IF_X64|IF_SM},
    /*  632 */ {I_MOV, 2, {MEM_OFFS,REG_AL,0,0,0}, nasm_bytecodes+20049, IF_8086|IF_SM},
    /*  633 */ {I_MOV, 2, {MEM_OFFS,REG_AX,0,0,0}, nasm_bytecodes+18475, IF_8086|IF_SM},
    /*  634 */ {I_MOV, 2, {MEM_OFFS,REG_EAX,0,0,0}, nasm_bytecodes+18480, IF_386|IF_SM},
    /*  635 */ {I_MOV, 2, {MEM_OFFS,REG_RAX,0,0,0}, nasm_bytecodes+18485, IF_X64|IF_SM},
    /*  636 */ {I_MOV, 2, {REG32,REG_CREG,0,0,0}, nasm_bytecodes+14592, IF_386|IF_PRIV|IF_NOLONG},
    /*  637 */ {I_MOV, 2, {REG64,REG_CREG,0,0,0}, nasm_bytecodes+14598, IF_X64|IF_PRIV},
    /*  638 */ {I_MOV, 2, {REG_CREG,REG32,0,0,0}, nasm_bytecodes+14604, IF_386|IF_PRIV|IF_NOLONG},
    /*  639 */ {I_MOV, 2, {REG_CREG,REG64,0,0,0}, nasm_bytecodes+14610, IF_X64|IF_PRIV},
    /*  640 */ {I_MOV, 2, {REG32,REG_DREG,0,0,0}, nasm_bytecodes+14617, IF_386|IF_PRIV|IF_NOLONG},
    /*  641 */ {I_MOV, 2, {REG64,REG_DREG,0,0,0}, nasm_bytecodes+14616, IF_X64|IF_PRIV},
    /*  642 */ {I_MOV, 2, {REG_DREG,REG32,0,0,0}, nasm_bytecodes+14623, IF_386|IF_PRIV|IF_NOLONG},
    /*  643 */ {I_MOV, 2, {REG_DREG,REG64,0,0,0}, nasm_bytecodes+14622, IF_X64|IF_PRIV},
    /*  644 */ {I_MOV, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20053, IF_8086|IF_SM},
    /*  645 */ {I_MOV, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20053, IF_8086},
    /*  646 */ {I_MOV, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+18500, IF_8086|IF_SM},
    /*  647 */ {I_MOV, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18500, IF_8086},
    /*  648 */ {I_MOV, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+18505, IF_386|IF_SM},
    /*  649 */ {I_MOV, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18505, IF_386},
    /*  650 */ {I_MOV, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+18510, IF_X64|IF_SM},
    /*  651 */ {I_MOV, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18510, IF_X64},
    /*  652 */ {I_MOV, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+20057, IF_8086|IF_SM},
    /*  653 */ {I_MOV, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20057, IF_8086},
    /*  654 */ {I_MOV, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18515, IF_8086|IF_SM},
    /*  655 */ {I_MOV, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18515, IF_8086},
    /*  656 */ {I_MOV, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18520, IF_386|IF_SM},
    /*  657 */ {I_MOV, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18520, IF_386},
    /*  658 */ {I_MOV, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+18525, IF_X64|IF_SM},
    /*  659 */ {I_MOV, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18525, IF_X64},
    /*  660 */ {I_MOV, 2, {REG8,IMMEDIATE,0,0,0}, nasm_bytecodes+20061, IF_8086|IF_SM},
    /*  661 */ {I_MOV, 2, {REG16,IMMEDIATE,0,0,0}, nasm_bytecodes+18530, IF_8086|IF_SM},
    /*  662 */ {I_MOV, 2, {REG32,IMMEDIATE,0,0,0}, nasm_bytecodes+18535, IF_386|IF_SM},
    /*  663 */ {I_MOV, 2, {REG64,IMMEDIATE,0,0,0}, nasm_bytecodes+18540, IF_X64|IF_SM},
    /*  664 */ {I_MOV, 2, {REG64,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14628, IF_X64},
    /*  665 */ {I_MOV, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18545, IF_8086|IF_SM},
    /*  666 */ {I_MOV, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14634, IF_8086|IF_SM},
    /*  667 */ {I_MOV, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14640, IF_386|IF_SM},
    /*  668 */ {I_MOV, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14628, IF_X64|IF_SM},
    /*  669 */ {I_MOV, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+18545, IF_8086|IF_SM},
    /*  670 */ {I_MOV, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14634, IF_8086|IF_SM},
    /*  671 */ {I_MOV, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14640, IF_386|IF_SM},
    /*  672 */ {I_MOVD, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+14646, IF_PENT|IF_MMX|IF_SD},
    /*  673 */ {I_MOVD, 2, {MMXREG,REG32,0,0,0}, nasm_bytecodes+14646, IF_PENT|IF_MMX},
    /*  674 */ {I_MOVD, 2, {MEMORY,MMXREG,0,0,0}, nasm_bytecodes+14652, IF_PENT|IF_MMX|IF_SD},
    /*  675 */ {I_MOVD, 2, {REG32,MMXREG,0,0,0}, nasm_bytecodes+14652, IF_PENT|IF_MMX},
    /*  676 */ {I_MOVD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+6854, IF_X64|IF_SD},
    /*  677 */ {I_MOVD, 2, {XMMREG,REG32,0,0,0}, nasm_bytecodes+6854, IF_X64},
    /*  678 */ {I_MOVD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+6861, IF_X64|IF_SD},
    /*  679 */ {I_MOVD, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+6861, IF_X64|IF_SSE},
    /*  680 */ {I_MOVQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6868, IF_PENT|IF_MMX|IF_SQ},
    /*  681 */ {I_MOVQ, 2, {RM_MMX,MMXREG,0,0,0}, nasm_bytecodes+6875, IF_PENT|IF_MMX|IF_SQ},
    /*  682 */ {I_MOVQ, 2, {MMXREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+14646, IF_X64|IF_MMX},
    /*  683 */ {I_MOVQ, 2, {RM_GPR|BITS64,MMXREG,0,0,0}, nasm_bytecodes+14652, IF_X64|IF_MMX},
    /*  684 */ {I_MOVSB, 0, {0,0,0,0,0}, nasm_bytecodes+5164, IF_8086},
    /*  685 */ {I_MOVSD, 0, {0,0,0,0,0}, nasm_bytecodes+20065, IF_386},
    /*  686 */ {I_MOVSQ, 0, {0,0,0,0,0}, nasm_bytecodes+20069, IF_X64},
    /*  687 */ {I_MOVSW, 0, {0,0,0,0,0}, nasm_bytecodes+20073, IF_8086},
    /*  688 */ {I_MOVSX, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14658, IF_386|IF_SB},
    /*  689 */ {I_MOVSX, 2, {REG16,REG8,0,0,0}, nasm_bytecodes+14658, IF_386},
    /*  690 */ {I_MOVSX, 2, {REG32,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+14664, IF_386},
    /*  691 */ {I_MOVSX, 2, {REG32,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+14670, IF_386},
    /*  692 */ {I_MOVSX, 2, {REG64,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+14676, IF_X64},
    /*  693 */ {I_MOVSX, 2, {REG64,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+14682, IF_X64},
    /*  694 */ {I_MOVSXD, 2, {REG64,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+18550, IF_X64},
    /*  695 */ {I_MOVZX, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+14688, IF_386|IF_SB},
    /*  696 */ {I_MOVZX, 2, {REG16,REG8,0,0,0}, nasm_bytecodes+14688, IF_386},
    /*  697 */ {I_MOVZX, 2, {REG32,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+14694, IF_386},
    /*  698 */ {I_MOVZX, 2, {REG32,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+14700, IF_386},
    /*  699 */ {I_MOVZX, 2, {REG64,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+14706, IF_X64},
    /*  700 */ {I_MOVZX, 2, {REG64,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+14712, IF_X64},
    /*  701 */ {I_MUL, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+20077, IF_8086},
    /*  702 */ {I_MUL, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18555, IF_8086},
    /*  703 */ {I_MUL, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18560, IF_386},
    /*  704 */ {I_MUL, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18565, IF_X64},
    /*  705 */ {I_MWAIT, 0, {0,0,0,0,0}, nasm_bytecodes+18570, IF_PRESCOTT},
    /*  706 */ {I_NEG, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+20081, IF_8086},
    /*  707 */ {I_NEG, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18575, IF_8086},
    /*  708 */ {I_NEG, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18580, IF_386},
    /*  709 */ {I_NEG, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18585, IF_X64},
    /*  710 */ {I_NOP, 0, {0,0,0,0,0}, nasm_bytecodes+20085, IF_8086},
    /*  711 */ {I_NOP, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+14718, IF_P6},
    /*  712 */ {I_NOP, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+14724, IF_P6},
    /*  713 */ {I_NOP, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+14730, IF_X64},
    /*  714 */ {I_NOT, 1, {RM_GPR|BITS8,0,0,0,0}, nasm_bytecodes+20089, IF_8086},
    /*  715 */ {I_NOT, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18590, IF_8086},
    /*  716 */ {I_NOT, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18595, IF_386},
    /*  717 */ {I_NOT, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18600, IF_X64},
    /*  718 */ {I_OR, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20093, IF_8086|IF_SM},
    /*  719 */ {I_OR, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20093, IF_8086},
    /*  720 */ {I_OR, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+18605, IF_8086|IF_SM},
    /*  721 */ {I_OR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18605, IF_8086},
    /*  722 */ {I_OR, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+18610, IF_386|IF_SM},
    /*  723 */ {I_OR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18610, IF_386},
    /*  724 */ {I_OR, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+18615, IF_X64|IF_SM},
    /*  725 */ {I_OR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18615, IF_X64},
    /*  726 */ {I_OR, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+11855, IF_8086|IF_SM},
    /*  727 */ {I_OR, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+11855, IF_8086},
    /*  728 */ {I_OR, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18620, IF_8086|IF_SM},
    /*  729 */ {I_OR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18620, IF_8086},
    /*  730 */ {I_OR, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18625, IF_386|IF_SM},
    /*  731 */ {I_OR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18625, IF_386},
    /*  732 */ {I_OR, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+18630, IF_X64|IF_SM},
    /*  733 */ {I_OR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18630, IF_X64},
    /*  734 */ {I_OR, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14736, IF_8086},
    /*  735 */ {I_OR, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14742, IF_386},
    /*  736 */ {I_OR, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14748, IF_X64},
    /*  737 */ {I_OR, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+20097, IF_8086|IF_SM},
    /*  738 */ {I_OR, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+14736, IF_8086|IF_SM},
    /*  739 */ {I_OR, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+18635, IF_8086|IF_SM},
    /*  740 */ {I_OR, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+14742, IF_386|IF_SM},
    /*  741 */ {I_OR, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+18640, IF_386|IF_SM},
    /*  742 */ {I_OR, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+14748, IF_X64|IF_SM},
    /*  743 */ {I_OR, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+18645, IF_X64|IF_SM},
    /*  744 */ {I_OR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18650, IF_8086|IF_SM},
    /*  745 */ {I_OR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14754, IF_8086|IF_SM},
    /*  746 */ {I_OR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14760, IF_386|IF_SM},
    /*  747 */ {I_OR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14766, IF_X64|IF_SM},
    /*  748 */ {I_OR, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+18650, IF_8086|IF_SM},
    /*  749 */ {I_OR, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14754, IF_8086|IF_SM},
    /*  750 */ {I_OR, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14760, IF_386|IF_SM},
    /*  751 */ {I_OUT, 2, {IMMEDIATE,REG_AL,0,0,0}, nasm_bytecodes+20101, IF_8086|IF_SB},
    /*  752 */ {I_OUT, 2, {IMMEDIATE,REG_AX,0,0,0}, nasm_bytecodes+18655, IF_8086|IF_SB},
    /*  753 */ {I_OUT, 2, {IMMEDIATE,REG_EAX,0,0,0}, nasm_bytecodes+18660, IF_386|IF_SB},
    /*  754 */ {I_OUT, 2, {REG_DX,REG_AL,0,0,0}, nasm_bytecodes+20458, IF_8086},
    /*  755 */ {I_OUT, 2, {REG_DX,REG_AX,0,0,0}, nasm_bytecodes+20105, IF_8086},
    /*  756 */ {I_OUT, 2, {REG_DX,REG_EAX,0,0,0}, nasm_bytecodes+20109, IF_386},
    /*  757 */ {I_OUTSB, 0, {0,0,0,0,0}, nasm_bytecodes+20461, IF_186},
    /*  758 */ {I_OUTSD, 0, {0,0,0,0,0}, nasm_bytecodes+20113, IF_386},
    /*  759 */ {I_OUTSW, 0, {0,0,0,0,0}, nasm_bytecodes+20117, IF_186},
    /*  760 */ {I_PACKSSDW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6882, IF_PENT|IF_MMX|IF_SQ},
    /*  761 */ {I_PACKSSWB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6889, IF_PENT|IF_MMX|IF_SQ},
    /*  762 */ {I_PACKUSWB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6896, IF_PENT|IF_MMX|IF_SQ},
    /*  763 */ {I_PADDB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6903, IF_PENT|IF_MMX|IF_SQ},
    /*  764 */ {I_PADDD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6910, IF_PENT|IF_MMX|IF_SQ},
    /*  765 */ {I_PADDSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6917, IF_PENT|IF_MMX|IF_SQ},
    /*  766 */ {I_PADDSIW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14772, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    /*  767 */ {I_PADDSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6924, IF_PENT|IF_MMX|IF_SQ},
    /*  768 */ {I_PADDUSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6931, IF_PENT|IF_MMX|IF_SQ},
    /*  769 */ {I_PADDUSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6938, IF_PENT|IF_MMX|IF_SQ},
    /*  770 */ {I_PADDW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6945, IF_PENT|IF_MMX|IF_SQ},
    /*  771 */ {I_PAND, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6952, IF_PENT|IF_MMX|IF_SQ},
    /*  772 */ {I_PANDN, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6959, IF_PENT|IF_MMX|IF_SQ},
    /*  773 */ {I_PAUSE, 0, {0,0,0,0,0}, nasm_bytecodes+18665, IF_8086},
    /*  774 */ {I_PAVEB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14778, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    /*  775 */ {I_PAVGUSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5103, IF_PENT|IF_3DNOW|IF_SQ},
    /*  776 */ {I_PCMPEQB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6966, IF_PENT|IF_MMX|IF_SQ},
    /*  777 */ {I_PCMPEQD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6973, IF_PENT|IF_MMX|IF_SQ},
    /*  778 */ {I_PCMPEQW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6980, IF_PENT|IF_MMX|IF_SQ},
    /*  779 */ {I_PCMPGTB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6987, IF_PENT|IF_MMX|IF_SQ},
    /*  780 */ {I_PCMPGTD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+6994, IF_PENT|IF_MMX|IF_SQ},
    /*  781 */ {I_PCMPGTW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7001, IF_PENT|IF_MMX|IF_SQ},
    /*  782 */ {I_PDISTIB, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+15943, IF_PENT|IF_MMX|IF_SM|IF_CYRIX},
    /*  783 */ {I_PF2ID, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5111, IF_PENT|IF_3DNOW|IF_SQ},
    /*  784 */ {I_PFACC, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5119, IF_PENT|IF_3DNOW|IF_SQ},
    /*  785 */ {I_PFADD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5127, IF_PENT|IF_3DNOW|IF_SQ},
    /*  786 */ {I_PFCMPEQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5135, IF_PENT|IF_3DNOW|IF_SQ},
    /*  787 */ {I_PFCMPGE, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5143, IF_PENT|IF_3DNOW|IF_SQ},
    /*  788 */ {I_PFCMPGT, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5151, IF_PENT|IF_3DNOW|IF_SQ},
    /*  789 */ {I_PFMAX, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5159, IF_PENT|IF_3DNOW|IF_SQ},
    /*  790 */ {I_PFMIN, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5167, IF_PENT|IF_3DNOW|IF_SQ},
    /*  791 */ {I_PFMUL, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5175, IF_PENT|IF_3DNOW|IF_SQ},
    /*  792 */ {I_PFRCP, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5183, IF_PENT|IF_3DNOW|IF_SQ},
    /*  793 */ {I_PFRCPIT1, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5191, IF_PENT|IF_3DNOW|IF_SQ},
    /*  794 */ {I_PFRCPIT2, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5199, IF_PENT|IF_3DNOW|IF_SQ},
    /*  795 */ {I_PFRSQIT1, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5207, IF_PENT|IF_3DNOW|IF_SQ},
    /*  796 */ {I_PFRSQRT, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5215, IF_PENT|IF_3DNOW|IF_SQ},
    /*  797 */ {I_PFSUB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5223, IF_PENT|IF_3DNOW|IF_SQ},
    /*  798 */ {I_PFSUBR, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5231, IF_PENT|IF_3DNOW|IF_SQ},
    /*  799 */ {I_PI2FD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5239, IF_PENT|IF_3DNOW|IF_SQ},
    /*  800 */ {I_PMACHRIW, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+16039, IF_PENT|IF_MMX|IF_SM|IF_CYRIX},
    /*  801 */ {I_PMADDWD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7008, IF_PENT|IF_MMX|IF_SQ},
    /*  802 */ {I_PMAGW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14784, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    /*  803 */ {I_PMULHRIW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14790, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    /*  804 */ {I_PMULHRWA, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5247, IF_PENT|IF_3DNOW|IF_SQ},
    /*  805 */ {I_PMULHRWC, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14796, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    /*  806 */ {I_PMULHW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7015, IF_PENT|IF_MMX|IF_SQ},
    /*  807 */ {I_PMULLW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7022, IF_PENT|IF_MMX|IF_SQ},
    /*  808 */ {I_PMVGEZB, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+16171, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    /*  809 */ {I_PMVLZB, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+16027, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    /*  810 */ {I_PMVNZB, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+16009, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    /*  811 */ {I_PMVZB, 2, {MMXREG,MEMORY,0,0,0}, nasm_bytecodes+15931, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    /*  812 */ {I_POP, 1, {REG16,0,0,0,0}, nasm_bytecodes+20121, IF_8086},
    /*  813 */ {I_POP, 1, {REG32,0,0,0,0}, nasm_bytecodes+20125, IF_386|IF_NOLONG},
    /*  814 */ {I_POP, 1, {REG64,0,0,0,0}, nasm_bytecodes+20129, IF_X64},
    /*  815 */ {I_POP, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18670, IF_8086},
    /*  816 */ {I_POP, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18675, IF_386|IF_NOLONG},
    /*  817 */ {I_POP, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18680, IF_X64},
    /*  818 */ {I_POP, 1, {REG_DESS,0,0,0,0}, nasm_bytecodes+19939, IF_8086|IF_NOLONG},
    /*  819 */ {I_POP, 1, {REG_FSGS,0,0,0,0}, nasm_bytecodes+20133, IF_386},
    /*  820 */ {I_POPA, 0, {0,0,0,0,0}, nasm_bytecodes+20137, IF_186|IF_NOLONG},
    /*  821 */ {I_POPAD, 0, {0,0,0,0,0}, nasm_bytecodes+20141, IF_386|IF_NOLONG},
    /*  822 */ {I_POPAW, 0, {0,0,0,0,0}, nasm_bytecodes+20145, IF_186|IF_NOLONG},
    /*  823 */ {I_POPF, 0, {0,0,0,0,0}, nasm_bytecodes+20149, IF_8086},
    /*  824 */ {I_POPFD, 0, {0,0,0,0,0}, nasm_bytecodes+20153, IF_386|IF_NOLONG},
    /*  825 */ {I_POPFQ, 0, {0,0,0,0,0}, nasm_bytecodes+20153, IF_X64},
    /*  826 */ {I_POPFW, 0, {0,0,0,0,0}, nasm_bytecodes+20157, IF_8086},
    /*  827 */ {I_POR, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7029, IF_PENT|IF_MMX|IF_SQ},
    /*  828 */ {I_PREFETCH, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18685, IF_PENT|IF_3DNOW|IF_SQ},
    /*  829 */ {I_PREFETCHW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+18690, IF_PENT|IF_3DNOW|IF_SQ},
    /*  830 */ {I_PSLLD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7036, IF_PENT|IF_MMX|IF_SQ},
    /*  831 */ {I_PSLLD, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7043, IF_PENT|IF_MMX},
    /*  832 */ {I_PSLLQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7050, IF_PENT|IF_MMX|IF_SQ},
    /*  833 */ {I_PSLLQ, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7057, IF_PENT|IF_MMX},
    /*  834 */ {I_PSLLW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7064, IF_PENT|IF_MMX|IF_SQ},
    /*  835 */ {I_PSLLW, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7071, IF_PENT|IF_MMX},
    /*  836 */ {I_PSRAD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7078, IF_PENT|IF_MMX|IF_SQ},
    /*  837 */ {I_PSRAD, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7085, IF_PENT|IF_MMX},
    /*  838 */ {I_PSRAW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7092, IF_PENT|IF_MMX|IF_SQ},
    /*  839 */ {I_PSRAW, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7099, IF_PENT|IF_MMX},
    /*  840 */ {I_PSRLD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7106, IF_PENT|IF_MMX|IF_SQ},
    /*  841 */ {I_PSRLD, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7113, IF_PENT|IF_MMX},
    /*  842 */ {I_PSRLQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7120, IF_PENT|IF_MMX|IF_SQ},
    /*  843 */ {I_PSRLQ, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7127, IF_PENT|IF_MMX},
    /*  844 */ {I_PSRLW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7134, IF_PENT|IF_MMX|IF_SQ},
    /*  845 */ {I_PSRLW, 2, {MMXREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7141, IF_PENT|IF_MMX},
    /*  846 */ {I_PSUBB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7148, IF_PENT|IF_MMX|IF_SQ},
    /*  847 */ {I_PSUBD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7155, IF_PENT|IF_MMX|IF_SQ},
    /*  848 */ {I_PSUBSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7162, IF_PENT|IF_MMX|IF_SQ},
    /*  849 */ {I_PSUBSIW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+14802, IF_PENT|IF_MMX|IF_SQ|IF_CYRIX},
    /*  850 */ {I_PSUBSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7169, IF_PENT|IF_MMX|IF_SQ},
    /*  851 */ {I_PSUBUSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7176, IF_PENT|IF_MMX|IF_SQ},
    /*  852 */ {I_PSUBUSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7183, IF_PENT|IF_MMX|IF_SQ},
    /*  853 */ {I_PSUBW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7190, IF_PENT|IF_MMX|IF_SQ},
    /*  854 */ {I_PUNPCKHBW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7197, IF_PENT|IF_MMX|IF_SQ},
    /*  855 */ {I_PUNPCKHDQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7204, IF_PENT|IF_MMX|IF_SQ},
    /*  856 */ {I_PUNPCKHWD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7211, IF_PENT|IF_MMX|IF_SQ},
    /*  857 */ {I_PUNPCKLBW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7218, IF_PENT|IF_MMX|IF_SQ},
    /*  858 */ {I_PUNPCKLDQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7225, IF_PENT|IF_MMX|IF_SQ},
    /*  859 */ {I_PUNPCKLWD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7232, IF_PENT|IF_MMX|IF_SQ},
    /*  860 */ {I_PUSH, 1, {REG16,0,0,0,0}, nasm_bytecodes+20161, IF_8086},
    /*  861 */ {I_PUSH, 1, {REG32,0,0,0,0}, nasm_bytecodes+20165, IF_386|IF_NOLONG},
    /*  862 */ {I_PUSH, 1, {REG64,0,0,0,0}, nasm_bytecodes+20169, IF_X64},
    /*  863 */ {I_PUSH, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+18695, IF_8086},
    /*  864 */ {I_PUSH, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+18700, IF_386|IF_NOLONG},
    /*  865 */ {I_PUSH, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+18705, IF_X64},
    /*  866 */ {I_PUSH, 1, {REG_CS,0,0,0,0}, nasm_bytecodes+19915, IF_8086|IF_NOLONG},
    /*  867 */ {I_PUSH, 1, {REG_DESS,0,0,0,0}, nasm_bytecodes+19915, IF_8086|IF_NOLONG},
    /*  868 */ {I_PUSH, 1, {REG_FSGS,0,0,0,0}, nasm_bytecodes+20173, IF_386},
    /*  869 */ {I_PUSH, 1, {IMMEDIATE|BITS8,0,0,0,0}, nasm_bytecodes+20177, IF_186},
    /*  870 */ {I_PUSH, 1, {IMMEDIATE|BITS16,0,0,0,0}, nasm_bytecodes+18710, IF_186|IF_AR0|IF_SZ},
    /*  871 */ {I_PUSH, 1, {IMMEDIATE|BITS32,0,0,0,0}, nasm_bytecodes+18715, IF_386|IF_NOLONG|IF_AR0|IF_SZ},
    /*  872 */ {I_PUSH, 1, {IMMEDIATE|BITS32,0,0,0,0}, nasm_bytecodes+18715, IF_386|IF_NOLONG|IF_SD},
    /*  873 */ {I_PUSH, 1, {IMMEDIATE|BITS64,0,0,0,0}, nasm_bytecodes+18720, IF_X64|IF_AR0|IF_SZ},
    /*  874 */ {I_PUSHA, 0, {0,0,0,0,0}, nasm_bytecodes+20181, IF_186|IF_NOLONG},
    /*  875 */ {I_PUSHAD, 0, {0,0,0,0,0}, nasm_bytecodes+20185, IF_386|IF_NOLONG},
    /*  876 */ {I_PUSHAW, 0, {0,0,0,0,0}, nasm_bytecodes+20189, IF_186|IF_NOLONG},
    /*  877 */ {I_PUSHF, 0, {0,0,0,0,0}, nasm_bytecodes+20193, IF_8086},
    /*  878 */ {I_PUSHFD, 0, {0,0,0,0,0}, nasm_bytecodes+20197, IF_386|IF_NOLONG},
    /*  879 */ {I_PUSHFQ, 0, {0,0,0,0,0}, nasm_bytecodes+20197, IF_X64},
    /*  880 */ {I_PUSHFW, 0, {0,0,0,0,0}, nasm_bytecodes+20201, IF_8086},
    /*  881 */ {I_PXOR, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7239, IF_PENT|IF_MMX|IF_SQ},
    /*  882 */ {I_RCL, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20205, IF_8086},
    /*  883 */ {I_RCL, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20209, IF_8086},
    /*  884 */ {I_RCL, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18725, IF_186|IF_SB},
    /*  885 */ {I_RCL, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18730, IF_8086},
    /*  886 */ {I_RCL, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18735, IF_8086},
    /*  887 */ {I_RCL, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14808, IF_186|IF_SB},
    /*  888 */ {I_RCL, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18740, IF_386},
    /*  889 */ {I_RCL, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18745, IF_386},
    /*  890 */ {I_RCL, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14814, IF_386|IF_SB},
    /*  891 */ {I_RCL, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18750, IF_X64},
    /*  892 */ {I_RCL, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18755, IF_X64},
    /*  893 */ {I_RCL, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14820, IF_X64|IF_SB},
    /*  894 */ {I_RCR, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20213, IF_8086},
    /*  895 */ {I_RCR, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20217, IF_8086},
    /*  896 */ {I_RCR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18760, IF_186|IF_SB},
    /*  897 */ {I_RCR, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18765, IF_8086},
    /*  898 */ {I_RCR, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18770, IF_8086},
    /*  899 */ {I_RCR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14826, IF_186|IF_SB},
    /*  900 */ {I_RCR, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18775, IF_386},
    /*  901 */ {I_RCR, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18780, IF_386},
    /*  902 */ {I_RCR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14832, IF_386|IF_SB},
    /*  903 */ {I_RCR, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18785, IF_X64},
    /*  904 */ {I_RCR, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18790, IF_X64},
    /*  905 */ {I_RCR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14838, IF_X64|IF_SB},
    /*  906 */ {I_RDSHR, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+14844, IF_P6|IF_CYRIX|IF_SMM},
    /*  907 */ {I_RDMSR, 0, {0,0,0,0,0}, nasm_bytecodes+20221, IF_PENT|IF_PRIV},
    /*  908 */ {I_RDPMC, 0, {0,0,0,0,0}, nasm_bytecodes+20225, IF_P6},
    /*  909 */ {I_RDTSC, 0, {0,0,0,0,0}, nasm_bytecodes+20229, IF_PENT},
    /*  910 */ {I_RDTSCP, 0, {0,0,0,0,0}, nasm_bytecodes+18795, IF_X86_64},
    /*  911 */ {I_RET, 0, {0,0,0,0,0}, nasm_bytecodes+19342, IF_8086},
    /*  912 */ {I_RET, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+20233, IF_8086|IF_SW},
    /*  913 */ {I_RETF, 0, {0,0,0,0,0}, nasm_bytecodes+20464, IF_8086},
    /*  914 */ {I_RETF, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+20237, IF_8086|IF_SW},
    /*  915 */ {I_RETN, 0, {0,0,0,0,0}, nasm_bytecodes+19342, IF_8086},
    /*  916 */ {I_RETN, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+20233, IF_8086|IF_SW},
    /*  917 */ {I_ROL, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20241, IF_8086},
    /*  918 */ {I_ROL, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20245, IF_8086},
    /*  919 */ {I_ROL, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18800, IF_186|IF_SB},
    /*  920 */ {I_ROL, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18805, IF_8086},
    /*  921 */ {I_ROL, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18810, IF_8086},
    /*  922 */ {I_ROL, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14850, IF_186|IF_SB},
    /*  923 */ {I_ROL, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18815, IF_386},
    /*  924 */ {I_ROL, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18820, IF_386},
    /*  925 */ {I_ROL, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14856, IF_386|IF_SB},
    /*  926 */ {I_ROL, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18825, IF_X64},
    /*  927 */ {I_ROL, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18830, IF_X64},
    /*  928 */ {I_ROL, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14862, IF_X64|IF_SB},
    /*  929 */ {I_ROR, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20249, IF_8086},
    /*  930 */ {I_ROR, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20253, IF_8086},
    /*  931 */ {I_ROR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18835, IF_186|IF_SB},
    /*  932 */ {I_ROR, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18840, IF_8086},
    /*  933 */ {I_ROR, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18845, IF_8086},
    /*  934 */ {I_ROR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14868, IF_186|IF_SB},
    /*  935 */ {I_ROR, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18850, IF_386},
    /*  936 */ {I_ROR, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18855, IF_386},
    /*  937 */ {I_ROR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14874, IF_386|IF_SB},
    /*  938 */ {I_ROR, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18860, IF_X64},
    /*  939 */ {I_ROR, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18865, IF_X64},
    /*  940 */ {I_ROR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14880, IF_X64|IF_SB},
    /*  941 */ {I_RSDC, 2, {REG_SREG,MEMORY|BITS80,0,0,0}, nasm_bytecodes+16279, IF_486|IF_CYRIX|IF_SMM},
    /*  942 */ {I_RSLDT, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+18870, IF_486|IF_CYRIX|IF_SMM},
    /*  943 */ {I_RSM, 0, {0,0,0,0,0}, nasm_bytecodes+20257, IF_PENT|IF_SMM},
    /*  944 */ {I_RSTS, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+18875, IF_486|IF_CYRIX|IF_SMM},
    /*  945 */ {I_SAHF, 0, {0,0,0,0,0}, nasm_bytecodes+5132, IF_8086},
    /*  946 */ {I_SALC, 0, {0,0,0,0,0}, nasm_bytecodes+20467, IF_8086|IF_UNDOC},
    /*  947 */ {I_SAR, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20269, IF_8086},
    /*  948 */ {I_SAR, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20273, IF_8086},
    /*  949 */ {I_SAR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18915, IF_186|IF_SB},
    /*  950 */ {I_SAR, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18920, IF_8086},
    /*  951 */ {I_SAR, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18925, IF_8086},
    /*  952 */ {I_SAR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14904, IF_186|IF_SB},
    /*  953 */ {I_SAR, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18930, IF_386},
    /*  954 */ {I_SAR, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18935, IF_386},
    /*  955 */ {I_SAR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14910, IF_386|IF_SB},
    /*  956 */ {I_SAR, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18940, IF_X64},
    /*  957 */ {I_SAR, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18945, IF_X64},
    /*  958 */ {I_SAR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14916, IF_X64|IF_SB},
    /*  959 */ {I_SBB, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20277, IF_8086|IF_SM},
    /*  960 */ {I_SBB, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20277, IF_8086},
    /*  961 */ {I_SBB, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+18950, IF_8086|IF_SM},
    /*  962 */ {I_SBB, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18950, IF_8086},
    /*  963 */ {I_SBB, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+18955, IF_386|IF_SM},
    /*  964 */ {I_SBB, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18955, IF_386},
    /*  965 */ {I_SBB, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+18960, IF_X64|IF_SM},
    /*  966 */ {I_SBB, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18960, IF_X64},
    /*  967 */ {I_SBB, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+9559, IF_8086|IF_SM},
    /*  968 */ {I_SBB, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+9559, IF_8086},
    /*  969 */ {I_SBB, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+18965, IF_8086|IF_SM},
    /*  970 */ {I_SBB, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+18965, IF_8086},
    /*  971 */ {I_SBB, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+18970, IF_386|IF_SM},
    /*  972 */ {I_SBB, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+18970, IF_386},
    /*  973 */ {I_SBB, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+18975, IF_X64|IF_SM},
    /*  974 */ {I_SBB, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+18975, IF_X64},
    /*  975 */ {I_SBB, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14922, IF_8086},
    /*  976 */ {I_SBB, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14928, IF_386},
    /*  977 */ {I_SBB, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+14934, IF_X64},
    /*  978 */ {I_SBB, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+20281, IF_8086|IF_SM},
    /*  979 */ {I_SBB, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+14922, IF_8086|IF_SM},
    /*  980 */ {I_SBB, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+18980, IF_8086|IF_SM},
    /*  981 */ {I_SBB, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+14928, IF_386|IF_SM},
    /*  982 */ {I_SBB, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+18985, IF_386|IF_SM},
    /*  983 */ {I_SBB, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+14934, IF_X64|IF_SM},
    /*  984 */ {I_SBB, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+18990, IF_X64|IF_SM},
    /*  985 */ {I_SBB, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18995, IF_8086|IF_SM},
    /*  986 */ {I_SBB, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14940, IF_8086|IF_SM},
    /*  987 */ {I_SBB, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14946, IF_386|IF_SM},
    /*  988 */ {I_SBB, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14952, IF_X64|IF_SM},
    /*  989 */ {I_SBB, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+18995, IF_8086|IF_SM},
    /*  990 */ {I_SBB, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+14940, IF_8086|IF_SM},
    /*  991 */ {I_SBB, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+14946, IF_386|IF_SM},
    /*  992 */ {I_SCASB, 0, {0,0,0,0,0}, nasm_bytecodes+20285, IF_8086},
    /*  993 */ {I_SCASD, 0, {0,0,0,0,0}, nasm_bytecodes+19000, IF_386},
    /*  994 */ {I_SCASQ, 0, {0,0,0,0,0}, nasm_bytecodes+19005, IF_X64},
    /*  995 */ {I_SCASW, 0, {0,0,0,0,0}, nasm_bytecodes+19010, IF_8086},
    /*  996 */ {I_SFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+19015, IF_X64|IF_AMD},
    /*  997 */ {I_SGDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19020, IF_286},
    /*  998 */ {I_SHL, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20261, IF_8086},
    /*  999 */ {I_SHL, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20265, IF_8086},
    /* 1000 */ {I_SHL, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+18880, IF_186|IF_SB},
    /* 1001 */ {I_SHL, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+18885, IF_8086},
    /* 1002 */ {I_SHL, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+18890, IF_8086},
    /* 1003 */ {I_SHL, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14886, IF_186|IF_SB},
    /* 1004 */ {I_SHL, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+18895, IF_386},
    /* 1005 */ {I_SHL, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+18900, IF_386},
    /* 1006 */ {I_SHL, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14892, IF_386|IF_SB},
    /* 1007 */ {I_SHL, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+18905, IF_X64},
    /* 1008 */ {I_SHL, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+18910, IF_X64},
    /* 1009 */ {I_SHL, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14898, IF_X64|IF_SB},
    /* 1010 */ {I_SHLD, 3, {MEMORY,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7246, IF_386|IF_SM2|IF_SB|IF_AR2},
    /* 1011 */ {I_SHLD, 3, {REG16,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7246, IF_386|IF_SM2|IF_SB|IF_AR2},
    /* 1012 */ {I_SHLD, 3, {MEMORY,REG32,IMMEDIATE,0,0}, nasm_bytecodes+7253, IF_386|IF_SM2|IF_SB|IF_AR2},
    /* 1013 */ {I_SHLD, 3, {REG32,REG32,IMMEDIATE,0,0}, nasm_bytecodes+7253, IF_386|IF_SM2|IF_SB|IF_AR2},
    /* 1014 */ {I_SHLD, 3, {MEMORY,REG64,IMMEDIATE,0,0}, nasm_bytecodes+7260, IF_X64|IF_SM2|IF_SB|IF_AR2},
    /* 1015 */ {I_SHLD, 3, {REG64,REG64,IMMEDIATE,0,0}, nasm_bytecodes+7260, IF_X64|IF_SM2|IF_SB|IF_AR2},
    /* 1016 */ {I_SHLD, 3, {MEMORY,REG16,REG_CL,0,0}, nasm_bytecodes+14958, IF_386|IF_SM},
    /* 1017 */ {I_SHLD, 3, {REG16,REG16,REG_CL,0,0}, nasm_bytecodes+14958, IF_386},
    /* 1018 */ {I_SHLD, 3, {MEMORY,REG32,REG_CL,0,0}, nasm_bytecodes+14964, IF_386|IF_SM},
    /* 1019 */ {I_SHLD, 3, {REG32,REG32,REG_CL,0,0}, nasm_bytecodes+14964, IF_386},
    /* 1020 */ {I_SHLD, 3, {MEMORY,REG64,REG_CL,0,0}, nasm_bytecodes+14970, IF_X64|IF_SM},
    /* 1021 */ {I_SHLD, 3, {REG64,REG64,REG_CL,0,0}, nasm_bytecodes+14970, IF_X64},
    /* 1022 */ {I_SHR, 2, {RM_GPR|BITS8,UNITY,0,0,0}, nasm_bytecodes+20289, IF_8086},
    /* 1023 */ {I_SHR, 2, {RM_GPR|BITS8,REG_CL,0,0,0}, nasm_bytecodes+20293, IF_8086},
    /* 1024 */ {I_SHR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+19025, IF_186|IF_SB},
    /* 1025 */ {I_SHR, 2, {RM_GPR|BITS16,UNITY,0,0,0}, nasm_bytecodes+19030, IF_8086},
    /* 1026 */ {I_SHR, 2, {RM_GPR|BITS16,REG_CL,0,0,0}, nasm_bytecodes+19035, IF_8086},
    /* 1027 */ {I_SHR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+14976, IF_186|IF_SB},
    /* 1028 */ {I_SHR, 2, {RM_GPR|BITS32,UNITY,0,0,0}, nasm_bytecodes+19040, IF_386},
    /* 1029 */ {I_SHR, 2, {RM_GPR|BITS32,REG_CL,0,0,0}, nasm_bytecodes+19045, IF_386},
    /* 1030 */ {I_SHR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+14982, IF_386|IF_SB},
    /* 1031 */ {I_SHR, 2, {RM_GPR|BITS64,UNITY,0,0,0}, nasm_bytecodes+19050, IF_X64},
    /* 1032 */ {I_SHR, 2, {RM_GPR|BITS64,REG_CL,0,0,0}, nasm_bytecodes+19055, IF_X64},
    /* 1033 */ {I_SHR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+14988, IF_X64|IF_SB},
    /* 1034 */ {I_SHRD, 3, {MEMORY,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7267, IF_386|IF_SM2|IF_SB|IF_AR2},
    /* 1035 */ {I_SHRD, 3, {REG16,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7267, IF_386|IF_SM2|IF_SB|IF_AR2},
    /* 1036 */ {I_SHRD, 3, {MEMORY,REG32,IMMEDIATE,0,0}, nasm_bytecodes+7274, IF_386|IF_SM2|IF_SB|IF_AR2},
    /* 1037 */ {I_SHRD, 3, {REG32,REG32,IMMEDIATE,0,0}, nasm_bytecodes+7274, IF_386|IF_SM2|IF_SB|IF_AR2},
    /* 1038 */ {I_SHRD, 3, {MEMORY,REG64,IMMEDIATE,0,0}, nasm_bytecodes+7281, IF_X64|IF_SM2|IF_SB|IF_AR2},
    /* 1039 */ {I_SHRD, 3, {REG64,REG64,IMMEDIATE,0,0}, nasm_bytecodes+7281, IF_X64|IF_SM2|IF_SB|IF_AR2},
    /* 1040 */ {I_SHRD, 3, {MEMORY,REG16,REG_CL,0,0}, nasm_bytecodes+14994, IF_386|IF_SM},
    /* 1041 */ {I_SHRD, 3, {REG16,REG16,REG_CL,0,0}, nasm_bytecodes+14994, IF_386},
    /* 1042 */ {I_SHRD, 3, {MEMORY,REG32,REG_CL,0,0}, nasm_bytecodes+15000, IF_386|IF_SM},
    /* 1043 */ {I_SHRD, 3, {REG32,REG32,REG_CL,0,0}, nasm_bytecodes+15000, IF_386},
    /* 1044 */ {I_SHRD, 3, {MEMORY,REG64,REG_CL,0,0}, nasm_bytecodes+15006, IF_X64|IF_SM},
    /* 1045 */ {I_SHRD, 3, {REG64,REG64,REG_CL,0,0}, nasm_bytecodes+15006, IF_X64},
    /* 1046 */ {I_SIDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19060, IF_286},
    /* 1047 */ {I_SLDT, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15031, IF_286},
    /* 1048 */ {I_SLDT, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+15031, IF_286},
    /* 1049 */ {I_SLDT, 1, {REG16,0,0,0,0}, nasm_bytecodes+15012, IF_286},
    /* 1050 */ {I_SLDT, 1, {REG32,0,0,0,0}, nasm_bytecodes+15018, IF_386},
    /* 1051 */ {I_SLDT, 1, {REG64,0,0,0,0}, nasm_bytecodes+15030, IF_X64},
    /* 1052 */ {I_SKINIT, 0, {0,0,0,0,0}, nasm_bytecodes+19065, IF_X64},
    /* 1053 */ {I_SMI, 0, {0,0,0,0,0}, nasm_bytecodes+20437, IF_386|IF_UNDOC},
    /* 1054 */ {I_SMSW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15043, IF_286},
    /* 1055 */ {I_SMSW, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+15043, IF_286},
    /* 1056 */ {I_SMSW, 1, {REG16,0,0,0,0}, nasm_bytecodes+15036, IF_286},
    /* 1057 */ {I_SMSW, 1, {REG32,0,0,0,0}, nasm_bytecodes+15042, IF_386},
    /* 1058 */ {I_STC, 0, {0,0,0,0,0}, nasm_bytecodes+18797, IF_8086},
    /* 1059 */ {I_STD, 0, {0,0,0,0,0}, nasm_bytecodes+20470, IF_8086},
    /* 1060 */ {I_STGI, 0, {0,0,0,0,0}, nasm_bytecodes+19070, IF_X64},
    /* 1061 */ {I_STI, 0, {0,0,0,0,0}, nasm_bytecodes+20473, IF_8086},
    /* 1062 */ {I_STOSB, 0, {0,0,0,0,0}, nasm_bytecodes+5236, IF_8086},
    /* 1063 */ {I_STOSD, 0, {0,0,0,0,0}, nasm_bytecodes+20305, IF_386},
    /* 1064 */ {I_STOSQ, 0, {0,0,0,0,0}, nasm_bytecodes+20309, IF_X64},
    /* 1065 */ {I_STOSW, 0, {0,0,0,0,0}, nasm_bytecodes+20313, IF_8086},
    /* 1066 */ {I_STR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15061, IF_286|IF_PROT},
    /* 1067 */ {I_STR, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+15061, IF_286|IF_PROT},
    /* 1068 */ {I_STR, 1, {REG16,0,0,0,0}, nasm_bytecodes+15048, IF_286|IF_PROT},
    /* 1069 */ {I_STR, 1, {REG32,0,0,0,0}, nasm_bytecodes+15054, IF_386|IF_PROT},
    /* 1070 */ {I_STR, 1, {REG64,0,0,0,0}, nasm_bytecodes+15060, IF_X64},
    /* 1071 */ {I_SUB, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20317, IF_8086|IF_SM},
    /* 1072 */ {I_SUB, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20317, IF_8086},
    /* 1073 */ {I_SUB, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+19075, IF_8086|IF_SM},
    /* 1074 */ {I_SUB, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19075, IF_8086},
    /* 1075 */ {I_SUB, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+19080, IF_386|IF_SM},
    /* 1076 */ {I_SUB, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19080, IF_386},
    /* 1077 */ {I_SUB, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+19085, IF_X64|IF_SM},
    /* 1078 */ {I_SUB, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19085, IF_X64},
    /* 1079 */ {I_SUB, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+10574, IF_8086|IF_SM},
    /* 1080 */ {I_SUB, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+10574, IF_8086},
    /* 1081 */ {I_SUB, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+19090, IF_8086|IF_SM},
    /* 1082 */ {I_SUB, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19090, IF_8086},
    /* 1083 */ {I_SUB, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+19095, IF_386|IF_SM},
    /* 1084 */ {I_SUB, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19095, IF_386},
    /* 1085 */ {I_SUB, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+19100, IF_X64|IF_SM},
    /* 1086 */ {I_SUB, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19100, IF_X64},
    /* 1087 */ {I_SUB, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15066, IF_8086},
    /* 1088 */ {I_SUB, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15072, IF_386},
    /* 1089 */ {I_SUB, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15078, IF_X64},
    /* 1090 */ {I_SUB, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+20321, IF_8086|IF_SM},
    /* 1091 */ {I_SUB, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+15066, IF_8086|IF_SM},
    /* 1092 */ {I_SUB, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+19105, IF_8086|IF_SM},
    /* 1093 */ {I_SUB, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+15072, IF_386|IF_SM},
    /* 1094 */ {I_SUB, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19110, IF_386|IF_SM},
    /* 1095 */ {I_SUB, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+15078, IF_X64|IF_SM},
    /* 1096 */ {I_SUB, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19115, IF_X64|IF_SM},
    /* 1097 */ {I_SUB, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+19120, IF_8086|IF_SM},
    /* 1098 */ {I_SUB, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+15084, IF_8086|IF_SM},
    /* 1099 */ {I_SUB, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+15090, IF_386|IF_SM},
    /* 1100 */ {I_SUB, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+15096, IF_X64|IF_SM},
    /* 1101 */ {I_SUB, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+19120, IF_8086|IF_SM},
    /* 1102 */ {I_SUB, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+15084, IF_8086|IF_SM},
    /* 1103 */ {I_SUB, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+15090, IF_386|IF_SM},
    /* 1104 */ {I_SVDC, 2, {MEMORY|BITS80,REG_SREG,0,0,0}, nasm_bytecodes+7675, IF_486|IF_CYRIX|IF_SMM},
    /* 1105 */ {I_SVTS, 1, {MEMORY|BITS80,0,0,0,0}, nasm_bytecodes+19130, IF_486|IF_CYRIX|IF_SMM},
    /* 1106 */ {I_SWAPGS, 0, {0,0,0,0,0}, nasm_bytecodes+19135, IF_X64},
    /* 1107 */ {I_SYSCALL, 0, {0,0,0,0,0}, nasm_bytecodes+20025, IF_P6|IF_AMD},
    /* 1108 */ {I_SYSENTER, 0, {0,0,0,0,0}, nasm_bytecodes+20325, IF_P6},
    /* 1109 */ {I_SYSEXIT, 0, {0,0,0,0,0}, nasm_bytecodes+20329, IF_P6|IF_PRIV},
    /* 1110 */ {I_SYSRET, 0, {0,0,0,0,0}, nasm_bytecodes+20021, IF_P6|IF_PRIV|IF_AMD},
    /* 1111 */ {I_TEST, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20333, IF_8086|IF_SM},
    /* 1112 */ {I_TEST, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20333, IF_8086},
    /* 1113 */ {I_TEST, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+19140, IF_8086|IF_SM},
    /* 1114 */ {I_TEST, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19140, IF_8086},
    /* 1115 */ {I_TEST, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+19145, IF_386|IF_SM},
    /* 1116 */ {I_TEST, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19145, IF_386},
    /* 1117 */ {I_TEST, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+19150, IF_X64|IF_SM},
    /* 1118 */ {I_TEST, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19150, IF_X64},
    /* 1119 */ {I_TEST, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+20337, IF_8086|IF_SM},
    /* 1120 */ {I_TEST, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+19155, IF_8086|IF_SM},
    /* 1121 */ {I_TEST, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+19160, IF_386|IF_SM},
    /* 1122 */ {I_TEST, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+19165, IF_X64|IF_SM},
    /* 1123 */ {I_TEST, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+20341, IF_8086|IF_SM},
    /* 1124 */ {I_TEST, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+19170, IF_8086|IF_SM},
    /* 1125 */ {I_TEST, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19175, IF_386|IF_SM},
    /* 1126 */ {I_TEST, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19180, IF_X64|IF_SM},
    /* 1127 */ {I_TEST, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+19185, IF_8086|IF_SM},
    /* 1128 */ {I_TEST, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+15102, IF_8086|IF_SM},
    /* 1129 */ {I_TEST, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+15108, IF_386|IF_SM},
    /* 1130 */ {I_TEST, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+15114, IF_X64|IF_SM},
    /* 1131 */ {I_TEST, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+19185, IF_8086|IF_SM},
    /* 1132 */ {I_TEST, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+15102, IF_8086|IF_SM},
    /* 1133 */ {I_TEST, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+15108, IF_386|IF_SM},
    /* 1134 */ {I_UD0, 0, {0,0,0,0,0}, nasm_bytecodes+20345, IF_186|IF_UNDOC},
    /* 1135 */ {I_UD1, 0, {0,0,0,0,0}, nasm_bytecodes+20349, IF_186|IF_UNDOC},
    /* 1136 */ {I_UD2, 0, {0,0,0,0,0}, nasm_bytecodes+20353, IF_186},
    /* 1137 */ {I_VERR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19190, IF_286|IF_PROT},
    /* 1138 */ {I_VERR, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19190, IF_286|IF_PROT},
    /* 1139 */ {I_VERR, 1, {REG16,0,0,0,0}, nasm_bytecodes+19190, IF_286|IF_PROT},
    /* 1140 */ {I_VERW, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19195, IF_286|IF_PROT},
    /* 1141 */ {I_VERW, 1, {MEMORY|BITS16,0,0,0,0}, nasm_bytecodes+19195, IF_286|IF_PROT},
    /* 1142 */ {I_VERW, 1, {REG16,0,0,0,0}, nasm_bytecodes+19195, IF_286|IF_PROT},
    /* 1143 */ {I_FWAIT, 0, {0,0,0,0,0}, nasm_bytecodes+19919, IF_8086},
    /* 1144 */ {I_WBINVD, 0, {0,0,0,0,0}, nasm_bytecodes+20357, IF_486|IF_PRIV},
    /* 1145 */ {I_WRSHR, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+15132, IF_P6|IF_CYRIX|IF_SMM},
    /* 1146 */ {I_WRMSR, 0, {0,0,0,0,0}, nasm_bytecodes+20361, IF_PENT|IF_PRIV},
    /* 1147 */ {I_XADD, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+19200, IF_486|IF_SM},
    /* 1148 */ {I_XADD, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+19200, IF_486},
    /* 1149 */ {I_XADD, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+15138, IF_486|IF_SM},
    /* 1150 */ {I_XADD, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+15138, IF_486},
    /* 1151 */ {I_XADD, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+15144, IF_486|IF_SM},
    /* 1152 */ {I_XADD, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+15144, IF_486},
    /* 1153 */ {I_XADD, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+15150, IF_X64|IF_SM},
    /* 1154 */ {I_XADD, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+15150, IF_X64},
    /* 1155 */ {I_XCHG, 2, {REG_AX,REG16,0,0,0}, nasm_bytecodes+20365, IF_8086},
    /* 1156 */ {I_XCHG, 2, {REG_EAX,REG32NA,0,0,0}, nasm_bytecodes+20369, IF_386},
    /* 1157 */ {I_XCHG, 2, {REG_RAX,REG64,0,0,0}, nasm_bytecodes+20373, IF_X64},
    /* 1158 */ {I_XCHG, 2, {REG16,REG_AX,0,0,0}, nasm_bytecodes+20377, IF_8086},
    /* 1159 */ {I_XCHG, 2, {REG32NA,REG_EAX,0,0,0}, nasm_bytecodes+20381, IF_386},
    /* 1160 */ {I_XCHG, 2, {REG64,REG_RAX,0,0,0}, nasm_bytecodes+20385, IF_X64},
    /* 1161 */ {I_XCHG, 2, {REG_EAX,REG_EAX,0,0,0}, nasm_bytecodes+20389, IF_386|IF_NOLONG},
    /* 1162 */ {I_XCHG, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+20393, IF_8086|IF_SM},
    /* 1163 */ {I_XCHG, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20393, IF_8086},
    /* 1164 */ {I_XCHG, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+19205, IF_8086|IF_SM},
    /* 1165 */ {I_XCHG, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19205, IF_8086},
    /* 1166 */ {I_XCHG, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+19210, IF_386|IF_SM},
    /* 1167 */ {I_XCHG, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19210, IF_386},
    /* 1168 */ {I_XCHG, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+19215, IF_X64|IF_SM},
    /* 1169 */ {I_XCHG, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19215, IF_X64},
    /* 1170 */ {I_XCHG, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20397, IF_8086|IF_SM},
    /* 1171 */ {I_XCHG, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20397, IF_8086},
    /* 1172 */ {I_XCHG, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+19220, IF_8086|IF_SM},
    /* 1173 */ {I_XCHG, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19220, IF_8086},
    /* 1174 */ {I_XCHG, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+19225, IF_386|IF_SM},
    /* 1175 */ {I_XCHG, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19225, IF_386},
    /* 1176 */ {I_XCHG, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+19230, IF_X64|IF_SM},
    /* 1177 */ {I_XCHG, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19230, IF_X64},
    /* 1178 */ {I_XLATB, 0, {0,0,0,0,0}, nasm_bytecodes+20476, IF_8086},
    /* 1179 */ {I_XLAT, 0, {0,0,0,0,0}, nasm_bytecodes+20476, IF_8086},
    /* 1180 */ {I_XOR, 2, {MEMORY,REG8,0,0,0}, nasm_bytecodes+20401, IF_8086|IF_SM},
    /* 1181 */ {I_XOR, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+20401, IF_8086},
    /* 1182 */ {I_XOR, 2, {MEMORY,REG16,0,0,0}, nasm_bytecodes+19235, IF_8086|IF_SM},
    /* 1183 */ {I_XOR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19235, IF_8086},
    /* 1184 */ {I_XOR, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+19240, IF_386|IF_SM},
    /* 1185 */ {I_XOR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19240, IF_386},
    /* 1186 */ {I_XOR, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+19245, IF_X64|IF_SM},
    /* 1187 */ {I_XOR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19245, IF_X64},
    /* 1188 */ {I_XOR, 2, {REG8,MEMORY,0,0,0}, nasm_bytecodes+11652, IF_8086|IF_SM},
    /* 1189 */ {I_XOR, 2, {REG8,REG8,0,0,0}, nasm_bytecodes+11652, IF_8086},
    /* 1190 */ {I_XOR, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+19250, IF_8086|IF_SM},
    /* 1191 */ {I_XOR, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+19250, IF_8086},
    /* 1192 */ {I_XOR, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+19255, IF_386|IF_SM},
    /* 1193 */ {I_XOR, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+19255, IF_386},
    /* 1194 */ {I_XOR, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+19260, IF_X64|IF_SM},
    /* 1195 */ {I_XOR, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+19260, IF_X64},
    /* 1196 */ {I_XOR, 2, {RM_GPR|BITS16,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15168, IF_8086},
    /* 1197 */ {I_XOR, 2, {RM_GPR|BITS32,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15174, IF_386},
    /* 1198 */ {I_XOR, 2, {RM_GPR|BITS64,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+15180, IF_X64},
    /* 1199 */ {I_XOR, 2, {REG_AL,IMMEDIATE,0,0,0}, nasm_bytecodes+20405, IF_8086|IF_SM},
    /* 1200 */ {I_XOR, 2, {REG_AX,SBYTE16,0,0,0}, nasm_bytecodes+15168, IF_8086|IF_SM},
    /* 1201 */ {I_XOR, 2, {REG_AX,IMMEDIATE,0,0,0}, nasm_bytecodes+19265, IF_8086|IF_SM},
    /* 1202 */ {I_XOR, 2, {REG_EAX,SBYTE32,0,0,0}, nasm_bytecodes+15174, IF_386|IF_SM},
    /* 1203 */ {I_XOR, 2, {REG_EAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19270, IF_386|IF_SM},
    /* 1204 */ {I_XOR, 2, {REG_RAX,SBYTE64,0,0,0}, nasm_bytecodes+15180, IF_X64|IF_SM},
    /* 1205 */ {I_XOR, 2, {REG_RAX,IMMEDIATE,0,0,0}, nasm_bytecodes+19275, IF_X64|IF_SM},
    /* 1206 */ {I_XOR, 2, {RM_GPR|BITS8,IMMEDIATE,0,0,0}, nasm_bytecodes+19280, IF_8086|IF_SM},
    /* 1207 */ {I_XOR, 2, {RM_GPR|BITS16,IMMEDIATE,0,0,0}, nasm_bytecodes+15186, IF_8086|IF_SM},
    /* 1208 */ {I_XOR, 2, {RM_GPR|BITS32,IMMEDIATE,0,0,0}, nasm_bytecodes+15192, IF_386|IF_SM},
    /* 1209 */ {I_XOR, 2, {RM_GPR|BITS64,IMMEDIATE,0,0,0}, nasm_bytecodes+15198, IF_X64|IF_SM},
    /* 1210 */ {I_XOR, 2, {MEMORY,IMMEDIATE|BITS8,0,0,0}, nasm_bytecodes+19280, IF_8086|IF_SM},
    /* 1211 */ {I_XOR, 2, {MEMORY,IMMEDIATE|BITS16,0,0,0}, nasm_bytecodes+15186, IF_8086|IF_SM},
    /* 1212 */ {I_XOR, 2, {MEMORY,IMMEDIATE|BITS32,0,0,0}, nasm_bytecodes+15192, IF_386|IF_SM},
    /* 1213 */ {I_CMOVcc, 2, {REG16,MEMORY,0,0,0}, nasm_bytecodes+7316, IF_P6|IF_SM},
    /* 1214 */ {I_CMOVcc, 2, {REG16,REG16,0,0,0}, nasm_bytecodes+7316, IF_P6},
    /* 1215 */ {I_CMOVcc, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+7323, IF_P6|IF_SM},
    /* 1216 */ {I_CMOVcc, 2, {REG32,REG32,0,0,0}, nasm_bytecodes+7323, IF_P6},
    /* 1217 */ {I_CMOVcc, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+7330, IF_X64|IF_SM},
    /* 1218 */ {I_CMOVcc, 2, {REG64,REG64,0,0,0}, nasm_bytecodes+7330, IF_X64},
    /* 1219 */ {I_Jcc, 1, {IMMEDIATE|NEAR,0,0,0,0}, nasm_bytecodes+7337, IF_386},
    /* 1220 */ {I_Jcc, 1, {IMMEDIATE|BITS16|NEAR,0,0,0,0}, nasm_bytecodes+7344, IF_386},
    /* 1221 */ {I_Jcc, 1, {IMMEDIATE|BITS32|NEAR,0,0,0,0}, nasm_bytecodes+7351, IF_386},
    /* 1222 */ {I_Jcc, 1, {IMMEDIATE,0,0,0,0}, nasm_bytecodes+19286, IF_8086},
    /* 1223 */ {I_SETcc, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15204, IF_386|IF_SB},
    /* 1224 */ {I_SETcc, 1, {REG8,0,0,0,0}, nasm_bytecodes+15204, IF_386},
    /* 1225 */ {I_ADDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15210, IF_KATMAI|IF_SSE},
    /* 1226 */ {I_ADDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15216, IF_KATMAI|IF_SSE|IF_SD},
    /* 1227 */ {I_ANDNPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15222, IF_KATMAI|IF_SSE},
    /* 1228 */ {I_ANDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15228, IF_KATMAI|IF_SSE},
    /* 1229 */ {I_CMPEQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5255, IF_KATMAI|IF_SSE},
    /* 1230 */ {I_CMPEQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5263, IF_KATMAI|IF_SSE},
    /* 1231 */ {I_CMPLEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5271, IF_KATMAI|IF_SSE},
    /* 1232 */ {I_CMPLESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5279, IF_KATMAI|IF_SSE},
    /* 1233 */ {I_CMPLTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5287, IF_KATMAI|IF_SSE},
    /* 1234 */ {I_CMPLTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5295, IF_KATMAI|IF_SSE},
    /* 1235 */ {I_CMPNEQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5303, IF_KATMAI|IF_SSE},
    /* 1236 */ {I_CMPNEQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5311, IF_KATMAI|IF_SSE},
    /* 1237 */ {I_CMPNLEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5319, IF_KATMAI|IF_SSE},
    /* 1238 */ {I_CMPNLESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5327, IF_KATMAI|IF_SSE},
    /* 1239 */ {I_CMPNLTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5335, IF_KATMAI|IF_SSE},
    /* 1240 */ {I_CMPNLTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5343, IF_KATMAI|IF_SSE},
    /* 1241 */ {I_CMPORDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5351, IF_KATMAI|IF_SSE},
    /* 1242 */ {I_CMPORDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5359, IF_KATMAI|IF_SSE},
    /* 1243 */ {I_CMPUNORDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5367, IF_KATMAI|IF_SSE},
    /* 1244 */ {I_CMPUNORDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5375, IF_KATMAI|IF_SSE},
    /* 1245 */ {I_CMPPS, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7365, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    /* 1246 */ {I_CMPPS, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7365, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    /* 1247 */ {I_CMPSS, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7372, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    /* 1248 */ {I_CMPSS, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7372, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    /* 1249 */ {I_COMISS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15234, IF_KATMAI|IF_SSE},
    /* 1250 */ {I_CVTPI2PS, 2, {XMMREG,RM_MMX,0,0,0}, nasm_bytecodes+15240, IF_KATMAI|IF_SSE|IF_MMX|IF_SQ},
    /* 1251 */ {I_CVTPS2PI, 2, {MMXREG,RM_XMM,0,0,0}, nasm_bytecodes+15246, IF_KATMAI|IF_SSE|IF_MMX|IF_SQ},
    /* 1252 */ {I_CVTSI2SS, 2, {XMMREG,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+7380, IF_KATMAI|IF_SSE|IF_SD|IF_AR1},
    /* 1253 */ {I_CVTSI2SS, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+7379, IF_X64|IF_SSE|IF_SQ|IF_AR1},
    /* 1254 */ {I_CVTSS2SI, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+7387, IF_KATMAI|IF_SSE|IF_SD|IF_AR1},
    /* 1255 */ {I_CVTSS2SI, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+7387, IF_KATMAI|IF_SSE|IF_SD|IF_AR1},
    /* 1256 */ {I_CVTSS2SI, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+7386, IF_X64|IF_SSE|IF_SD|IF_AR1},
    /* 1257 */ {I_CVTSS2SI, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+7386, IF_X64|IF_SSE|IF_SD|IF_AR1},
    /* 1258 */ {I_CVTTPS2PI, 2, {MMXREG,RM_XMM,0,0,0}, nasm_bytecodes+15252, IF_KATMAI|IF_SSE|IF_MMX|IF_SQ},
    /* 1259 */ {I_CVTTSS2SI, 2, {REG32,RM_XMM,0,0,0}, nasm_bytecodes+7394, IF_KATMAI|IF_SSE|IF_SD|IF_AR1},
    /* 1260 */ {I_CVTTSS2SI, 2, {REG64,RM_XMM,0,0,0}, nasm_bytecodes+7393, IF_X64|IF_SSE|IF_SD|IF_AR1},
    /* 1261 */ {I_DIVPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15258, IF_KATMAI|IF_SSE},
    /* 1262 */ {I_DIVSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15264, IF_KATMAI|IF_SSE},
    /* 1263 */ {I_LDMXCSR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19290, IF_KATMAI|IF_SSE|IF_SD},
    /* 1264 */ {I_MAXPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15270, IF_KATMAI|IF_SSE},
    /* 1265 */ {I_MAXSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15276, IF_KATMAI|IF_SSE},
    /* 1266 */ {I_MINPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15282, IF_KATMAI|IF_SSE},
    /* 1267 */ {I_MINSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15288, IF_KATMAI|IF_SSE},
    /* 1268 */ {I_MOVAPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15294, IF_KATMAI|IF_SSE},
    /* 1269 */ {I_MOVAPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15300, IF_KATMAI|IF_SSE},
    /* 1270 */ {I_MOVAPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15294, IF_KATMAI|IF_SSE},
    /* 1271 */ {I_MOVAPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15300, IF_KATMAI|IF_SSE},
    /* 1272 */ {I_MOVHPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15306, IF_KATMAI|IF_SSE},
    /* 1273 */ {I_MOVHPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15312, IF_KATMAI|IF_SSE},
    /* 1274 */ {I_MOVLHPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15306, IF_KATMAI|IF_SSE},
    /* 1275 */ {I_MOVLPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15126, IF_KATMAI|IF_SSE},
    /* 1276 */ {I_MOVLPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15318, IF_KATMAI|IF_SSE},
    /* 1277 */ {I_MOVHLPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15126, IF_KATMAI|IF_SSE},
    /* 1278 */ {I_MOVMSKPS, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+15324, IF_KATMAI|IF_SSE},
    /* 1279 */ {I_MOVMSKPS, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+7400, IF_X64|IF_SSE},
    /* 1280 */ {I_MOVNTPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15330, IF_KATMAI|IF_SSE},
    /* 1281 */ {I_MOVSS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15336, IF_KATMAI|IF_SSE},
    /* 1282 */ {I_MOVSS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15342, IF_KATMAI|IF_SSE},
    /* 1283 */ {I_MOVSS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15336, IF_KATMAI|IF_SSE},
    /* 1284 */ {I_MOVSS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15342, IF_KATMAI|IF_SSE},
    /* 1285 */ {I_MOVUPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15348, IF_KATMAI|IF_SSE},
    /* 1286 */ {I_MOVUPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15354, IF_KATMAI|IF_SSE},
    /* 1287 */ {I_MOVUPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15348, IF_KATMAI|IF_SSE},
    /* 1288 */ {I_MOVUPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15354, IF_KATMAI|IF_SSE},
    /* 1289 */ {I_MULPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15360, IF_KATMAI|IF_SSE},
    /* 1290 */ {I_MULSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15366, IF_KATMAI|IF_SSE},
    /* 1291 */ {I_ORPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15372, IF_KATMAI|IF_SSE},
    /* 1292 */ {I_RCPPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15378, IF_KATMAI|IF_SSE},
    /* 1293 */ {I_RCPSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15384, IF_KATMAI|IF_SSE},
    /* 1294 */ {I_RSQRTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15390, IF_KATMAI|IF_SSE},
    /* 1295 */ {I_RSQRTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15396, IF_KATMAI|IF_SSE},
    /* 1296 */ {I_SHUFPS, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7407, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    /* 1297 */ {I_SHUFPS, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7407, IF_KATMAI|IF_SSE|IF_SB|IF_AR2},
    /* 1298 */ {I_SQRTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15402, IF_KATMAI|IF_SSE},
    /* 1299 */ {I_SQRTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15408, IF_KATMAI|IF_SSE},
    /* 1300 */ {I_STMXCSR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19295, IF_KATMAI|IF_SSE|IF_SD},
    /* 1301 */ {I_SUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15414, IF_KATMAI|IF_SSE},
    /* 1302 */ {I_SUBSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15420, IF_KATMAI|IF_SSE},
    /* 1303 */ {I_UCOMISS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15426, IF_KATMAI|IF_SSE},
    /* 1304 */ {I_UNPCKHPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15432, IF_KATMAI|IF_SSE},
    /* 1305 */ {I_UNPCKLPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15438, IF_KATMAI|IF_SSE},
    /* 1306 */ {I_XORPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15444, IF_KATMAI|IF_SSE},
    /* 1307 */ {I_FXRSTOR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19300, IF_P6|IF_SSE|IF_FPU},
    /* 1308 */ {I_FXSAVE, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19305, IF_P6|IF_SSE|IF_FPU},
    /* 1309 */ {I_XGETBV, 0, {0,0,0,0,0}, nasm_bytecodes+15450, IF_NEHALEM},
    /* 1310 */ {I_XSETBV, 0, {0,0,0,0,0}, nasm_bytecodes+15456, IF_NEHALEM|IF_PRIV},
    /* 1311 */ {I_XSAVE, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15462, IF_NEHALEM},
    /* 1312 */ {I_XRSTOR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+15468, IF_NEHALEM},
    /* 1313 */ {I_PREFETCHNTA, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16369, IF_KATMAI},
    /* 1314 */ {I_PREFETCHT0, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16387, IF_KATMAI},
    /* 1315 */ {I_PREFETCHT1, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16405, IF_KATMAI},
    /* 1316 */ {I_PREFETCHT2, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16423, IF_KATMAI},
    /* 1317 */ {I_SFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+19015, IF_KATMAI},
    /* 1318 */ {I_MASKMOVQ, 2, {MMXREG,MMXREG,0,0,0}, nasm_bytecodes+15474, IF_KATMAI|IF_MMX},
    /* 1319 */ {I_MOVNTQ, 2, {MEMORY,MMXREG,0,0,0}, nasm_bytecodes+15480, IF_KATMAI|IF_MMX|IF_SQ},
    /* 1320 */ {I_PAVGB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7414, IF_KATMAI|IF_MMX|IF_SQ},
    /* 1321 */ {I_PAVGW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7421, IF_KATMAI|IF_MMX|IF_SQ},
    /* 1322 */ {I_PEXTRW, 3, {REG32,MMXREG,IMMEDIATE,0,0}, nasm_bytecodes+7428, IF_KATMAI|IF_MMX|IF_SB|IF_AR2},
    /* 1323 */ {I_PINSRW, 3, {MMXREG,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7435, IF_KATMAI|IF_MMX|IF_SB|IF_AR2},
    /* 1324 */ {I_PINSRW, 3, {MMXREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7435, IF_KATMAI|IF_MMX|IF_SB|IF_AR2},
    /* 1325 */ {I_PMAXSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7442, IF_KATMAI|IF_MMX|IF_SQ},
    /* 1326 */ {I_PMAXUB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7449, IF_KATMAI|IF_MMX|IF_SQ},
    /* 1327 */ {I_PMINSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7456, IF_KATMAI|IF_MMX|IF_SQ},
    /* 1328 */ {I_PMINUB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7463, IF_KATMAI|IF_MMX|IF_SQ},
    /* 1329 */ {I_PMOVMSKB, 2, {REG32,MMXREG,0,0,0}, nasm_bytecodes+15486, IF_KATMAI|IF_MMX},
    /* 1330 */ {I_PMULHUW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7470, IF_KATMAI|IF_MMX|IF_SQ},
    /* 1331 */ {I_PSADBW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7477, IF_KATMAI|IF_MMX|IF_SQ},
    /* 1332 */ {I_PSHUFW, 3, {MMXREG,RM_MMX,IMMEDIATE,0,0}, nasm_bytecodes+5383, IF_KATMAI|IF_MMX|IF_SM2|IF_SB|IF_AR2},
    /* 1333 */ {I_PF2IW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5391, IF_PENT|IF_3DNOW|IF_SQ},
    /* 1334 */ {I_PFNACC, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5399, IF_PENT|IF_3DNOW|IF_SQ},
    /* 1335 */ {I_PFPNACC, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5407, IF_PENT|IF_3DNOW|IF_SQ},
    /* 1336 */ {I_PI2FW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5415, IF_PENT|IF_3DNOW|IF_SQ},
    /* 1337 */ {I_PSWAPD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5423, IF_PENT|IF_3DNOW|IF_SQ},
    /* 1338 */ {I_MASKMOVDQU, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15492, IF_WILLAMETTE|IF_SSE2},
    /* 1339 */ {I_CLFLUSH, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19310, IF_WILLAMETTE|IF_SSE2},
    /* 1340 */ {I_MOVNTDQ, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15498, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1341 */ {I_MOVNTI, 2, {MEMORY,REG32,0,0,0}, nasm_bytecodes+7485, IF_WILLAMETTE|IF_SD},
    /* 1342 */ {I_MOVNTI, 2, {MEMORY,REG64,0,0,0}, nasm_bytecodes+7484, IF_X64|IF_SQ},
    /* 1343 */ {I_MOVNTPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15504, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1344 */ {I_LFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+18350, IF_WILLAMETTE|IF_SSE2},
    /* 1345 */ {I_MFENCE, 0, {0,0,0,0,0}, nasm_bytecodes+18440, IF_WILLAMETTE|IF_SSE2},
    /* 1346 */ {I_MOVD, 2, {XMMREG,REG32,0,0,0}, nasm_bytecodes+15510, IF_WILLAMETTE|IF_SSE2},
    /* 1347 */ {I_MOVD, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+15516, IF_WILLAMETTE|IF_SSE2},
    /* 1348 */ {I_MOVD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15516, IF_WILLAMETTE|IF_SSE2|IF_SD},
    /* 1349 */ {I_MOVD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15510, IF_WILLAMETTE|IF_SSE2|IF_SD},
    /* 1350 */ {I_MOVDQA, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15522, IF_WILLAMETTE|IF_SSE2},
    /* 1351 */ {I_MOVDQA, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15528, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1352 */ {I_MOVDQA, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15522, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1353 */ {I_MOVDQA, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15528, IF_WILLAMETTE|IF_SSE2},
    /* 1354 */ {I_MOVDQU, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15534, IF_WILLAMETTE|IF_SSE2},
    /* 1355 */ {I_MOVDQU, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15540, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1356 */ {I_MOVDQU, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15534, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1357 */ {I_MOVDQU, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15540, IF_WILLAMETTE|IF_SSE2},
    /* 1358 */ {I_MOVDQ2Q, 2, {MMXREG,XMMREG,0,0,0}, nasm_bytecodes+15546, IF_WILLAMETTE|IF_SSE2},
    /* 1359 */ {I_MOVQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15552, IF_WILLAMETTE|IF_SSE2},
    /* 1360 */ {I_MOVQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+15558, IF_WILLAMETTE|IF_SSE2},
    /* 1361 */ {I_MOVQ, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+15558, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    /* 1362 */ {I_MOVQ, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+15552, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    /* 1363 */ {I_MOVQ, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+7491, IF_X64|IF_SSE2},
    /* 1364 */ {I_MOVQ, 2, {RM_GPR|BITS64,XMMREG,0,0,0}, nasm_bytecodes+7498, IF_X64|IF_SSE2},
    /* 1365 */ {I_MOVQ2DQ, 2, {XMMREG,MMXREG,0,0,0}, nasm_bytecodes+15564, IF_WILLAMETTE|IF_SSE2},
    /* 1366 */ {I_PACKSSWB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15570, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1367 */ {I_PACKSSDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15576, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1368 */ {I_PACKUSWB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15582, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1369 */ {I_PADDB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15588, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1370 */ {I_PADDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15594, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1371 */ {I_PADDD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15600, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1372 */ {I_PADDQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+15606, IF_WILLAMETTE|IF_MMX|IF_SQ},
    /* 1373 */ {I_PADDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15612, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1374 */ {I_PADDSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15618, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1375 */ {I_PADDSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15624, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1376 */ {I_PADDUSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15630, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1377 */ {I_PADDUSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15636, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1378 */ {I_PAND, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15642, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1379 */ {I_PANDN, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15648, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1380 */ {I_PAVGB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15654, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1381 */ {I_PAVGW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15660, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1382 */ {I_PCMPEQB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15666, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1383 */ {I_PCMPEQW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15672, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1384 */ {I_PCMPEQD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15678, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1385 */ {I_PCMPGTB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15684, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1386 */ {I_PCMPGTW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15690, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1387 */ {I_PCMPGTD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15696, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1388 */ {I_PEXTRW, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7505, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    /* 1389 */ {I_PINSRW, 3, {XMMREG,REG16,IMMEDIATE,0,0}, nasm_bytecodes+7512, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    /* 1390 */ {I_PINSRW, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7512, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    /* 1391 */ {I_PMADDWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15702, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1392 */ {I_PMAXSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15708, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1393 */ {I_PMAXUB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15714, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1394 */ {I_PMINSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15720, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1395 */ {I_PMINUB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15726, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1396 */ {I_PMOVMSKB, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+15732, IF_WILLAMETTE|IF_SSE2},
    /* 1397 */ {I_PMULHUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15738, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1398 */ {I_PMULHW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15744, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1399 */ {I_PMULLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15750, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1400 */ {I_PMULUDQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7519, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1401 */ {I_PMULUDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15756, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1402 */ {I_POR, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15762, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1403 */ {I_PSADBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15768, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1404 */ {I_PSHUFD, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7526, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    /* 1405 */ {I_PSHUFD, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7526, IF_WILLAMETTE|IF_SSE2|IF_SM2|IF_SB|IF_AR2},
    /* 1406 */ {I_PSHUFHW, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7533, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    /* 1407 */ {I_PSHUFHW, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7533, IF_WILLAMETTE|IF_SSE2|IF_SM2|IF_SB|IF_AR2},
    /* 1408 */ {I_PSHUFLW, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7540, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    /* 1409 */ {I_PSHUFLW, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7540, IF_WILLAMETTE|IF_SSE2|IF_SM2|IF_SB|IF_AR2},
    /* 1410 */ {I_PSLLDQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7547, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    /* 1411 */ {I_PSLLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15774, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1412 */ {I_PSLLW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7554, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    /* 1413 */ {I_PSLLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15780, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1414 */ {I_PSLLD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7561, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    /* 1415 */ {I_PSLLQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15786, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1416 */ {I_PSLLQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7568, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    /* 1417 */ {I_PSRAW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15792, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1418 */ {I_PSRAW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7575, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    /* 1419 */ {I_PSRAD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15798, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1420 */ {I_PSRAD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7582, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    /* 1421 */ {I_PSRLDQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7589, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    /* 1422 */ {I_PSRLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15804, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1423 */ {I_PSRLW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7596, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    /* 1424 */ {I_PSRLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15810, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1425 */ {I_PSRLD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7603, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    /* 1426 */ {I_PSRLQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15816, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1427 */ {I_PSRLQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+7610, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR1},
    /* 1428 */ {I_PSUBB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15822, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1429 */ {I_PSUBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15828, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1430 */ {I_PSUBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15834, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1431 */ {I_PSUBQ, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7617, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1432 */ {I_PSUBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15840, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1433 */ {I_PSUBSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15846, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1434 */ {I_PSUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15852, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1435 */ {I_PSUBUSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15858, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1436 */ {I_PSUBUSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15864, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1437 */ {I_PUNPCKHBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15870, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1438 */ {I_PUNPCKHWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15876, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1439 */ {I_PUNPCKHDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15882, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1440 */ {I_PUNPCKHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15888, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1441 */ {I_PUNPCKLBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15894, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1442 */ {I_PUNPCKLWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15900, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1443 */ {I_PUNPCKLDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15906, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1444 */ {I_PUNPCKLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15912, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1445 */ {I_PXOR, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15918, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1446 */ {I_ADDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15924, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1447 */ {I_ADDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15930, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    /* 1448 */ {I_ANDNPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15936, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1449 */ {I_ANDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15942, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1450 */ {I_CMPEQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5431, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1451 */ {I_CMPEQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5439, IF_WILLAMETTE|IF_SSE2},
    /* 1452 */ {I_CMPLEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5447, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1453 */ {I_CMPLESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5455, IF_WILLAMETTE|IF_SSE2},
    /* 1454 */ {I_CMPLTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5463, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1455 */ {I_CMPLTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5471, IF_WILLAMETTE|IF_SSE2},
    /* 1456 */ {I_CMPNEQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5479, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1457 */ {I_CMPNEQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5487, IF_WILLAMETTE|IF_SSE2},
    /* 1458 */ {I_CMPNLEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5495, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1459 */ {I_CMPNLESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5503, IF_WILLAMETTE|IF_SSE2},
    /* 1460 */ {I_CMPNLTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5511, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1461 */ {I_CMPNLTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5519, IF_WILLAMETTE|IF_SSE2},
    /* 1462 */ {I_CMPORDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5527, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1463 */ {I_CMPORDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5535, IF_WILLAMETTE|IF_SSE2},
    /* 1464 */ {I_CMPUNORDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5543, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1465 */ {I_CMPUNORDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5551, IF_WILLAMETTE|IF_SSE2},
    /* 1466 */ {I_CMPPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+7624, IF_WILLAMETTE|IF_SSE2|IF_SM2|IF_SB|IF_AR2},
    /* 1467 */ {I_CMPSD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+7631, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    /* 1468 */ {I_COMISD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15948, IF_WILLAMETTE|IF_SSE2},
    /* 1469 */ {I_CVTDQ2PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15954, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    /* 1470 */ {I_CVTDQ2PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15960, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1471 */ {I_CVTPD2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15966, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1472 */ {I_CVTPD2PI, 2, {MMXREG,RM_XMM,0,0,0}, nasm_bytecodes+15972, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1473 */ {I_CVTPD2PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15978, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1474 */ {I_CVTPI2PD, 2, {XMMREG,RM_MMX,0,0,0}, nasm_bytecodes+15984, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    /* 1475 */ {I_CVTPS2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15990, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1476 */ {I_CVTPS2PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+15996, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    /* 1477 */ {I_CVTSD2SI, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+7639, IF_WILLAMETTE|IF_SSE2|IF_SQ|IF_AR1},
    /* 1478 */ {I_CVTSD2SI, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+7639, IF_WILLAMETTE|IF_SSE2|IF_SQ|IF_AR1},
    /* 1479 */ {I_CVTSD2SI, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+7638, IF_X64|IF_SSE2|IF_SQ|IF_AR1},
    /* 1480 */ {I_CVTSD2SI, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+7638, IF_X64|IF_SSE2|IF_SQ|IF_AR1},
    /* 1481 */ {I_CVTSD2SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16002, IF_WILLAMETTE|IF_SSE2|IF_SQ},
    /* 1482 */ {I_CVTSI2SD, 2, {XMMREG,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+7646, IF_WILLAMETTE|IF_SSE2|IF_SD|IF_AR1},
    /* 1483 */ {I_CVTSI2SD, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+7645, IF_X64|IF_SSE2|IF_SQ|IF_AR1},
    /* 1484 */ {I_CVTSS2SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16008, IF_WILLAMETTE|IF_SSE2|IF_SD},
    /* 1485 */ {I_CVTTPD2PI, 2, {MMXREG,RM_XMM,0,0,0}, nasm_bytecodes+16014, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1486 */ {I_CVTTPD2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16020, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1487 */ {I_CVTTPS2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16026, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1488 */ {I_CVTTSD2SI, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+7653, IF_WILLAMETTE|IF_SSE2|IF_SQ|IF_AR1},
    /* 1489 */ {I_CVTTSD2SI, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+7653, IF_WILLAMETTE|IF_SSE2|IF_SQ|IF_AR1},
    /* 1490 */ {I_CVTTSD2SI, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+7652, IF_X64|IF_SSE2|IF_SQ|IF_AR1},
    /* 1491 */ {I_CVTTSD2SI, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+7652, IF_X64|IF_SSE2|IF_SQ|IF_AR1},
    /* 1492 */ {I_DIVPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16032, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1493 */ {I_DIVSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16038, IF_WILLAMETTE|IF_SSE2},
    /* 1494 */ {I_MAXPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16044, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1495 */ {I_MAXSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16050, IF_WILLAMETTE|IF_SSE2},
    /* 1496 */ {I_MINPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16056, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1497 */ {I_MINSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16062, IF_WILLAMETTE|IF_SSE2},
    /* 1498 */ {I_MOVAPD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16068, IF_WILLAMETTE|IF_SSE2},
    /* 1499 */ {I_MOVAPD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16074, IF_WILLAMETTE|IF_SSE2},
    /* 1500 */ {I_MOVAPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16074, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1501 */ {I_MOVAPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16068, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1502 */ {I_MOVHPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16080, IF_WILLAMETTE|IF_SSE2},
    /* 1503 */ {I_MOVHPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16086, IF_WILLAMETTE|IF_SSE2},
    /* 1504 */ {I_MOVLPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16092, IF_WILLAMETTE|IF_SSE2},
    /* 1505 */ {I_MOVLPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16098, IF_WILLAMETTE|IF_SSE2},
    /* 1506 */ {I_MOVMSKPD, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+16104, IF_WILLAMETTE|IF_SSE2},
    /* 1507 */ {I_MOVMSKPD, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+7659, IF_X64|IF_SSE2},
    /* 1508 */ {I_MOVSD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16110, IF_WILLAMETTE|IF_SSE2},
    /* 1509 */ {I_MOVSD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16116, IF_WILLAMETTE|IF_SSE2},
    /* 1510 */ {I_MOVSD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16116, IF_WILLAMETTE|IF_SSE2},
    /* 1511 */ {I_MOVSD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16110, IF_WILLAMETTE|IF_SSE2},
    /* 1512 */ {I_MOVUPD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16122, IF_WILLAMETTE|IF_SSE2},
    /* 1513 */ {I_MOVUPD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16128, IF_WILLAMETTE|IF_SSE2},
    /* 1514 */ {I_MOVUPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16128, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1515 */ {I_MOVUPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16122, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1516 */ {I_MULPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16134, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1517 */ {I_MULSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16140, IF_WILLAMETTE|IF_SSE2},
    /* 1518 */ {I_ORPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16146, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1519 */ {I_SHUFPD, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+7666, IF_WILLAMETTE|IF_SSE2|IF_SB|IF_AR2},
    /* 1520 */ {I_SHUFPD, 3, {XMMREG,MEMORY,IMMEDIATE,0,0}, nasm_bytecodes+7666, IF_WILLAMETTE|IF_SSE2|IF_SM|IF_SB|IF_AR2},
    /* 1521 */ {I_SQRTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16152, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1522 */ {I_SQRTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16158, IF_WILLAMETTE|IF_SSE2},
    /* 1523 */ {I_SUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16164, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1524 */ {I_SUBSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16170, IF_WILLAMETTE|IF_SSE2},
    /* 1525 */ {I_UCOMISD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16176, IF_WILLAMETTE|IF_SSE2},
    /* 1526 */ {I_UNPCKHPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16182, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1527 */ {I_UNPCKLPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16188, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1528 */ {I_XORPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16194, IF_WILLAMETTE|IF_SSE2|IF_SO},
    /* 1529 */ {I_ADDSUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16200, IF_PRESCOTT|IF_SSE3|IF_SO},
    /* 1530 */ {I_ADDSUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16206, IF_PRESCOTT|IF_SSE3|IF_SO},
    /* 1531 */ {I_HADDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16212, IF_PRESCOTT|IF_SSE3|IF_SO},
    /* 1532 */ {I_HADDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16218, IF_PRESCOTT|IF_SSE3|IF_SO},
    /* 1533 */ {I_HSUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16224, IF_PRESCOTT|IF_SSE3|IF_SO},
    /* 1534 */ {I_HSUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16230, IF_PRESCOTT|IF_SSE3|IF_SO},
    /* 1535 */ {I_LDDQU, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+16236, IF_PRESCOTT|IF_SSE3|IF_SO},
    /* 1536 */ {I_MOVDDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16242, IF_PRESCOTT|IF_SSE3},
    /* 1537 */ {I_MOVSHDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16248, IF_PRESCOTT|IF_SSE3},
    /* 1538 */ {I_MOVSLDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+16254, IF_PRESCOTT|IF_SSE3},
    /* 1539 */ {I_VMCALL, 0, {0,0,0,0,0}, nasm_bytecodes+19315, IF_VMX},
    /* 1540 */ {I_VMCLEAR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16260, IF_VMX},
    /* 1541 */ {I_VMLAUNCH, 0, {0,0,0,0,0}, nasm_bytecodes+19320, IF_VMX},
    /* 1542 */ {I_VMLOAD, 0, {0,0,0,0,0}, nasm_bytecodes+19325, IF_X64|IF_VMX},
    /* 1543 */ {I_VMMCALL, 0, {0,0,0,0,0}, nasm_bytecodes+19330, IF_X64|IF_VMX},
    /* 1544 */ {I_VMPTRLD, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16267, IF_VMX},
    /* 1545 */ {I_VMPTRST, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+19335, IF_VMX},
    /* 1546 */ {I_VMREAD, 2, {RM_GPR|BITS32,REG32,0,0,0}, nasm_bytecodes+7674, IF_VMX|IF_NOLONG|IF_SD},
    /* 1547 */ {I_VMREAD, 2, {RM_GPR|BITS64,REG64,0,0,0}, nasm_bytecodes+7673, IF_X64|IF_VMX|IF_SQ},
    /* 1548 */ {I_VMRESUME, 0, {0,0,0,0,0}, nasm_bytecodes+19340, IF_VMX},
    /* 1549 */ {I_VMRUN, 0, {0,0,0,0,0}, nasm_bytecodes+19345, IF_X64|IF_VMX},
    /* 1550 */ {I_VMSAVE, 0, {0,0,0,0,0}, nasm_bytecodes+19350, IF_X64|IF_VMX},
    /* 1551 */ {I_VMWRITE, 2, {REG32,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+7681, IF_VMX|IF_NOLONG|IF_SD},
    /* 1552 */ {I_VMWRITE, 2, {REG64,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+7680, IF_X64|IF_VMX|IF_SQ},
    /* 1553 */ {I_VMXOFF, 0, {0,0,0,0,0}, nasm_bytecodes+19355, IF_VMX},
    /* 1554 */ {I_VMXON, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+16266, IF_VMX},
    /* 1555 */ {I_INVEPT, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+5560, IF_VMX|IF_SO|IF_NOLONG},
    /* 1556 */ {I_INVEPT, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+5559, IF_VMX|IF_SO|IF_LONG},
    /* 1557 */ {I_INVVPID, 2, {REG32,MEMORY,0,0,0}, nasm_bytecodes+5568, IF_VMX|IF_SO|IF_NOLONG},
    /* 1558 */ {I_INVVPID, 2, {REG64,MEMORY,0,0,0}, nasm_bytecodes+5567, IF_VMX|IF_SO|IF_LONG},
    /* 1559 */ {I_PABSB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7687, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1560 */ {I_PABSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7694, IF_SSSE3},
    /* 1561 */ {I_PABSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7701, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1562 */ {I_PABSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7708, IF_SSSE3},
    /* 1563 */ {I_PABSD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7715, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1564 */ {I_PABSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7722, IF_SSSE3},
    /* 1565 */ {I_PALIGNR, 3, {MMXREG,RM_MMX,IMMEDIATE,0,0}, nasm_bytecodes+5575, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1566 */ {I_PALIGNR, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5583, IF_SSSE3},
    /* 1567 */ {I_PHADDW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7729, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1568 */ {I_PHADDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7736, IF_SSSE3},
    /* 1569 */ {I_PHADDD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7743, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1570 */ {I_PHADDD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7750, IF_SSSE3},
    /* 1571 */ {I_PHADDSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7757, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1572 */ {I_PHADDSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7764, IF_SSSE3},
    /* 1573 */ {I_PHSUBW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7771, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1574 */ {I_PHSUBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7778, IF_SSSE3},
    /* 1575 */ {I_PHSUBD, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7785, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1576 */ {I_PHSUBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7792, IF_SSSE3},
    /* 1577 */ {I_PHSUBSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7799, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1578 */ {I_PHSUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7806, IF_SSSE3},
    /* 1579 */ {I_PMADDUBSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7813, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1580 */ {I_PMADDUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7820, IF_SSSE3},
    /* 1581 */ {I_PMULHRSW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7827, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1582 */ {I_PMULHRSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7834, IF_SSSE3},
    /* 1583 */ {I_PSHUFB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7841, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1584 */ {I_PSHUFB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7848, IF_SSSE3},
    /* 1585 */ {I_PSIGNB, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7855, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1586 */ {I_PSIGNB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7862, IF_SSSE3},
    /* 1587 */ {I_PSIGNW, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7869, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1588 */ {I_PSIGNW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7876, IF_SSSE3},
    /* 1589 */ {I_PSIGND, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+7883, IF_SSSE3|IF_MMX|IF_SQ},
    /* 1590 */ {I_PSIGND, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7890, IF_SSSE3},
    /* 1591 */ {I_EXTRQ, 3, {XMMREG,IMMEDIATE,IMMEDIATE,0,0}, nasm_bytecodes+5591, IF_SSE4A|IF_AMD},
    /* 1592 */ {I_EXTRQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16272, IF_SSE4A|IF_AMD},
    /* 1593 */ {I_INSERTQ, 4, {XMMREG,XMMREG,IMMEDIATE,IMMEDIATE,0}, nasm_bytecodes+5599, IF_SSE4A|IF_AMD},
    /* 1594 */ {I_INSERTQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+16278, IF_SSE4A|IF_AMD},
    /* 1595 */ {I_MOVNTSD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16284, IF_SSE4A|IF_AMD|IF_SQ},
    /* 1596 */ {I_MOVNTSS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+16290, IF_SSE4A|IF_AMD|IF_SD},
    /* 1597 */ {I_LZCNT, 2, {REG16,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+7897, IF_P6|IF_AMD},
    /* 1598 */ {I_LZCNT, 2, {REG32,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+7904, IF_P6|IF_AMD},
    /* 1599 */ {I_LZCNT, 2, {REG64,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+7911, IF_X64|IF_AMD},
    /* 1600 */ {I_BLENDPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5607, IF_SSE41},
    /* 1601 */ {I_BLENDPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5615, IF_SSE41},
    /* 1602 */ {I_BLENDVPD, 3, {XMMREG,RM_XMM,XMM0,0,0}, nasm_bytecodes+7918, IF_SSE41},
    /* 1603 */ {I_BLENDVPS, 3, {XMMREG,RM_XMM,XMM0,0,0}, nasm_bytecodes+7925, IF_SSE41},
    /* 1604 */ {I_DPPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5623, IF_SSE41},
    /* 1605 */ {I_DPPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5631, IF_SSE41},
    /* 1606 */ {I_EXTRACTPS, 3, {RM_GPR|BITS32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+1, IF_SSE41},
    /* 1607 */ {I_EXTRACTPS, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+0, IF_SSE41|IF_X64},
    /* 1608 */ {I_INSERTPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5639, IF_SSE41|IF_SD},
    /* 1609 */ {I_MOVNTDQA, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+7932, IF_SSE41},
    /* 1610 */ {I_MPSADBW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5647, IF_SSE41},
    /* 1611 */ {I_PACKUSDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7939, IF_SSE41},
    /* 1612 */ {I_PBLENDVB, 3, {XMMREG,RM_XMM,XMM0,0,0}, nasm_bytecodes+7946, IF_SSE41},
    /* 1613 */ {I_PBLENDW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5655, IF_SSE41},
    /* 1614 */ {I_PCMPEQQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7953, IF_SSE41},
    /* 1615 */ {I_PEXTRB, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+10, IF_SSE41},
    /* 1616 */ {I_PEXTRB, 3, {MEMORY|BITS8,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+10, IF_SSE41},
    /* 1617 */ {I_PEXTRB, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+9, IF_SSE41|IF_X64},
    /* 1618 */ {I_PEXTRD, 3, {RM_GPR|BITS32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+19, IF_SSE41},
    /* 1619 */ {I_PEXTRQ, 3, {RM_GPR|BITS64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+18, IF_SSE41|IF_X64},
    /* 1620 */ {I_PEXTRW, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+28, IF_SSE41},
    /* 1621 */ {I_PEXTRW, 3, {MEMORY|BITS16,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+28, IF_SSE41},
    /* 1622 */ {I_PEXTRW, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+27, IF_SSE41|IF_X64},
    /* 1623 */ {I_PHMINPOSUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7960, IF_SSE41},
    /* 1624 */ {I_PINSRB, 3, {XMMREG,REG32,IMMEDIATE,0,0}, nasm_bytecodes+5663, IF_SSE41},
    /* 1625 */ {I_PINSRB, 3, {XMMREG,MEMORY|BITS8,IMMEDIATE,0,0}, nasm_bytecodes+5663, IF_SSE41},
    /* 1626 */ {I_PINSRD, 3, {XMMREG,RM_GPR|BITS32,IMMEDIATE,0,0}, nasm_bytecodes+37, IF_SSE41},
    /* 1627 */ {I_PINSRQ, 3, {XMMREG,RM_GPR|BITS64,IMMEDIATE,0,0}, nasm_bytecodes+36, IF_SSE41|IF_X64},
    /* 1628 */ {I_PMAXSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7967, IF_SSE41},
    /* 1629 */ {I_PMAXSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7974, IF_SSE41},
    /* 1630 */ {I_PMAXUD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7981, IF_SSE41},
    /* 1631 */ {I_PMAXUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7988, IF_SSE41},
    /* 1632 */ {I_PMINSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+7995, IF_SSE41},
    /* 1633 */ {I_PMINSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8002, IF_SSE41},
    /* 1634 */ {I_PMINUD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8009, IF_SSE41},
    /* 1635 */ {I_PMINUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8016, IF_SSE41},
    /* 1636 */ {I_PMOVSXBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8023, IF_SSE41|IF_SQ},
    /* 1637 */ {I_PMOVSXBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8030, IF_SSE41|IF_SD},
    /* 1638 */ {I_PMOVSXBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8037, IF_SSE41|IF_SW},
    /* 1639 */ {I_PMOVSXWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8044, IF_SSE41|IF_SQ},
    /* 1640 */ {I_PMOVSXWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8051, IF_SSE41|IF_SD},
    /* 1641 */ {I_PMOVSXDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8058, IF_SSE41|IF_SQ},
    /* 1642 */ {I_PMOVZXBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8065, IF_SSE41|IF_SQ},
    /* 1643 */ {I_PMOVZXBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8072, IF_SSE41|IF_SD},
    /* 1644 */ {I_PMOVZXBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8079, IF_SSE41|IF_SW},
    /* 1645 */ {I_PMOVZXWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8086, IF_SSE41|IF_SQ},
    /* 1646 */ {I_PMOVZXWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8093, IF_SSE41|IF_SD},
    /* 1647 */ {I_PMOVZXDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8100, IF_SSE41|IF_SQ},
    /* 1648 */ {I_PMULDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8107, IF_SSE41},
    /* 1649 */ {I_PMULLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8114, IF_SSE41},
    /* 1650 */ {I_PTEST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8121, IF_SSE41},
    /* 1651 */ {I_ROUNDPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5671, IF_SSE41},
    /* 1652 */ {I_ROUNDPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5679, IF_SSE41},
    /* 1653 */ {I_ROUNDSD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5687, IF_SSE41},
    /* 1654 */ {I_ROUNDSS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5695, IF_SSE41},
    /* 1655 */ {I_CRC32, 2, {REG32,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+5720, IF_SSE42},
    /* 1656 */ {I_CRC32, 2, {REG32,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+5703, IF_SSE42},
    /* 1657 */ {I_CRC32, 2, {REG32,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+5711, IF_SSE42},
    /* 1658 */ {I_CRC32, 2, {REG64,RM_GPR|BITS8,0,0,0}, nasm_bytecodes+5719, IF_SSE42|IF_X64},
    /* 1659 */ {I_CRC32, 2, {REG64,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+5727, IF_SSE42|IF_X64},
    /* 1660 */ {I_PCMPESTRI, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5735, IF_SSE42},
    /* 1661 */ {I_PCMPESTRM, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5743, IF_SSE42},
    /* 1662 */ {I_PCMPISTRI, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5751, IF_SSE42},
    /* 1663 */ {I_PCMPISTRM, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5759, IF_SSE42},
    /* 1664 */ {I_PCMPGTQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8128, IF_SSE42},
    /* 1665 */ {I_POPCNT, 2, {REG16,RM_GPR|BITS16,0,0,0}, nasm_bytecodes+8135, IF_NEHALEM|IF_SW},
    /* 1666 */ {I_POPCNT, 2, {REG32,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+8142, IF_NEHALEM|IF_SD},
    /* 1667 */ {I_POPCNT, 2, {REG64,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+8149, IF_NEHALEM|IF_SQ|IF_X64},
    /* 1668 */ {I_FMADDPS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8156, IF_SSE5|IF_AMD},
    /* 1669 */ {I_FMADDPS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8163, IF_SSE5|IF_AMD},
    /* 1670 */ {I_FMADDPS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8170, IF_SSE5|IF_AMD},
    /* 1671 */ {I_FMADDPS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8177, IF_SSE5|IF_AMD},
    /* 1672 */ {I_FMADDPD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8184, IF_SSE5|IF_AMD},
    /* 1673 */ {I_FMADDPD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8191, IF_SSE5|IF_AMD},
    /* 1674 */ {I_FMADDPD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8198, IF_SSE5|IF_AMD},
    /* 1675 */ {I_FMADDPD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8205, IF_SSE5|IF_AMD},
    /* 1676 */ {I_FMADDSS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8212, IF_SSE5|IF_AMD},
    /* 1677 */ {I_FMADDSS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8219, IF_SSE5|IF_AMD},
    /* 1678 */ {I_FMADDSS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8226, IF_SSE5|IF_AMD},
    /* 1679 */ {I_FMADDSS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8233, IF_SSE5|IF_AMD},
    /* 1680 */ {I_FMADDSD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8240, IF_SSE5|IF_AMD},
    /* 1681 */ {I_FMADDSD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8247, IF_SSE5|IF_AMD},
    /* 1682 */ {I_FMADDSD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8254, IF_SSE5|IF_AMD},
    /* 1683 */ {I_FMADDSD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8261, IF_SSE5|IF_AMD},
    /* 1684 */ {I_FMSUBPS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8268, IF_SSE5|IF_AMD},
    /* 1685 */ {I_FMSUBPS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8275, IF_SSE5|IF_AMD},
    /* 1686 */ {I_FMSUBPS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8282, IF_SSE5|IF_AMD},
    /* 1687 */ {I_FMSUBPS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8289, IF_SSE5|IF_AMD},
    /* 1688 */ {I_FMSUBPD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8296, IF_SSE5|IF_AMD},
    /* 1689 */ {I_FMSUBPD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8303, IF_SSE5|IF_AMD},
    /* 1690 */ {I_FMSUBPD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8310, IF_SSE5|IF_AMD},
    /* 1691 */ {I_FMSUBPD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8317, IF_SSE5|IF_AMD},
    /* 1692 */ {I_FMSUBSS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8324, IF_SSE5|IF_AMD},
    /* 1693 */ {I_FMSUBSS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8331, IF_SSE5|IF_AMD},
    /* 1694 */ {I_FMSUBSS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8338, IF_SSE5|IF_AMD},
    /* 1695 */ {I_FMSUBSS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8345, IF_SSE5|IF_AMD},
    /* 1696 */ {I_FMSUBSD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8352, IF_SSE5|IF_AMD},
    /* 1697 */ {I_FMSUBSD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8359, IF_SSE5|IF_AMD},
    /* 1698 */ {I_FMSUBSD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8366, IF_SSE5|IF_AMD},
    /* 1699 */ {I_FMSUBSD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8373, IF_SSE5|IF_AMD},
    /* 1700 */ {I_FNMADDPS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8380, IF_SSE5|IF_AMD},
    /* 1701 */ {I_FNMADDPS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8387, IF_SSE5|IF_AMD},
    /* 1702 */ {I_FNMADDPS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8394, IF_SSE5|IF_AMD},
    /* 1703 */ {I_FNMADDPS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8401, IF_SSE5|IF_AMD},
    /* 1704 */ {I_FNMADDPD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8408, IF_SSE5|IF_AMD},
    /* 1705 */ {I_FNMADDPD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8415, IF_SSE5|IF_AMD},
    /* 1706 */ {I_FNMADDPD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8422, IF_SSE5|IF_AMD},
    /* 1707 */ {I_FNMADDPD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8429, IF_SSE5|IF_AMD},
    /* 1708 */ {I_FNMADDSS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8436, IF_SSE5|IF_AMD},
    /* 1709 */ {I_FNMADDSS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8443, IF_SSE5|IF_AMD},
    /* 1710 */ {I_FNMADDSS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8450, IF_SSE5|IF_AMD},
    /* 1711 */ {I_FNMADDSS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8457, IF_SSE5|IF_AMD},
    /* 1712 */ {I_FNMADDSD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8464, IF_SSE5|IF_AMD},
    /* 1713 */ {I_FNMADDSD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8471, IF_SSE5|IF_AMD},
    /* 1714 */ {I_FNMADDSD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8478, IF_SSE5|IF_AMD},
    /* 1715 */ {I_FNMADDSD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8485, IF_SSE5|IF_AMD},
    /* 1716 */ {I_FNMSUBPS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8492, IF_SSE5|IF_AMD},
    /* 1717 */ {I_FNMSUBPS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8499, IF_SSE5|IF_AMD},
    /* 1718 */ {I_FNMSUBPS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8506, IF_SSE5|IF_AMD},
    /* 1719 */ {I_FNMSUBPS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8513, IF_SSE5|IF_AMD},
    /* 1720 */ {I_FNMSUBPD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8520, IF_SSE5|IF_AMD},
    /* 1721 */ {I_FNMSUBPD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8527, IF_SSE5|IF_AMD},
    /* 1722 */ {I_FNMSUBPD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8534, IF_SSE5|IF_AMD},
    /* 1723 */ {I_FNMSUBPD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8541, IF_SSE5|IF_AMD},
    /* 1724 */ {I_FNMSUBSS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8548, IF_SSE5|IF_AMD},
    /* 1725 */ {I_FNMSUBSS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8555, IF_SSE5|IF_AMD},
    /* 1726 */ {I_FNMSUBSS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8562, IF_SSE5|IF_AMD},
    /* 1727 */ {I_FNMSUBSS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8569, IF_SSE5|IF_AMD},
    /* 1728 */ {I_FNMSUBSD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8576, IF_SSE5|IF_AMD},
    /* 1729 */ {I_FNMSUBSD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8583, IF_SSE5|IF_AMD},
    /* 1730 */ {I_FNMSUBSD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8590, IF_SSE5|IF_AMD},
    /* 1731 */ {I_FNMSUBSD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8597, IF_SSE5|IF_AMD},
    /* 1732 */ {I_COMEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+45, IF_SSE5|IF_AMD|IF_SO},
    /* 1733 */ {I_COMLTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+54, IF_SSE5|IF_AMD|IF_SO},
    /* 1734 */ {I_COMLEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+63, IF_SSE5|IF_AMD|IF_SO},
    /* 1735 */ {I_COMUNORDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+72, IF_SSE5|IF_AMD|IF_SO},
    /* 1736 */ {I_COMUNEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+81, IF_SSE5|IF_AMD|IF_SO},
    /* 1737 */ {I_COMUNLTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+90, IF_SSE5|IF_AMD|IF_SO},
    /* 1738 */ {I_COMUNLEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+99, IF_SSE5|IF_AMD|IF_SO},
    /* 1739 */ {I_COMORDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+108, IF_SSE5|IF_AMD|IF_SO},
    /* 1740 */ {I_COMUEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+117, IF_SSE5|IF_AMD|IF_SO},
    /* 1741 */ {I_COMULTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+126, IF_SSE5|IF_AMD|IF_SO},
    /* 1742 */ {I_COMULEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+135, IF_SSE5|IF_AMD|IF_SO},
    /* 1743 */ {I_COMFALSEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+144, IF_SSE5|IF_AMD|IF_SO},
    /* 1744 */ {I_COMNEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+153, IF_SSE5|IF_AMD|IF_SO},
    /* 1745 */ {I_COMNLTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+162, IF_SSE5|IF_AMD|IF_SO},
    /* 1746 */ {I_COMNLEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+171, IF_SSE5|IF_AMD|IF_SO},
    /* 1747 */ {I_COMTRUEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+180, IF_SSE5|IF_AMD|IF_SO},
    /* 1748 */ {I_COMPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5767, IF_SSE5|IF_AMD|IF_SO},
    /* 1749 */ {I_COMEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+189, IF_SSE5|IF_AMD|IF_SO},
    /* 1750 */ {I_COMLTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+198, IF_SSE5|IF_AMD|IF_SO},
    /* 1751 */ {I_COMLEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+207, IF_SSE5|IF_AMD|IF_SO},
    /* 1752 */ {I_COMUNORDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+216, IF_SSE5|IF_AMD|IF_SO},
    /* 1753 */ {I_COMUNEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+225, IF_SSE5|IF_AMD|IF_SO},
    /* 1754 */ {I_COMUNLTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+234, IF_SSE5|IF_AMD|IF_SO},
    /* 1755 */ {I_COMUNLEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+243, IF_SSE5|IF_AMD|IF_SO},
    /* 1756 */ {I_COMORDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+252, IF_SSE5|IF_AMD|IF_SO},
    /* 1757 */ {I_COMUEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+261, IF_SSE5|IF_AMD|IF_SO},
    /* 1758 */ {I_COMULTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+270, IF_SSE5|IF_AMD|IF_SO},
    /* 1759 */ {I_COMULEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+279, IF_SSE5|IF_AMD|IF_SO},
    /* 1760 */ {I_COMFALSEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+288, IF_SSE5|IF_AMD|IF_SO},
    /* 1761 */ {I_COMNEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+297, IF_SSE5|IF_AMD|IF_SO},
    /* 1762 */ {I_COMNLTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+306, IF_SSE5|IF_AMD|IF_SO},
    /* 1763 */ {I_COMNLEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+315, IF_SSE5|IF_AMD|IF_SO},
    /* 1764 */ {I_COMTRUEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+324, IF_SSE5|IF_AMD|IF_SO},
    /* 1765 */ {I_COMPD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5775, IF_SSE5|IF_AMD|IF_SO},
    /* 1766 */ {I_COMEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+333, IF_SSE5|IF_AMD|IF_SD},
    /* 1767 */ {I_COMLTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+342, IF_SSE5|IF_AMD|IF_SD},
    /* 1768 */ {I_COMLESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+351, IF_SSE5|IF_AMD|IF_SD},
    /* 1769 */ {I_COMUNORDSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+360, IF_SSE5|IF_AMD|IF_SD},
    /* 1770 */ {I_COMUNEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+369, IF_SSE5|IF_AMD|IF_SD},
    /* 1771 */ {I_COMUNLTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+378, IF_SSE5|IF_AMD|IF_SD},
    /* 1772 */ {I_COMUNLESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+387, IF_SSE5|IF_AMD|IF_SD},
    /* 1773 */ {I_COMORDSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+396, IF_SSE5|IF_AMD|IF_SD},
    /* 1774 */ {I_COMUEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+405, IF_SSE5|IF_AMD|IF_SD},
    /* 1775 */ {I_COMULTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+414, IF_SSE5|IF_AMD|IF_SD},
    /* 1776 */ {I_COMULESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+423, IF_SSE5|IF_AMD|IF_SD},
    /* 1777 */ {I_COMFALSESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+432, IF_SSE5|IF_AMD|IF_SD},
    /* 1778 */ {I_COMNEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+441, IF_SSE5|IF_AMD|IF_SD},
    /* 1779 */ {I_COMNLTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+450, IF_SSE5|IF_AMD|IF_SD},
    /* 1780 */ {I_COMNLESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+459, IF_SSE5|IF_AMD|IF_SD},
    /* 1781 */ {I_COMTRUESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+468, IF_SSE5|IF_AMD|IF_SD},
    /* 1782 */ {I_COMSS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5783, IF_SSE5|IF_AMD|IF_SD},
    /* 1783 */ {I_COMEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+477, IF_SSE5|IF_AMD|IF_SQ},
    /* 1784 */ {I_COMLTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+486, IF_SSE5|IF_AMD|IF_SQ},
    /* 1785 */ {I_COMLESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+495, IF_SSE5|IF_AMD|IF_SQ},
    /* 1786 */ {I_COMUNORDSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+504, IF_SSE5|IF_AMD|IF_SQ},
    /* 1787 */ {I_COMUNEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+513, IF_SSE5|IF_AMD|IF_SQ},
    /* 1788 */ {I_COMUNLTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+522, IF_SSE5|IF_AMD|IF_SQ},
    /* 1789 */ {I_COMUNLESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+531, IF_SSE5|IF_AMD|IF_SQ},
    /* 1790 */ {I_COMORDSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+540, IF_SSE5|IF_AMD|IF_SQ},
    /* 1791 */ {I_COMUEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+549, IF_SSE5|IF_AMD|IF_SQ},
    /* 1792 */ {I_COMULTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+558, IF_SSE5|IF_AMD|IF_SQ},
    /* 1793 */ {I_COMULESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+567, IF_SSE5|IF_AMD|IF_SQ},
    /* 1794 */ {I_COMFALSESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+576, IF_SSE5|IF_AMD|IF_SQ},
    /* 1795 */ {I_COMNEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+585, IF_SSE5|IF_AMD|IF_SQ},
    /* 1796 */ {I_COMNLTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+594, IF_SSE5|IF_AMD|IF_SQ},
    /* 1797 */ {I_COMNLESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+603, IF_SSE5|IF_AMD|IF_SQ},
    /* 1798 */ {I_COMTRUESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+612, IF_SSE5|IF_AMD|IF_SQ},
    /* 1799 */ {I_COMSD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5791, IF_SSE5|IF_AMD|IF_SQ},
    /* 1800 */ {I_PCOMLTB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+621, IF_SSE5|IF_AMD|IF_SO},
    /* 1801 */ {I_PCOMLEB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+630, IF_SSE5|IF_AMD|IF_SO},
    /* 1802 */ {I_PCOMGTB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+639, IF_SSE5|IF_AMD|IF_SO},
    /* 1803 */ {I_PCOMGEB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+648, IF_SSE5|IF_AMD|IF_SO},
    /* 1804 */ {I_PCOMEQB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+657, IF_SSE5|IF_AMD|IF_SO},
    /* 1805 */ {I_PCOMNEQB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+666, IF_SSE5|IF_AMD|IF_SO},
    /* 1806 */ {I_PCOMFALSEB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+675, IF_SSE5|IF_AMD|IF_SO},
    /* 1807 */ {I_PCOMTRUEB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+684, IF_SSE5|IF_AMD|IF_SO},
    /* 1808 */ {I_PCOMB, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5799, IF_SSE5|IF_AMD|IF_SO},
    /* 1809 */ {I_PCOMLTW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+693, IF_SSE5|IF_AMD|IF_SO},
    /* 1810 */ {I_PCOMLEW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+702, IF_SSE5|IF_AMD|IF_SO},
    /* 1811 */ {I_PCOMGTW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+711, IF_SSE5|IF_AMD|IF_SO},
    /* 1812 */ {I_PCOMGEW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+720, IF_SSE5|IF_AMD|IF_SO},
    /* 1813 */ {I_PCOMEQW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+729, IF_SSE5|IF_AMD|IF_SO},
    /* 1814 */ {I_PCOMNEQW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+738, IF_SSE5|IF_AMD|IF_SO},
    /* 1815 */ {I_PCOMFALSEW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+747, IF_SSE5|IF_AMD|IF_SO},
    /* 1816 */ {I_PCOMTRUEW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+756, IF_SSE5|IF_AMD|IF_SO},
    /* 1817 */ {I_PCOMW, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5807, IF_SSE5|IF_AMD|IF_SO},
    /* 1818 */ {I_PCOMLTD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+765, IF_SSE5|IF_AMD|IF_SO},
    /* 1819 */ {I_PCOMLED, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+774, IF_SSE5|IF_AMD|IF_SO},
    /* 1820 */ {I_PCOMGTD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+783, IF_SSE5|IF_AMD|IF_SO},
    /* 1821 */ {I_PCOMGED, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+792, IF_SSE5|IF_AMD|IF_SO},
    /* 1822 */ {I_PCOMEQD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+801, IF_SSE5|IF_AMD|IF_SO},
    /* 1823 */ {I_PCOMNEQD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+810, IF_SSE5|IF_AMD|IF_SO},
    /* 1824 */ {I_PCOMFALSED, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+819, IF_SSE5|IF_AMD|IF_SO},
    /* 1825 */ {I_PCOMTRUED, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+828, IF_SSE5|IF_AMD|IF_SO},
    /* 1826 */ {I_PCOMD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5815, IF_SSE5|IF_AMD|IF_SO},
    /* 1827 */ {I_PCOMLTQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+837, IF_SSE5|IF_AMD|IF_SO},
    /* 1828 */ {I_PCOMLEQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+846, IF_SSE5|IF_AMD|IF_SO},
    /* 1829 */ {I_PCOMGTQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+855, IF_SSE5|IF_AMD|IF_SO},
    /* 1830 */ {I_PCOMGEQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+864, IF_SSE5|IF_AMD|IF_SO},
    /* 1831 */ {I_PCOMEQQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+873, IF_SSE5|IF_AMD|IF_SO},
    /* 1832 */ {I_PCOMNEQQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+882, IF_SSE5|IF_AMD|IF_SO},
    /* 1833 */ {I_PCOMFALSEQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+891, IF_SSE5|IF_AMD|IF_SO},
    /* 1834 */ {I_PCOMTRUEQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+900, IF_SSE5|IF_AMD|IF_SO},
    /* 1835 */ {I_PCOMQ, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5823, IF_SSE5|IF_AMD|IF_SO},
    /* 1836 */ {I_PCOMLTUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+909, IF_SSE5|IF_AMD|IF_SO},
    /* 1837 */ {I_PCOMLEUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+918, IF_SSE5|IF_AMD|IF_SO},
    /* 1838 */ {I_PCOMGTUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+927, IF_SSE5|IF_AMD|IF_SO},
    /* 1839 */ {I_PCOMGEUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+936, IF_SSE5|IF_AMD|IF_SO},
    /* 1840 */ {I_PCOMEQUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+945, IF_SSE5|IF_AMD|IF_SO},
    /* 1841 */ {I_PCOMNEQUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+954, IF_SSE5|IF_AMD|IF_SO},
    /* 1842 */ {I_PCOMFALSEUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+963, IF_SSE5|IF_AMD|IF_SO},
    /* 1843 */ {I_PCOMTRUEUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+972, IF_SSE5|IF_AMD|IF_SO},
    /* 1844 */ {I_PCOMUB, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5831, IF_SSE5|IF_AMD|IF_SO},
    /* 1845 */ {I_PCOMLTUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+981, IF_SSE5|IF_AMD|IF_SO},
    /* 1846 */ {I_PCOMLEUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+990, IF_SSE5|IF_AMD|IF_SO},
    /* 1847 */ {I_PCOMGTUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+999, IF_SSE5|IF_AMD|IF_SO},
    /* 1848 */ {I_PCOMGEUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1008, IF_SSE5|IF_AMD|IF_SO},
    /* 1849 */ {I_PCOMEQUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1017, IF_SSE5|IF_AMD|IF_SO},
    /* 1850 */ {I_PCOMNEQUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1026, IF_SSE5|IF_AMD|IF_SO},
    /* 1851 */ {I_PCOMFALSEUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1035, IF_SSE5|IF_AMD|IF_SO},
    /* 1852 */ {I_PCOMTRUEUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1044, IF_SSE5|IF_AMD|IF_SO},
    /* 1853 */ {I_PCOMUW, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5839, IF_SSE5|IF_AMD|IF_SO},
    /* 1854 */ {I_PCOMLTUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1053, IF_SSE5|IF_AMD|IF_SO},
    /* 1855 */ {I_PCOMLEUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1062, IF_SSE5|IF_AMD|IF_SO},
    /* 1856 */ {I_PCOMGTUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1071, IF_SSE5|IF_AMD|IF_SO},
    /* 1857 */ {I_PCOMGEUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1080, IF_SSE5|IF_AMD|IF_SO},
    /* 1858 */ {I_PCOMEQUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1089, IF_SSE5|IF_AMD|IF_SO},
    /* 1859 */ {I_PCOMNEQUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1098, IF_SSE5|IF_AMD|IF_SO},
    /* 1860 */ {I_PCOMFALSEUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1107, IF_SSE5|IF_AMD|IF_SO},
    /* 1861 */ {I_PCOMTRUEUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1116, IF_SSE5|IF_AMD|IF_SO},
    /* 1862 */ {I_PCOMUD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5847, IF_SSE5|IF_AMD|IF_SO},
    /* 1863 */ {I_PCOMLTUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1125, IF_SSE5|IF_AMD|IF_SO},
    /* 1864 */ {I_PCOMLEUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1134, IF_SSE5|IF_AMD|IF_SO},
    /* 1865 */ {I_PCOMGTUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1143, IF_SSE5|IF_AMD|IF_SO},
    /* 1866 */ {I_PCOMGEUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1152, IF_SSE5|IF_AMD|IF_SO},
    /* 1867 */ {I_PCOMEQUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1161, IF_SSE5|IF_AMD|IF_SO},
    /* 1868 */ {I_PCOMNEQUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1170, IF_SSE5|IF_AMD|IF_SO},
    /* 1869 */ {I_PCOMFALSEUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1179, IF_SSE5|IF_AMD|IF_SO},
    /* 1870 */ {I_PCOMTRUEUQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1188, IF_SSE5|IF_AMD|IF_SO},
    /* 1871 */ {I_PCOMUQ, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5855, IF_SSE5|IF_AMD|IF_SO},
    /* 1872 */ {I_PERMPS, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8604, IF_SSE5|IF_AMD},
    /* 1873 */ {I_PERMPS, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8611, IF_SSE5|IF_AMD},
    /* 1874 */ {I_PERMPS, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8618, IF_SSE5|IF_AMD},
    /* 1875 */ {I_PERMPS, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8625, IF_SSE5|IF_AMD},
    /* 1876 */ {I_PERMPD, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8632, IF_SSE5|IF_AMD},
    /* 1877 */ {I_PERMPD, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8639, IF_SSE5|IF_AMD},
    /* 1878 */ {I_PERMPD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8646, IF_SSE5|IF_AMD},
    /* 1879 */ {I_PERMPD, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8653, IF_SSE5|IF_AMD},
    /* 1880 */ {I_PCMOV, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8660, IF_SSE5|IF_AMD},
    /* 1881 */ {I_PCMOV, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8667, IF_SSE5|IF_AMD},
    /* 1882 */ {I_PCMOV, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8674, IF_SSE5|IF_AMD},
    /* 1883 */ {I_PCMOV, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8681, IF_SSE5|IF_AMD},
    /* 1884 */ {I_PPERM, 4, {XMMREG,SAME_AS|0,XMMREG,RM_XMM,0}, nasm_bytecodes+8688, IF_SSE5|IF_AMD},
    /* 1885 */ {I_PPERM, 4, {XMMREG,SAME_AS|0,RM_XMM,XMMREG,0}, nasm_bytecodes+8695, IF_SSE5|IF_AMD},
    /* 1886 */ {I_PPERM, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8702, IF_SSE5|IF_AMD},
    /* 1887 */ {I_PPERM, 4, {XMMREG,RM_XMM,XMMREG,SAME_AS|0,0}, nasm_bytecodes+8709, IF_SSE5|IF_AMD},
    /* 1888 */ {I_PMACSSWW, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8716, IF_SSE5|IF_AMD},
    /* 1889 */ {I_PMACSWW, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8723, IF_SSE5|IF_AMD},
    /* 1890 */ {I_PMACSSWD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8730, IF_SSE5|IF_AMD},
    /* 1891 */ {I_PMACSWD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8737, IF_SSE5|IF_AMD},
    /* 1892 */ {I_PMACSSDD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8744, IF_SSE5|IF_AMD},
    /* 1893 */ {I_PMACSDD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8751, IF_SSE5|IF_AMD},
    /* 1894 */ {I_PMACSSDQL, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8758, IF_SSE5|IF_AMD},
    /* 1895 */ {I_PMACSDQL, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8765, IF_SSE5|IF_AMD},
    /* 1896 */ {I_PMACSSDQH, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8772, IF_SSE5|IF_AMD},
    /* 1897 */ {I_PMACSDQH, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8779, IF_SSE5|IF_AMD},
    /* 1898 */ {I_PMADCSSWD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8786, IF_SSE5|IF_AMD},
    /* 1899 */ {I_PMADCSWD, 4, {XMMREG,XMMREG,RM_XMM,SAME_AS|0,0}, nasm_bytecodes+8793, IF_SSE5|IF_AMD},
    /* 1900 */ {I_PROTB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8800, IF_SSE5|IF_AMD},
    /* 1901 */ {I_PROTB, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8807, IF_SSE5|IF_AMD},
    /* 1902 */ {I_PROTW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8814, IF_SSE5|IF_AMD},
    /* 1903 */ {I_PROTW, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8821, IF_SSE5|IF_AMD},
    /* 1904 */ {I_PROTD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8828, IF_SSE5|IF_AMD},
    /* 1905 */ {I_PROTD, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8835, IF_SSE5|IF_AMD},
    /* 1906 */ {I_PROTQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8842, IF_SSE5|IF_AMD},
    /* 1907 */ {I_PROTQ, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8849, IF_SSE5|IF_AMD},
    /* 1908 */ {I_PSHLB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8856, IF_SSE5|IF_AMD},
    /* 1909 */ {I_PSHLB, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8863, IF_SSE5|IF_AMD},
    /* 1910 */ {I_PSHLW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8870, IF_SSE5|IF_AMD},
    /* 1911 */ {I_PSHLW, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8877, IF_SSE5|IF_AMD},
    /* 1912 */ {I_PSHLD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8884, IF_SSE5|IF_AMD},
    /* 1913 */ {I_PSHLD, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8891, IF_SSE5|IF_AMD},
    /* 1914 */ {I_PSHLQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8898, IF_SSE5|IF_AMD},
    /* 1915 */ {I_PSHLQ, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8905, IF_SSE5|IF_AMD},
    /* 1916 */ {I_PSHAB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8912, IF_SSE5|IF_AMD},
    /* 1917 */ {I_PSHAB, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8919, IF_SSE5|IF_AMD},
    /* 1918 */ {I_PSHAW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8926, IF_SSE5|IF_AMD},
    /* 1919 */ {I_PSHAW, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8933, IF_SSE5|IF_AMD},
    /* 1920 */ {I_PSHAD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8940, IF_SSE5|IF_AMD},
    /* 1921 */ {I_PSHAD, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8947, IF_SSE5|IF_AMD},
    /* 1922 */ {I_PSHAQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+8954, IF_SSE5|IF_AMD},
    /* 1923 */ {I_PSHAQ, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+8961, IF_SSE5|IF_AMD},
    /* 1924 */ {I_FRCZPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8968, IF_SSE5|IF_AMD},
    /* 1925 */ {I_FRCZPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8975, IF_SSE5|IF_AMD},
    /* 1926 */ {I_FRCZSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8982, IF_SSE5|IF_AMD},
    /* 1927 */ {I_FRCZSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8989, IF_SSE5|IF_AMD},
    /* 1928 */ {I_CVTPH2PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+8996, IF_SSE5|IF_AMD|IF_SQ},
    /* 1929 */ {I_CVTPS2PH, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+9003, IF_SSE5|IF_AMD|IF_SQ},
    /* 1930 */ {I_PHADDBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9010, IF_SSE5|IF_AMD},
    /* 1931 */ {I_PHADDBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9017, IF_SSE5|IF_AMD},
    /* 1932 */ {I_PHADDBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9024, IF_SSE5|IF_AMD},
    /* 1933 */ {I_PHADDWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9031, IF_SSE5|IF_AMD},
    /* 1934 */ {I_PHADDWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9038, IF_SSE5|IF_AMD},
    /* 1935 */ {I_PHADDDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9045, IF_SSE5|IF_AMD},
    /* 1936 */ {I_PHADDUBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9052, IF_SSE5|IF_AMD},
    /* 1937 */ {I_PHADDUBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9059, IF_SSE5|IF_AMD},
    /* 1938 */ {I_PHADDUBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9066, IF_SSE5|IF_AMD},
    /* 1939 */ {I_PHADDUWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9073, IF_SSE5|IF_AMD},
    /* 1940 */ {I_PHADDUWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9080, IF_SSE5|IF_AMD},
    /* 1941 */ {I_PHADDUDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9087, IF_SSE5|IF_AMD},
    /* 1942 */ {I_PHSUBBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9094, IF_SSE5|IF_AMD},
    /* 1943 */ {I_PHSUBWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9101, IF_SSE5|IF_AMD},
    /* 1944 */ {I_PHSUBDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9108, IF_SSE5|IF_AMD},
    /* 1945 */ {I_PROTB, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5863, IF_SSE5|IF_AMD},
    /* 1946 */ {I_PROTW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5871, IF_SSE5|IF_AMD},
    /* 1947 */ {I_PROTD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5879, IF_SSE5|IF_AMD},
    /* 1948 */ {I_PROTQ, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5887, IF_SSE5|IF_AMD},
    /* 1949 */ {I_ROUNDPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5679, IF_SSE5|IF_AMD},
    /* 1950 */ {I_ROUNDPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5679, IF_SSE5|IF_AMD},
    /* 1951 */ {I_ROUNDSS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5679, IF_SSE5|IF_AMD},
    /* 1952 */ {I_ROUNDSD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5679, IF_SSE5|IF_AMD},
    /* 1953 */ {I_GETSEC, 0, {0,0,0,0,0}, nasm_bytecodes+20409, IF_KATMAI},
    /* 1954 */ {I_PFRCPV, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5895, IF_PENT|IF_3DNOW|IF_SQ|IF_CYRIX},
    /* 1955 */ {I_PFRSQRTV, 2, {MMXREG,RM_MMX,0,0,0}, nasm_bytecodes+5903, IF_PENT|IF_3DNOW|IF_SQ|IF_CYRIX},
    /* 1956 */ {I_MOVBE, 2, {REG16,MEMORY|BITS16,0,0,0}, nasm_bytecodes+9115, IF_NEHALEM|IF_SM},
    /* 1957 */ {I_MOVBE, 2, {REG32,MEMORY|BITS32,0,0,0}, nasm_bytecodes+9122, IF_NEHALEM|IF_SM},
    /* 1958 */ {I_MOVBE, 2, {REG64,MEMORY|BITS64,0,0,0}, nasm_bytecodes+9129, IF_NEHALEM|IF_SM},
    /* 1959 */ {I_MOVBE, 2, {MEMORY|BITS16,REG16,0,0,0}, nasm_bytecodes+9136, IF_NEHALEM|IF_SM},
    /* 1960 */ {I_MOVBE, 2, {MEMORY|BITS32,REG32,0,0,0}, nasm_bytecodes+9143, IF_NEHALEM|IF_SM},
    /* 1961 */ {I_MOVBE, 2, {MEMORY|BITS64,REG64,0,0,0}, nasm_bytecodes+9150, IF_NEHALEM|IF_SM},
    /* 1962 */ {I_AESENC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9157, IF_SSE|IF_WESTMERE|IF_SO},
    /* 1963 */ {I_AESENCLAST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9164, IF_SSE|IF_WESTMERE|IF_SO},
    /* 1964 */ {I_AESDEC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9171, IF_SSE|IF_WESTMERE|IF_SO},
    /* 1965 */ {I_AESDECLAST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9178, IF_SSE|IF_WESTMERE|IF_SO},
    /* 1966 */ {I_AESIMC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9185, IF_SSE|IF_WESTMERE|IF_SO},
    /* 1967 */ {I_AESKEYGENASSIST, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5911, IF_SSE|IF_WESTMERE|IF_SO},
    /* 1968 */ {I_VAESENC, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9192, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1969 */ {I_VAESENC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9199, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1970 */ {I_VAESENCLAST, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9206, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1971 */ {I_VAESENCLAST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9213, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1972 */ {I_VAESDEC, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9220, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1973 */ {I_VAESDEC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9227, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1974 */ {I_VAESDECLAST, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9234, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1975 */ {I_VAESDECLAST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9241, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1976 */ {I_VAESIMC, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9248, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1977 */ {I_VAESKEYGENASSIST, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5919, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1978 */ {I_VADDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9255, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1979 */ {I_VADDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9262, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1980 */ {I_VADDPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9269, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 1981 */ {I_VADDPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9276, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 1982 */ {I_VADDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9283, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1983 */ {I_VADDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9290, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1984 */ {I_VADDPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9297, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 1985 */ {I_VADDPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9304, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 1986 */ {I_VADDSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9311, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 1987 */ {I_VADDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9318, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 1988 */ {I_VADDSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9325, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 1989 */ {I_VADDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9332, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 1990 */ {I_VADDSUBPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9339, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1991 */ {I_VADDSUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9346, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1992 */ {I_VADDSUBPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9353, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 1993 */ {I_VADDSUBPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9360, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 1994 */ {I_VADDSUBPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9367, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1995 */ {I_VADDSUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9374, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1996 */ {I_VADDSUBPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9381, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 1997 */ {I_VADDSUBPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9388, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 1998 */ {I_VANDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9395, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 1999 */ {I_VANDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9402, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2000 */ {I_VANDPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9409, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2001 */ {I_VANDPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9416, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2002 */ {I_VANDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9423, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2003 */ {I_VANDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9430, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2004 */ {I_VANDPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9437, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2005 */ {I_VANDPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9444, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2006 */ {I_VANDNPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9451, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2007 */ {I_VANDNPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9458, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2008 */ {I_VANDNPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9465, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2009 */ {I_VANDNPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9472, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2010 */ {I_VANDNPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9479, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2011 */ {I_VANDNPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9486, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2012 */ {I_VANDNPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9493, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2013 */ {I_VANDNPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9500, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2014 */ {I_VBLENDPD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5927, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2015 */ {I_VBLENDPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5935, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2016 */ {I_VBLENDPD, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+5943, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2017 */ {I_VBLENDPD, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+5951, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2018 */ {I_VBLENDPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5959, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2019 */ {I_VBLENDPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5967, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2020 */ {I_VBLENDPS, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+5975, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2021 */ {I_VBLENDPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+5983, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2022 */ {I_VBLENDVPD, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+1197, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2023 */ {I_VBLENDVPD, 3, {XMMREG,RM_XMM,XMM0,0,0}, nasm_bytecodes+9507, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2024 */ {I_VBLENDVPD, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+1206, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2025 */ {I_VBLENDVPD, 3, {YMMREG,RM_YMM,YMM0,0,0}, nasm_bytecodes+9514, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2026 */ {I_VBLENDVPS, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+1215, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2027 */ {I_VBLENDVPS, 3, {XMMREG,RM_XMM,XMM0,0,0}, nasm_bytecodes+9521, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2028 */ {I_VBLENDVPS, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+1224, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2029 */ {I_VBLENDVPD, 3, {YMMREG,RM_YMM,YMM0,0,0}, nasm_bytecodes+9528, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2030 */ {I_VBROADCASTSS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+9535, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2031 */ {I_VBROADCASTSS, 2, {YMMREG,MEMORY,0,0,0}, nasm_bytecodes+9542, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2032 */ {I_VBROADCASTSD, 2, {YMMREG,MEMORY,0,0,0}, nasm_bytecodes+9549, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2033 */ {I_VBROADCASTF128, 2, {YMMREG,MEMORY,0,0,0}, nasm_bytecodes+9556, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2034 */ {I_VCMPEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1233, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2035 */ {I_VCMPEQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1242, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2036 */ {I_VCMPEQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1251, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2037 */ {I_VCMPEQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1260, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2038 */ {I_VCMPLTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1269, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2039 */ {I_VCMPLTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1278, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2040 */ {I_VCMPLTPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1287, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2041 */ {I_VCMPLTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1296, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2042 */ {I_VCMPLEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1305, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2043 */ {I_VCMPLEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1314, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2044 */ {I_VCMPLEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1323, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2045 */ {I_VCMPLEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1332, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2046 */ {I_VCMPUNORDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1341, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2047 */ {I_VCMPUNORDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1350, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2048 */ {I_VCMPUNORDPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1359, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2049 */ {I_VCMPUNORDPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1368, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2050 */ {I_VCMPNEQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1377, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2051 */ {I_VCMPNEQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1386, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2052 */ {I_VCMPNEQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1395, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2053 */ {I_VCMPNEQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1404, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2054 */ {I_VCMPNLTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1413, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2055 */ {I_VCMPNLTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1422, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2056 */ {I_VCMPNLTPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1431, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2057 */ {I_VCMPNLTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1440, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2058 */ {I_VCMPNLEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1449, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2059 */ {I_VCMPNLEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1458, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2060 */ {I_VCMPNLEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1467, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2061 */ {I_VCMPNLEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1476, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2062 */ {I_VCMPORDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1485, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2063 */ {I_VCMPORDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1494, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2064 */ {I_VCMPORDPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1503, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2065 */ {I_VCMPORDPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1512, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2066 */ {I_VCMPEQ_UQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1521, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2067 */ {I_VCMPEQ_UQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1530, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2068 */ {I_VCMPEQ_UQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1539, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2069 */ {I_VCMPEQ_UQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1548, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2070 */ {I_VCMPNGEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1557, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2071 */ {I_VCMPNGEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1566, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2072 */ {I_VCMPNGEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1575, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2073 */ {I_VCMPNGEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1584, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2074 */ {I_VCMPNGTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1593, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2075 */ {I_VCMPNGTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1602, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2076 */ {I_VCMPNGTPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1611, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2077 */ {I_VCMPNGTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1620, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2078 */ {I_VCMPFALSEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1629, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2079 */ {I_VCMPFALSEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1638, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2080 */ {I_VCMPFALSEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1647, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2081 */ {I_VCMPFALSEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1656, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2082 */ {I_VCMPNEQ_OQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1665, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2083 */ {I_VCMPNEQ_OQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1674, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2084 */ {I_VCMPNEQ_OQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1683, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2085 */ {I_VCMPNEQ_OQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1692, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2086 */ {I_VCMPGEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1701, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2087 */ {I_VCMPGEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1710, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2088 */ {I_VCMPGEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1719, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2089 */ {I_VCMPGEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1728, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2090 */ {I_VCMPGTPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1737, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2091 */ {I_VCMPGTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1746, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2092 */ {I_VCMPGTPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1755, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2093 */ {I_VCMPGTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1764, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2094 */ {I_VCMPTRUEPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1773, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2095 */ {I_VCMPTRUEPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1782, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2096 */ {I_VCMPTRUEPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1791, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2097 */ {I_VCMPTRUEPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1800, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2098 */ {I_VCMPEQ_OSPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1809, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2099 */ {I_VCMPEQ_OSPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1818, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2100 */ {I_VCMPEQ_OSPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1827, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2101 */ {I_VCMPEQ_OSPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1836, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2102 */ {I_VCMPLT_OQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1845, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2103 */ {I_VCMPLT_OQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1854, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2104 */ {I_VCMPLT_OQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1863, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2105 */ {I_VCMPLT_OQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1872, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2106 */ {I_VCMPLE_OQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1881, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2107 */ {I_VCMPLE_OQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1890, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2108 */ {I_VCMPLE_OQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1899, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2109 */ {I_VCMPLE_OQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1908, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2110 */ {I_VCMPUNORD_SPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1917, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2111 */ {I_VCMPUNORD_SPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1926, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2112 */ {I_VCMPUNORD_SPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1935, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2113 */ {I_VCMPUNORD_SPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1944, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2114 */ {I_VCMPNEQ_USPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1953, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2115 */ {I_VCMPNEQ_USPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1962, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2116 */ {I_VCMPNEQ_USPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+1971, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2117 */ {I_VCMPNEQ_USPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+1980, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2118 */ {I_VCMPNLT_UQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+1989, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2119 */ {I_VCMPNLT_UQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+1998, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2120 */ {I_VCMPNLT_UQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2007, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2121 */ {I_VCMPNLT_UQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2016, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2122 */ {I_VCMPNLE_UQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2025, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2123 */ {I_VCMPNLE_UQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2034, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2124 */ {I_VCMPNLE_UQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2043, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2125 */ {I_VCMPNLE_UQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2052, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2126 */ {I_VCMPORD_SPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2061, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2127 */ {I_VCMPORD_SPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2070, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2128 */ {I_VCMPORD_SPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2079, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2129 */ {I_VCMPORS_SPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2088, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2130 */ {I_VCMPEQ_USPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2097, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2131 */ {I_VCMPEQ_USPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2106, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2132 */ {I_VCMPEQ_USPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2115, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2133 */ {I_VCMPEQ_USPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2124, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2134 */ {I_VCMPNGE_UQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2133, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2135 */ {I_VCMPNGE_UQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2142, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2136 */ {I_VCMPNGE_UQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2151, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2137 */ {I_VCMPNGE_UQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2160, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2138 */ {I_VCMPNGT_UQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2169, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2139 */ {I_VCMPNGT_UQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2178, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2140 */ {I_VCMPNGT_UQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2187, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2141 */ {I_VCMPNGT_UQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2196, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2142 */ {I_VCMPFALSE_OSPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2205, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2143 */ {I_VCMPFALSE_OSPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2214, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2144 */ {I_VCMPFALSE_OSPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2223, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2145 */ {I_VCMPFALSE_OSPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2232, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2146 */ {I_VCMPNEQ_OSPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2241, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2147 */ {I_VCMPNEQ_OSPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2250, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2148 */ {I_VCMPNEQ_OSPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2259, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2149 */ {I_VCMPNEQ_OSPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2268, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2150 */ {I_VCMPGE_OQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2277, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2151 */ {I_VCMPGE_OQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2286, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2152 */ {I_VCMPGE_OQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2295, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2153 */ {I_VCMPGE_OQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2304, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2154 */ {I_VCMPGT_OQPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2313, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2155 */ {I_VCMPGT_OQPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2322, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2156 */ {I_VCMPGT_OQPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2331, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2157 */ {I_VCMPGT_OQPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2340, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2158 */ {I_VCMPTRUE_USPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2349, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2159 */ {I_VCMPTRUE_USPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2358, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2160 */ {I_VCMPTRUE_USPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2367, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2161 */ {I_VCMPTRUE_USPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2376, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2162 */ {I_VCMPPD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+5991, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2163 */ {I_VCMPPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+5999, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2164 */ {I_VCMPPD, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6007, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2165 */ {I_VCMPPD, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6015, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2166 */ {I_VCMPEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2385, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2167 */ {I_VCMPEQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2394, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2168 */ {I_VCMPEQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2403, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2169 */ {I_VCMPEQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2412, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2170 */ {I_VCMPLTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2421, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2171 */ {I_VCMPLTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2430, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2172 */ {I_VCMPLTPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2439, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2173 */ {I_VCMPLTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2448, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2174 */ {I_VCMPLEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2457, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2175 */ {I_VCMPLEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2466, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2176 */ {I_VCMPLEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2475, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2177 */ {I_VCMPLEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2484, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2178 */ {I_VCMPUNORDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2493, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2179 */ {I_VCMPUNORDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2502, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2180 */ {I_VCMPUNORDPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2511, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2181 */ {I_VCMPUNORDPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2520, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2182 */ {I_VCMPNEQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2529, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2183 */ {I_VCMPNEQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2538, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2184 */ {I_VCMPNEQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2547, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2185 */ {I_VCMPNEQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2556, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2186 */ {I_VCMPNLTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2565, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2187 */ {I_VCMPNLTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2574, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2188 */ {I_VCMPNLTPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2583, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2189 */ {I_VCMPNLTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2592, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2190 */ {I_VCMPNLEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2601, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2191 */ {I_VCMPNLEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2610, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2192 */ {I_VCMPNLEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2619, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2193 */ {I_VCMPNLEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2628, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2194 */ {I_VCMPORDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2637, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2195 */ {I_VCMPORDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2646, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2196 */ {I_VCMPORDPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2655, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2197 */ {I_VCMPORDPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2664, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2198 */ {I_VCMPEQ_UQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2673, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2199 */ {I_VCMPEQ_UQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2682, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2200 */ {I_VCMPEQ_UQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2691, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2201 */ {I_VCMPEQ_UQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2700, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2202 */ {I_VCMPNGEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2709, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2203 */ {I_VCMPNGEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2718, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2204 */ {I_VCMPNGEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2727, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2205 */ {I_VCMPNGEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2736, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2206 */ {I_VCMPNGTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2745, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2207 */ {I_VCMPNGTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2754, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2208 */ {I_VCMPNGTPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2763, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2209 */ {I_VCMPNGTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2772, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2210 */ {I_VCMPFALSEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2781, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2211 */ {I_VCMPFALSEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2790, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2212 */ {I_VCMPFALSEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2799, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2213 */ {I_VCMPFALSEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2808, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2214 */ {I_VCMPNEQ_OQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2817, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2215 */ {I_VCMPNEQ_OQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2826, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2216 */ {I_VCMPNEQ_OQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2835, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2217 */ {I_VCMPNEQ_OQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2844, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2218 */ {I_VCMPGEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2853, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2219 */ {I_VCMPGEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2862, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2220 */ {I_VCMPGEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2871, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2221 */ {I_VCMPGEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2880, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2222 */ {I_VCMPGTPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2889, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2223 */ {I_VCMPGTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2898, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2224 */ {I_VCMPGTPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2907, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2225 */ {I_VCMPGTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2916, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2226 */ {I_VCMPTRUEPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2925, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2227 */ {I_VCMPTRUEPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2934, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2228 */ {I_VCMPTRUEPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2943, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2229 */ {I_VCMPTRUEPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2952, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2230 */ {I_VCMPEQ_OSPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2961, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2231 */ {I_VCMPEQ_OSPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+2970, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2232 */ {I_VCMPEQ_OSPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+2979, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2233 */ {I_VCMPEQ_OSPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+2988, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2234 */ {I_VCMPLT_OQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+2997, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2235 */ {I_VCMPLT_OQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3006, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2236 */ {I_VCMPLT_OQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3015, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2237 */ {I_VCMPLT_OQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3024, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2238 */ {I_VCMPLE_OQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3033, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2239 */ {I_VCMPLE_OQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3042, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2240 */ {I_VCMPLE_OQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3051, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2241 */ {I_VCMPLE_OQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3060, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2242 */ {I_VCMPUNORD_SPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3069, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2243 */ {I_VCMPUNORD_SPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3078, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2244 */ {I_VCMPUNORD_SPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3087, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2245 */ {I_VCMPUNORD_SPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3096, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2246 */ {I_VCMPNEQ_USPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3105, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2247 */ {I_VCMPNEQ_USPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3114, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2248 */ {I_VCMPNEQ_USPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3123, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2249 */ {I_VCMPNEQ_USPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3132, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2250 */ {I_VCMPNLT_UQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3141, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2251 */ {I_VCMPNLT_UQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3150, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2252 */ {I_VCMPNLT_UQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3159, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2253 */ {I_VCMPNLT_UQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3168, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2254 */ {I_VCMPNLE_UQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3177, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2255 */ {I_VCMPNLE_UQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3186, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2256 */ {I_VCMPNLE_UQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3195, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2257 */ {I_VCMPNLE_UQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3204, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2258 */ {I_VCMPORD_SPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3213, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2259 */ {I_VCMPORD_SPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3222, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2260 */ {I_VCMPORD_SPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3231, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2261 */ {I_VCMPORS_SPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3240, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2262 */ {I_VCMPEQ_USPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3249, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2263 */ {I_VCMPEQ_USPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3258, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2264 */ {I_VCMPEQ_USPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3267, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2265 */ {I_VCMPEQ_USPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3276, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2266 */ {I_VCMPNGE_UQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3285, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2267 */ {I_VCMPNGE_UQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3294, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2268 */ {I_VCMPNGE_UQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3303, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2269 */ {I_VCMPNGE_UQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3312, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2270 */ {I_VCMPNGT_UQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3321, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2271 */ {I_VCMPNGT_UQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3330, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2272 */ {I_VCMPNGT_UQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3339, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2273 */ {I_VCMPNGT_UQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3348, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2274 */ {I_VCMPFALSE_OSPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3357, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2275 */ {I_VCMPFALSE_OSPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3366, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2276 */ {I_VCMPFALSE_OSPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3375, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2277 */ {I_VCMPFALSE_OSPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3384, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2278 */ {I_VCMPNEQ_OSPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3393, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2279 */ {I_VCMPNEQ_OSPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3402, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2280 */ {I_VCMPNEQ_OSPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3411, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2281 */ {I_VCMPNEQ_OSPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3420, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2282 */ {I_VCMPGE_OQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3429, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2283 */ {I_VCMPGE_OQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3438, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2284 */ {I_VCMPGE_OQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3447, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2285 */ {I_VCMPGE_OQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3456, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2286 */ {I_VCMPGT_OQPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3465, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2287 */ {I_VCMPGT_OQPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3474, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2288 */ {I_VCMPGT_OQPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3483, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2289 */ {I_VCMPGT_OQPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3492, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2290 */ {I_VCMPTRUE_USPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3501, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2291 */ {I_VCMPTRUE_USPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3510, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2292 */ {I_VCMPTRUE_USPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+3519, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2293 */ {I_VCMPTRUE_USPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+3528, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2294 */ {I_VCMPPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6023, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2295 */ {I_VCMPPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6031, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2296 */ {I_VCMPPS, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6039, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2297 */ {I_VCMPPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6047, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2298 */ {I_VCMPEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3537, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2299 */ {I_VCMPEQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3546, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2300 */ {I_VCMPLTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3555, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2301 */ {I_VCMPLTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3564, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2302 */ {I_VCMPLESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3573, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2303 */ {I_VCMPLESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3582, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2304 */ {I_VCMPUNORDSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3591, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2305 */ {I_VCMPUNORDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3600, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2306 */ {I_VCMPNEQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3609, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2307 */ {I_VCMPNEQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3618, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2308 */ {I_VCMPNLTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3627, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2309 */ {I_VCMPNLTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3636, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2310 */ {I_VCMPNLESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3645, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2311 */ {I_VCMPNLESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3654, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2312 */ {I_VCMPORDSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3663, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2313 */ {I_VCMPORDSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3672, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2314 */ {I_VCMPEQ_UQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3681, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2315 */ {I_VCMPEQ_UQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3690, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2316 */ {I_VCMPNGESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3699, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2317 */ {I_VCMPNGESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3708, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2318 */ {I_VCMPNGTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3717, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2319 */ {I_VCMPNGTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3726, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2320 */ {I_VCMPFALSESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3735, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2321 */ {I_VCMPFALSESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3744, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2322 */ {I_VCMPNEQ_OQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3753, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2323 */ {I_VCMPNEQ_OQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3762, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2324 */ {I_VCMPGESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3771, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2325 */ {I_VCMPGESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3780, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2326 */ {I_VCMPGTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3789, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2327 */ {I_VCMPGTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3798, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2328 */ {I_VCMPTRUESD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3807, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2329 */ {I_VCMPTRUESD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3816, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2330 */ {I_VCMPEQ_OSSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3825, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2331 */ {I_VCMPEQ_OSSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3834, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2332 */ {I_VCMPLT_OQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3843, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2333 */ {I_VCMPLT_OQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3852, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2334 */ {I_VCMPLE_OQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3861, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2335 */ {I_VCMPLE_OQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3870, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2336 */ {I_VCMPUNORD_SSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3879, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2337 */ {I_VCMPUNORD_SSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3888, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2338 */ {I_VCMPNEQ_USSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3897, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2339 */ {I_VCMPNEQ_USSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3906, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2340 */ {I_VCMPNLT_UQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3915, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2341 */ {I_VCMPNLT_UQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3924, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2342 */ {I_VCMPNLE_UQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3933, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2343 */ {I_VCMPNLE_UQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3942, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2344 */ {I_VCMPORD_SSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3951, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2345 */ {I_VCMPORD_SSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3960, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2346 */ {I_VCMPEQ_USSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3969, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2347 */ {I_VCMPEQ_USSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3978, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2348 */ {I_VCMPNGE_UQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+3987, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2349 */ {I_VCMPNGE_UQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+3996, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2350 */ {I_VCMPNGT_UQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4005, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2351 */ {I_VCMPNGT_UQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4014, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2352 */ {I_VCMPFALSE_OSSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4023, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2353 */ {I_VCMPFALSE_OSSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4032, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2354 */ {I_VCMPNEQ_OSSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4041, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2355 */ {I_VCMPNEQ_OSSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4050, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2356 */ {I_VCMPGE_OQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4059, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2357 */ {I_VCMPGE_OQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4068, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2358 */ {I_VCMPGT_OQSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4077, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2359 */ {I_VCMPGT_OQSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4086, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2360 */ {I_VCMPTRUE_USSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4095, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2361 */ {I_VCMPTRUE_USSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4104, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2362 */ {I_VCMPSD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6055, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2363 */ {I_VCMPSD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6063, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2364 */ {I_VCMPEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4113, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2365 */ {I_VCMPEQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4122, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2366 */ {I_VCMPLTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4131, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2367 */ {I_VCMPLTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4140, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2368 */ {I_VCMPLESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4149, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2369 */ {I_VCMPLESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4158, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2370 */ {I_VCMPUNORDSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4167, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2371 */ {I_VCMPUNORDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4176, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2372 */ {I_VCMPNEQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4185, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2373 */ {I_VCMPNEQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4194, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2374 */ {I_VCMPNLTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4203, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2375 */ {I_VCMPNLTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4212, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2376 */ {I_VCMPNLESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4221, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2377 */ {I_VCMPNLESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4230, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2378 */ {I_VCMPORDSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4239, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2379 */ {I_VCMPORDSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4248, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2380 */ {I_VCMPEQ_UQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4257, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2381 */ {I_VCMPEQ_UQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4266, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2382 */ {I_VCMPNGESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4275, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2383 */ {I_VCMPNGESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4284, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2384 */ {I_VCMPNGTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4293, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2385 */ {I_VCMPNGTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4302, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2386 */ {I_VCMPFALSESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4311, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2387 */ {I_VCMPFALSESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4320, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2388 */ {I_VCMPNEQ_OQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4329, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2389 */ {I_VCMPNEQ_OQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4338, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2390 */ {I_VCMPGESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4347, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2391 */ {I_VCMPGESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4356, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2392 */ {I_VCMPGTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4365, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2393 */ {I_VCMPGTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4374, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2394 */ {I_VCMPTRUESS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4383, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2395 */ {I_VCMPTRUESS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4392, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2396 */ {I_VCMPEQ_OSSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4401, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2397 */ {I_VCMPEQ_OSSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4410, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2398 */ {I_VCMPLT_OQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4419, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2399 */ {I_VCMPLT_OQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4428, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2400 */ {I_VCMPLE_OQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4437, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2401 */ {I_VCMPLE_OQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4446, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2402 */ {I_VCMPUNORD_SSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4455, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2403 */ {I_VCMPUNORD_SSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4464, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2404 */ {I_VCMPNEQ_USSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4473, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2405 */ {I_VCMPNEQ_USSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4482, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2406 */ {I_VCMPNLT_UQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4491, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2407 */ {I_VCMPNLT_UQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4500, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2408 */ {I_VCMPNLE_UQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4509, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2409 */ {I_VCMPNLE_UQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4518, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2410 */ {I_VCMPORD_SSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4527, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2411 */ {I_VCMPORD_SSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4536, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2412 */ {I_VCMPEQ_USSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4545, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2413 */ {I_VCMPEQ_USSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4554, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2414 */ {I_VCMPNGE_UQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4563, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2415 */ {I_VCMPNGE_UQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4572, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2416 */ {I_VCMPNGT_UQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4581, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2417 */ {I_VCMPNGT_UQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4590, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2418 */ {I_VCMPFALSE_OSSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4599, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2419 */ {I_VCMPFALSE_OSSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4608, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2420 */ {I_VCMPNEQ_OSSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4617, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2421 */ {I_VCMPNEQ_OSSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4626, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2422 */ {I_VCMPGE_OQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4635, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2423 */ {I_VCMPGE_OQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4644, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2424 */ {I_VCMPGT_OQSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4653, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2425 */ {I_VCMPGT_OQSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4662, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2426 */ {I_VCMPTRUE_USSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+4671, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2427 */ {I_VCMPTRUE_USSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4680, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2428 */ {I_VCMPSS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6071, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2429 */ {I_VCMPSS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6079, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2430 */ {I_VCOMISD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9563, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2431 */ {I_VCOMISS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9570, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2432 */ {I_VCVTDQ2PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9577, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2433 */ {I_VCVTDQ2PD, 2, {YMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9584, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2434 */ {I_VCVTDQ2PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9591, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2435 */ {I_VCVTDQ2PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9598, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2436 */ {I_VCVTPD2DQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+9605, IF_AVX|IF_SANDYBRIDGE},
    /* 2437 */ {I_VCVTPD2DQ, 2, {XMMREG,MEMORY|BITS128,0,0,0}, nasm_bytecodes+9605, IF_AVX|IF_SANDYBRIDGE},
    /* 2438 */ {I_VCVTPD2DQ, 2, {XMMREG,YMMREG,0,0,0}, nasm_bytecodes+9612, IF_AVX|IF_SANDYBRIDGE},
    /* 2439 */ {I_VCVTPD2DQ, 2, {XMMREG,MEMORY|BITS256,0,0,0}, nasm_bytecodes+9612, IF_AVX|IF_SANDYBRIDGE},
    /* 2440 */ {I_VCVTPD2PS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+9619, IF_AVX|IF_SANDYBRIDGE},
    /* 2441 */ {I_VCVTPD2PS, 2, {XMMREG,MEMORY|BITS128,0,0,0}, nasm_bytecodes+9619, IF_AVX|IF_SANDYBRIDGE},
    /* 2442 */ {I_VCVTPD2PS, 2, {XMMREG,YMMREG,0,0,0}, nasm_bytecodes+9626, IF_AVX|IF_SANDYBRIDGE},
    /* 2443 */ {I_VCVTPD2PS, 2, {XMMREG,MEMORY|BITS256,0,0,0}, nasm_bytecodes+9626, IF_AVX|IF_SANDYBRIDGE},
    /* 2444 */ {I_VCVTPS2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9633, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2445 */ {I_VCVTPS2DQ, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9640, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2446 */ {I_VCVTPS2PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9647, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2447 */ {I_VCVTPS2PD, 2, {YMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9654, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2448 */ {I_VCVTSD2SI, 2, {REG32,RM_XMM,0,0,0}, nasm_bytecodes+9661, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2449 */ {I_VCVTSD2SI, 2, {REG64,RM_XMM,0,0,0}, nasm_bytecodes+9668, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    /* 2450 */ {I_VCVTSD2SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9675, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2451 */ {I_VCVTSD2SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9682, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2452 */ {I_VCVTSI2SD, 3, {XMMREG,XMMREG,RM_GPR|BITS32,0,0}, nasm_bytecodes+9689, IF_AVX|IF_SANDYBRIDGE},
    /* 2453 */ {I_VCVTSI2SD, 2, {XMMREG,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+9696, IF_AVX|IF_SANDYBRIDGE},
    /* 2454 */ {I_VCVTSI2SD, 3, {XMMREG,XMMREG,RM_GPR|BITS64,0,0}, nasm_bytecodes+9703, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2455 */ {I_VCVTSI2SD, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+9710, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2456 */ {I_VCVTSI2SS, 3, {XMMREG,XMMREG,RM_GPR|BITS32,0,0}, nasm_bytecodes+9717, IF_AVX|IF_SANDYBRIDGE},
    /* 2457 */ {I_VCVTSI2SS, 2, {XMMREG,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+9724, IF_AVX|IF_SANDYBRIDGE},
    /* 2458 */ {I_VCVTSI2SS, 3, {XMMREG,XMMREG,RM_GPR|BITS64,0,0}, nasm_bytecodes+9731, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2459 */ {I_VCVTSI2SS, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+9738, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2460 */ {I_VCVTSS2SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9745, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2461 */ {I_VCVTSS2SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9752, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2462 */ {I_VCVTSS2SI, 2, {REG32,RM_XMM,0,0,0}, nasm_bytecodes+9759, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2463 */ {I_VCVTSS2SI, 2, {REG64,RM_XMM,0,0,0}, nasm_bytecodes+9766, IF_AVX|IF_SANDYBRIDGE|IF_SD|IF_LONG},
    /* 2464 */ {I_VCVTTPD2DQ, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+9773, IF_AVX|IF_SANDYBRIDGE},
    /* 2465 */ {I_VCVTTPD2DQ, 2, {XMMREG,MEMORY|BITS128,0,0,0}, nasm_bytecodes+9773, IF_AVX|IF_SANDYBRIDGE},
    /* 2466 */ {I_VCVTTPD2DQ, 2, {XMMREG,YMMREG,0,0,0}, nasm_bytecodes+9780, IF_AVX|IF_SANDYBRIDGE},
    /* 2467 */ {I_VCVTTPD2DQ, 2, {XMMREG,MEMORY|BITS256,0,0,0}, nasm_bytecodes+9780, IF_AVX|IF_SANDYBRIDGE},
    /* 2468 */ {I_VCVTTPS2DQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9787, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2469 */ {I_VCVTTPS2DQ, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9794, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2470 */ {I_VCVTTSD2SI, 2, {REG32,RM_XMM,0,0,0}, nasm_bytecodes+9801, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2471 */ {I_VCVTTSD2SI, 2, {REG64,RM_XMM,0,0,0}, nasm_bytecodes+9808, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    /* 2472 */ {I_VCVTTSS2SI, 2, {REG32,RM_XMM,0,0,0}, nasm_bytecodes+9815, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2473 */ {I_VCVTTSS2SI, 2, {REG64,RM_XMM,0,0,0}, nasm_bytecodes+9822, IF_AVX|IF_SANDYBRIDGE|IF_SD|IF_LONG},
    /* 2474 */ {I_VDIVPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9829, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2475 */ {I_VDIVPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9836, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2476 */ {I_VDIVPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9843, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2477 */ {I_VDIVPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9850, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2478 */ {I_VDIVPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9857, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2479 */ {I_VDIVPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9864, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2480 */ {I_VDIVPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9871, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2481 */ {I_VDIVPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9878, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2482 */ {I_VDIVSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9885, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2483 */ {I_VDIVSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9892, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2484 */ {I_VDIVSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9899, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2485 */ {I_VDIVSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9906, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2486 */ {I_VDPPD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6087, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2487 */ {I_VDPPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6095, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2488 */ {I_VDPPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6103, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2489 */ {I_VDPPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6111, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2490 */ {I_VDPPS, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6119, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2491 */ {I_VDPPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6127, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2492 */ {I_VEXTRACTF128, 3, {RM_XMM,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6135, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2493 */ {I_VEXTRACTPS, 3, {RM_GPR|BITS32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6143, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2494 */ {I_VHADDPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9913, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2495 */ {I_VHADDPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9920, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2496 */ {I_VHADDPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9927, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2497 */ {I_VHADDPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9934, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2498 */ {I_VHADDPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9941, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2499 */ {I_VHADDPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9948, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2500 */ {I_VHADDPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9955, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2501 */ {I_VHADDPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9962, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2502 */ {I_VHSUBPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9969, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2503 */ {I_VHSUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+9976, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2504 */ {I_VHSUBPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+9983, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2505 */ {I_VHSUBPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+9990, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2506 */ {I_VHSUBPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+9997, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2507 */ {I_VHSUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10004, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2508 */ {I_VHSUBPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10011, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2509 */ {I_VHSUBPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10018, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2510 */ {I_VINSERTF128, 4, {YMMREG,YMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6151, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2511 */ {I_VINSERTPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6159, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2512 */ {I_VINSERTPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6167, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2513 */ {I_VLDDQU, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10025, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2514 */ {I_VLDQQU, 2, {YMMREG,MEMORY,0,0,0}, nasm_bytecodes+10032, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2515 */ {I_VLDDQU, 2, {YMMREG,MEMORY,0,0,0}, nasm_bytecodes+10032, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2516 */ {I_VLDMXCSR, 1, {MEMORY|BITS32,0,0,0,0}, nasm_bytecodes+10039, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2517 */ {I_VMASKMOVDQU, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10046, IF_AVX|IF_SANDYBRIDGE},
    /* 2518 */ {I_VMASKMOVPS, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10053, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2519 */ {I_VMASKMOVPS, 3, {YMMREG,YMMREG,MEMORY,0,0}, nasm_bytecodes+10060, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2520 */ {I_VMASKMOVPS, 3, {MEMORY,XMMREG,XMMREG,0,0}, nasm_bytecodes+10067, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2521 */ {I_VMASKMOVPS, 3, {MEMORY,XMMREG,XMMREG,0,0}, nasm_bytecodes+10074, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2522 */ {I_VMASKMOVPD, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10081, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2523 */ {I_VMASKMOVPD, 3, {YMMREG,YMMREG,MEMORY,0,0}, nasm_bytecodes+10088, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2524 */ {I_VMASKMOVPD, 3, {MEMORY,XMMREG,XMMREG,0,0}, nasm_bytecodes+10095, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2525 */ {I_VMASKMOVPD, 3, {MEMORY,YMMREG,YMMREG,0,0}, nasm_bytecodes+10102, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2526 */ {I_VMAXPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10109, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2527 */ {I_VMAXPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10116, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2528 */ {I_VMAXPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10123, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2529 */ {I_VMAXPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10130, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2530 */ {I_VMAXPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10137, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2531 */ {I_VMAXPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10144, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2532 */ {I_VMAXPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10151, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2533 */ {I_VMAXPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10158, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2534 */ {I_VMAXSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10165, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2535 */ {I_VMAXSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10172, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2536 */ {I_VMAXSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10179, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2537 */ {I_VMAXSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10186, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2538 */ {I_VMINPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10193, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2539 */ {I_VMINPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10200, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2540 */ {I_VMINPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10207, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2541 */ {I_VMINPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10214, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2542 */ {I_VMINPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10221, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2543 */ {I_VMINPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10228, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2544 */ {I_VMINPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10235, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2545 */ {I_VMINPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10242, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2546 */ {I_VMINSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10249, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2547 */ {I_VMINSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10256, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2548 */ {I_VMINSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10263, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2549 */ {I_VMINSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10270, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2550 */ {I_VMOVAPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10277, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2551 */ {I_VMOVAPD, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10284, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2552 */ {I_VMOVAPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10291, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2553 */ {I_VMOVAPD, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10298, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2554 */ {I_VMOVAPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10305, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2555 */ {I_VMOVAPS, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10312, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2556 */ {I_VMOVAPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10319, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2557 */ {I_VMOVAPS, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10326, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2558 */ {I_VMOVQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10333, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2559 */ {I_VMOVQ, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10340, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2560 */ {I_VMOVD, 2, {XMMREG,RM_GPR|BITS32,0,0,0}, nasm_bytecodes+10347, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2561 */ {I_VMOVQ, 2, {XMMREG,RM_GPR|BITS64,0,0,0}, nasm_bytecodes+10354, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    /* 2562 */ {I_VMOVD, 2, {RM_GPR|BITS32,XMMREG,0,0,0}, nasm_bytecodes+10361, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2563 */ {I_VMOVQ, 2, {RM_GPR|BITS64,XMMREG,0,0,0}, nasm_bytecodes+10368, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    /* 2564 */ {I_VMOVDDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10375, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2565 */ {I_VMOVDDUP, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10382, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2566 */ {I_VMOVDQA, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10389, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2567 */ {I_VMOVDQA, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10396, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2568 */ {I_VMOVQQA, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10403, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2569 */ {I_VMOVQQA, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10410, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2570 */ {I_VMOVDQA, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10403, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2571 */ {I_VMOVDQA, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10410, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2572 */ {I_VMOVDQU, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10417, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2573 */ {I_VMOVDQU, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10424, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2574 */ {I_VMOVQQU, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10431, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2575 */ {I_VMOVQQU, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10438, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2576 */ {I_VMOVDQU, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10431, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2577 */ {I_VMOVDQU, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10438, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2578 */ {I_VMOVHLPS, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10445, IF_AVX|IF_SANDYBRIDGE},
    /* 2579 */ {I_VMOVHLPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10452, IF_AVX|IF_SANDYBRIDGE},
    /* 2580 */ {I_VMOVHPD, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10459, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2581 */ {I_VMOVHPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10466, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2582 */ {I_VMOVHPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10473, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2583 */ {I_VMOVHPS, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10480, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2584 */ {I_VMOVHPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10487, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2585 */ {I_VMOVHPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10494, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2586 */ {I_VMOVLHPS, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10480, IF_AVX|IF_SANDYBRIDGE},
    /* 2587 */ {I_VMOVLHPS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10487, IF_AVX|IF_SANDYBRIDGE},
    /* 2588 */ {I_VMOVLPD, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10501, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2589 */ {I_VMOVLPD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10508, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2590 */ {I_VMOVLPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10515, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2591 */ {I_VMOVLPS, 3, {XMMREG,XMMREG,MEMORY,0,0}, nasm_bytecodes+10445, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2592 */ {I_VMOVLPS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10452, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2593 */ {I_VMOVLPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10522, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2594 */ {I_VMOVMSKPD, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+10529, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2595 */ {I_VMOVMSKPD, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+10529, IF_AVX|IF_SANDYBRIDGE},
    /* 2596 */ {I_VMOVMSKPD, 2, {REG64,YMMREG,0,0,0}, nasm_bytecodes+10536, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2597 */ {I_VMOVMSKPD, 2, {REG32,YMMREG,0,0,0}, nasm_bytecodes+10536, IF_AVX|IF_SANDYBRIDGE},
    /* 2598 */ {I_VMOVMSKPS, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+10543, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2599 */ {I_VMOVMSKPS, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+10543, IF_AVX|IF_SANDYBRIDGE},
    /* 2600 */ {I_VMOVMSKPS, 2, {REG64,YMMREG,0,0,0}, nasm_bytecodes+10550, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2601 */ {I_VMOVMSKPS, 2, {REG32,YMMREG,0,0,0}, nasm_bytecodes+10550, IF_AVX|IF_SANDYBRIDGE},
    /* 2602 */ {I_VMOVNTDQ, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10557, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2603 */ {I_VMOVNTQQ, 2, {MEMORY,YMMREG,0,0,0}, nasm_bytecodes+10564, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2604 */ {I_VMOVNTDQ, 2, {MEMORY,YMMREG,0,0,0}, nasm_bytecodes+10564, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2605 */ {I_VMOVNTDQA, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10571, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2606 */ {I_VMOVNTPD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10578, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2607 */ {I_VMOVNTPD, 2, {MEMORY,YMMREG,0,0,0}, nasm_bytecodes+10585, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2608 */ {I_VMOVNTPS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10592, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2609 */ {I_VMOVNTPS, 2, {MEMORY,YMMREG,0,0,0}, nasm_bytecodes+10599, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2610 */ {I_VMOVSD, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10606, IF_AVX|IF_SANDYBRIDGE},
    /* 2611 */ {I_VMOVSD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10613, IF_AVX|IF_SANDYBRIDGE},
    /* 2612 */ {I_VMOVSD, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10620, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2613 */ {I_VMOVSD, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10627, IF_AVX|IF_SANDYBRIDGE},
    /* 2614 */ {I_VMOVSD, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10634, IF_AVX|IF_SANDYBRIDGE},
    /* 2615 */ {I_VMOVSD, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10641, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2616 */ {I_VMOVSHDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10648, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2617 */ {I_VMOVSHDUP, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10655, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2618 */ {I_VMOVSLDUP, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10662, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2619 */ {I_VMOVSLDUP, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10669, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2620 */ {I_VMOVSS, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10676, IF_AVX|IF_SANDYBRIDGE},
    /* 2621 */ {I_VMOVSS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10683, IF_AVX|IF_SANDYBRIDGE},
    /* 2622 */ {I_VMOVSS, 2, {XMMREG,MEMORY,0,0,0}, nasm_bytecodes+10690, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2623 */ {I_VMOVSS, 3, {XMMREG,XMMREG,XMMREG,0,0}, nasm_bytecodes+10697, IF_AVX|IF_SANDYBRIDGE},
    /* 2624 */ {I_VMOVSS, 2, {XMMREG,XMMREG,0,0,0}, nasm_bytecodes+10704, IF_AVX|IF_SANDYBRIDGE},
    /* 2625 */ {I_VMOVSS, 2, {MEMORY,XMMREG,0,0,0}, nasm_bytecodes+10711, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2626 */ {I_VMOVUPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10718, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2627 */ {I_VMOVUPD, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10725, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2628 */ {I_VMOVUPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10732, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2629 */ {I_VMOVUPD, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10739, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2630 */ {I_VMOVUPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10746, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2631 */ {I_VMOVUPS, 2, {RM_XMM,XMMREG,0,0,0}, nasm_bytecodes+10753, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2632 */ {I_VMOVUPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10760, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2633 */ {I_VMOVUPS, 2, {RM_YMM,YMMREG,0,0,0}, nasm_bytecodes+10767, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2634 */ {I_VMPSADBW, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6175, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2635 */ {I_VMPSADBW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6183, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2636 */ {I_VMULPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10774, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2637 */ {I_VMULPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10781, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2638 */ {I_VMULPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10788, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2639 */ {I_VMULPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10795, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2640 */ {I_VMULPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10802, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2641 */ {I_VMULPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10809, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2642 */ {I_VMULPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10816, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2643 */ {I_VMULPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10823, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2644 */ {I_VMULSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10830, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2645 */ {I_VMULSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10837, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2646 */ {I_VMULSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10844, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2647 */ {I_VMULSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10851, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2648 */ {I_VORPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10858, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2649 */ {I_VORPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10865, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2650 */ {I_VORPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10872, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2651 */ {I_VORPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10879, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2652 */ {I_VORPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10886, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2653 */ {I_VORPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10893, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2654 */ {I_VORPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+10900, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2655 */ {I_VORPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+10907, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2656 */ {I_VPABSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10914, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2657 */ {I_VPABSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10921, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2658 */ {I_VPABSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10928, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2659 */ {I_VPACKSSWB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10935, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2660 */ {I_VPACKSSWB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10942, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2661 */ {I_VPACKSSDW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10949, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2662 */ {I_VPACKSSDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10956, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2663 */ {I_VPACKUSWB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10963, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2664 */ {I_VPACKUSWB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10970, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2665 */ {I_VPACKUSDW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10977, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2666 */ {I_VPACKUSDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10984, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2667 */ {I_VPADDB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+10991, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2668 */ {I_VPADDB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+10998, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2669 */ {I_VPADDW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11005, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2670 */ {I_VPADDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11012, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2671 */ {I_VPADDD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11019, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2672 */ {I_VPADDD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11026, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2673 */ {I_VPADDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11033, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2674 */ {I_VPADDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11040, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2675 */ {I_VPADDSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11047, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2676 */ {I_VPADDSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11054, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2677 */ {I_VPADDSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11061, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2678 */ {I_VPADDSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11068, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2679 */ {I_VPADDUSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11075, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2680 */ {I_VPADDUSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11082, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2681 */ {I_VPADDUSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11089, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2682 */ {I_VPADDUSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11096, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2683 */ {I_VPALIGNR, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6191, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2684 */ {I_VPALIGNR, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6199, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2685 */ {I_VPAND, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11103, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2686 */ {I_VPAND, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11110, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2687 */ {I_VPANDN, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11117, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2688 */ {I_VPANDN, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11124, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2689 */ {I_VPAVGB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11131, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2690 */ {I_VPAVGB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11138, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2691 */ {I_VPAVGW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11145, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2692 */ {I_VPAVGW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11152, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2693 */ {I_VPBLENDVB, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4689, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2694 */ {I_VPBLENDVB, 3, {XMMREG,RM_XMM,XMMREG,0,0}, nasm_bytecodes+4698, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2695 */ {I_VPBLENDW, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6207, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2696 */ {I_VPBLENDW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6215, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2697 */ {I_VPCMPESTRI, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6223, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2698 */ {I_VPCMPESTRM, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6231, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2699 */ {I_VPCMPISTRI, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6239, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2700 */ {I_VPCMPISTRM, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6247, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2701 */ {I_VPCMPEQB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11159, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2702 */ {I_VPCMPEQB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11166, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2703 */ {I_VPCMPEQW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11173, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2704 */ {I_VPCMPEQW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11180, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2705 */ {I_VPCMPEQD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11187, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2706 */ {I_VPCMPEQD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11194, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2707 */ {I_VPCMPEQQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11201, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2708 */ {I_VPCMPEQQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11208, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2709 */ {I_VPCMPGTB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11215, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2710 */ {I_VPCMPGTB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11222, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2711 */ {I_VPCMPGTW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11229, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2712 */ {I_VPCMPGTW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11236, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2713 */ {I_VPCMPGTD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11243, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2714 */ {I_VPCMPGTD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11250, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2715 */ {I_VPCMPGTQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11257, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2716 */ {I_VPCMPGTQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11264, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2717 */ {I_VPERMILPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11271, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2718 */ {I_VPERMILPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+11278, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2719 */ {I_VPERMILPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6255, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2720 */ {I_VPERMILPD, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6263, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2721 */ {I_VPERMILTD2PD, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4707, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2722 */ {I_VPERMILTD2PD, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4716, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2723 */ {I_VPERMILTD2PD, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4725, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2724 */ {I_VPERMILTD2PD, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4734, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2725 */ {I_VPERMILMO2PD, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4743, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2726 */ {I_VPERMILMO2PD, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4752, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2727 */ {I_VPERMILMO2PD, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4761, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2728 */ {I_VPERMILMO2PD, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4770, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2729 */ {I_VPERMILMZ2PD, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4779, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2730 */ {I_VPERMILMZ2PD, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4788, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2731 */ {I_VPERMILMZ2PD, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4797, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2732 */ {I_VPERMILMZ2PD, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4806, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2733 */ {I_VPERMIL2PD, 5, {XMMREG,XMMREG,RM_XMM,XMMREG,IMMEDIATE}, nasm_bytecodes+4815, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2734 */ {I_VPERMIL2PD, 5, {XMMREG,XMMREG,XMMREG,RM_XMM,IMMEDIATE}, nasm_bytecodes+4824, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2735 */ {I_VPERMIL2PD, 5, {YMMREG,YMMREG,RM_YMM,YMMREG,IMMEDIATE}, nasm_bytecodes+4833, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2736 */ {I_VPERMIL2PD, 5, {YMMREG,YMMREG,YMMREG,RM_YMM,IMMEDIATE}, nasm_bytecodes+4842, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2737 */ {I_VPERMILPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11285, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2738 */ {I_VPERMILPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+11292, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2739 */ {I_VPERMILPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6271, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2740 */ {I_VPERMILPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6279, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2741 */ {I_VPERMILTD2PS, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4851, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2742 */ {I_VPERMILTD2PS, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4860, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2743 */ {I_VPERMILTD2PS, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4869, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2744 */ {I_VPERMILTD2PS, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4878, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2745 */ {I_VPERMILMO2PS, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4887, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2746 */ {I_VPERMILMO2PS, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4896, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2747 */ {I_VPERMILMO2PS, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4905, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2748 */ {I_VPERMILMO2PS, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4914, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2749 */ {I_VPERMILMZ2PS, 4, {XMMREG,XMMREG,RM_XMM,XMMREG,0}, nasm_bytecodes+4923, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2750 */ {I_VPERMILMZ2PS, 4, {XMMREG,XMMREG,XMMREG,RM_XMM,0}, nasm_bytecodes+4932, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2751 */ {I_VPERMILMZ2PS, 4, {YMMREG,YMMREG,RM_YMM,YMMREG,0}, nasm_bytecodes+4941, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2752 */ {I_VPERMILMZ2PS, 4, {YMMREG,YMMREG,YMMREG,RM_YMM,0}, nasm_bytecodes+4950, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2753 */ {I_VPERMIL2PS, 5, {XMMREG,XMMREG,RM_XMM,XMMREG,IMMEDIATE}, nasm_bytecodes+4959, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2754 */ {I_VPERMIL2PS, 5, {XMMREG,XMMREG,XMMREG,RM_XMM,IMMEDIATE}, nasm_bytecodes+4968, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2755 */ {I_VPERMIL2PS, 5, {YMMREG,YMMREG,RM_YMM,YMMREG,IMMEDIATE}, nasm_bytecodes+4977, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2756 */ {I_VPERMIL2PS, 5, {YMMREG,YMMREG,YMMREG,RM_YMM,IMMEDIATE}, nasm_bytecodes+4986, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2757 */ {I_VPERM2F128, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6287, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2758 */ {I_VPEXTRB, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6295, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2759 */ {I_VPEXTRB, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6295, IF_AVX|IF_SANDYBRIDGE},
    /* 2760 */ {I_VPEXTRB, 3, {MEMORY,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6295, IF_AVX|IF_SANDYBRIDGE|IF_SB},
    /* 2761 */ {I_VPEXTRW, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6303, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2762 */ {I_VPEXTRW, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6303, IF_AVX|IF_SANDYBRIDGE},
    /* 2763 */ {I_VPEXTRW, 3, {MEMORY,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6303, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    /* 2764 */ {I_VPEXTRW, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6311, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2765 */ {I_VPEXTRW, 3, {REG32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6311, IF_AVX|IF_SANDYBRIDGE},
    /* 2766 */ {I_VPEXTRW, 3, {MEMORY,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6311, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    /* 2767 */ {I_VPEXTRD, 3, {REG64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6319, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2768 */ {I_VPEXTRD, 3, {RM_GPR|BITS32,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6319, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2769 */ {I_VPEXTRQ, 3, {RM_GPR|BITS64,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6327, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    /* 2770 */ {I_VPHADDW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11299, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2771 */ {I_VPHADDW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11306, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2772 */ {I_VPHADDD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11313, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2773 */ {I_VPHADDD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11320, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2774 */ {I_VPHADDSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11327, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2775 */ {I_VPHADDSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11334, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2776 */ {I_VPHMINPOSUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11341, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2777 */ {I_VPHSUBW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11348, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2778 */ {I_VPHSUBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11355, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2779 */ {I_VPHSUBD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11362, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2780 */ {I_VPHSUBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11369, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2781 */ {I_VPHSUBSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11376, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2782 */ {I_VPHSUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11383, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2783 */ {I_VPINSRB, 4, {XMMREG,XMMREG,REG32,IMMEDIATE,0}, nasm_bytecodes+6335, IF_AVX|IF_SANDYBRIDGE},
    /* 2784 */ {I_VPINSRB, 3, {XMMREG,REG32,IMMEDIATE,0,0}, nasm_bytecodes+6343, IF_AVX|IF_SANDYBRIDGE},
    /* 2785 */ {I_VPINSRB, 4, {XMMREG,XMMREG,MEMORY,IMMEDIATE,0}, nasm_bytecodes+6335, IF_AVX|IF_SANDYBRIDGE|IF_SB},
    /* 2786 */ {I_VPINSRB, 4, {XMMREG,REG32,MEMORY,IMMEDIATE,0}, nasm_bytecodes+6343, IF_AVX|IF_SANDYBRIDGE|IF_SB},
    /* 2787 */ {I_VPINSRW, 4, {XMMREG,XMMREG,REG32,IMMEDIATE,0}, nasm_bytecodes+6351, IF_AVX|IF_SANDYBRIDGE},
    /* 2788 */ {I_VPINSRW, 3, {XMMREG,REG32,IMMEDIATE,0,0}, nasm_bytecodes+6359, IF_AVX|IF_SANDYBRIDGE},
    /* 2789 */ {I_VPINSRW, 4, {XMMREG,XMMREG,MEMORY,IMMEDIATE,0}, nasm_bytecodes+6351, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    /* 2790 */ {I_VPINSRW, 4, {XMMREG,REG32,MEMORY,IMMEDIATE,0}, nasm_bytecodes+6359, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    /* 2791 */ {I_VPINSRD, 4, {XMMREG,XMMREG,RM_GPR|BITS32,IMMEDIATE,0}, nasm_bytecodes+6367, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2792 */ {I_VPINSRD, 3, {XMMREG,RM_GPR|BITS32,IMMEDIATE,0,0}, nasm_bytecodes+6375, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2793 */ {I_VPINSRQ, 4, {XMMREG,XMMREG,RM_GPR|BITS64,IMMEDIATE,0}, nasm_bytecodes+6383, IF_AVX|IF_SANDYBRIDGE|IF_SQ|IF_LONG},
    /* 2794 */ {I_VPINSRQ, 3, {XMMREG,RM_GPR|BITS64,IMMEDIATE,0,0}, nasm_bytecodes+6391, IF_AVX|IF_SANDYBRIDGE|IF_SD|IF_LONG},
    /* 2795 */ {I_VPMADDWD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11390, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2796 */ {I_VPMADDWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11397, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2797 */ {I_VPMADDUBSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11404, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2798 */ {I_VPMADDUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11411, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2799 */ {I_VPMAXSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11418, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2800 */ {I_VPMAXSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11425, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2801 */ {I_VPMAXSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11432, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2802 */ {I_VPMAXSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11439, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2803 */ {I_VPMAXSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11446, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2804 */ {I_VPMAXSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11453, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2805 */ {I_VPMAXUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11460, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2806 */ {I_VPMAXUB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11467, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2807 */ {I_VPMAXUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11474, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2808 */ {I_VPMAXUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11481, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2809 */ {I_VPMAXUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11488, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2810 */ {I_VPMAXUD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11495, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2811 */ {I_VPMINSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11502, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2812 */ {I_VPMINSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11509, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2813 */ {I_VPMINSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11516, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2814 */ {I_VPMINSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11523, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2815 */ {I_VPMINSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11530, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2816 */ {I_VPMINSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11537, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2817 */ {I_VPMINUB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11544, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2818 */ {I_VPMINUB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11551, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2819 */ {I_VPMINUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11558, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2820 */ {I_VPMINUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11565, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2821 */ {I_VPMINUD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11572, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2822 */ {I_VPMINUD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11579, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2823 */ {I_VPMOVMSKB, 2, {REG64,XMMREG,0,0,0}, nasm_bytecodes+11586, IF_AVX|IF_SANDYBRIDGE|IF_LONG},
    /* 2824 */ {I_VPMOVMSKB, 2, {REG32,XMMREG,0,0,0}, nasm_bytecodes+11586, IF_AVX|IF_SANDYBRIDGE},
    /* 2825 */ {I_VPMOVSXBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11593, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2826 */ {I_VPMOVSXBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11600, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2827 */ {I_VPMOVSXBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11607, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    /* 2828 */ {I_VPMOVSXWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11614, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2829 */ {I_VPMOVSXWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11621, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2830 */ {I_VPMOVSXDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11628, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2831 */ {I_VPMOVZXBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11635, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2832 */ {I_VPMOVZXBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11642, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2833 */ {I_VPMOVZXBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11649, IF_AVX|IF_SANDYBRIDGE|IF_SW},
    /* 2834 */ {I_VPMOVZXWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11656, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2835 */ {I_VPMOVZXWQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11663, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2836 */ {I_VPMOVZXDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11670, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2837 */ {I_VPMULHUW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11677, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2838 */ {I_VPMULHUW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11684, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2839 */ {I_VPMULHRSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11691, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2840 */ {I_VPMULHRSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11698, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2841 */ {I_VPMULHW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11705, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2842 */ {I_VPMULHW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11712, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2843 */ {I_VPMULLW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11719, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2844 */ {I_VPMULLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11726, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2845 */ {I_VPMULLD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11733, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2846 */ {I_VPMULLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11740, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2847 */ {I_VPMULUDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11747, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2848 */ {I_VPMULUDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11754, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2849 */ {I_VPMULDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11761, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2850 */ {I_VPMULDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11768, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2851 */ {I_VPOR, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11775, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2852 */ {I_VPOR, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11782, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2853 */ {I_VPSADBW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11789, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2854 */ {I_VPSADBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11796, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2855 */ {I_VPSHUFB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11803, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2856 */ {I_VPSHUFB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11810, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2857 */ {I_VPSHUFD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6399, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2858 */ {I_VPSHUFHW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6407, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2859 */ {I_VPSHUFLW, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6415, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2860 */ {I_VPSIGNB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11817, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2861 */ {I_VPSIGNB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11824, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2862 */ {I_VPSIGNW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11831, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2863 */ {I_VPSIGNW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11838, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2864 */ {I_VPSIGND, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11845, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2865 */ {I_VPSIGND, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11852, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2866 */ {I_VPSLLDQ, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6423, IF_AVX|IF_SANDYBRIDGE},
    /* 2867 */ {I_VPSLLDQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6431, IF_AVX|IF_SANDYBRIDGE},
    /* 2868 */ {I_VPSRLDQ, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6439, IF_AVX|IF_SANDYBRIDGE},
    /* 2869 */ {I_VPSRLDQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6447, IF_AVX|IF_SANDYBRIDGE},
    /* 2870 */ {I_VPSLLW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11859, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2871 */ {I_VPSLLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11866, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2872 */ {I_VPSLLW, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6455, IF_AVX|IF_SANDYBRIDGE},
    /* 2873 */ {I_VPSLLW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6463, IF_AVX|IF_SANDYBRIDGE},
    /* 2874 */ {I_VPSLLD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11873, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2875 */ {I_VPSLLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11880, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2876 */ {I_VPSLLD, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6471, IF_AVX|IF_SANDYBRIDGE},
    /* 2877 */ {I_VPSLLD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6479, IF_AVX|IF_SANDYBRIDGE},
    /* 2878 */ {I_VPSLLQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11887, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2879 */ {I_VPSLLQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11894, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2880 */ {I_VPSLLQ, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6487, IF_AVX|IF_SANDYBRIDGE},
    /* 2881 */ {I_VPSLLQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6495, IF_AVX|IF_SANDYBRIDGE},
    /* 2882 */ {I_VPSRAW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11901, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2883 */ {I_VPSRAW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11908, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2884 */ {I_VPSRAW, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6503, IF_AVX|IF_SANDYBRIDGE},
    /* 2885 */ {I_VPSRAW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6511, IF_AVX|IF_SANDYBRIDGE},
    /* 2886 */ {I_VPSRAD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11915, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2887 */ {I_VPSRAD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11922, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2888 */ {I_VPSRAD, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6519, IF_AVX|IF_SANDYBRIDGE},
    /* 2889 */ {I_VPSRAD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6527, IF_AVX|IF_SANDYBRIDGE},
    /* 2890 */ {I_VPSRLW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11929, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2891 */ {I_VPSRLW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11936, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2892 */ {I_VPSRLW, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6535, IF_AVX|IF_SANDYBRIDGE},
    /* 2893 */ {I_VPSRLW, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6543, IF_AVX|IF_SANDYBRIDGE},
    /* 2894 */ {I_VPSRLD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11943, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2895 */ {I_VPSRLD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11950, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2896 */ {I_VPSRLD, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6551, IF_AVX|IF_SANDYBRIDGE},
    /* 2897 */ {I_VPSRLD, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6559, IF_AVX|IF_SANDYBRIDGE},
    /* 2898 */ {I_VPSRLQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11957, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2899 */ {I_VPSRLQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11964, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2900 */ {I_VPSRLQ, 3, {XMMREG,XMMREG,IMMEDIATE,0,0}, nasm_bytecodes+6567, IF_AVX|IF_SANDYBRIDGE},
    /* 2901 */ {I_VPSRLQ, 2, {XMMREG,IMMEDIATE,0,0,0}, nasm_bytecodes+6575, IF_AVX|IF_SANDYBRIDGE},
    /* 2902 */ {I_VPTEST, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11971, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2903 */ {I_VPTEST, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+11978, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2904 */ {I_VPSUBB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11985, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2905 */ {I_VPSUBB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+11992, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2906 */ {I_VPSUBW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+11999, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2907 */ {I_VPSUBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12006, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2908 */ {I_VPSUBD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12013, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2909 */ {I_VPSUBD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12020, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2910 */ {I_VPSUBQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12027, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2911 */ {I_VPSUBQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12034, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2912 */ {I_VPSUBSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12041, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2913 */ {I_VPSUBSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12048, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2914 */ {I_VPSUBSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12055, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2915 */ {I_VPSUBSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12062, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2916 */ {I_VPSUBUSB, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12069, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2917 */ {I_VPSUBUSB, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12076, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2918 */ {I_VPSUBUSW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12083, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2919 */ {I_VPSUBUSW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12090, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2920 */ {I_VPUNPCKHBW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12097, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2921 */ {I_VPUNPCKHBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12104, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2922 */ {I_VPUNPCKHWD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12111, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2923 */ {I_VPUNPCKHWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12118, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2924 */ {I_VPUNPCKHDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12125, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2925 */ {I_VPUNPCKHDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12132, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2926 */ {I_VPUNPCKHQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12139, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2927 */ {I_VPUNPCKHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12146, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2928 */ {I_VPUNPCKLBW, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12153, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2929 */ {I_VPUNPCKLBW, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12160, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2930 */ {I_VPUNPCKLWD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12167, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2931 */ {I_VPUNPCKLWD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12174, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2932 */ {I_VPUNPCKLDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12181, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2933 */ {I_VPUNPCKLDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12188, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2934 */ {I_VPUNPCKLQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12195, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2935 */ {I_VPUNPCKLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12202, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2936 */ {I_VPXOR, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12209, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2937 */ {I_VPXOR, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12216, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2938 */ {I_VRCPPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12223, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2939 */ {I_VRCPPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12230, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2940 */ {I_VRCPSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12237, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2941 */ {I_VRCPSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12244, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2942 */ {I_VRSQRTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12251, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2943 */ {I_VRSQRTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12258, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2944 */ {I_VRSQRTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12265, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2945 */ {I_VRSQRTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12272, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2946 */ {I_VROUNDPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6583, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2947 */ {I_VROUNDPD, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6591, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2948 */ {I_VROUNDPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6599, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2949 */ {I_VROUNDPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6607, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2950 */ {I_VROUNDSD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6615, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2951 */ {I_VROUNDSD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6623, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2952 */ {I_VROUNDSS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6631, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2953 */ {I_VROUNDSS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6639, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2954 */ {I_VSHUFPD, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6647, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2955 */ {I_VSHUFPD, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6655, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2956 */ {I_VSHUFPD, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6663, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2957 */ {I_VSHUFPD, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6671, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2958 */ {I_VSHUFPS, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6679, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2959 */ {I_VSHUFPS, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6687, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2960 */ {I_VSHUFPS, 4, {YMMREG,YMMREG,RM_YMM,IMMEDIATE,0}, nasm_bytecodes+6695, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2961 */ {I_VSHUFPS, 3, {YMMREG,RM_YMM,IMMEDIATE,0,0}, nasm_bytecodes+6703, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2962 */ {I_VSQRTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12279, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2963 */ {I_VSQRTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12286, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2964 */ {I_VSQRTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12293, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2965 */ {I_VSQRTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12300, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2966 */ {I_VSQRTSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12307, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2967 */ {I_VSQRTSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12314, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2968 */ {I_VSQRTSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12321, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2969 */ {I_VSQRTSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12328, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2970 */ {I_VSTMXCSR, 1, {MEMORY,0,0,0,0}, nasm_bytecodes+12335, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2971 */ {I_VSUBPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12342, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2972 */ {I_VSUBPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12349, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2973 */ {I_VSUBPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12356, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2974 */ {I_VSUBPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12363, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2975 */ {I_VSUBPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12370, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2976 */ {I_VSUBPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12377, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2977 */ {I_VSUBPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12384, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2978 */ {I_VSUBPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12391, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2979 */ {I_VSUBSD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12398, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2980 */ {I_VSUBSD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12405, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2981 */ {I_VSUBSS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12412, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2982 */ {I_VSUBSS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12419, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2983 */ {I_VTESTPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12426, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2984 */ {I_VTESTPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12433, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2985 */ {I_VTESTPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12440, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2986 */ {I_VTESTPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12447, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2987 */ {I_VUCOMISD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12454, IF_AVX|IF_SANDYBRIDGE|IF_SQ},
    /* 2988 */ {I_VUCOMISS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12461, IF_AVX|IF_SANDYBRIDGE|IF_SD},
    /* 2989 */ {I_VUNPCKHPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12468, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2990 */ {I_VUNPCKHPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12475, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2991 */ {I_VUNPCKHPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12482, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2992 */ {I_VUNPCKHPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12489, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2993 */ {I_VUNPCKHPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12496, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2994 */ {I_VUNPCKHPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12503, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2995 */ {I_VUNPCKHPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12510, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2996 */ {I_VUNPCKHPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12517, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 2997 */ {I_VUNPCKLPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12524, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2998 */ {I_VUNPCKLPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12531, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 2999 */ {I_VUNPCKLPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12538, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 3000 */ {I_VUNPCKLPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12545, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 3001 */ {I_VUNPCKLPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12552, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3002 */ {I_VUNPCKLPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12559, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3003 */ {I_VUNPCKLPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12566, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 3004 */ {I_VUNPCKLPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12573, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 3005 */ {I_VXORPD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12580, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3006 */ {I_VXORPD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12587, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3007 */ {I_VXORPD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12594, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 3008 */ {I_VXORPD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12601, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 3009 */ {I_VXORPS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12608, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3010 */ {I_VXORPS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12615, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3011 */ {I_VXORPS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12622, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 3012 */ {I_VXORPS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12629, IF_AVX|IF_SANDYBRIDGE|IF_SY},
    /* 3013 */ {I_VZEROALL, 0, {0,0,0,0,0}, nasm_bytecodes+16296, IF_AVX|IF_SANDYBRIDGE},
    /* 3014 */ {I_VZEROUPPER, 0, {0,0,0,0,0}, nasm_bytecodes+16302, IF_AVX|IF_SANDYBRIDGE},
    /* 3015 */ {I_PCLMULLQLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+4995, IF_SSE|IF_WESTMERE|IF_SO},
    /* 3016 */ {I_PCLMULHQLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5004, IF_SSE|IF_WESTMERE|IF_SO},
    /* 3017 */ {I_PCLMULLQHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5013, IF_SSE|IF_WESTMERE|IF_SO},
    /* 3018 */ {I_PCLMULHQHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5022, IF_SSE|IF_WESTMERE|IF_SO},
    /* 3019 */ {I_PCLMULQDQ, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6711, IF_SSE|IF_WESTMERE|IF_SO},
    /* 3020 */ {I_VPCLMULLQLQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+5031, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3021 */ {I_VPCLMULLQLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5040, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3022 */ {I_VPCLMULHQLQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+5049, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3023 */ {I_VPCLMULHQLQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5058, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3024 */ {I_VPCLMULLQHQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+5067, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3025 */ {I_VPCLMULLQHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5076, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3026 */ {I_VPCLMULHQHQDQ, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+5085, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3027 */ {I_VPCLMULHQHQDQ, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+5094, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3028 */ {I_VPCLMULQDQ, 4, {XMMREG,XMMREG,RM_XMM,IMMEDIATE,0}, nasm_bytecodes+6719, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3029 */ {I_VPCLMULQDQ, 3, {XMMREG,RM_XMM,IMMEDIATE,0,0}, nasm_bytecodes+6727, IF_AVX|IF_SANDYBRIDGE|IF_SO},
    /* 3030 */ {I_VFMADD132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12636, IF_FMA|IF_FUTURE|IF_SO},
    /* 3031 */ {I_VFMADD132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12643, IF_FMA|IF_FUTURE|IF_SO},
    /* 3032 */ {I_VFMADD132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12650, IF_FMA|IF_FUTURE|IF_SY},
    /* 3033 */ {I_VFMADD132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12657, IF_FMA|IF_FUTURE|IF_SY},
    /* 3034 */ {I_VFMADD132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12664, IF_FMA|IF_FUTURE|IF_SO},
    /* 3035 */ {I_VFMADD132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12671, IF_FMA|IF_FUTURE|IF_SO},
    /* 3036 */ {I_VFMADD132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12678, IF_FMA|IF_FUTURE|IF_SY},
    /* 3037 */ {I_VFMADD132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12685, IF_FMA|IF_FUTURE|IF_SY},
    /* 3038 */ {I_VFMADD312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12636, IF_FMA|IF_FUTURE|IF_SO},
    /* 3039 */ {I_VFMADD312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12643, IF_FMA|IF_FUTURE|IF_SO},
    /* 3040 */ {I_VFMADD312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12650, IF_FMA|IF_FUTURE|IF_SY},
    /* 3041 */ {I_VFMADD312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12657, IF_FMA|IF_FUTURE|IF_SY},
    /* 3042 */ {I_VFMADD312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12664, IF_FMA|IF_FUTURE|IF_SO},
    /* 3043 */ {I_VFMADD312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12671, IF_FMA|IF_FUTURE|IF_SO},
    /* 3044 */ {I_VFMADD312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12678, IF_FMA|IF_FUTURE|IF_SY},
    /* 3045 */ {I_VFMADD312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12685, IF_FMA|IF_FUTURE|IF_SY},
    /* 3046 */ {I_VFMADD213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12692, IF_FMA|IF_FUTURE|IF_SO},
    /* 3047 */ {I_VFMADD213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12699, IF_FMA|IF_FUTURE|IF_SO},
    /* 3048 */ {I_VFMADD213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12706, IF_FMA|IF_FUTURE|IF_SY},
    /* 3049 */ {I_VFMADD213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12713, IF_FMA|IF_FUTURE|IF_SY},
    /* 3050 */ {I_VFMADD213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12720, IF_FMA|IF_FUTURE|IF_SO},
    /* 3051 */ {I_VFMADD213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12727, IF_FMA|IF_FUTURE|IF_SO},
    /* 3052 */ {I_VFMADD213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12734, IF_FMA|IF_FUTURE|IF_SY},
    /* 3053 */ {I_VFMADD213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12741, IF_FMA|IF_FUTURE|IF_SY},
    /* 3054 */ {I_VFMADD123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12692, IF_FMA|IF_FUTURE|IF_SO},
    /* 3055 */ {I_VFMADD123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12699, IF_FMA|IF_FUTURE|IF_SO},
    /* 3056 */ {I_VFMADD123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12706, IF_FMA|IF_FUTURE|IF_SY},
    /* 3057 */ {I_VFMADD123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12713, IF_FMA|IF_FUTURE|IF_SY},
    /* 3058 */ {I_VFMADD123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12720, IF_FMA|IF_FUTURE|IF_SO},
    /* 3059 */ {I_VFMADD123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12727, IF_FMA|IF_FUTURE|IF_SO},
    /* 3060 */ {I_VFMADD123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12734, IF_FMA|IF_FUTURE|IF_SY},
    /* 3061 */ {I_VFMADD123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12741, IF_FMA|IF_FUTURE|IF_SY},
    /* 3062 */ {I_VFMADD231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12748, IF_FMA|IF_FUTURE|IF_SO},
    /* 3063 */ {I_VFMADD231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12755, IF_FMA|IF_FUTURE|IF_SO},
    /* 3064 */ {I_VFMADD231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12762, IF_FMA|IF_FUTURE|IF_SY},
    /* 3065 */ {I_VFMADD231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12769, IF_FMA|IF_FUTURE|IF_SY},
    /* 3066 */ {I_VFMADD231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12776, IF_FMA|IF_FUTURE|IF_SO},
    /* 3067 */ {I_VFMADD231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12783, IF_FMA|IF_FUTURE|IF_SO},
    /* 3068 */ {I_VFMADD231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12790, IF_FMA|IF_FUTURE|IF_SY},
    /* 3069 */ {I_VFMADD231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12797, IF_FMA|IF_FUTURE|IF_SY},
    /* 3070 */ {I_VFMADD321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12748, IF_FMA|IF_FUTURE|IF_SO},
    /* 3071 */ {I_VFMADD321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12755, IF_FMA|IF_FUTURE|IF_SO},
    /* 3072 */ {I_VFMADD321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12762, IF_FMA|IF_FUTURE|IF_SY},
    /* 3073 */ {I_VFMADD321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12769, IF_FMA|IF_FUTURE|IF_SY},
    /* 3074 */ {I_VFMADD321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12776, IF_FMA|IF_FUTURE|IF_SO},
    /* 3075 */ {I_VFMADD321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12783, IF_FMA|IF_FUTURE|IF_SO},
    /* 3076 */ {I_VFMADD321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12790, IF_FMA|IF_FUTURE|IF_SY},
    /* 3077 */ {I_VFMADD321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12797, IF_FMA|IF_FUTURE|IF_SY},
    /* 3078 */ {I_VFMADDSUB132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12804, IF_FMA|IF_FUTURE|IF_SO},
    /* 3079 */ {I_VFMADDSUB132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12811, IF_FMA|IF_FUTURE|IF_SO},
    /* 3080 */ {I_VFMADDSUB132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12818, IF_FMA|IF_FUTURE|IF_SY},
    /* 3081 */ {I_VFMADDSUB132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12825, IF_FMA|IF_FUTURE|IF_SY},
    /* 3082 */ {I_VFMADDSUB132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12832, IF_FMA|IF_FUTURE|IF_SO},
    /* 3083 */ {I_VFMADDSUB132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12839, IF_FMA|IF_FUTURE|IF_SO},
    /* 3084 */ {I_VFMADDSUB132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12846, IF_FMA|IF_FUTURE|IF_SY},
    /* 3085 */ {I_VFMADDSUB132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12853, IF_FMA|IF_FUTURE|IF_SY},
    /* 3086 */ {I_VFMADDSUB312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12804, IF_FMA|IF_FUTURE|IF_SO},
    /* 3087 */ {I_VFMADDSUB312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12811, IF_FMA|IF_FUTURE|IF_SO},
    /* 3088 */ {I_VFMADDSUB312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12818, IF_FMA|IF_FUTURE|IF_SY},
    /* 3089 */ {I_VFMADDSUB312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12825, IF_FMA|IF_FUTURE|IF_SY},
    /* 3090 */ {I_VFMADDSUB312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12832, IF_FMA|IF_FUTURE|IF_SO},
    /* 3091 */ {I_VFMADDSUB312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12839, IF_FMA|IF_FUTURE|IF_SO},
    /* 3092 */ {I_VFMADDSUB312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12846, IF_FMA|IF_FUTURE|IF_SY},
    /* 3093 */ {I_VFMADDSUB312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12853, IF_FMA|IF_FUTURE|IF_SY},
    /* 3094 */ {I_VFMADDSUB213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12860, IF_FMA|IF_FUTURE|IF_SO},
    /* 3095 */ {I_VFMADDSUB213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12867, IF_FMA|IF_FUTURE|IF_SO},
    /* 3096 */ {I_VFMADDSUB213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12874, IF_FMA|IF_FUTURE|IF_SY},
    /* 3097 */ {I_VFMADDSUB213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12881, IF_FMA|IF_FUTURE|IF_SY},
    /* 3098 */ {I_VFMADDSUB213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12888, IF_FMA|IF_FUTURE|IF_SO},
    /* 3099 */ {I_VFMADDSUB213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12895, IF_FMA|IF_FUTURE|IF_SO},
    /* 3100 */ {I_VFMADDSUB213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12902, IF_FMA|IF_FUTURE|IF_SY},
    /* 3101 */ {I_VFMADDSUB213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12909, IF_FMA|IF_FUTURE|IF_SY},
    /* 3102 */ {I_VFMADDSUB123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12860, IF_FMA|IF_FUTURE|IF_SO},
    /* 3103 */ {I_VFMADDSUB123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12867, IF_FMA|IF_FUTURE|IF_SO},
    /* 3104 */ {I_VFMADDSUB123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12874, IF_FMA|IF_FUTURE|IF_SY},
    /* 3105 */ {I_VFMADDSUB123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12881, IF_FMA|IF_FUTURE|IF_SY},
    /* 3106 */ {I_VFMADDSUB123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12888, IF_FMA|IF_FUTURE|IF_SO},
    /* 3107 */ {I_VFMADDSUB123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12895, IF_FMA|IF_FUTURE|IF_SO},
    /* 3108 */ {I_VFMADDSUB123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12902, IF_FMA|IF_FUTURE|IF_SY},
    /* 3109 */ {I_VFMADDSUB123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12909, IF_FMA|IF_FUTURE|IF_SY},
    /* 3110 */ {I_VFMADDSUB231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12916, IF_FMA|IF_FUTURE|IF_SO},
    /* 3111 */ {I_VFMADDSUB231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12923, IF_FMA|IF_FUTURE|IF_SO},
    /* 3112 */ {I_VFMADDSUB231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12930, IF_FMA|IF_FUTURE|IF_SY},
    /* 3113 */ {I_VFMADDSUB231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12937, IF_FMA|IF_FUTURE|IF_SY},
    /* 3114 */ {I_VFMADDSUB231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12944, IF_FMA|IF_FUTURE|IF_SO},
    /* 3115 */ {I_VFMADDSUB231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12951, IF_FMA|IF_FUTURE|IF_SO},
    /* 3116 */ {I_VFMADDSUB231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12958, IF_FMA|IF_FUTURE|IF_SY},
    /* 3117 */ {I_VFMADDSUB231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12965, IF_FMA|IF_FUTURE|IF_SY},
    /* 3118 */ {I_VFMADDSUB321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12916, IF_FMA|IF_FUTURE|IF_SO},
    /* 3119 */ {I_VFMADDSUB321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12923, IF_FMA|IF_FUTURE|IF_SO},
    /* 3120 */ {I_VFMADDSUB321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12930, IF_FMA|IF_FUTURE|IF_SY},
    /* 3121 */ {I_VFMADDSUB321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12937, IF_FMA|IF_FUTURE|IF_SY},
    /* 3122 */ {I_VFMADDSUB321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12944, IF_FMA|IF_FUTURE|IF_SO},
    /* 3123 */ {I_VFMADDSUB321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12951, IF_FMA|IF_FUTURE|IF_SO},
    /* 3124 */ {I_VFMADDSUB321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12958, IF_FMA|IF_FUTURE|IF_SY},
    /* 3125 */ {I_VFMADDSUB321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12965, IF_FMA|IF_FUTURE|IF_SY},
    /* 3126 */ {I_VFMSUB132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12972, IF_FMA|IF_FUTURE|IF_SO},
    /* 3127 */ {I_VFMSUB132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12979, IF_FMA|IF_FUTURE|IF_SO},
    /* 3128 */ {I_VFMSUB132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12986, IF_FMA|IF_FUTURE|IF_SY},
    /* 3129 */ {I_VFMSUB132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12993, IF_FMA|IF_FUTURE|IF_SY},
    /* 3130 */ {I_VFMSUB132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13000, IF_FMA|IF_FUTURE|IF_SO},
    /* 3131 */ {I_VFMSUB132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13007, IF_FMA|IF_FUTURE|IF_SO},
    /* 3132 */ {I_VFMSUB132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13014, IF_FMA|IF_FUTURE|IF_SY},
    /* 3133 */ {I_VFMSUB132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13021, IF_FMA|IF_FUTURE|IF_SY},
    /* 3134 */ {I_VFMSUB312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+12972, IF_FMA|IF_FUTURE|IF_SO},
    /* 3135 */ {I_VFMSUB312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+12979, IF_FMA|IF_FUTURE|IF_SO},
    /* 3136 */ {I_VFMSUB312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+12986, IF_FMA|IF_FUTURE|IF_SY},
    /* 3137 */ {I_VFMSUB312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+12993, IF_FMA|IF_FUTURE|IF_SY},
    /* 3138 */ {I_VFMSUB312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13000, IF_FMA|IF_FUTURE|IF_SO},
    /* 3139 */ {I_VFMSUB312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13007, IF_FMA|IF_FUTURE|IF_SO},
    /* 3140 */ {I_VFMSUB312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13014, IF_FMA|IF_FUTURE|IF_SY},
    /* 3141 */ {I_VFMSUB312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13021, IF_FMA|IF_FUTURE|IF_SY},
    /* 3142 */ {I_VFMSUB213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13028, IF_FMA|IF_FUTURE|IF_SO},
    /* 3143 */ {I_VFMSUB213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13035, IF_FMA|IF_FUTURE|IF_SO},
    /* 3144 */ {I_VFMSUB213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13042, IF_FMA|IF_FUTURE|IF_SY},
    /* 3145 */ {I_VFMSUB213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13049, IF_FMA|IF_FUTURE|IF_SY},
    /* 3146 */ {I_VFMSUB213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13056, IF_FMA|IF_FUTURE|IF_SO},
    /* 3147 */ {I_VFMSUB213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13063, IF_FMA|IF_FUTURE|IF_SO},
    /* 3148 */ {I_VFMSUB213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13070, IF_FMA|IF_FUTURE|IF_SY},
    /* 3149 */ {I_VFMSUB213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13077, IF_FMA|IF_FUTURE|IF_SY},
    /* 3150 */ {I_VFMSUB123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13028, IF_FMA|IF_FUTURE|IF_SO},
    /* 3151 */ {I_VFMSUB123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13035, IF_FMA|IF_FUTURE|IF_SO},
    /* 3152 */ {I_VFMSUB123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13042, IF_FMA|IF_FUTURE|IF_SY},
    /* 3153 */ {I_VFMSUB123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13049, IF_FMA|IF_FUTURE|IF_SY},
    /* 3154 */ {I_VFMSUB123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13056, IF_FMA|IF_FUTURE|IF_SO},
    /* 3155 */ {I_VFMSUB123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13063, IF_FMA|IF_FUTURE|IF_SO},
    /* 3156 */ {I_VFMSUB123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13070, IF_FMA|IF_FUTURE|IF_SY},
    /* 3157 */ {I_VFMSUB123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13077, IF_FMA|IF_FUTURE|IF_SY},
    /* 3158 */ {I_VFMSUB231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13084, IF_FMA|IF_FUTURE|IF_SO},
    /* 3159 */ {I_VFMSUB231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13091, IF_FMA|IF_FUTURE|IF_SO},
    /* 3160 */ {I_VFMSUB231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13098, IF_FMA|IF_FUTURE|IF_SY},
    /* 3161 */ {I_VFMSUB231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13105, IF_FMA|IF_FUTURE|IF_SY},
    /* 3162 */ {I_VFMSUB231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13112, IF_FMA|IF_FUTURE|IF_SO},
    /* 3163 */ {I_VFMSUB231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13119, IF_FMA|IF_FUTURE|IF_SO},
    /* 3164 */ {I_VFMSUB231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13126, IF_FMA|IF_FUTURE|IF_SY},
    /* 3165 */ {I_VFMSUB231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13133, IF_FMA|IF_FUTURE|IF_SY},
    /* 3166 */ {I_VFMSUB321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13084, IF_FMA|IF_FUTURE|IF_SO},
    /* 3167 */ {I_VFMSUB321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13091, IF_FMA|IF_FUTURE|IF_SO},
    /* 3168 */ {I_VFMSUB321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13098, IF_FMA|IF_FUTURE|IF_SY},
    /* 3169 */ {I_VFMSUB321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13105, IF_FMA|IF_FUTURE|IF_SY},
    /* 3170 */ {I_VFMSUB321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13112, IF_FMA|IF_FUTURE|IF_SO},
    /* 3171 */ {I_VFMSUB321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13119, IF_FMA|IF_FUTURE|IF_SO},
    /* 3172 */ {I_VFMSUB321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13126, IF_FMA|IF_FUTURE|IF_SY},
    /* 3173 */ {I_VFMSUB321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13133, IF_FMA|IF_FUTURE|IF_SY},
    /* 3174 */ {I_VFMSUBADD132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13140, IF_FMA|IF_FUTURE|IF_SO},
    /* 3175 */ {I_VFMSUBADD132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13147, IF_FMA|IF_FUTURE|IF_SO},
    /* 3176 */ {I_VFMSUBADD132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13154, IF_FMA|IF_FUTURE|IF_SY},
    /* 3177 */ {I_VFMSUBADD132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13161, IF_FMA|IF_FUTURE|IF_SY},
    /* 3178 */ {I_VFMSUBADD132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13168, IF_FMA|IF_FUTURE|IF_SO},
    /* 3179 */ {I_VFMSUBADD132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13175, IF_FMA|IF_FUTURE|IF_SO},
    /* 3180 */ {I_VFMSUBADD132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13182, IF_FMA|IF_FUTURE|IF_SY},
    /* 3181 */ {I_VFMSUBADD132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13189, IF_FMA|IF_FUTURE|IF_SY},
    /* 3182 */ {I_VFMSUBADD312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13140, IF_FMA|IF_FUTURE|IF_SO},
    /* 3183 */ {I_VFMSUBADD312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13147, IF_FMA|IF_FUTURE|IF_SO},
    /* 3184 */ {I_VFMSUBADD312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13154, IF_FMA|IF_FUTURE|IF_SY},
    /* 3185 */ {I_VFMSUBADD312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13161, IF_FMA|IF_FUTURE|IF_SY},
    /* 3186 */ {I_VFMSUBADD312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13168, IF_FMA|IF_FUTURE|IF_SO},
    /* 3187 */ {I_VFMSUBADD312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13175, IF_FMA|IF_FUTURE|IF_SO},
    /* 3188 */ {I_VFMSUBADD312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13182, IF_FMA|IF_FUTURE|IF_SY},
    /* 3189 */ {I_VFMSUBADD312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13189, IF_FMA|IF_FUTURE|IF_SY},
    /* 3190 */ {I_VFMSUBADD213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13196, IF_FMA|IF_FUTURE|IF_SO},
    /* 3191 */ {I_VFMSUBADD213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13203, IF_FMA|IF_FUTURE|IF_SO},
    /* 3192 */ {I_VFMSUBADD213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13210, IF_FMA|IF_FUTURE|IF_SY},
    /* 3193 */ {I_VFMSUBADD213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13217, IF_FMA|IF_FUTURE|IF_SY},
    /* 3194 */ {I_VFMSUBADD213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13224, IF_FMA|IF_FUTURE|IF_SO},
    /* 3195 */ {I_VFMSUBADD213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13231, IF_FMA|IF_FUTURE|IF_SO},
    /* 3196 */ {I_VFMSUBADD213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13238, IF_FMA|IF_FUTURE|IF_SY},
    /* 3197 */ {I_VFMSUBADD213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13245, IF_FMA|IF_FUTURE|IF_SY},
    /* 3198 */ {I_VFMSUBADD123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13196, IF_FMA|IF_FUTURE|IF_SO},
    /* 3199 */ {I_VFMSUBADD123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13203, IF_FMA|IF_FUTURE|IF_SO},
    /* 3200 */ {I_VFMSUBADD123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13210, IF_FMA|IF_FUTURE|IF_SY},
    /* 3201 */ {I_VFMSUBADD123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13217, IF_FMA|IF_FUTURE|IF_SY},
    /* 3202 */ {I_VFMSUBADD123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13224, IF_FMA|IF_FUTURE|IF_SO},
    /* 3203 */ {I_VFMSUBADD123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13231, IF_FMA|IF_FUTURE|IF_SO},
    /* 3204 */ {I_VFMSUBADD123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13238, IF_FMA|IF_FUTURE|IF_SY},
    /* 3205 */ {I_VFMSUBADD123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13245, IF_FMA|IF_FUTURE|IF_SY},
    /* 3206 */ {I_VFMSUBADD231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13252, IF_FMA|IF_FUTURE|IF_SO},
    /* 3207 */ {I_VFMSUBADD231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13259, IF_FMA|IF_FUTURE|IF_SO},
    /* 3208 */ {I_VFMSUBADD231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13266, IF_FMA|IF_FUTURE|IF_SY},
    /* 3209 */ {I_VFMSUBADD231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13273, IF_FMA|IF_FUTURE|IF_SY},
    /* 3210 */ {I_VFMSUBADD231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13280, IF_FMA|IF_FUTURE|IF_SO},
    /* 3211 */ {I_VFMSUBADD231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13287, IF_FMA|IF_FUTURE|IF_SO},
    /* 3212 */ {I_VFMSUBADD231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13294, IF_FMA|IF_FUTURE|IF_SY},
    /* 3213 */ {I_VFMSUBADD231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13301, IF_FMA|IF_FUTURE|IF_SY},
    /* 3214 */ {I_VFMSUBADD321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13252, IF_FMA|IF_FUTURE|IF_SO},
    /* 3215 */ {I_VFMSUBADD321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13259, IF_FMA|IF_FUTURE|IF_SO},
    /* 3216 */ {I_VFMSUBADD321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13266, IF_FMA|IF_FUTURE|IF_SY},
    /* 3217 */ {I_VFMSUBADD321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13273, IF_FMA|IF_FUTURE|IF_SY},
    /* 3218 */ {I_VFMSUBADD321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13280, IF_FMA|IF_FUTURE|IF_SO},
    /* 3219 */ {I_VFMSUBADD321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13287, IF_FMA|IF_FUTURE|IF_SO},
    /* 3220 */ {I_VFMSUBADD321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13294, IF_FMA|IF_FUTURE|IF_SY},
    /* 3221 */ {I_VFMSUBADD321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13301, IF_FMA|IF_FUTURE|IF_SY},
    /* 3222 */ {I_VFNMADD132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13308, IF_FMA|IF_FUTURE|IF_SO},
    /* 3223 */ {I_VFNMADD132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13315, IF_FMA|IF_FUTURE|IF_SO},
    /* 3224 */ {I_VFNMADD132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13322, IF_FMA|IF_FUTURE|IF_SY},
    /* 3225 */ {I_VFNMADD132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13329, IF_FMA|IF_FUTURE|IF_SY},
    /* 3226 */ {I_VFNMADD132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13336, IF_FMA|IF_FUTURE|IF_SO},
    /* 3227 */ {I_VFNMADD132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13343, IF_FMA|IF_FUTURE|IF_SO},
    /* 3228 */ {I_VFNMADD132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13350, IF_FMA|IF_FUTURE|IF_SY},
    /* 3229 */ {I_VFNMADD132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13357, IF_FMA|IF_FUTURE|IF_SY},
    /* 3230 */ {I_VFNMADD312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13308, IF_FMA|IF_FUTURE|IF_SO},
    /* 3231 */ {I_VFNMADD312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13315, IF_FMA|IF_FUTURE|IF_SO},
    /* 3232 */ {I_VFNMADD312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13322, IF_FMA|IF_FUTURE|IF_SY},
    /* 3233 */ {I_VFNMADD312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13329, IF_FMA|IF_FUTURE|IF_SY},
    /* 3234 */ {I_VFNMADD312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13336, IF_FMA|IF_FUTURE|IF_SO},
    /* 3235 */ {I_VFNMADD312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13343, IF_FMA|IF_FUTURE|IF_SO},
    /* 3236 */ {I_VFNMADD312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13350, IF_FMA|IF_FUTURE|IF_SY},
    /* 3237 */ {I_VFNMADD312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13357, IF_FMA|IF_FUTURE|IF_SY},
    /* 3238 */ {I_VFNMADD213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13364, IF_FMA|IF_FUTURE|IF_SO},
    /* 3239 */ {I_VFNMADD213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13371, IF_FMA|IF_FUTURE|IF_SO},
    /* 3240 */ {I_VFNMADD213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13378, IF_FMA|IF_FUTURE|IF_SY},
    /* 3241 */ {I_VFNMADD213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13385, IF_FMA|IF_FUTURE|IF_SY},
    /* 3242 */ {I_VFNMADD213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13392, IF_FMA|IF_FUTURE|IF_SO},
    /* 3243 */ {I_VFNMADD213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13399, IF_FMA|IF_FUTURE|IF_SO},
    /* 3244 */ {I_VFNMADD213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13406, IF_FMA|IF_FUTURE|IF_SY},
    /* 3245 */ {I_VFNMADD213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13413, IF_FMA|IF_FUTURE|IF_SY},
    /* 3246 */ {I_VFNMADD123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13364, IF_FMA|IF_FUTURE|IF_SO},
    /* 3247 */ {I_VFNMADD123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13371, IF_FMA|IF_FUTURE|IF_SO},
    /* 3248 */ {I_VFNMADD123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13378, IF_FMA|IF_FUTURE|IF_SY},
    /* 3249 */ {I_VFNMADD123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13385, IF_FMA|IF_FUTURE|IF_SY},
    /* 3250 */ {I_VFNMADD123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13392, IF_FMA|IF_FUTURE|IF_SO},
    /* 3251 */ {I_VFNMADD123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13399, IF_FMA|IF_FUTURE|IF_SO},
    /* 3252 */ {I_VFNMADD123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13406, IF_FMA|IF_FUTURE|IF_SY},
    /* 3253 */ {I_VFNMADD123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13413, IF_FMA|IF_FUTURE|IF_SY},
    /* 3254 */ {I_VFNMADD231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13420, IF_FMA|IF_FUTURE|IF_SO},
    /* 3255 */ {I_VFNMADD231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13427, IF_FMA|IF_FUTURE|IF_SO},
    /* 3256 */ {I_VFNMADD231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13434, IF_FMA|IF_FUTURE|IF_SY},
    /* 3257 */ {I_VFNMADD231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13441, IF_FMA|IF_FUTURE|IF_SY},
    /* 3258 */ {I_VFNMADD231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13448, IF_FMA|IF_FUTURE|IF_SO},
    /* 3259 */ {I_VFNMADD231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13455, IF_FMA|IF_FUTURE|IF_SO},
    /* 3260 */ {I_VFNMADD231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13462, IF_FMA|IF_FUTURE|IF_SY},
    /* 3261 */ {I_VFNMADD231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13469, IF_FMA|IF_FUTURE|IF_SY},
    /* 3262 */ {I_VFNMADD321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13420, IF_FMA|IF_FUTURE|IF_SO},
    /* 3263 */ {I_VFNMADD321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13427, IF_FMA|IF_FUTURE|IF_SO},
    /* 3264 */ {I_VFNMADD321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13434, IF_FMA|IF_FUTURE|IF_SY},
    /* 3265 */ {I_VFNMADD321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13441, IF_FMA|IF_FUTURE|IF_SY},
    /* 3266 */ {I_VFNMADD321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13448, IF_FMA|IF_FUTURE|IF_SO},
    /* 3267 */ {I_VFNMADD321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13455, IF_FMA|IF_FUTURE|IF_SO},
    /* 3268 */ {I_VFNMADD321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13462, IF_FMA|IF_FUTURE|IF_SY},
    /* 3269 */ {I_VFNMADD321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13469, IF_FMA|IF_FUTURE|IF_SY},
    /* 3270 */ {I_VFNMSUB132PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13476, IF_FMA|IF_FUTURE|IF_SO},
    /* 3271 */ {I_VFNMSUB132PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13483, IF_FMA|IF_FUTURE|IF_SO},
    /* 3272 */ {I_VFNMSUB132PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13490, IF_FMA|IF_FUTURE|IF_SY},
    /* 3273 */ {I_VFNMSUB132PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13497, IF_FMA|IF_FUTURE|IF_SY},
    /* 3274 */ {I_VFNMSUB132PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13504, IF_FMA|IF_FUTURE|IF_SO},
    /* 3275 */ {I_VFNMSUB132PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13511, IF_FMA|IF_FUTURE|IF_SO},
    /* 3276 */ {I_VFNMSUB132PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13518, IF_FMA|IF_FUTURE|IF_SY},
    /* 3277 */ {I_VFNMSUB132PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13525, IF_FMA|IF_FUTURE|IF_SY},
    /* 3278 */ {I_VFNMSUB312PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13476, IF_FMA|IF_FUTURE|IF_SO},
    /* 3279 */ {I_VFNMSUB312PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13483, IF_FMA|IF_FUTURE|IF_SO},
    /* 3280 */ {I_VFNMSUB312PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13490, IF_FMA|IF_FUTURE|IF_SY},
    /* 3281 */ {I_VFNMSUB312PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13497, IF_FMA|IF_FUTURE|IF_SY},
    /* 3282 */ {I_VFNMSUB312PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13504, IF_FMA|IF_FUTURE|IF_SO},
    /* 3283 */ {I_VFNMSUB312PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13511, IF_FMA|IF_FUTURE|IF_SO},
    /* 3284 */ {I_VFNMSUB312PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13518, IF_FMA|IF_FUTURE|IF_SY},
    /* 3285 */ {I_VFNMSUB312PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13525, IF_FMA|IF_FUTURE|IF_SY},
    /* 3286 */ {I_VFNMSUB213PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13532, IF_FMA|IF_FUTURE|IF_SO},
    /* 3287 */ {I_VFNMSUB213PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13539, IF_FMA|IF_FUTURE|IF_SO},
    /* 3288 */ {I_VFNMSUB213PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13546, IF_FMA|IF_FUTURE|IF_SY},
    /* 3289 */ {I_VFNMSUB213PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13553, IF_FMA|IF_FUTURE|IF_SY},
    /* 3290 */ {I_VFNMSUB213PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13560, IF_FMA|IF_FUTURE|IF_SO},
    /* 3291 */ {I_VFNMSUB213PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13567, IF_FMA|IF_FUTURE|IF_SO},
    /* 3292 */ {I_VFNMSUB213PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13574, IF_FMA|IF_FUTURE|IF_SY},
    /* 3293 */ {I_VFNMSUB213PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13581, IF_FMA|IF_FUTURE|IF_SY},
    /* 3294 */ {I_VFNMSUB123PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13532, IF_FMA|IF_FUTURE|IF_SO},
    /* 3295 */ {I_VFNMSUB123PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13539, IF_FMA|IF_FUTURE|IF_SO},
    /* 3296 */ {I_VFNMSUB123PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13546, IF_FMA|IF_FUTURE|IF_SY},
    /* 3297 */ {I_VFNMSUB123PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13553, IF_FMA|IF_FUTURE|IF_SY},
    /* 3298 */ {I_VFNMSUB123PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13560, IF_FMA|IF_FUTURE|IF_SO},
    /* 3299 */ {I_VFNMSUB123PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13567, IF_FMA|IF_FUTURE|IF_SO},
    /* 3300 */ {I_VFNMSUB123PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13574, IF_FMA|IF_FUTURE|IF_SY},
    /* 3301 */ {I_VFNMSUB123PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13581, IF_FMA|IF_FUTURE|IF_SY},
    /* 3302 */ {I_VFNMSUB231PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13588, IF_FMA|IF_FUTURE|IF_SO},
    /* 3303 */ {I_VFNMSUB231PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13595, IF_FMA|IF_FUTURE|IF_SO},
    /* 3304 */ {I_VFNMSUB231PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13602, IF_FMA|IF_FUTURE|IF_SY},
    /* 3305 */ {I_VFNMSUB231PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13609, IF_FMA|IF_FUTURE|IF_SY},
    /* 3306 */ {I_VFNMSUB231PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13616, IF_FMA|IF_FUTURE|IF_SO},
    /* 3307 */ {I_VFNMSUB231PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13623, IF_FMA|IF_FUTURE|IF_SO},
    /* 3308 */ {I_VFNMSUB231PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13630, IF_FMA|IF_FUTURE|IF_SY},
    /* 3309 */ {I_VFNMSUB231PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13637, IF_FMA|IF_FUTURE|IF_SY},
    /* 3310 */ {I_VFNMSUB321PS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13588, IF_FMA|IF_FUTURE|IF_SO},
    /* 3311 */ {I_VFNMSUB321PS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13595, IF_FMA|IF_FUTURE|IF_SO},
    /* 3312 */ {I_VFNMSUB321PS, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13602, IF_FMA|IF_FUTURE|IF_SY},
    /* 3313 */ {I_VFNMSUB321PS, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13609, IF_FMA|IF_FUTURE|IF_SY},
    /* 3314 */ {I_VFNMSUB321PD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13616, IF_FMA|IF_FUTURE|IF_SO},
    /* 3315 */ {I_VFNMSUB321PD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13623, IF_FMA|IF_FUTURE|IF_SO},
    /* 3316 */ {I_VFNMSUB321PD, 3, {YMMREG,YMMREG,RM_YMM,0,0}, nasm_bytecodes+13630, IF_FMA|IF_FUTURE|IF_SY},
    /* 3317 */ {I_VFNMSUB321PD, 2, {YMMREG,RM_YMM,0,0,0}, nasm_bytecodes+13637, IF_FMA|IF_FUTURE|IF_SY},
    /* 3318 */ {I_VFMADD132SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13644, IF_FMA|IF_FUTURE|IF_SD},
    /* 3319 */ {I_VFMADD132SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13651, IF_FMA|IF_FUTURE|IF_SD},
    /* 3320 */ {I_VFMADD132SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13658, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3321 */ {I_VFMADD132SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13665, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3322 */ {I_VFMADD312SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13644, IF_FMA|IF_FUTURE|IF_SD},
    /* 3323 */ {I_VFMADD312SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13651, IF_FMA|IF_FUTURE|IF_SD},
    /* 3324 */ {I_VFMADD312SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13658, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3325 */ {I_VFMADD312SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13665, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3326 */ {I_VFMADD213SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13672, IF_FMA|IF_FUTURE|IF_SD},
    /* 3327 */ {I_VFMADD213SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13679, IF_FMA|IF_FUTURE|IF_SD},
    /* 3328 */ {I_VFMADD213SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13686, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3329 */ {I_VFMADD213SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13693, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3330 */ {I_VFMADD123SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13672, IF_FMA|IF_FUTURE|IF_SD},
    /* 3331 */ {I_VFMADD123SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13679, IF_FMA|IF_FUTURE|IF_SD},
    /* 3332 */ {I_VFMADD123SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13686, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3333 */ {I_VFMADD123SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13693, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3334 */ {I_VFMADD231SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13700, IF_FMA|IF_FUTURE|IF_SD},
    /* 3335 */ {I_VFMADD231SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13707, IF_FMA|IF_FUTURE|IF_SD},
    /* 3336 */ {I_VFMADD231SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13714, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3337 */ {I_VFMADD231SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13721, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3338 */ {I_VFMADD321SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13700, IF_FMA|IF_FUTURE|IF_SD},
    /* 3339 */ {I_VFMADD321SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13707, IF_FMA|IF_FUTURE|IF_SD},
    /* 3340 */ {I_VFMADD321SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13714, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3341 */ {I_VFMADD321SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13721, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3342 */ {I_VFMSUB132SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13728, IF_FMA|IF_FUTURE|IF_SD},
    /* 3343 */ {I_VFMSUB132SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13735, IF_FMA|IF_FUTURE|IF_SD},
    /* 3344 */ {I_VFMSUB132SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13742, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3345 */ {I_VFMSUB132SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13749, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3346 */ {I_VFMSUB312SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13728, IF_FMA|IF_FUTURE|IF_SD},
    /* 3347 */ {I_VFMSUB312SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13735, IF_FMA|IF_FUTURE|IF_SD},
    /* 3348 */ {I_VFMSUB312SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13742, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3349 */ {I_VFMSUB312SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13749, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3350 */ {I_VFMSUB213SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13756, IF_FMA|IF_FUTURE|IF_SD},
    /* 3351 */ {I_VFMSUB213SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13763, IF_FMA|IF_FUTURE|IF_SD},
    /* 3352 */ {I_VFMSUB213SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13770, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3353 */ {I_VFMSUB213SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13777, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3354 */ {I_VFMSUB123SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13756, IF_FMA|IF_FUTURE|IF_SD},
    /* 3355 */ {I_VFMSUB123SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13763, IF_FMA|IF_FUTURE|IF_SD},
    /* 3356 */ {I_VFMSUB123SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13770, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3357 */ {I_VFMSUB123SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13777, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3358 */ {I_VFMSUB231SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13784, IF_FMA|IF_FUTURE|IF_SD},
    /* 3359 */ {I_VFMSUB231SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13791, IF_FMA|IF_FUTURE|IF_SD},
    /* 3360 */ {I_VFMSUB231SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13798, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3361 */ {I_VFMSUB231SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13805, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3362 */ {I_VFMSUB321SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13784, IF_FMA|IF_FUTURE|IF_SD},
    /* 3363 */ {I_VFMSUB321SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13791, IF_FMA|IF_FUTURE|IF_SD},
    /* 3364 */ {I_VFMSUB321SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13798, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3365 */ {I_VFMSUB321SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13805, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3366 */ {I_VFNMADD132SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13812, IF_FMA|IF_FUTURE|IF_SD},
    /* 3367 */ {I_VFNMADD132SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13819, IF_FMA|IF_FUTURE|IF_SD},
    /* 3368 */ {I_VFNMADD132SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13826, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3369 */ {I_VFNMADD132SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13833, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3370 */ {I_VFNMADD312SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13812, IF_FMA|IF_FUTURE|IF_SD},
    /* 3371 */ {I_VFNMADD312SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13819, IF_FMA|IF_FUTURE|IF_SD},
    /* 3372 */ {I_VFNMADD312SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13826, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3373 */ {I_VFNMADD312SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13833, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3374 */ {I_VFNMADD213SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13840, IF_FMA|IF_FUTURE|IF_SD},
    /* 3375 */ {I_VFNMADD213SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13847, IF_FMA|IF_FUTURE|IF_SD},
    /* 3376 */ {I_VFNMADD213SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13854, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3377 */ {I_VFNMADD213SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13861, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3378 */ {I_VFNMADD123SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13840, IF_FMA|IF_FUTURE|IF_SD},
    /* 3379 */ {I_VFNMADD123SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13847, IF_FMA|IF_FUTURE|IF_SD},
    /* 3380 */ {I_VFNMADD123SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13854, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3381 */ {I_VFNMADD123SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13861, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3382 */ {I_VFNMADD231SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13868, IF_FMA|IF_FUTURE|IF_SD},
    /* 3383 */ {I_VFNMADD231SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13875, IF_FMA|IF_FUTURE|IF_SD},
    /* 3384 */ {I_VFNMADD231SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13882, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3385 */ {I_VFNMADD231SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13889, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3386 */ {I_VFNMADD321SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13868, IF_FMA|IF_FUTURE|IF_SD},
    /* 3387 */ {I_VFNMADD321SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13875, IF_FMA|IF_FUTURE|IF_SD},
    /* 3388 */ {I_VFNMADD321SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13882, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3389 */ {I_VFNMADD321SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13889, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3390 */ {I_VFNMSUB132SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13896, IF_FMA|IF_FUTURE|IF_SD},
    /* 3391 */ {I_VFNMSUB132SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13903, IF_FMA|IF_FUTURE|IF_SD},
    /* 3392 */ {I_VFNMSUB132SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13910, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3393 */ {I_VFNMSUB132SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13917, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3394 */ {I_VFNMSUB312SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13896, IF_FMA|IF_FUTURE|IF_SD},
    /* 3395 */ {I_VFNMSUB312SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13903, IF_FMA|IF_FUTURE|IF_SD},
    /* 3396 */ {I_VFNMSUB312SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13910, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3397 */ {I_VFNMSUB312SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13917, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3398 */ {I_VFNMSUB213SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13924, IF_FMA|IF_FUTURE|IF_SD},
    /* 3399 */ {I_VFNMSUB213SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13931, IF_FMA|IF_FUTURE|IF_SD},
    /* 3400 */ {I_VFNMSUB213SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13938, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3401 */ {I_VFNMSUB213SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13945, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3402 */ {I_VFNMSUB123SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13924, IF_FMA|IF_FUTURE|IF_SD},
    /* 3403 */ {I_VFNMSUB123SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13931, IF_FMA|IF_FUTURE|IF_SD},
    /* 3404 */ {I_VFNMSUB123SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13938, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3405 */ {I_VFNMSUB123SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13945, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3406 */ {I_VFNMSUB231SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13952, IF_FMA|IF_FUTURE|IF_SD},
    /* 3407 */ {I_VFNMSUB231SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13959, IF_FMA|IF_FUTURE|IF_SD},
    /* 3408 */ {I_VFNMSUB231SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13966, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3409 */ {I_VFNMSUB231SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13973, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3410 */ {I_VFNMSUB321SS, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13952, IF_FMA|IF_FUTURE|IF_SD},
    /* 3411 */ {I_VFNMSUB321SS, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13959, IF_FMA|IF_FUTURE|IF_SD},
    /* 3412 */ {I_VFNMSUB321SD, 3, {XMMREG,XMMREG,RM_XMM,0,0}, nasm_bytecodes+13966, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3413 */ {I_VFNMSUB321SD, 2, {XMMREG,RM_XMM,0,0,0}, nasm_bytecodes+13973, IF_FMA|IF_FUTURE|IF_SQ},
    /* 3414 */ {I_XSTORE, 0, {0,0,0,0,0}, nasm_bytecodes+19360, IF_PENT|IF_CYRIX},
    /* 3415 */ {I_XCRYPTECB, 0, {0,0,0,0,0}, nasm_bytecodes+16308, IF_PENT|IF_CYRIX},
    /* 3416 */ {I_XCRYPTCBC, 0, {0,0,0,0,0}, nasm_bytecodes+16314, IF_PENT|IF_CYRIX},
    /* 3417 */ {I_XCRYPTCTR, 0, {0,0,0,0,0}, nasm_bytecodes+16320, IF_PENT|IF_CYRIX},
    /* 3418 */ {I_XCRYPTCFB, 0, {0,0,0,0,0}, nasm_bytecodes+16326, IF_PENT|IF_CYRIX},
    /* 3419 */ {I_XCRYPTOFB, 0, {0,0,0,0,0}, nasm_bytecodes+16332, IF_PENT|IF_CYRIX},
    /* 3420 */ {I_MONTMUL, 0, {0,0,0,0,0}, nasm_bytecodes+16338, IF_PENT|IF_CYRIX},
    /* 3421 */ {I_XSHA1, 0, {0,0,0,0,0}, nasm_bytecodes+16344, IF_PENT|IF_CYRIX},
    /* 3422 */ {I_XSHA256, 0, {0,0,0,0,0}, nasm_bytecodes+16350, IF_PENT|IF_CYRIX},
    /* 3423 */ {I_HINT_NOP0, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16356, IF_P6|IF_UNDOC},
    /* 3424 */ {I_HINT_NOP0, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16362, IF_P6|IF_UNDOC},
    /* 3425 */ {I_HINT_NOP0, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16368, IF_X64|IF_UNDOC},
    /* 3426 */ {I_HINT_NOP1, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16374, IF_P6|IF_UNDOC},
    /* 3427 */ {I_HINT_NOP1, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16380, IF_P6|IF_UNDOC},
    /* 3428 */ {I_HINT_NOP1, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16386, IF_X64|IF_UNDOC},
    /* 3429 */ {I_HINT_NOP2, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16392, IF_P6|IF_UNDOC},
    /* 3430 */ {I_HINT_NOP2, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16398, IF_P6|IF_UNDOC},
    /* 3431 */ {I_HINT_NOP2, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16404, IF_X64|IF_UNDOC},
    /* 3432 */ {I_HINT_NOP3, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16410, IF_P6|IF_UNDOC},
    /* 3433 */ {I_HINT_NOP3, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16416, IF_P6|IF_UNDOC},
    /* 3434 */ {I_HINT_NOP3, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16422, IF_X64|IF_UNDOC},
    /* 3435 */ {I_HINT_NOP4, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16428, IF_P6|IF_UNDOC},
    /* 3436 */ {I_HINT_NOP4, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16434, IF_P6|IF_UNDOC},
    /* 3437 */ {I_HINT_NOP4, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16440, IF_X64|IF_UNDOC},
    /* 3438 */ {I_HINT_NOP5, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16446, IF_P6|IF_UNDOC},
    /* 3439 */ {I_HINT_NOP5, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16452, IF_P6|IF_UNDOC},
    /* 3440 */ {I_HINT_NOP5, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16458, IF_X64|IF_UNDOC},
    /* 3441 */ {I_HINT_NOP6, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16464, IF_P6|IF_UNDOC},
    /* 3442 */ {I_HINT_NOP6, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16470, IF_P6|IF_UNDOC},
    /* 3443 */ {I_HINT_NOP6, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16476, IF_X64|IF_UNDOC},
    /* 3444 */ {I_HINT_NOP7, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16482, IF_P6|IF_UNDOC},
    /* 3445 */ {I_HINT_NOP7, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16488, IF_P6|IF_UNDOC},
    /* 3446 */ {I_HINT_NOP7, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16494, IF_X64|IF_UNDOC},
    /* 3447 */ {I_HINT_NOP8, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16500, IF_P6|IF_UNDOC},
    /* 3448 */ {I_HINT_NOP8, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16506, IF_P6|IF_UNDOC},
    /* 3449 */ {I_HINT_NOP8, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16512, IF_X64|IF_UNDOC},
    /* 3450 */ {I_HINT_NOP9, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16518, IF_P6|IF_UNDOC},
    /* 3451 */ {I_HINT_NOP9, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16524, IF_P6|IF_UNDOC},
    /* 3452 */ {I_HINT_NOP9, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16530, IF_X64|IF_UNDOC},
    /* 3453 */ {I_HINT_NOP10, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16536, IF_P6|IF_UNDOC},
    /* 3454 */ {I_HINT_NOP10, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16542, IF_P6|IF_UNDOC},
    /* 3455 */ {I_HINT_NOP10, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16548, IF_X64|IF_UNDOC},
    /* 3456 */ {I_HINT_NOP11, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16554, IF_P6|IF_UNDOC},
    /* 3457 */ {I_HINT_NOP11, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16560, IF_P6|IF_UNDOC},
    /* 3458 */ {I_HINT_NOP11, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16566, IF_X64|IF_UNDOC},
    /* 3459 */ {I_HINT_NOP12, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16572, IF_P6|IF_UNDOC},
    /* 3460 */ {I_HINT_NOP12, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16578, IF_P6|IF_UNDOC},
    /* 3461 */ {I_HINT_NOP12, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16584, IF_X64|IF_UNDOC},
    /* 3462 */ {I_HINT_NOP13, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16590, IF_P6|IF_UNDOC},
    /* 3463 */ {I_HINT_NOP13, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16596, IF_P6|IF_UNDOC},
    /* 3464 */ {I_HINT_NOP13, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16602, IF_X64|IF_UNDOC},
    /* 3465 */ {I_HINT_NOP14, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16608, IF_P6|IF_UNDOC},
    /* 3466 */ {I_HINT_NOP14, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16614, IF_P6|IF_UNDOC},
    /* 3467 */ {I_HINT_NOP14, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16620, IF_X64|IF_UNDOC},
    /* 3468 */ {I_HINT_NOP15, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16626, IF_P6|IF_UNDOC},
    /* 3469 */ {I_HINT_NOP15, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16632, IF_P6|IF_UNDOC},
    /* 3470 */ {I_HINT_NOP15, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16638, IF_X64|IF_UNDOC},
    /* 3471 */ {I_HINT_NOP16, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16644, IF_P6|IF_UNDOC},
    /* 3472 */ {I_HINT_NOP16, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16650, IF_P6|IF_UNDOC},
    /* 3473 */ {I_HINT_NOP16, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16656, IF_X64|IF_UNDOC},
    /* 3474 */ {I_HINT_NOP17, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16662, IF_P6|IF_UNDOC},
    /* 3475 */ {I_HINT_NOP17, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16668, IF_P6|IF_UNDOC},
    /* 3476 */ {I_HINT_NOP17, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16674, IF_X64|IF_UNDOC},
    /* 3477 */ {I_HINT_NOP18, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16680, IF_P6|IF_UNDOC},
    /* 3478 */ {I_HINT_NOP18, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16686, IF_P6|IF_UNDOC},
    /* 3479 */ {I_HINT_NOP18, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16692, IF_X64|IF_UNDOC},
    /* 3480 */ {I_HINT_NOP19, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16698, IF_P6|IF_UNDOC},
    /* 3481 */ {I_HINT_NOP19, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16704, IF_P6|IF_UNDOC},
    /* 3482 */ {I_HINT_NOP19, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16710, IF_X64|IF_UNDOC},
    /* 3483 */ {I_HINT_NOP20, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16716, IF_P6|IF_UNDOC},
    /* 3484 */ {I_HINT_NOP20, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16722, IF_P6|IF_UNDOC},
    /* 3485 */ {I_HINT_NOP20, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16728, IF_X64|IF_UNDOC},
    /* 3486 */ {I_HINT_NOP21, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16734, IF_P6|IF_UNDOC},
    /* 3487 */ {I_HINT_NOP21, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16740, IF_P6|IF_UNDOC},
    /* 3488 */ {I_HINT_NOP21, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16746, IF_X64|IF_UNDOC},
    /* 3489 */ {I_HINT_NOP22, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16752, IF_P6|IF_UNDOC},
    /* 3490 */ {I_HINT_NOP22, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16758, IF_P6|IF_UNDOC},
    /* 3491 */ {I_HINT_NOP22, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16764, IF_X64|IF_UNDOC},
    /* 3492 */ {I_HINT_NOP23, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16770, IF_P6|IF_UNDOC},
    /* 3493 */ {I_HINT_NOP23, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16776, IF_P6|IF_UNDOC},
    /* 3494 */ {I_HINT_NOP23, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16782, IF_X64|IF_UNDOC},
    /* 3495 */ {I_HINT_NOP24, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16788, IF_P6|IF_UNDOC},
    /* 3496 */ {I_HINT_NOP24, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16794, IF_P6|IF_UNDOC},
    /* 3497 */ {I_HINT_NOP24, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16800, IF_X64|IF_UNDOC},
    /* 3498 */ {I_HINT_NOP25, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16806, IF_P6|IF_UNDOC},
    /* 3499 */ {I_HINT_NOP25, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16812, IF_P6|IF_UNDOC},
    /* 3500 */ {I_HINT_NOP25, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16818, IF_X64|IF_UNDOC},
    /* 3501 */ {I_HINT_NOP26, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16824, IF_P6|IF_UNDOC},
    /* 3502 */ {I_HINT_NOP26, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16830, IF_P6|IF_UNDOC},
    /* 3503 */ {I_HINT_NOP26, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16836, IF_X64|IF_UNDOC},
    /* 3504 */ {I_HINT_NOP27, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16842, IF_P6|IF_UNDOC},
    /* 3505 */ {I_HINT_NOP27, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16848, IF_P6|IF_UNDOC},
    /* 3506 */ {I_HINT_NOP27, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16854, IF_X64|IF_UNDOC},
    /* 3507 */ {I_HINT_NOP28, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16860, IF_P6|IF_UNDOC},
    /* 3508 */ {I_HINT_NOP28, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16866, IF_P6|IF_UNDOC},
    /* 3509 */ {I_HINT_NOP28, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16872, IF_X64|IF_UNDOC},
    /* 3510 */ {I_HINT_NOP29, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16878, IF_P6|IF_UNDOC},
    /* 3511 */ {I_HINT_NOP29, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16884, IF_P6|IF_UNDOC},
    /* 3512 */ {I_HINT_NOP29, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16890, IF_X64|IF_UNDOC},
    /* 3513 */ {I_HINT_NOP30, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16896, IF_P6|IF_UNDOC},
    /* 3514 */ {I_HINT_NOP30, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16902, IF_P6|IF_UNDOC},
    /* 3515 */ {I_HINT_NOP30, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16908, IF_X64|IF_UNDOC},
    /* 3516 */ {I_HINT_NOP31, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16914, IF_P6|IF_UNDOC},
    /* 3517 */ {I_HINT_NOP31, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16920, IF_P6|IF_UNDOC},
    /* 3518 */ {I_HINT_NOP31, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16926, IF_X64|IF_UNDOC},
    /* 3519 */ {I_HINT_NOP32, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16932, IF_P6|IF_UNDOC},
    /* 3520 */ {I_HINT_NOP32, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16938, IF_P6|IF_UNDOC},
    /* 3521 */ {I_HINT_NOP32, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16944, IF_X64|IF_UNDOC},
    /* 3522 */ {I_HINT_NOP33, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16950, IF_P6|IF_UNDOC},
    /* 3523 */ {I_HINT_NOP33, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16956, IF_P6|IF_UNDOC},
    /* 3524 */ {I_HINT_NOP33, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16962, IF_X64|IF_UNDOC},
    /* 3525 */ {I_HINT_NOP34, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16968, IF_P6|IF_UNDOC},
    /* 3526 */ {I_HINT_NOP34, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16974, IF_P6|IF_UNDOC},
    /* 3527 */ {I_HINT_NOP34, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16980, IF_X64|IF_UNDOC},
    /* 3528 */ {I_HINT_NOP35, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+16986, IF_P6|IF_UNDOC},
    /* 3529 */ {I_HINT_NOP35, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+16992, IF_P6|IF_UNDOC},
    /* 3530 */ {I_HINT_NOP35, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+16998, IF_X64|IF_UNDOC},
    /* 3531 */ {I_HINT_NOP36, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17004, IF_P6|IF_UNDOC},
    /* 3532 */ {I_HINT_NOP36, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17010, IF_P6|IF_UNDOC},
    /* 3533 */ {I_HINT_NOP36, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17016, IF_X64|IF_UNDOC},
    /* 3534 */ {I_HINT_NOP37, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17022, IF_P6|IF_UNDOC},
    /* 3535 */ {I_HINT_NOP37, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17028, IF_P6|IF_UNDOC},
    /* 3536 */ {I_HINT_NOP37, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17034, IF_X64|IF_UNDOC},
    /* 3537 */ {I_HINT_NOP38, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17040, IF_P6|IF_UNDOC},
    /* 3538 */ {I_HINT_NOP38, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17046, IF_P6|IF_UNDOC},
    /* 3539 */ {I_HINT_NOP38, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17052, IF_X64|IF_UNDOC},
    /* 3540 */ {I_HINT_NOP39, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17058, IF_P6|IF_UNDOC},
    /* 3541 */ {I_HINT_NOP39, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17064, IF_P6|IF_UNDOC},
    /* 3542 */ {I_HINT_NOP39, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17070, IF_X64|IF_UNDOC},
    /* 3543 */ {I_HINT_NOP40, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17076, IF_P6|IF_UNDOC},
    /* 3544 */ {I_HINT_NOP40, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17082, IF_P6|IF_UNDOC},
    /* 3545 */ {I_HINT_NOP40, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17088, IF_X64|IF_UNDOC},
    /* 3546 */ {I_HINT_NOP41, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17094, IF_P6|IF_UNDOC},
    /* 3547 */ {I_HINT_NOP41, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17100, IF_P6|IF_UNDOC},
    /* 3548 */ {I_HINT_NOP41, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17106, IF_X64|IF_UNDOC},
    /* 3549 */ {I_HINT_NOP42, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17112, IF_P6|IF_UNDOC},
    /* 3550 */ {I_HINT_NOP42, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17118, IF_P6|IF_UNDOC},
    /* 3551 */ {I_HINT_NOP42, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17124, IF_X64|IF_UNDOC},
    /* 3552 */ {I_HINT_NOP43, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17130, IF_P6|IF_UNDOC},
    /* 3553 */ {I_HINT_NOP43, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17136, IF_P6|IF_UNDOC},
    /* 3554 */ {I_HINT_NOP43, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17142, IF_X64|IF_UNDOC},
    /* 3555 */ {I_HINT_NOP44, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17148, IF_P6|IF_UNDOC},
    /* 3556 */ {I_HINT_NOP44, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17154, IF_P6|IF_UNDOC},
    /* 3557 */ {I_HINT_NOP44, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17160, IF_X64|IF_UNDOC},
    /* 3558 */ {I_HINT_NOP45, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17166, IF_P6|IF_UNDOC},
    /* 3559 */ {I_HINT_NOP45, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17172, IF_P6|IF_UNDOC},
    /* 3560 */ {I_HINT_NOP45, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17178, IF_X64|IF_UNDOC},
    /* 3561 */ {I_HINT_NOP46, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17184, IF_P6|IF_UNDOC},
    /* 3562 */ {I_HINT_NOP46, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17190, IF_P6|IF_UNDOC},
    /* 3563 */ {I_HINT_NOP46, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17196, IF_X64|IF_UNDOC},
    /* 3564 */ {I_HINT_NOP47, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17202, IF_P6|IF_UNDOC},
    /* 3565 */ {I_HINT_NOP47, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17208, IF_P6|IF_UNDOC},
    /* 3566 */ {I_HINT_NOP47, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17214, IF_X64|IF_UNDOC},
    /* 3567 */ {I_HINT_NOP48, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17220, IF_P6|IF_UNDOC},
    /* 3568 */ {I_HINT_NOP48, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17226, IF_P6|IF_UNDOC},
    /* 3569 */ {I_HINT_NOP48, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17232, IF_X64|IF_UNDOC},
    /* 3570 */ {I_HINT_NOP49, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17238, IF_P6|IF_UNDOC},
    /* 3571 */ {I_HINT_NOP49, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17244, IF_P6|IF_UNDOC},
    /* 3572 */ {I_HINT_NOP49, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17250, IF_X64|IF_UNDOC},
    /* 3573 */ {I_HINT_NOP50, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17256, IF_P6|IF_UNDOC},
    /* 3574 */ {I_HINT_NOP50, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17262, IF_P6|IF_UNDOC},
    /* 3575 */ {I_HINT_NOP50, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17268, IF_X64|IF_UNDOC},
    /* 3576 */ {I_HINT_NOP51, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17274, IF_P6|IF_UNDOC},
    /* 3577 */ {I_HINT_NOP51, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17280, IF_P6|IF_UNDOC},
    /* 3578 */ {I_HINT_NOP51, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17286, IF_X64|IF_UNDOC},
    /* 3579 */ {I_HINT_NOP52, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17292, IF_P6|IF_UNDOC},
    /* 3580 */ {I_HINT_NOP52, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17298, IF_P6|IF_UNDOC},
    /* 3581 */ {I_HINT_NOP52, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17304, IF_X64|IF_UNDOC},
    /* 3582 */ {I_HINT_NOP53, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17310, IF_P6|IF_UNDOC},
    /* 3583 */ {I_HINT_NOP53, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17316, IF_P6|IF_UNDOC},
    /* 3584 */ {I_HINT_NOP53, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17322, IF_X64|IF_UNDOC},
    /* 3585 */ {I_HINT_NOP54, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17328, IF_P6|IF_UNDOC},
    /* 3586 */ {I_HINT_NOP54, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17334, IF_P6|IF_UNDOC},
    /* 3587 */ {I_HINT_NOP54, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17340, IF_X64|IF_UNDOC},
    /* 3588 */ {I_HINT_NOP55, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17346, IF_P6|IF_UNDOC},
    /* 3589 */ {I_HINT_NOP55, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17352, IF_P6|IF_UNDOC},
    /* 3590 */ {I_HINT_NOP55, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17358, IF_X64|IF_UNDOC},
    /* 3591 */ {I_HINT_NOP56, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+14718, IF_P6|IF_UNDOC},
    /* 3592 */ {I_HINT_NOP56, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+14724, IF_P6|IF_UNDOC},
    /* 3593 */ {I_HINT_NOP56, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+14730, IF_X64|IF_UNDOC},
    /* 3594 */ {I_HINT_NOP57, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17364, IF_P6|IF_UNDOC},
    /* 3595 */ {I_HINT_NOP57, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17370, IF_P6|IF_UNDOC},
    /* 3596 */ {I_HINT_NOP57, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17376, IF_X64|IF_UNDOC},
    /* 3597 */ {I_HINT_NOP58, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17382, IF_P6|IF_UNDOC},
    /* 3598 */ {I_HINT_NOP58, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17388, IF_P6|IF_UNDOC},
    /* 3599 */ {I_HINT_NOP58, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17394, IF_X64|IF_UNDOC},
    /* 3600 */ {I_HINT_NOP59, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17400, IF_P6|IF_UNDOC},
    /* 3601 */ {I_HINT_NOP59, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17406, IF_P6|IF_UNDOC},
    /* 3602 */ {I_HINT_NOP59, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17412, IF_X64|IF_UNDOC},
    /* 3603 */ {I_HINT_NOP60, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17418, IF_P6|IF_UNDOC},
    /* 3604 */ {I_HINT_NOP60, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17424, IF_P6|IF_UNDOC},
    /* 3605 */ {I_HINT_NOP60, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17430, IF_X64|IF_UNDOC},
    /* 3606 */ {I_HINT_NOP61, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17436, IF_P6|IF_UNDOC},
    /* 3607 */ {I_HINT_NOP61, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17442, IF_P6|IF_UNDOC},
    /* 3608 */ {I_HINT_NOP61, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17448, IF_X64|IF_UNDOC},
    /* 3609 */ {I_HINT_NOP62, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17454, IF_P6|IF_UNDOC},
    /* 3610 */ {I_HINT_NOP62, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17460, IF_P6|IF_UNDOC},
    /* 3611 */ {I_HINT_NOP62, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17466, IF_X64|IF_UNDOC},
    /* 3612 */ {I_HINT_NOP63, 1, {RM_GPR|BITS16,0,0,0,0}, nasm_bytecodes+17472, IF_P6|IF_UNDOC},
    /* 3613 */ {I_HINT_NOP63, 1, {RM_GPR|BITS32,0,0,0,0}, nasm_bytecodes+17478, IF_P6|IF_UNDOC},
    /* 3614 */ {I_HINT_NOP63, 1, {RM_GPR|BITS64,0,0,0,0}, nasm_bytecodes+17484, IF_X64|IF_UNDOC},
};

static const struct itemplate * const itable_00[] = {
    instrux + 40,
    instrux + 41,
};

static const struct itemplate * const itable_01[] = {
    instrux + 42,
    instrux + 43,
    instrux + 44,
    instrux + 45,
    instrux + 46,
    instrux + 47,
};

static const struct itemplate * const itable_02[] = {
    instrux + 48,
    instrux + 49,
};

static const struct itemplate * const itable_03[] = {
    instrux + 50,
    instrux + 51,
    instrux + 52,
    instrux + 53,
    instrux + 54,
    instrux + 55,
};

static const struct itemplate * const itable_04[] = {
    instrux + 59,
};

static const struct itemplate * const itable_05[] = {
    instrux + 61,
    instrux + 63,
    instrux + 65,
};

static const struct itemplate * const itable_06[] = {
    instrux + 866,
    instrux + 867,
};

static const struct itemplate * const itable_07[] = {
    instrux + 818,
};

static const struct itemplate * const itable_08[] = {
    instrux + 718,
    instrux + 719,
};

static const struct itemplate * const itable_09[] = {
    instrux + 720,
    instrux + 721,
    instrux + 722,
    instrux + 723,
    instrux + 724,
    instrux + 725,
};

static const struct itemplate * const itable_0A[] = {
    instrux + 726,
    instrux + 727,
};

static const struct itemplate * const itable_0B[] = {
    instrux + 728,
    instrux + 729,
    instrux + 730,
    instrux + 731,
    instrux + 732,
    instrux + 733,
};

static const struct itemplate * const itable_0C[] = {
    instrux + 737,
};

static const struct itemplate * const itable_0D[] = {
    instrux + 739,
    instrux + 741,
    instrux + 743,
};

static const struct itemplate * const itable_0E[] = {
    instrux + 866,
    instrux + 867,
};

static const struct itemplate * const itable_0F00[] = {
    instrux + 544,
    instrux + 545,
    instrux + 573,
    instrux + 574,
    instrux + 575,
    instrux + 617,
    instrux + 618,
    instrux + 619,
    instrux + 1047,
    instrux + 1048,
    instrux + 1049,
    instrux + 1050,
    instrux + 1051,
    instrux + 1066,
    instrux + 1067,
    instrux + 1068,
    instrux + 1069,
    instrux + 1070,
    instrux + 1137,
    instrux + 1138,
    instrux + 1139,
    instrux + 1140,
    instrux + 1141,
    instrux + 1142,
};

static const struct itemplate * const itable_0F01[] = {
    instrux + 192,
    instrux + 505,
    instrux + 506,
    instrux + 507,
    instrux + 508,
    instrux + 509,
    instrux + 569,
    instrux + 572,
    instrux + 576,
    instrux + 577,
    instrux + 578,
    instrux + 621,
    instrux + 705,
    instrux + 910,
    instrux + 997,
    instrux + 1046,
    instrux + 1052,
    instrux + 1054,
    instrux + 1055,
    instrux + 1056,
    instrux + 1057,
    instrux + 1060,
    instrux + 1106,
    instrux + 1309,
    instrux + 1310,
    instrux + 1539,
    instrux + 1541,
    instrux + 1542,
    instrux + 1543,
    instrux + 1548,
    instrux + 1549,
    instrux + 1550,
    instrux + 1553,
};

static const struct itemplate * const itable_0F02[] = {
    instrux + 548,
    instrux + 549,
    instrux + 550,
    instrux + 551,
    instrux + 552,
    instrux + 553,
    instrux + 554,
    instrux + 555,
    instrux + 556,
    instrux + 557,
};

static const struct itemplate * const itable_0F03[] = {
    instrux + 605,
    instrux + 606,
    instrux + 607,
    instrux + 608,
    instrux + 609,
    instrux + 610,
    instrux + 611,
    instrux + 612,
    instrux + 613,
    instrux + 614,
};

static const struct itemplate * const itable_0F05[] = {
    instrux + 580,
    instrux + 1107,
};

static const struct itemplate * const itable_0F06[] = {
    instrux + 194,
};

static const struct itemplate * const itable_0F07[] = {
    instrux + 579,
    instrux + 1110,
};

static const struct itemplate * const itable_0F08[] = {
    instrux + 504,
};

static const struct itemplate * const itable_0F09[] = {
    instrux + 1144,
};

static const struct itemplate * const itable_0F0B[] = {
    instrux + 1136,
};

static const struct itemplate * const itable_0F0D[] = {
    instrux + 828,
    instrux + 829,
};

static const struct itemplate * const itable_0F0E[] = {
    instrux + 330,
};

static const struct itemplate * const itable_0F0F[] = {
    instrux + 775,
    instrux + 783,
    instrux + 784,
    instrux + 785,
    instrux + 786,
    instrux + 787,
    instrux + 788,
    instrux + 789,
    instrux + 790,
    instrux + 791,
    instrux + 792,
    instrux + 793,
    instrux + 794,
    instrux + 795,
    instrux + 796,
    instrux + 797,
    instrux + 798,
    instrux + 799,
    instrux + 804,
    instrux + 1333,
    instrux + 1334,
    instrux + 1335,
    instrux + 1336,
    instrux + 1337,
    instrux + 1954,
    instrux + 1955,
};

static const struct itemplate * const itable_0F10[] = {
    instrux + 1281,
    instrux + 1283,
    instrux + 1285,
    instrux + 1287,
    instrux + 1508,
    instrux + 1511,
    instrux + 1512,
    instrux + 1515,
};

static const struct itemplate * const itable_0F11[] = {
    instrux + 1282,
    instrux + 1284,
    instrux + 1286,
    instrux + 1288,
    instrux + 1509,
    instrux + 1510,
    instrux + 1513,
    instrux + 1514,
};

static const struct itemplate * const itable_0F12[] = {
    instrux + 1275,
    instrux + 1277,
    instrux + 1505,
    instrux + 1536,
    instrux + 1538,
};

static const struct itemplate * const itable_0F13[] = {
    instrux + 1276,
    instrux + 1504,
};

static const struct itemplate * const itable_0F14[] = {
    instrux + 1305,
    instrux + 1527,
};

static const struct itemplate * const itable_0F15[] = {
    instrux + 1304,
    instrux + 1526,
};

static const struct itemplate * const itable_0F16[] = {
    instrux + 1272,
    instrux + 1274,
    instrux + 1503,
    instrux + 1537,
};

static const struct itemplate * const itable_0F17[] = {
    instrux + 1273,
    instrux + 1502,
};

static const struct itemplate * const itable_0F18[] = {
    instrux + 1313,
    instrux + 1314,
    instrux + 1315,
    instrux + 1316,
    instrux + 3423,
    instrux + 3424,
    instrux + 3425,
    instrux + 3426,
    instrux + 3427,
    instrux + 3428,
    instrux + 3429,
    instrux + 3430,
    instrux + 3431,
    instrux + 3432,
    instrux + 3433,
    instrux + 3434,
    instrux + 3435,
    instrux + 3436,
    instrux + 3437,
    instrux + 3438,
    instrux + 3439,
    instrux + 3440,
    instrux + 3441,
    instrux + 3442,
    instrux + 3443,
    instrux + 3444,
    instrux + 3445,
    instrux + 3446,
};

static const struct itemplate * const itable_0F19[] = {
    instrux + 3447,
    instrux + 3448,
    instrux + 3449,
    instrux + 3450,
    instrux + 3451,
    instrux + 3452,
    instrux + 3453,
    instrux + 3454,
    instrux + 3455,
    instrux + 3456,
    instrux + 3457,
    instrux + 3458,
    instrux + 3459,
    instrux + 3460,
    instrux + 3461,
    instrux + 3462,
    instrux + 3463,
    instrux + 3464,
    instrux + 3465,
    instrux + 3466,
    instrux + 3467,
    instrux + 3468,
    instrux + 3469,
    instrux + 3470,
};

static const struct itemplate * const itable_0F1A[] = {
    instrux + 3471,
    instrux + 3472,
    instrux + 3473,
    instrux + 3474,
    instrux + 3475,
    instrux + 3476,
    instrux + 3477,
    instrux + 3478,
    instrux + 3479,
    instrux + 3480,
    instrux + 3481,
    instrux + 3482,
    instrux + 3483,
    instrux + 3484,
    instrux + 3485,
    instrux + 3486,
    instrux + 3487,
    instrux + 3488,
    instrux + 3489,
    instrux + 3490,
    instrux + 3491,
    instrux + 3492,
    instrux + 3493,
    instrux + 3494,
};

static const struct itemplate * const itable_0F1B[] = {
    instrux + 3495,
    instrux + 3496,
    instrux + 3497,
    instrux + 3498,
    instrux + 3499,
    instrux + 3500,
    instrux + 3501,
    instrux + 3502,
    instrux + 3503,
    instrux + 3504,
    instrux + 3505,
    instrux + 3506,
    instrux + 3507,
    instrux + 3508,
    instrux + 3509,
    instrux + 3510,
    instrux + 3511,
    instrux + 3512,
    instrux + 3513,
    instrux + 3514,
    instrux + 3515,
    instrux + 3516,
    instrux + 3517,
    instrux + 3518,
};

static const struct itemplate * const itable_0F1C[] = {
    instrux + 3519,
    instrux + 3520,
    instrux + 3521,
    instrux + 3522,
    instrux + 3523,
    instrux + 3524,
    instrux + 3525,
    instrux + 3526,
    instrux + 3527,
    instrux + 3528,
    instrux + 3529,
    instrux + 3530,
    instrux + 3531,
    instrux + 3532,
    instrux + 3533,
    instrux + 3534,
    instrux + 3535,
    instrux + 3536,
    instrux + 3537,
    instrux + 3538,
    instrux + 3539,
    instrux + 3540,
    instrux + 3541,
    instrux + 3542,
};

static const struct itemplate * const itable_0F1D[] = {
    instrux + 3543,
    instrux + 3544,
    instrux + 3545,
    instrux + 3546,
    instrux + 3547,
    instrux + 3548,
    instrux + 3549,
    instrux + 3550,
    instrux + 3551,
    instrux + 3552,
    instrux + 3553,
    instrux + 3554,
    instrux + 3555,
    instrux + 3556,
    instrux + 3557,
    instrux + 3558,
    instrux + 3559,
    instrux + 3560,
    instrux + 3561,
    instrux + 3562,
    instrux + 3563,
    instrux + 3564,
    instrux + 3565,
    instrux + 3566,
};

static const struct itemplate * const itable_0F1E[] = {
    instrux + 3567,
    instrux + 3568,
    instrux + 3569,
    instrux + 3570,
    instrux + 3571,
    instrux + 3572,
    instrux + 3573,
    instrux + 3574,
    instrux + 3575,
    instrux + 3576,
    instrux + 3577,
    instrux + 3578,
    instrux + 3579,
    instrux + 3580,
    instrux + 3581,
    instrux + 3582,
    instrux + 3583,
    instrux + 3584,
    instrux + 3585,
    instrux + 3586,
    instrux + 3587,
    instrux + 3588,
    instrux + 3589,
    instrux + 3590,
};

static const struct itemplate * const itable_0F1F[] = {
    instrux + 711,
    instrux + 712,
    instrux + 713,
    instrux + 3591,
    instrux + 3592,
    instrux + 3593,
    instrux + 3594,
    instrux + 3595,
    instrux + 3596,
    instrux + 3597,
    instrux + 3598,
    instrux + 3599,
    instrux + 3600,
    instrux + 3601,
    instrux + 3602,
    instrux + 3603,
    instrux + 3604,
    instrux + 3605,
    instrux + 3606,
    instrux + 3607,
    instrux + 3608,
    instrux + 3609,
    instrux + 3610,
    instrux + 3611,
    instrux + 3612,
    instrux + 3613,
    instrux + 3614,
};

static const struct itemplate * const itable_0F20[] = {
    instrux + 636,
    instrux + 637,
};

static const struct itemplate * const itable_0F21[] = {
    instrux + 640,
    instrux + 641,
};

static const struct itemplate * const itable_0F22[] = {
    instrux + 638,
    instrux + 639,
};

static const struct itemplate * const itable_0F23[] = {
    instrux + 642,
    instrux + 643,
};

static const struct itemplate * const itable_0F2400[] = {
    instrux + 1668,
    instrux + 1669,
};

static const struct itemplate * const itable_0F2401[] = {
    instrux + 1672,
    instrux + 1673,
};

static const struct itemplate * const itable_0F2402[] = {
    instrux + 1676,
    instrux + 1677,
};

static const struct itemplate * const itable_0F2403[] = {
    instrux + 1680,
    instrux + 1681,
};

static const struct itemplate * const itable_0F2404[] = {
    instrux + 1670,
    instrux + 1671,
};

static const struct itemplate * const itable_0F2405[] = {
    instrux + 1674,
    instrux + 1675,
};

static const struct itemplate * const itable_0F2406[] = {
    instrux + 1678,
    instrux + 1679,
};

static const struct itemplate * const itable_0F2407[] = {
    instrux + 1682,
    instrux + 1683,
};

static const struct itemplate * const itable_0F2408[] = {
    instrux + 1684,
    instrux + 1685,
};

static const struct itemplate * const itable_0F2409[] = {
    instrux + 1688,
    instrux + 1689,
};

static const struct itemplate * const itable_0F240A[] = {
    instrux + 1692,
    instrux + 1693,
};

static const struct itemplate * const itable_0F240B[] = {
    instrux + 1696,
    instrux + 1697,
};

static const struct itemplate * const itable_0F240C[] = {
    instrux + 1686,
    instrux + 1687,
};

static const struct itemplate * const itable_0F240D[] = {
    instrux + 1690,
    instrux + 1691,
};

static const struct itemplate * const itable_0F240E[] = {
    instrux + 1694,
    instrux + 1695,
};

static const struct itemplate * const itable_0F240F[] = {
    instrux + 1698,
    instrux + 1699,
};

static const struct itemplate * const itable_0F2410[] = {
    instrux + 1700,
    instrux + 1701,
};

static const struct itemplate * const itable_0F2411[] = {
    instrux + 1704,
    instrux + 1705,
};

static const struct itemplate * const itable_0F2412[] = {
    instrux + 1708,
    instrux + 1709,
};

static const struct itemplate * const itable_0F2413[] = {
    instrux + 1712,
    instrux + 1713,
};

static const struct itemplate * const itable_0F2414[] = {
    instrux + 1702,
    instrux + 1703,
};

static const struct itemplate * const itable_0F2415[] = {
    instrux + 1706,
    instrux + 1707,
};

static const struct itemplate * const itable_0F2416[] = {
    instrux + 1710,
    instrux + 1711,
};

static const struct itemplate * const itable_0F2417[] = {
    instrux + 1714,
    instrux + 1715,
};

static const struct itemplate * const itable_0F2418[] = {
    instrux + 1716,
    instrux + 1717,
};

static const struct itemplate * const itable_0F2419[] = {
    instrux + 1720,
    instrux + 1721,
};

static const struct itemplate * const itable_0F241A[] = {
    instrux + 1724,
    instrux + 1725,
};

static const struct itemplate * const itable_0F241B[] = {
    instrux + 1728,
    instrux + 1729,
};

static const struct itemplate * const itable_0F241C[] = {
    instrux + 1718,
    instrux + 1719,
};

static const struct itemplate * const itable_0F241D[] = {
    instrux + 1722,
    instrux + 1723,
};

static const struct itemplate * const itable_0F241E[] = {
    instrux + 1726,
    instrux + 1727,
};

static const struct itemplate * const itable_0F241F[] = {
    instrux + 1730,
    instrux + 1731,
};

static const struct itemplate * const itable_0F2420[] = {
    instrux + 1872,
    instrux + 1873,
};

static const struct itemplate * const itable_0F2421[] = {
    instrux + 1876,
    instrux + 1877,
};

static const struct itemplate * const itable_0F2422[] = {
    instrux + 1880,
    instrux + 1881,
};

static const struct itemplate * const itable_0F2423[] = {
    instrux + 1884,
    instrux + 1885,
};

static const struct itemplate * const itable_0F2424[] = {
    instrux + 1874,
    instrux + 1875,
};

static const struct itemplate * const itable_0F2425[] = {
    instrux + 1878,
    instrux + 1879,
};

static const struct itemplate * const itable_0F2426[] = {
    instrux + 1882,
    instrux + 1883,
};

static const struct itemplate * const itable_0F2427[] = {
    instrux + 1886,
    instrux + 1887,
};

static const struct itemplate * const itable_0F2440[] = {
    instrux + 1900,
    instrux + 1901,
};

static const struct itemplate * const itable_0F2441[] = {
    instrux + 1902,
    instrux + 1903,
};

static const struct itemplate * const itable_0F2442[] = {
    instrux + 1904,
    instrux + 1905,
};

static const struct itemplate * const itable_0F2443[] = {
    instrux + 1906,
    instrux + 1907,
};

static const struct itemplate * const itable_0F2444[] = {
    instrux + 1908,
    instrux + 1909,
};

static const struct itemplate * const itable_0F2445[] = {
    instrux + 1910,
    instrux + 1911,
};

static const struct itemplate * const itable_0F2446[] = {
    instrux + 1912,
    instrux + 1913,
};

static const struct itemplate * const itable_0F2447[] = {
    instrux + 1914,
    instrux + 1915,
};

static const struct itemplate * const itable_0F2448[] = {
    instrux + 1916,
    instrux + 1917,
};

static const struct itemplate * const itable_0F2449[] = {
    instrux + 1918,
    instrux + 1919,
};

static const struct itemplate * const itable_0F244A[] = {
    instrux + 1920,
    instrux + 1921,
};

static const struct itemplate * const itable_0F244B[] = {
    instrux + 1922,
    instrux + 1923,
};

static const struct itemplate * const itable_0F2485[] = {
    instrux + 1888,
};

static const struct itemplate * const itable_0F2486[] = {
    instrux + 1890,
};

static const struct itemplate * const itable_0F2487[] = {
    instrux + 1894,
};

static const struct itemplate * const itable_0F248E[] = {
    instrux + 1892,
};

static const struct itemplate * const itable_0F248F[] = {
    instrux + 1896,
};

static const struct itemplate * const itable_0F2495[] = {
    instrux + 1889,
};

static const struct itemplate * const itable_0F2496[] = {
    instrux + 1891,
};

static const struct itemplate * const itable_0F2497[] = {
    instrux + 1895,
};

static const struct itemplate * const itable_0F249E[] = {
    instrux + 1893,
};

static const struct itemplate * const itable_0F249F[] = {
    instrux + 1897,
};

static const struct itemplate * const itable_0F24A6[] = {
    instrux + 1898,
};

static const struct itemplate * const itable_0F24B6[] = {
    instrux + 1899,
};

static const struct itemplate * const itable_0F252C[] = {
    instrux + 1732,
    instrux + 1733,
    instrux + 1734,
    instrux + 1735,
    instrux + 1736,
    instrux + 1737,
    instrux + 1738,
    instrux + 1739,
    instrux + 1740,
    instrux + 1741,
    instrux + 1742,
    instrux + 1743,
    instrux + 1744,
    instrux + 1745,
    instrux + 1746,
    instrux + 1747,
    instrux + 1748,
};

static const struct itemplate * const itable_0F252D[] = {
    instrux + 1749,
    instrux + 1750,
    instrux + 1751,
    instrux + 1752,
    instrux + 1753,
    instrux + 1754,
    instrux + 1755,
    instrux + 1756,
    instrux + 1757,
    instrux + 1758,
    instrux + 1759,
    instrux + 1760,
    instrux + 1761,
    instrux + 1762,
    instrux + 1763,
    instrux + 1764,
    instrux + 1765,
};

static const struct itemplate * const itable_0F252E[] = {
    instrux + 1766,
    instrux + 1767,
    instrux + 1768,
    instrux + 1769,
    instrux + 1770,
    instrux + 1771,
    instrux + 1772,
    instrux + 1773,
    instrux + 1774,
    instrux + 1775,
    instrux + 1776,
    instrux + 1777,
    instrux + 1778,
    instrux + 1779,
    instrux + 1780,
    instrux + 1781,
    instrux + 1782,
};

static const struct itemplate * const itable_0F252F[] = {
    instrux + 1783,
    instrux + 1784,
    instrux + 1785,
    instrux + 1786,
    instrux + 1787,
    instrux + 1788,
    instrux + 1789,
    instrux + 1790,
    instrux + 1791,
    instrux + 1792,
    instrux + 1793,
    instrux + 1794,
    instrux + 1795,
    instrux + 1796,
    instrux + 1797,
    instrux + 1798,
    instrux + 1799,
};

static const struct itemplate * const itable_0F254C[] = {
    instrux + 1800,
    instrux + 1801,
    instrux + 1802,
    instrux + 1803,
    instrux + 1804,
    instrux + 1805,
    instrux + 1806,
    instrux + 1807,
    instrux + 1808,
};

static const struct itemplate * const itable_0F254D[] = {
    instrux + 1809,
    instrux + 1810,
    instrux + 1811,
    instrux + 1812,
    instrux + 1813,
    instrux + 1814,
    instrux + 1815,
    instrux + 1816,
    instrux + 1817,
};

static const struct itemplate * const itable_0F254E[] = {
    instrux + 1818,
    instrux + 1819,
    instrux + 1820,
    instrux + 1821,
    instrux + 1822,
    instrux + 1823,
    instrux + 1824,
    instrux + 1825,
    instrux + 1826,
};

static const struct itemplate * const itable_0F254F[] = {
    instrux + 1827,
    instrux + 1828,
    instrux + 1829,
    instrux + 1830,
    instrux + 1831,
    instrux + 1832,
    instrux + 1833,
    instrux + 1834,
    instrux + 1835,
};

static const struct itemplate * const itable_0F256C[] = {
    instrux + 1836,
    instrux + 1837,
    instrux + 1838,
    instrux + 1839,
    instrux + 1840,
    instrux + 1841,
    instrux + 1842,
    instrux + 1843,
    instrux + 1844,
};

static const struct itemplate * const itable_0F256D[] = {
    instrux + 1845,
    instrux + 1846,
    instrux + 1847,
    instrux + 1848,
    instrux + 1849,
    instrux + 1850,
    instrux + 1851,
    instrux + 1852,
    instrux + 1853,
};

static const struct itemplate * const itable_0F256E[] = {
    instrux + 1854,
    instrux + 1855,
    instrux + 1856,
    instrux + 1857,
    instrux + 1858,
    instrux + 1859,
    instrux + 1860,
    instrux + 1861,
    instrux + 1862,
};

static const struct itemplate * const itable_0F256F[] = {
    instrux + 1863,
    instrux + 1864,
    instrux + 1865,
    instrux + 1866,
    instrux + 1867,
    instrux + 1868,
    instrux + 1869,
    instrux + 1870,
    instrux + 1871,
};

static const struct itemplate * const itable_0F28[] = {
    instrux + 1268,
    instrux + 1270,
    instrux + 1498,
    instrux + 1501,
};

static const struct itemplate * const itable_0F29[] = {
    instrux + 1269,
    instrux + 1271,
    instrux + 1499,
    instrux + 1500,
};

static const struct itemplate * const itable_0F2A[] = {
    instrux + 1250,
    instrux + 1252,
    instrux + 1253,
    instrux + 1474,
    instrux + 1482,
    instrux + 1483,
};

static const struct itemplate * const itable_0F2B[] = {
    instrux + 1280,
    instrux + 1343,
    instrux + 1595,
    instrux + 1596,
};

static const struct itemplate * const itable_0F2C[] = {
    instrux + 1258,
    instrux + 1259,
    instrux + 1260,
    instrux + 1485,
    instrux + 1488,
    instrux + 1489,
    instrux + 1490,
    instrux + 1491,
};

static const struct itemplate * const itable_0F2D[] = {
    instrux + 1251,
    instrux + 1254,
    instrux + 1255,
    instrux + 1256,
    instrux + 1257,
    instrux + 1472,
    instrux + 1477,
    instrux + 1478,
    instrux + 1479,
    instrux + 1480,
};

static const struct itemplate * const itable_0F2E[] = {
    instrux + 1303,
    instrux + 1525,
};

static const struct itemplate * const itable_0F2F[] = {
    instrux + 1249,
    instrux + 1468,
};

static const struct itemplate * const itable_0F30[] = {
    instrux + 1146,
};

static const struct itemplate * const itable_0F31[] = {
    instrux + 909,
};

static const struct itemplate * const itable_0F32[] = {
    instrux + 907,
};

static const struct itemplate * const itable_0F33[] = {
    instrux + 908,
};

static const struct itemplate * const itable_0F34[] = {
    instrux + 1108,
};

static const struct itemplate * const itable_0F35[] = {
    instrux + 1109,
};

static const struct itemplate * const itable_0F36[] = {
    instrux + 906,
};

static const struct itemplate * const itable_0F37[] = {
    instrux + 1145,
    instrux + 1953,
};

static const struct itemplate * const itable_0F3800[] = {
    instrux + 1583,
    instrux + 1584,
};

static const struct itemplate * const itable_0F3801[] = {
    instrux + 1567,
    instrux + 1568,
};

static const struct itemplate * const itable_0F3802[] = {
    instrux + 1569,
    instrux + 1570,
};

static const struct itemplate * const itable_0F3803[] = {
    instrux + 1571,
    instrux + 1572,
};

static const struct itemplate * const itable_0F3804[] = {
    instrux + 1579,
    instrux + 1580,
};

static const struct itemplate * const itable_0F3805[] = {
    instrux + 1573,
    instrux + 1574,
};

static const struct itemplate * const itable_0F3806[] = {
    instrux + 1575,
    instrux + 1576,
};

static const struct itemplate * const itable_0F3807[] = {
    instrux + 1577,
    instrux + 1578,
};

static const struct itemplate * const itable_0F3808[] = {
    instrux + 1585,
    instrux + 1586,
};

static const struct itemplate * const itable_0F3809[] = {
    instrux + 1587,
    instrux + 1588,
};

static const struct itemplate * const itable_0F380A[] = {
    instrux + 1589,
    instrux + 1590,
};

static const struct itemplate * const itable_0F380B[] = {
    instrux + 1581,
    instrux + 1582,
};

static const struct itemplate * const itable_0F3810[] = {
    instrux + 1612,
};

static const struct itemplate * const itable_0F3814[] = {
    instrux + 1603,
};

static const struct itemplate * const itable_0F3815[] = {
    instrux + 1602,
};

static const struct itemplate * const itable_0F3817[] = {
    instrux + 1650,
};

static const struct itemplate * const itable_0F381C[] = {
    instrux + 1559,
    instrux + 1560,
};

static const struct itemplate * const itable_0F381D[] = {
    instrux + 1561,
    instrux + 1562,
};

static const struct itemplate * const itable_0F381E[] = {
    instrux + 1563,
    instrux + 1564,
};

static const struct itemplate * const itable_0F3820[] = {
    instrux + 1636,
};

static const struct itemplate * const itable_0F3821[] = {
    instrux + 1637,
};

static const struct itemplate * const itable_0F3822[] = {
    instrux + 1638,
};

static const struct itemplate * const itable_0F3823[] = {
    instrux + 1639,
};

static const struct itemplate * const itable_0F3824[] = {
    instrux + 1640,
};

static const struct itemplate * const itable_0F3825[] = {
    instrux + 1641,
};

static const struct itemplate * const itable_0F3828[] = {
    instrux + 1648,
};

static const struct itemplate * const itable_0F3829[] = {
    instrux + 1614,
};

static const struct itemplate * const itable_0F382A[] = {
    instrux + 1609,
};

static const struct itemplate * const itable_0F382B[] = {
    instrux + 1611,
};

static const struct itemplate * const itable_0F3830[] = {
    instrux + 1642,
};

static const struct itemplate * const itable_0F3831[] = {
    instrux + 1643,
};

static const struct itemplate * const itable_0F3832[] = {
    instrux + 1644,
};

static const struct itemplate * const itable_0F3833[] = {
    instrux + 1645,
};

static const struct itemplate * const itable_0F3834[] = {
    instrux + 1646,
};

static const struct itemplate * const itable_0F3835[] = {
    instrux + 1647,
};

static const struct itemplate * const itable_0F3837[] = {
    instrux + 1664,
};

static const struct itemplate * const itable_0F3838[] = {
    instrux + 1632,
};

static const struct itemplate * const itable_0F3839[] = {
    instrux + 1633,
};

static const struct itemplate * const itable_0F383A[] = {
    instrux + 1635,
};

static const struct itemplate * const itable_0F383B[] = {
    instrux + 1634,
};

static const struct itemplate * const itable_0F383C[] = {
    instrux + 1628,
};

static const struct itemplate * const itable_0F383D[] = {
    instrux + 1629,
};

static const struct itemplate * const itable_0F383E[] = {
    instrux + 1631,
};

static const struct itemplate * const itable_0F383F[] = {
    instrux + 1630,
};

static const struct itemplate * const itable_0F3840[] = {
    instrux + 1649,
};

static const struct itemplate * const itable_0F3841[] = {
    instrux + 1623,
};

static const struct itemplate * const itable_0F3880[] = {
    instrux + 1555,
    instrux + 1556,
};

static const struct itemplate * const itable_0F3881[] = {
    instrux + 1557,
    instrux + 1558,
};

static const struct itemplate * const itable_0F38DB[] = {
    instrux + 1966,
};

static const struct itemplate * const itable_0F38DC[] = {
    instrux + 1962,
};

static const struct itemplate * const itable_0F38DD[] = {
    instrux + 1963,
};

static const struct itemplate * const itable_0F38DE[] = {
    instrux + 1964,
};

static const struct itemplate * const itable_0F38DF[] = {
    instrux + 1965,
};

static const struct itemplate * const itable_0F38F0[] = {
    instrux + 1655,
    instrux + 1658,
    instrux + 1956,
    instrux + 1957,
    instrux + 1958,
};

static const struct itemplate * const itable_0F38F1[] = {
    instrux + 1656,
    instrux + 1657,
    instrux + 1659,
    instrux + 1959,
    instrux + 1960,
    instrux + 1961,
};

static const struct itemplate * const itable_0F39[] = {
    instrux + 261,
};

static const struct itemplate * const itable_0F3A08[] = {
    instrux + 1652,
    instrux + 1949,
    instrux + 1950,
    instrux + 1951,
    instrux + 1952,
};

static const struct itemplate * const itable_0F3A09[] = {
    instrux + 1651,
};

static const struct itemplate * const itable_0F3A0A[] = {
    instrux + 1654,
};

static const struct itemplate * const itable_0F3A0B[] = {
    instrux + 1653,
};

static const struct itemplate * const itable_0F3A0C[] = {
    instrux + 1601,
};

static const struct itemplate * const itable_0F3A0D[] = {
    instrux + 1600,
};

static const struct itemplate * const itable_0F3A0E[] = {
    instrux + 1613,
};

static const struct itemplate * const itable_0F3A0F[] = {
    instrux + 1565,
    instrux + 1566,
};

static const struct itemplate * const itable_0F3A14[] = {
    instrux + 1615,
    instrux + 1616,
    instrux + 1617,
};

static const struct itemplate * const itable_0F3A15[] = {
    instrux + 1620,
    instrux + 1621,
    instrux + 1622,
};

static const struct itemplate * const itable_0F3A16[] = {
    instrux + 1618,
    instrux + 1619,
};

static const struct itemplate * const itable_0F3A17[] = {
    instrux + 1606,
    instrux + 1607,
};

static const struct itemplate * const itable_0F3A20[] = {
    instrux + 1624,
    instrux + 1625,
};

static const struct itemplate * const itable_0F3A21[] = {
    instrux + 1608,
};

static const struct itemplate * const itable_0F3A22[] = {
    instrux + 1626,
    instrux + 1627,
};

static const struct itemplate * const itable_0F3A40[] = {
    instrux + 1605,
};

static const struct itemplate * const itable_0F3A41[] = {
    instrux + 1604,
};

static const struct itemplate * const itable_0F3A42[] = {
    instrux + 1610,
};

static const struct itemplate * const itable_0F3A44[] = {
    instrux + 3015,
    instrux + 3016,
    instrux + 3017,
    instrux + 3018,
    instrux + 3019,
};

static const struct itemplate * const itable_0F3A60[] = {
    instrux + 1661,
};

static const struct itemplate * const itable_0F3A61[] = {
    instrux + 1660,
};

static const struct itemplate * const itable_0F3A62[] = {
    instrux + 1663,
};

static const struct itemplate * const itable_0F3A63[] = {
    instrux + 1662,
};

static const struct itemplate * const itable_0F3ADF[] = {
    instrux + 1967,
};

static const struct itemplate * const itable_0F3C[] = {
    instrux + 245,
};

static const struct itemplate * const itable_0F3D[] = {
    instrux + 244,
};

static const struct itemplate * const itable_0F40[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F41[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F42[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F43[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F44[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F45[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F46[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F47[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F48[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F49[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F4A[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F4B[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F4C[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F4D[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F4E[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F4F[] = {
    instrux + 1213,
    instrux + 1214,
    instrux + 1215,
    instrux + 1216,
    instrux + 1217,
    instrux + 1218,
};

static const struct itemplate * const itable_0F50[] = {
    instrux + 774,
    instrux + 1278,
    instrux + 1279,
    instrux + 1506,
    instrux + 1507,
};

static const struct itemplate * const itable_0F51[] = {
    instrux + 766,
    instrux + 1298,
    instrux + 1299,
    instrux + 1521,
    instrux + 1522,
};

static const struct itemplate * const itable_0F52[] = {
    instrux + 802,
    instrux + 1294,
    instrux + 1295,
};

static const struct itemplate * const itable_0F53[] = {
    instrux + 1292,
    instrux + 1293,
};

static const struct itemplate * const itable_0F54[] = {
    instrux + 782,
    instrux + 1228,
    instrux + 1449,
};

static const struct itemplate * const itable_0F55[] = {
    instrux + 849,
    instrux + 1227,
    instrux + 1448,
};

static const struct itemplate * const itable_0F56[] = {
    instrux + 1291,
    instrux + 1518,
};

static const struct itemplate * const itable_0F57[] = {
    instrux + 1306,
    instrux + 1528,
};

static const struct itemplate * const itable_0F58[] = {
    instrux + 811,
    instrux + 1225,
    instrux + 1226,
    instrux + 1446,
    instrux + 1447,
};

static const struct itemplate * const itable_0F59[] = {
    instrux + 805,
    instrux + 1289,
    instrux + 1290,
    instrux + 1516,
    instrux + 1517,
};

static const struct itemplate * const itable_0F5A[] = {
    instrux + 810,
    instrux + 1473,
    instrux + 1476,
    instrux + 1481,
    instrux + 1484,
};

static const struct itemplate * const itable_0F5B[] = {
    instrux + 809,
    instrux + 1470,
    instrux + 1475,
    instrux + 1487,
};

static const struct itemplate * const itable_0F5C[] = {
    instrux + 808,
    instrux + 1301,
    instrux + 1302,
    instrux + 1523,
    instrux + 1524,
};

static const struct itemplate * const itable_0F5D[] = {
    instrux + 803,
    instrux + 1266,
    instrux + 1267,
    instrux + 1496,
    instrux + 1497,
};

static const struct itemplate * const itable_0F5E[] = {
    instrux + 800,
    instrux + 1261,
    instrux + 1262,
    instrux + 1492,
    instrux + 1493,
};

static const struct itemplate * const itable_0F5F[] = {
    instrux + 1264,
    instrux + 1265,
    instrux + 1494,
    instrux + 1495,
};

static const struct itemplate * const itable_0F60[] = {
    instrux + 857,
    instrux + 1441,
};

static const struct itemplate * const itable_0F61[] = {
    instrux + 859,
    instrux + 1442,
};

static const struct itemplate * const itable_0F62[] = {
    instrux + 858,
    instrux + 1443,
};

static const struct itemplate * const itable_0F63[] = {
    instrux + 761,
    instrux + 1366,
};

static const struct itemplate * const itable_0F64[] = {
    instrux + 779,
    instrux + 1385,
};

static const struct itemplate * const itable_0F65[] = {
    instrux + 781,
    instrux + 1386,
};

static const struct itemplate * const itable_0F66[] = {
    instrux + 780,
    instrux + 1387,
};

static const struct itemplate * const itable_0F67[] = {
    instrux + 762,
    instrux + 1368,
};

static const struct itemplate * const itable_0F68[] = {
    instrux + 854,
    instrux + 1437,
};

static const struct itemplate * const itable_0F69[] = {
    instrux + 856,
    instrux + 1438,
};

static const struct itemplate * const itable_0F6A[] = {
    instrux + 855,
    instrux + 1439,
};

static const struct itemplate * const itable_0F6B[] = {
    instrux + 760,
    instrux + 1367,
};

static const struct itemplate * const itable_0F6C[] = {
    instrux + 1444,
};

static const struct itemplate * const itable_0F6D[] = {
    instrux + 1440,
};

static const struct itemplate * const itable_0F6E[] = {
    instrux + 672,
    instrux + 673,
    instrux + 676,
    instrux + 677,
    instrux + 682,
    instrux + 1346,
    instrux + 1349,
    instrux + 1363,
};

static const struct itemplate * const itable_0F6F[] = {
    instrux + 680,
    instrux + 1350,
    instrux + 1352,
    instrux + 1354,
    instrux + 1356,
};

static const struct itemplate * const itable_0F70[] = {
    instrux + 1332,
    instrux + 1404,
    instrux + 1405,
    instrux + 1406,
    instrux + 1407,
    instrux + 1408,
    instrux + 1409,
};

static const struct itemplate * const itable_0F71[] = {
    instrux + 835,
    instrux + 839,
    instrux + 845,
    instrux + 1412,
    instrux + 1418,
    instrux + 1423,
};

static const struct itemplate * const itable_0F72[] = {
    instrux + 831,
    instrux + 837,
    instrux + 841,
    instrux + 1414,
    instrux + 1420,
    instrux + 1425,
};

static const struct itemplate * const itable_0F73[] = {
    instrux + 833,
    instrux + 843,
    instrux + 1410,
    instrux + 1416,
    instrux + 1421,
    instrux + 1427,
};

static const struct itemplate * const itable_0F74[] = {
    instrux + 776,
    instrux + 1382,
};

static const struct itemplate * const itable_0F75[] = {
    instrux + 778,
    instrux + 1383,
};

static const struct itemplate * const itable_0F76[] = {
    instrux + 777,
    instrux + 1384,
};

static const struct itemplate * const itable_0F77[] = {
    instrux + 262,
};

static const struct itemplate * const itable_0F78[] = {
    instrux + 1104,
    instrux + 1546,
    instrux + 1547,
    instrux + 1591,
    instrux + 1593,
};

static const struct itemplate * const itable_0F79[] = {
    instrux + 941,
    instrux + 1551,
    instrux + 1552,
    instrux + 1592,
    instrux + 1594,
};

static const struct itemplate * const itable_0F7A10[] = {
    instrux + 1924,
};

static const struct itemplate * const itable_0F7A11[] = {
    instrux + 1925,
};

static const struct itemplate * const itable_0F7A12[] = {
    instrux + 1926,
};

static const struct itemplate * const itable_0F7A13[] = {
    instrux + 1927,
};

static const struct itemplate * const itable_0F7A30[] = {
    instrux + 1928,
};

static const struct itemplate * const itable_0F7A31[] = {
    instrux + 1929,
};

static const struct itemplate * const itable_0F7A41[] = {
    instrux + 1930,
};

static const struct itemplate * const itable_0F7A42[] = {
    instrux + 1931,
};

static const struct itemplate * const itable_0F7A43[] = {
    instrux + 1932,
};

static const struct itemplate * const itable_0F7A46[] = {
    instrux + 1933,
};

static const struct itemplate * const itable_0F7A47[] = {
    instrux + 1934,
};

static const struct itemplate * const itable_0F7A4B[] = {
    instrux + 1935,
};

static const struct itemplate * const itable_0F7A51[] = {
    instrux + 1936,
};

static const struct itemplate * const itable_0F7A52[] = {
    instrux + 1937,
};

static const struct itemplate * const itable_0F7A53[] = {
    instrux + 1938,
};

static const struct itemplate * const itable_0F7A56[] = {
    instrux + 1939,
};

static const struct itemplate * const itable_0F7A57[] = {
    instrux + 1940,
};

static const struct itemplate * const itable_0F7A5B[] = {
    instrux + 1941,
};

static const struct itemplate * const itable_0F7A61[] = {
    instrux + 1942,
};

static const struct itemplate * const itable_0F7A62[] = {
    instrux + 1943,
};

static const struct itemplate * const itable_0F7A63[] = {
    instrux + 1944,
};

static const struct itemplate * const itable_0F7B[] = {
    instrux + 942,
    instrux + 1945,
    instrux + 1946,
    instrux + 1947,
    instrux + 1948,
};

static const struct itemplate * const itable_0F7C[] = {
    instrux + 1105,
    instrux + 1531,
    instrux + 1532,
};

static const struct itemplate * const itable_0F7D[] = {
    instrux + 944,
    instrux + 1533,
    instrux + 1534,
};

static const struct itemplate * const itable_0F7E[] = {
    instrux + 674,
    instrux + 675,
    instrux + 678,
    instrux + 679,
    instrux + 683,
    instrux + 1347,
    instrux + 1348,
    instrux + 1359,
    instrux + 1362,
    instrux + 1364,
};

static const struct itemplate * const itable_0F7F[] = {
    instrux + 681,
    instrux + 1351,
    instrux + 1353,
    instrux + 1355,
    instrux + 1357,
};

static const struct itemplate * const itable_0F80[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F81[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F82[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F83[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F84[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F85[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F86[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F87[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F88[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F89[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F8A[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F8B[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F8C[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F8D[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F8E[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F8F[] = {
    instrux + 1219,
    instrux + 1220,
    instrux + 1221,
};

static const struct itemplate * const itable_0F90[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F91[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F92[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F93[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F94[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F95[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F96[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F97[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F98[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F99[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F9A[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F9B[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F9C[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F9D[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F9E[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0F9F[] = {
    instrux + 1223,
    instrux + 1224,
};

static const struct itemplate * const itable_0FA0[] = {
    instrux + 868,
};

static const struct itemplate * const itable_0FA1[] = {
    instrux + 819,
};

static const struct itemplate * const itable_0FA2[] = {
    instrux + 243,
};

static const struct itemplate * const itable_0FA3[] = {
    instrux + 124,
    instrux + 125,
    instrux + 126,
    instrux + 127,
    instrux + 128,
    instrux + 129,
};

static const struct itemplate * const itable_0FA4[] = {
    instrux + 1010,
    instrux + 1011,
    instrux + 1012,
    instrux + 1013,
    instrux + 1014,
    instrux + 1015,
};

static const struct itemplate * const itable_0FA5[] = {
    instrux + 1016,
    instrux + 1017,
    instrux + 1018,
    instrux + 1019,
    instrux + 1020,
    instrux + 1021,
};

static const struct itemplate * const itable_0FA6C0[] = {
    instrux + 3420,
};

static const struct itemplate * const itable_0FA6C8[] = {
    instrux + 3421,
};

static const struct itemplate * const itable_0FA6D0[] = {
    instrux + 3422,
};

static const struct itemplate * const itable_0FA7C0[] = {
    instrux + 3414,
};

static const struct itemplate * const itable_0FA7C8[] = {
    instrux + 3415,
};

static const struct itemplate * const itable_0FA7D0[] = {
    instrux + 3416,
};

static const struct itemplate * const itable_0FA7D8[] = {
    instrux + 3417,
};

static const struct itemplate * const itable_0FA7E0[] = {
    instrux + 3418,
};

static const struct itemplate * const itable_0FA7E8[] = {
    instrux + 3419,
};

static const struct itemplate * const itable_0FA8[] = {
    instrux + 868,
};

static const struct itemplate * const itable_0FA9[] = {
    instrux + 819,
};

static const struct itemplate * const itable_0FAA[] = {
    instrux + 943,
};

static const struct itemplate * const itable_0FAB[] = {
    instrux + 151,
    instrux + 152,
    instrux + 153,
    instrux + 154,
    instrux + 155,
    instrux + 156,
};

static const struct itemplate * const itable_0FAC[] = {
    instrux + 1034,
    instrux + 1035,
    instrux + 1036,
    instrux + 1037,
    instrux + 1038,
    instrux + 1039,
};

static const struct itemplate * const itable_0FAD[] = {
    instrux + 1040,
    instrux + 1041,
    instrux + 1042,
    instrux + 1043,
    instrux + 1044,
    instrux + 1045,
};

static const struct itemplate * const itable_0FAE[] = {
    instrux + 566,
    instrux + 620,
    instrux + 996,
    instrux + 1263,
    instrux + 1300,
    instrux + 1307,
    instrux + 1308,
    instrux + 1311,
    instrux + 1312,
    instrux + 1317,
    instrux + 1339,
    instrux + 1344,
    instrux + 1345,
};

static const struct itemplate * const itable_0FAF[] = {
    instrux + 461,
    instrux + 462,
    instrux + 463,
    instrux + 464,
    instrux + 465,
    instrux + 466,
};

static const struct itemplate * const itable_0FB0[] = {
    instrux + 233,
    instrux + 234,
};

static const struct itemplate * const itable_0FB1[] = {
    instrux + 235,
    instrux + 236,
    instrux + 237,
    instrux + 238,
    instrux + 239,
    instrux + 240,
};

static const struct itemplate * const itable_0FB2[] = {
    instrux + 615,
    instrux + 616,
};

static const struct itemplate * const itable_0FB3[] = {
    instrux + 142,
    instrux + 143,
    instrux + 144,
    instrux + 145,
    instrux + 146,
    instrux + 147,
};

static const struct itemplate * const itable_0FB4[] = {
    instrux + 567,
    instrux + 568,
};

static const struct itemplate * const itable_0FB5[] = {
    instrux + 570,
    instrux + 571,
};

static const struct itemplate * const itable_0FB6[] = {
    instrux + 695,
    instrux + 696,
    instrux + 697,
    instrux + 699,
};

static const struct itemplate * const itable_0FB7[] = {
    instrux + 698,
    instrux + 700,
};

static const struct itemplate * const itable_0FB8[] = {
    instrux + 541,
    instrux + 542,
    instrux + 543,
    instrux + 1665,
    instrux + 1666,
    instrux + 1667,
};

static const struct itemplate * const itable_0FB9[] = {
    instrux + 1135,
};

static const struct itemplate * const itable_0FBA[] = {
    instrux + 130,
    instrux + 131,
    instrux + 132,
    instrux + 139,
    instrux + 140,
    instrux + 141,
    instrux + 148,
    instrux + 149,
    instrux + 150,
    instrux + 157,
    instrux + 158,
    instrux + 159,
};

static const struct itemplate * const itable_0FBB[] = {
    instrux + 133,
    instrux + 134,
    instrux + 135,
    instrux + 136,
    instrux + 137,
    instrux + 138,
};

static const struct itemplate * const itable_0FBC[] = {
    instrux + 110,
    instrux + 111,
    instrux + 112,
    instrux + 113,
    instrux + 114,
    instrux + 115,
};

static const struct itemplate * const itable_0FBD[] = {
    instrux + 116,
    instrux + 117,
    instrux + 118,
    instrux + 119,
    instrux + 120,
    instrux + 121,
    instrux + 1597,
    instrux + 1598,
    instrux + 1599,
};

static const struct itemplate * const itable_0FBE[] = {
    instrux + 688,
    instrux + 689,
    instrux + 690,
    instrux + 692,
};

static const struct itemplate * const itable_0FBF[] = {
    instrux + 691,
    instrux + 693,
};

static const struct itemplate * const itable_0FC0[] = {
    instrux + 1147,
    instrux + 1148,
};

static const struct itemplate * const itable_0FC1[] = {
    instrux + 1149,
    instrux + 1150,
    instrux + 1151,
    instrux + 1152,
    instrux + 1153,
    instrux + 1154,
};

static const struct itemplate * const itable_0FC2[] = {
    instrux + 1229,
    instrux + 1230,
    instrux + 1231,
    instrux + 1232,
    instrux + 1233,
    instrux + 1234,
    instrux + 1235,
    instrux + 1236,
    instrux + 1237,
    instrux + 1238,
    instrux + 1239,
    instrux + 1240,
    instrux + 1241,
    instrux + 1242,
    instrux + 1243,
    instrux + 1244,
    instrux + 1245,
    instrux + 1246,
    instrux + 1247,
    instrux + 1248,
    instrux + 1450,
    instrux + 1451,
    instrux + 1452,
    instrux + 1453,
    instrux + 1454,
    instrux + 1455,
    instrux + 1456,
    instrux + 1457,
    instrux + 1458,
    instrux + 1459,
    instrux + 1460,
    instrux + 1461,
    instrux + 1462,
    instrux + 1463,
    instrux + 1464,
    instrux + 1465,
    instrux + 1466,
    instrux + 1467,
};

static const struct itemplate * const itable_0FC3[] = {
    instrux + 1341,
    instrux + 1342,
};

static const struct itemplate * const itable_0FC4[] = {
    instrux + 1323,
    instrux + 1324,
    instrux + 1389,
    instrux + 1390,
};

static const struct itemplate * const itable_0FC5[] = {
    instrux + 1322,
    instrux + 1388,
};

static const struct itemplate * const itable_0FC6[] = {
    instrux + 1296,
    instrux + 1297,
    instrux + 1519,
    instrux + 1520,
};

static const struct itemplate * const itable_0FC7[] = {
    instrux + 241,
    instrux + 242,
    instrux + 1540,
    instrux + 1544,
    instrux + 1545,
    instrux + 1554,
};

static const struct itemplate * const itable_0FC8[] = {
    instrux + 122,
    instrux + 123,
};

static const struct itemplate * const itable_0FC9[] = {
    instrux + 122,
    instrux + 123,
};

static const struct itemplate * const itable_0FCA[] = {
    instrux + 122,
    instrux + 123,
};

static const struct itemplate * const itable_0FCB[] = {
    instrux + 122,
    instrux + 123,
};

static const struct itemplate * const itable_0FCC[] = {
    instrux + 122,
    instrux + 123,
};

static const struct itemplate * const itable_0FCD[] = {
    instrux + 122,
    instrux + 123,
};

static const struct itemplate * const itable_0FCE[] = {
    instrux + 122,
    instrux + 123,
};

static const struct itemplate * const itable_0FCF[] = {
    instrux + 122,
    instrux + 123,
};

static const struct itemplate * const itable_0FD0[] = {
    instrux + 1529,
    instrux + 1530,
};

static const struct itemplate * const itable_0FD1[] = {
    instrux + 844,
    instrux + 1422,
};

static const struct itemplate * const itable_0FD2[] = {
    instrux + 840,
    instrux + 1424,
};

static const struct itemplate * const itable_0FD3[] = {
    instrux + 842,
    instrux + 1426,
};

static const struct itemplate * const itable_0FD4[] = {
    instrux + 1372,
    instrux + 1373,
};

static const struct itemplate * const itable_0FD5[] = {
    instrux + 807,
    instrux + 1399,
};

static const struct itemplate * const itable_0FD6[] = {
    instrux + 1358,
    instrux + 1360,
    instrux + 1361,
    instrux + 1365,
};

static const struct itemplate * const itable_0FD7[] = {
    instrux + 1329,
    instrux + 1396,
};

static const struct itemplate * const itable_0FD8[] = {
    instrux + 851,
    instrux + 1435,
};

static const struct itemplate * const itable_0FD9[] = {
    instrux + 852,
    instrux + 1436,
};

static const struct itemplate * const itable_0FDA[] = {
    instrux + 1328,
    instrux + 1395,
};

static const struct itemplate * const itable_0FDB[] = {
    instrux + 771,
    instrux + 1378,
};

static const struct itemplate * const itable_0FDC[] = {
    instrux + 768,
    instrux + 1376,
};

static const struct itemplate * const itable_0FDD[] = {
    instrux + 769,
    instrux + 1377,
};

static const struct itemplate * const itable_0FDE[] = {
    instrux + 1326,
    instrux + 1393,
};

static const struct itemplate * const itable_0FDF[] = {
    instrux + 772,
    instrux + 1379,
};

static const struct itemplate * const itable_0FE0[] = {
    instrux + 1320,
    instrux + 1380,
};

static const struct itemplate * const itable_0FE1[] = {
    instrux + 838,
    instrux + 1417,
};

static const struct itemplate * const itable_0FE2[] = {
    instrux + 836,
    instrux + 1419,
};

static const struct itemplate * const itable_0FE3[] = {
    instrux + 1321,
    instrux + 1381,
};

static const struct itemplate * const itable_0FE4[] = {
    instrux + 1330,
    instrux + 1397,
};

static const struct itemplate * const itable_0FE5[] = {
    instrux + 806,
    instrux + 1398,
};

static const struct itemplate * const itable_0FE6[] = {
    instrux + 1469,
    instrux + 1471,
    instrux + 1486,
};

static const struct itemplate * const itable_0FE7[] = {
    instrux + 1319,
    instrux + 1340,
};

static const struct itemplate * const itable_0FE8[] = {
    instrux + 848,
    instrux + 1433,
};

static const struct itemplate * const itable_0FE9[] = {
    instrux + 850,
    instrux + 1434,
};

static const struct itemplate * const itable_0FEA[] = {
    instrux + 1327,
    instrux + 1394,
};

static const struct itemplate * const itable_0FEB[] = {
    instrux + 827,
    instrux + 1402,
};

static const struct itemplate * const itable_0FEC[] = {
    instrux + 765,
    instrux + 1374,
};

static const struct itemplate * const itable_0FED[] = {
    instrux + 767,
    instrux + 1375,
};

static const struct itemplate * const itable_0FEE[] = {
    instrux + 1325,
    instrux + 1392,
};

static const struct itemplate * const itable_0FEF[] = {
    instrux + 881,
    instrux + 1445,
};

static const struct itemplate * const itable_0FF0[] = {
    instrux + 1535,
};

static const struct itemplate * const itable_0FF1[] = {
    instrux + 834,
    instrux + 1411,
};

static const struct itemplate * const itable_0FF2[] = {
    instrux + 830,
    instrux + 1413,
};

static const struct itemplate * const itable_0FF3[] = {
    instrux + 832,
    instrux + 1415,
};

static const struct itemplate * const itable_0FF4[] = {
    instrux + 1400,
    instrux + 1401,
};

static const struct itemplate * const itable_0FF5[] = {
    instrux + 801,
    instrux + 1391,
};

static const struct itemplate * const itable_0FF6[] = {
    instrux + 1331,
    instrux + 1403,
};

static const struct itemplate * const itable_0FF7[] = {
    instrux + 1318,
    instrux + 1338,
};

static const struct itemplate * const itable_0FF8[] = {
    instrux + 846,
    instrux + 1428,
};

static const struct itemplate * const itable_0FF9[] = {
    instrux + 853,
    instrux + 1429,
};

static const struct itemplate * const itable_0FFA[] = {
    instrux + 847,
    instrux + 1430,
};

static const struct itemplate * const itable_0FFB[] = {
    instrux + 1431,
    instrux + 1432,
};

static const struct itemplate * const itable_0FFC[] = {
    instrux + 763,
    instrux + 1369,
};

static const struct itemplate * const itable_0FFD[] = {
    instrux + 770,
    instrux + 1370,
};

static const struct itemplate * const itable_0FFE[] = {
    instrux + 764,
    instrux + 1371,
};

static const struct itemplate * const itable_0FFF[] = {
    instrux + 1134,
};

static const struct itemplate * const itable_10[] = {
    instrux + 7,
    instrux + 8,
};

static const struct itemplate * const itable_11[] = {
    instrux + 9,
    instrux + 10,
    instrux + 11,
    instrux + 12,
    instrux + 13,
    instrux + 14,
};

static const struct itemplate * const itable_12[] = {
    instrux + 15,
    instrux + 16,
};

static const struct itemplate * const itable_13[] = {
    instrux + 17,
    instrux + 18,
    instrux + 19,
    instrux + 20,
    instrux + 21,
    instrux + 22,
};

static const struct itemplate * const itable_14[] = {
    instrux + 26,
};

static const struct itemplate * const itable_15[] = {
    instrux + 28,
    instrux + 30,
    instrux + 32,
};

static const struct itemplate * const itable_16[] = {
    instrux + 866,
    instrux + 867,
};

static const struct itemplate * const itable_17[] = {
    instrux + 818,
};

static const struct itemplate * const itable_18[] = {
    instrux + 959,
    instrux + 960,
};

static const struct itemplate * const itable_19[] = {
    instrux + 961,
    instrux + 962,
    instrux + 963,
    instrux + 964,
    instrux + 965,
    instrux + 966,
};

static const struct itemplate * const itable_1A[] = {
    instrux + 967,
    instrux + 968,
};

static const struct itemplate * const itable_1B[] = {
    instrux + 969,
    instrux + 970,
    instrux + 971,
    instrux + 972,
    instrux + 973,
    instrux + 974,
};

static const struct itemplate * const itable_1C[] = {
    instrux + 978,
};

static const struct itemplate * const itable_1D[] = {
    instrux + 980,
    instrux + 982,
    instrux + 984,
};

static const struct itemplate * const itable_1E[] = {
    instrux + 866,
    instrux + 867,
};

static const struct itemplate * const itable_1F[] = {
    instrux + 818,
};

static const struct itemplate * const itable_20[] = {
    instrux + 73,
    instrux + 74,
};

static const struct itemplate * const itable_21[] = {
    instrux + 75,
    instrux + 76,
    instrux + 77,
    instrux + 78,
    instrux + 79,
    instrux + 80,
};

static const struct itemplate * const itable_22[] = {
    instrux + 81,
    instrux + 82,
};

static const struct itemplate * const itable_23[] = {
    instrux + 83,
    instrux + 84,
    instrux + 85,
    instrux + 86,
    instrux + 87,
    instrux + 88,
};

static const struct itemplate * const itable_24[] = {
    instrux + 92,
};

static const struct itemplate * const itable_25[] = {
    instrux + 94,
    instrux + 96,
    instrux + 98,
};

static const struct itemplate * const itable_27[] = {
    instrux + 249,
};

static const struct itemplate * const itable_28[] = {
    instrux + 1071,
    instrux + 1072,
};

static const struct itemplate * const itable_29[] = {
    instrux + 1073,
    instrux + 1074,
    instrux + 1075,
    instrux + 1076,
    instrux + 1077,
    instrux + 1078,
};

static const struct itemplate * const itable_2A[] = {
    instrux + 1079,
    instrux + 1080,
};

static const struct itemplate * const itable_2B[] = {
    instrux + 1081,
    instrux + 1082,
    instrux + 1083,
    instrux + 1084,
    instrux + 1085,
    instrux + 1086,
};

static const struct itemplate * const itable_2C[] = {
    instrux + 1090,
};

static const struct itemplate * const itable_2D[] = {
    instrux + 1092,
    instrux + 1094,
    instrux + 1096,
};

static const struct itemplate * const itable_2F[] = {
    instrux + 250,
};

static const struct itemplate * const itable_30[] = {
    instrux + 1180,
    instrux + 1181,
};

static const struct itemplate * const itable_31[] = {
    instrux + 1182,
    instrux + 1183,
    instrux + 1184,
    instrux + 1185,
    instrux + 1186,
    instrux + 1187,
};

static const struct itemplate * const itable_32[] = {
    instrux + 1188,
    instrux + 1189,
};

static const struct itemplate * const itable_33[] = {
    instrux + 1190,
    instrux + 1191,
    instrux + 1192,
    instrux + 1193,
    instrux + 1194,
    instrux + 1195,
};

static const struct itemplate * const itable_34[] = {
    instrux + 1199,
};

static const struct itemplate * const itable_35[] = {
    instrux + 1201,
    instrux + 1203,
    instrux + 1205,
};

static const struct itemplate * const itable_37[] = {
    instrux + 1,
};

static const struct itemplate * const itable_38[] = {
    instrux + 196,
    instrux + 197,
};

static const struct itemplate * const itable_39[] = {
    instrux + 198,
    instrux + 199,
    instrux + 200,
    instrux + 201,
    instrux + 202,
    instrux + 203,
};

static const struct itemplate * const itable_3A[] = {
    instrux + 204,
    instrux + 205,
};

static const struct itemplate * const itable_3B[] = {
    instrux + 206,
    instrux + 207,
    instrux + 208,
    instrux + 209,
    instrux + 210,
    instrux + 211,
};

static const struct itemplate * const itable_3C[] = {
    instrux + 215,
};

static const struct itemplate * const itable_3D[] = {
    instrux + 217,
    instrux + 219,
    instrux + 221,
};

static const struct itemplate * const itable_3F[] = {
    instrux + 6,
};

static const struct itemplate * const itable_40[] = {
    instrux + 491,
    instrux + 492,
};

static const struct itemplate * const itable_41[] = {
    instrux + 491,
    instrux + 492,
};

static const struct itemplate * const itable_42[] = {
    instrux + 491,
    instrux + 492,
};

static const struct itemplate * const itable_43[] = {
    instrux + 491,
    instrux + 492,
};

static const struct itemplate * const itable_44[] = {
    instrux + 491,
    instrux + 492,
};

static const struct itemplate * const itable_45[] = {
    instrux + 491,
    instrux + 492,
};

static const struct itemplate * const itable_46[] = {
    instrux + 491,
    instrux + 492,
};

static const struct itemplate * const itable_47[] = {
    instrux + 491,
    instrux + 492,
};

static const struct itemplate * const itable_48[] = {
    instrux + 251,
    instrux + 252,
};

static const struct itemplate * const itable_49[] = {
    instrux + 251,
    instrux + 252,
};

static const struct itemplate * const itable_4A[] = {
    instrux + 251,
    instrux + 252,
};

static const struct itemplate * const itable_4B[] = {
    instrux + 251,
    instrux + 252,
};

static const struct itemplate * const itable_4C[] = {
    instrux + 251,
    instrux + 252,
};

static const struct itemplate * const itable_4D[] = {
    instrux + 251,
    instrux + 252,
};

static const struct itemplate * const itable_4E[] = {
    instrux + 251,
    instrux + 252,
};

static const struct itemplate * const itable_4F[] = {
    instrux + 251,
    instrux + 252,
};

static const struct itemplate * const itable_50[] = {
    instrux + 860,
    instrux + 861,
    instrux + 862,
};

static const struct itemplate * const itable_51[] = {
    instrux + 860,
    instrux + 861,
    instrux + 862,
};

static const struct itemplate * const itable_52[] = {
    instrux + 860,
    instrux + 861,
    instrux + 862,
};

static const struct itemplate * const itable_53[] = {
    instrux + 860,
    instrux + 861,
    instrux + 862,
};

static const struct itemplate * const itable_54[] = {
    instrux + 860,
    instrux + 861,
    instrux + 862,
};

static const struct itemplate * const itable_55[] = {
    instrux + 860,
    instrux + 861,
    instrux + 862,
};

static const struct itemplate * const itable_56[] = {
    instrux + 860,
    instrux + 861,
    instrux + 862,
};

static const struct itemplate * const itable_57[] = {
    instrux + 860,
    instrux + 861,
    instrux + 862,
};

static const struct itemplate * const itable_58[] = {
    instrux + 812,
    instrux + 813,
    instrux + 814,
};

static const struct itemplate * const itable_59[] = {
    instrux + 812,
    instrux + 813,
    instrux + 814,
};

static const struct itemplate * const itable_5A[] = {
    instrux + 812,
    instrux + 813,
    instrux + 814,
};

static const struct itemplate * const itable_5B[] = {
    instrux + 812,
    instrux + 813,
    instrux + 814,
};

static const struct itemplate * const itable_5C[] = {
    instrux + 812,
    instrux + 813,
    instrux + 814,
};

static const struct itemplate * const itable_5D[] = {
    instrux + 812,
    instrux + 813,
    instrux + 814,
};

static const struct itemplate * const itable_5E[] = {
    instrux + 812,
    instrux + 813,
    instrux + 814,
};

static const struct itemplate * const itable_5F[] = {
    instrux + 812,
    instrux + 813,
    instrux + 814,
};

static const struct itemplate * const itable_60[] = {
    instrux + 874,
    instrux + 875,
    instrux + 876,
};

static const struct itemplate * const itable_61[] = {
    instrux + 820,
    instrux + 821,
    instrux + 822,
};

static const struct itemplate * const itable_62[] = {
    instrux + 108,
    instrux + 109,
};

static const struct itemplate * const itable_63[] = {
    instrux + 106,
    instrux + 107,
    instrux + 694,
};

static const struct itemplate * const itable_68[] = {
    instrux + 870,
    instrux + 871,
    instrux + 872,
    instrux + 873,
};

static const struct itemplate * const itable_69[] = {
    instrux + 468,
    instrux + 470,
    instrux + 472,
    instrux + 474,
    instrux + 476,
    instrux + 478,
    instrux + 480,
    instrux + 482,
    instrux + 484,
};

static const struct itemplate * const itable_6A[] = {
    instrux + 869,
    instrux + 870,
    instrux + 871,
    instrux + 872,
    instrux + 873,
};

static const struct itemplate * const itable_6B[] = {
    instrux + 467,
    instrux + 469,
    instrux + 471,
    instrux + 473,
    instrux + 475,
    instrux + 477,
    instrux + 479,
    instrux + 481,
    instrux + 483,
};

static const struct itemplate * const itable_6C[] = {
    instrux + 497,
};

static const struct itemplate * const itable_6D[] = {
    instrux + 498,
    instrux + 499,
};

static const struct itemplate * const itable_6E[] = {
    instrux + 757,
};

static const struct itemplate * const itable_6F[] = {
    instrux + 758,
    instrux + 759,
};

static const struct itemplate * const itable_70[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_71[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_72[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_73[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_74[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_75[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_76[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_77[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_78[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_79[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_7A[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_7B[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_7C[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_7D[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_7E[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_7F[] = {
    instrux + 1222,
};

static const struct itemplate * const itable_80[] = {
    instrux + 33,
    instrux + 37,
    instrux + 66,
    instrux + 70,
    instrux + 99,
    instrux + 103,
    instrux + 222,
    instrux + 226,
    instrux + 744,
    instrux + 748,
    instrux + 985,
    instrux + 989,
    instrux + 1097,
    instrux + 1101,
    instrux + 1206,
    instrux + 1210,
};

static const struct itemplate * const itable_81[] = {
    instrux + 34,
    instrux + 35,
    instrux + 36,
    instrux + 38,
    instrux + 39,
    instrux + 67,
    instrux + 68,
    instrux + 69,
    instrux + 71,
    instrux + 72,
    instrux + 100,
    instrux + 101,
    instrux + 102,
    instrux + 104,
    instrux + 105,
    instrux + 223,
    instrux + 224,
    instrux + 225,
    instrux + 227,
    instrux + 228,
    instrux + 745,
    instrux + 746,
    instrux + 747,
    instrux + 749,
    instrux + 750,
    instrux + 986,
    instrux + 987,
    instrux + 988,
    instrux + 990,
    instrux + 991,
    instrux + 1098,
    instrux + 1099,
    instrux + 1100,
    instrux + 1102,
    instrux + 1103,
    instrux + 1207,
    instrux + 1208,
    instrux + 1209,
    instrux + 1211,
    instrux + 1212,
};

static const struct itemplate * const itable_83[] = {
    instrux + 23,
    instrux + 24,
    instrux + 25,
    instrux + 27,
    instrux + 29,
    instrux + 31,
    instrux + 34,
    instrux + 35,
    instrux + 36,
    instrux + 38,
    instrux + 39,
    instrux + 56,
    instrux + 57,
    instrux + 58,
    instrux + 60,
    instrux + 62,
    instrux + 64,
    instrux + 67,
    instrux + 68,
    instrux + 69,
    instrux + 71,
    instrux + 72,
    instrux + 89,
    instrux + 90,
    instrux + 91,
    instrux + 93,
    instrux + 95,
    instrux + 97,
    instrux + 100,
    instrux + 101,
    instrux + 102,
    instrux + 104,
    instrux + 105,
    instrux + 212,
    instrux + 213,
    instrux + 214,
    instrux + 216,
    instrux + 218,
    instrux + 220,
    instrux + 223,
    instrux + 224,
    instrux + 225,
    instrux + 227,
    instrux + 228,
    instrux + 734,
    instrux + 735,
    instrux + 736,
    instrux + 738,
    instrux + 740,
    instrux + 742,
    instrux + 745,
    instrux + 746,
    instrux + 747,
    instrux + 749,
    instrux + 750,
    instrux + 975,
    instrux + 976,
    instrux + 977,
    instrux + 979,
    instrux + 981,
    instrux + 983,
    instrux + 986,
    instrux + 987,
    instrux + 988,
    instrux + 990,
    instrux + 991,
    instrux + 1087,
    instrux + 1088,
    instrux + 1089,
    instrux + 1091,
    instrux + 1093,
    instrux + 1095,
    instrux + 1098,
    instrux + 1099,
    instrux + 1100,
    instrux + 1102,
    instrux + 1103,
    instrux + 1196,
    instrux + 1197,
    instrux + 1198,
    instrux + 1200,
    instrux + 1202,
    instrux + 1204,
    instrux + 1207,
    instrux + 1208,
    instrux + 1209,
    instrux + 1211,
    instrux + 1212,
};

static const struct itemplate * const itable_84[] = {
    instrux + 1111,
    instrux + 1112,
    instrux + 1119,
};

static const struct itemplate * const itable_85[] = {
    instrux + 1113,
    instrux + 1114,
    instrux + 1115,
    instrux + 1116,
    instrux + 1117,
    instrux + 1118,
    instrux + 1120,
    instrux + 1121,
    instrux + 1122,
};

static const struct itemplate * const itable_86[] = {
    instrux + 1162,
    instrux + 1163,
    instrux + 1170,
    instrux + 1171,
};

static const struct itemplate * const itable_87[] = {
    instrux + 1164,
    instrux + 1165,
    instrux + 1166,
    instrux + 1167,
    instrux + 1168,
    instrux + 1169,
    instrux + 1172,
    instrux + 1173,
    instrux + 1174,
    instrux + 1175,
    instrux + 1176,
    instrux + 1177,
};

static const struct itemplate * const itable_88[] = {
    instrux + 644,
    instrux + 645,
};

static const struct itemplate * const itable_89[] = {
    instrux + 646,
    instrux + 647,
    instrux + 648,
    instrux + 649,
    instrux + 650,
    instrux + 651,
};

static const struct itemplate * const itable_8A[] = {
    instrux + 652,
    instrux + 653,
};

static const struct itemplate * const itable_8B[] = {
    instrux + 654,
    instrux + 655,
    instrux + 656,
    instrux + 657,
    instrux + 658,
    instrux + 659,
};

static const struct itemplate * const itable_8C[] = {
    instrux + 622,
    instrux + 623,
    instrux + 624,
};

static const struct itemplate * const itable_8D[] = {
    instrux + 560,
    instrux + 561,
    instrux + 562,
};

static const struct itemplate * const itable_8E[] = {
    instrux + 625,
    instrux + 626,
    instrux + 627,
};

static const struct itemplate * const itable_8F[] = {
    instrux + 815,
    instrux + 816,
    instrux + 817,
};

static const struct itemplate * const itable_90[] = {
    instrux + 710,
    instrux + 773,
    instrux + 1155,
    instrux + 1156,
    instrux + 1157,
    instrux + 1158,
    instrux + 1159,
    instrux + 1160,
    instrux + 1161,
};

static const struct itemplate * const itable_91[] = {
    instrux + 1155,
    instrux + 1156,
    instrux + 1157,
    instrux + 1158,
    instrux + 1159,
    instrux + 1160,
};

static const struct itemplate * const itable_92[] = {
    instrux + 1155,
    instrux + 1156,
    instrux + 1157,
    instrux + 1158,
    instrux + 1159,
    instrux + 1160,
};

static const struct itemplate * const itable_93[] = {
    instrux + 1155,
    instrux + 1156,
    instrux + 1157,
    instrux + 1158,
    instrux + 1159,
    instrux + 1160,
};

static const struct itemplate * const itable_94[] = {
    instrux + 1155,
    instrux + 1156,
    instrux + 1157,
    instrux + 1158,
    instrux + 1159,
    instrux + 1160,
};

static const struct itemplate * const itable_95[] = {
    instrux + 1155,
    instrux + 1156,
    instrux + 1157,
    instrux + 1158,
    instrux + 1159,
    instrux + 1160,
};

static const struct itemplate * const itable_96[] = {
    instrux + 1155,
    instrux + 1156,
    instrux + 1157,
    instrux + 1158,
    instrux + 1159,
    instrux + 1160,
};

static const struct itemplate * const itable_97[] = {
    instrux + 1155,
    instrux + 1156,
    instrux + 1157,
    instrux + 1158,
    instrux + 1159,
    instrux + 1160,
};

static const struct itemplate * const itable_98[] = {
    instrux + 187,
    instrux + 189,
    instrux + 248,
};

static const struct itemplate * const itable_99[] = {
    instrux + 188,
    instrux + 246,
    instrux + 247,
};

static const struct itemplate * const itable_9A[] = {
    instrux + 166,
    instrux + 167,
    instrux + 168,
    instrux + 169,
    instrux + 170,
};

static const struct itemplate * const itable_9C[] = {
    instrux + 877,
    instrux + 878,
    instrux + 879,
    instrux + 880,
};

static const struct itemplate * const itable_9D[] = {
    instrux + 823,
    instrux + 824,
    instrux + 825,
    instrux + 826,
};

static const struct itemplate * const itable_9E[] = {
    instrux + 945,
};

static const struct itemplate * const itable_9F[] = {
    instrux + 547,
};

static const struct itemplate * const itable_A0[] = {
    instrux + 628,
};

static const struct itemplate * const itable_A1[] = {
    instrux + 629,
    instrux + 630,
    instrux + 631,
};

static const struct itemplate * const itable_A2[] = {
    instrux + 632,
};

static const struct itemplate * const itable_A3[] = {
    instrux + 633,
    instrux + 634,
    instrux + 635,
};

static const struct itemplate * const itable_A4[] = {
    instrux + 684,
};

static const struct itemplate * const itable_A5[] = {
    instrux + 685,
    instrux + 686,
    instrux + 687,
};

static const struct itemplate * const itable_A6[] = {
    instrux + 229,
};

static const struct itemplate * const itable_A7[] = {
    instrux + 230,
    instrux + 231,
    instrux + 232,
};

static const struct itemplate * const itable_A8[] = {
    instrux + 1123,
};

static const struct itemplate * const itable_A9[] = {
    instrux + 1124,
    instrux + 1125,
    instrux + 1126,
};

static const struct itemplate * const itable_AA[] = {
    instrux + 1062,
};

static const struct itemplate * const itable_AB[] = {
    instrux + 1063,
    instrux + 1064,
    instrux + 1065,
};

static const struct itemplate * const itable_AC[] = {
    instrux + 581,
};

static const struct itemplate * const itable_AD[] = {
    instrux + 582,
    instrux + 583,
    instrux + 584,
};

static const struct itemplate * const itable_AE[] = {
    instrux + 992,
};

static const struct itemplate * const itable_AF[] = {
    instrux + 993,
    instrux + 994,
    instrux + 995,
};

static const struct itemplate * const itable_B0[] = {
    instrux + 660,
};

static const struct itemplate * const itable_B1[] = {
    instrux + 660,
};

static const struct itemplate * const itable_B2[] = {
    instrux + 660,
};

static const struct itemplate * const itable_B3[] = {
    instrux + 660,
};

static const struct itemplate * const itable_B4[] = {
    instrux + 660,
};

static const struct itemplate * const itable_B5[] = {
    instrux + 660,
};

static const struct itemplate * const itable_B6[] = {
    instrux + 660,
};

static const struct itemplate * const itable_B7[] = {
    instrux + 660,
};

static const struct itemplate * const itable_B8[] = {
    instrux + 661,
    instrux + 662,
    instrux + 663,
};

static const struct itemplate * const itable_B9[] = {
    instrux + 661,
    instrux + 662,
    instrux + 663,
};

static const struct itemplate * const itable_BA[] = {
    instrux + 661,
    instrux + 662,
    instrux + 663,
};

static const struct itemplate * const itable_BB[] = {
    instrux + 661,
    instrux + 662,
    instrux + 663,
};

static const struct itemplate * const itable_BC[] = {
    instrux + 661,
    instrux + 662,
    instrux + 663,
};

static const struct itemplate * const itable_BD[] = {
    instrux + 661,
    instrux + 662,
    instrux + 663,
};

static const struct itemplate * const itable_BE[] = {
    instrux + 661,
    instrux + 662,
    instrux + 663,
};

static const struct itemplate * const itable_BF[] = {
    instrux + 661,
    instrux + 662,
    instrux + 663,
};

static const struct itemplate * const itable_C0[] = {
    instrux + 884,
    instrux + 896,
    instrux + 919,
    instrux + 931,
    instrux + 949,
    instrux + 1000,
    instrux + 1024,
};

static const struct itemplate * const itable_C1[] = {
    instrux + 887,
    instrux + 890,
    instrux + 893,
    instrux + 899,
    instrux + 902,
    instrux + 905,
    instrux + 922,
    instrux + 925,
    instrux + 928,
    instrux + 934,
    instrux + 937,
    instrux + 940,
    instrux + 952,
    instrux + 955,
    instrux + 958,
    instrux + 1003,
    instrux + 1006,
    instrux + 1009,
    instrux + 1027,
    instrux + 1030,
    instrux + 1033,
};

static const struct itemplate * const itable_C2[] = {
    instrux + 912,
    instrux + 916,
};

static const struct itemplate * const itable_C3[] = {
    instrux + 911,
    instrux + 915,
};

static const struct itemplate * const itable_C4[] = {
    instrux + 564,
    instrux + 565,
};

static const struct itemplate * const itable_C5[] = {
    instrux + 558,
    instrux + 559,
};

static const struct itemplate * const itable_C6[] = {
    instrux + 665,
    instrux + 669,
};

static const struct itemplate * const itable_C7[] = {
    instrux + 664,
    instrux + 666,
    instrux + 667,
    instrux + 668,
    instrux + 670,
    instrux + 671,
};

static const struct itemplate * const itable_C8[] = {
    instrux + 263,
};

static const struct itemplate * const itable_C9[] = {
    instrux + 563,
};

static const struct itemplate * const itable_CA[] = {
    instrux + 914,
};

static const struct itemplate * const itable_CB[] = {
    instrux + 913,
};

static const struct itemplate * const itable_CC[] = {
    instrux + 502,
};

static const struct itemplate * const itable_CD[] = {
    instrux + 500,
};

static const struct itemplate * const itable_CE[] = {
    instrux + 503,
};

static const struct itemplate * const itable_CF[] = {
    instrux + 510,
    instrux + 511,
    instrux + 512,
    instrux + 513,
};

static const struct itemplate * const itable_D0[] = {
    instrux + 882,
    instrux + 894,
    instrux + 917,
    instrux + 929,
    instrux + 947,
    instrux + 998,
    instrux + 1022,
};

static const struct itemplate * const itable_D1[] = {
    instrux + 885,
    instrux + 888,
    instrux + 891,
    instrux + 897,
    instrux + 900,
    instrux + 903,
    instrux + 920,
    instrux + 923,
    instrux + 926,
    instrux + 932,
    instrux + 935,
    instrux + 938,
    instrux + 950,
    instrux + 953,
    instrux + 956,
    instrux + 1001,
    instrux + 1004,
    instrux + 1007,
    instrux + 1025,
    instrux + 1028,
    instrux + 1031,
};

static const struct itemplate * const itable_D2[] = {
    instrux + 883,
    instrux + 895,
    instrux + 918,
    instrux + 930,
    instrux + 948,
    instrux + 999,
    instrux + 1023,
};

static const struct itemplate * const itable_D3[] = {
    instrux + 886,
    instrux + 889,
    instrux + 892,
    instrux + 898,
    instrux + 901,
    instrux + 904,
    instrux + 921,
    instrux + 924,
    instrux + 927,
    instrux + 933,
    instrux + 936,
    instrux + 939,
    instrux + 951,
    instrux + 954,
    instrux + 957,
    instrux + 1002,
    instrux + 1005,
    instrux + 1008,
    instrux + 1026,
    instrux + 1029,
    instrux + 1032,
};

static const struct itemplate * const itable_D4[] = {
    instrux + 4,
    instrux + 5,
};

static const struct itemplate * const itable_D5[] = {
    instrux + 2,
    instrux + 3,
};

static const struct itemplate * const itable_D6[] = {
    instrux + 946,
};

static const struct itemplate * const itable_D7[] = {
    instrux + 1178,
    instrux + 1179,
};

static const struct itemplate * const itable_D8[] = {
    instrux + 268,
    instrux + 271,
    instrux + 273,
    instrux + 298,
    instrux + 300,
    instrux + 301,
    instrux + 306,
    instrux + 308,
    instrux + 309,
    instrux + 314,
    instrux + 317,
    instrux + 319,
    instrux + 322,
    instrux + 326,
    instrux + 327,
    instrux + 378,
    instrux + 382,
    instrux + 383,
    instrux + 419,
    instrux + 423,
    instrux + 424,
    instrux + 427,
    instrux + 431,
    instrux + 432,
};

static const struct itemplate * const itable_D9[] = {
    instrux + 266,
    instrux + 267,
    instrux + 280,
    instrux + 311,
    instrux + 312,
    instrux + 351,
    instrux + 365,
    instrux + 368,
    instrux + 369,
    instrux + 370,
    instrux + 371,
    instrux + 372,
    instrux + 373,
    instrux + 374,
    instrux + 375,
    instrux + 376,
    instrux + 377,
    instrux + 390,
    instrux + 392,
    instrux + 393,
    instrux + 396,
    instrux + 397,
    instrux + 398,
    instrux + 399,
    instrux + 400,
    instrux + 403,
    instrux + 405,
    instrux + 406,
    instrux + 407,
    instrux + 408,
    instrux + 411,
    instrux + 412,
    instrux + 413,
    instrux + 435,
    instrux + 445,
    instrux + 446,
    instrux + 447,
    instrux + 448,
    instrux + 449,
    instrux + 450,
    instrux + 451,
};

static const struct itemplate * const itable_DA[] = {
    instrux + 282,
    instrux + 283,
    instrux + 284,
    instrux + 285,
    instrux + 286,
    instrux + 287,
    instrux + 296,
    instrux + 297,
    instrux + 336,
    instrux + 338,
    instrux + 340,
    instrux + 342,
    instrux + 344,
    instrux + 349,
    instrux + 361,
    instrux + 363,
    instrux + 444,
};

static const struct itemplate * const itable_DB[] = {
    instrux + 281,
    instrux + 288,
    instrux + 289,
    instrux + 290,
    instrux + 291,
    instrux + 292,
    instrux + 293,
    instrux + 294,
    instrux + 295,
    instrux + 302,
    instrux + 303,
    instrux + 313,
    instrux + 331,
    instrux + 346,
    instrux + 352,
    instrux + 353,
    instrux + 355,
    instrux + 359,
    instrux + 367,
    instrux + 386,
    instrux + 387,
    instrux + 388,
    instrux + 389,
    instrux + 404,
    instrux + 415,
    instrux + 438,
    instrux + 439,
};

static const struct itemplate * const itable_DC[] = {
    instrux + 269,
    instrux + 270,
    instrux + 272,
    instrux + 299,
    instrux + 307,
    instrux + 315,
    instrux + 316,
    instrux + 318,
    instrux + 323,
    instrux + 324,
    instrux + 325,
    instrux + 379,
    instrux + 380,
    instrux + 381,
    instrux + 420,
    instrux + 421,
    instrux + 422,
    instrux + 428,
    instrux + 429,
    instrux + 430,
};

static const struct itemplate * const itable_DD[] = {
    instrux + 332,
    instrux + 333,
    instrux + 360,
    instrux + 366,
    instrux + 391,
    instrux + 394,
    instrux + 401,
    instrux + 402,
    instrux + 409,
    instrux + 410,
    instrux + 414,
    instrux + 416,
    instrux + 417,
    instrux + 436,
    instrux + 437,
    instrux + 442,
    instrux + 443,
};

static const struct itemplate * const itable_DE[] = {
    instrux + 274,
    instrux + 275,
    instrux + 310,
    instrux + 320,
    instrux + 321,
    instrux + 328,
    instrux + 329,
    instrux + 337,
    instrux + 339,
    instrux + 341,
    instrux + 343,
    instrux + 345,
    instrux + 350,
    instrux + 362,
    instrux + 364,
    instrux + 384,
    instrux + 385,
    instrux + 425,
    instrux + 426,
    instrux + 433,
    instrux + 434,
};

static const struct itemplate * const itable_DF[] = {
    instrux + 276,
    instrux + 277,
    instrux + 278,
    instrux + 279,
    instrux + 304,
    instrux + 305,
    instrux + 334,
    instrux + 335,
    instrux + 347,
    instrux + 348,
    instrux + 354,
    instrux + 356,
    instrux + 357,
    instrux + 358,
    instrux + 395,
    instrux + 418,
    instrux + 440,
    instrux + 441,
};

static const struct itemplate * const itable_E0[] = {
    instrux + 593,
    instrux + 594,
    instrux + 595,
    instrux + 596,
    instrux + 597,
    instrux + 598,
    instrux + 599,
    instrux + 600,
};

static const struct itemplate * const itable_E1[] = {
    instrux + 589,
    instrux + 590,
    instrux + 591,
    instrux + 592,
    instrux + 601,
    instrux + 602,
    instrux + 603,
    instrux + 604,
};

static const struct itemplate * const itable_E2[] = {
    instrux + 585,
    instrux + 586,
    instrux + 587,
    instrux + 588,
};

static const struct itemplate * const itable_E3[] = {
    instrux + 514,
    instrux + 515,
    instrux + 546,
};

static const struct itemplate * const itable_E4[] = {
    instrux + 485,
};

static const struct itemplate * const itable_E5[] = {
    instrux + 486,
    instrux + 487,
};

static const struct itemplate * const itable_E6[] = {
    instrux + 751,
};

static const struct itemplate * const itable_E7[] = {
    instrux + 752,
    instrux + 753,
};

static const struct itemplate * const itable_E8[] = {
    instrux + 160,
    instrux + 161,
    instrux + 162,
    instrux + 163,
    instrux + 164,
    instrux + 165,
};

static const struct itemplate * const itable_E9[] = {
    instrux + 517,
    instrux + 518,
    instrux + 519,
};

static const struct itemplate * const itable_EA[] = {
    instrux + 520,
    instrux + 521,
    instrux + 522,
    instrux + 523,
    instrux + 524,
};

static const struct itemplate * const itable_EB[] = {
    instrux + 516,
};

static const struct itemplate * const itable_EC[] = {
    instrux + 488,
};

static const struct itemplate * const itable_ED[] = {
    instrux + 489,
    instrux + 490,
};

static const struct itemplate * const itable_EE[] = {
    instrux + 754,
};

static const struct itemplate * const itable_EF[] = {
    instrux + 755,
    instrux + 756,
};

static const struct itemplate * const itable_F1[] = {
    instrux + 501,
    instrux + 1053,
};

static const struct itemplate * const itable_F4[] = {
    instrux + 452,
};

static const struct itemplate * const itable_F5[] = {
    instrux + 195,
};

static const struct itemplate * const itable_F6[] = {
    instrux + 257,
    instrux + 453,
    instrux + 457,
    instrux + 701,
    instrux + 706,
    instrux + 714,
    instrux + 1127,
    instrux + 1131,
};

static const struct itemplate * const itable_F7[] = {
    instrux + 258,
    instrux + 259,
    instrux + 260,
    instrux + 454,
    instrux + 455,
    instrux + 456,
    instrux + 458,
    instrux + 459,
    instrux + 460,
    instrux + 702,
    instrux + 703,
    instrux + 704,
    instrux + 707,
    instrux + 708,
    instrux + 709,
    instrux + 715,
    instrux + 716,
    instrux + 717,
    instrux + 1128,
    instrux + 1129,
    instrux + 1130,
    instrux + 1132,
    instrux + 1133,
};

static const struct itemplate * const itable_F8[] = {
    instrux + 190,
};

static const struct itemplate * const itable_F9[] = {
    instrux + 1058,
};

static const struct itemplate * const itable_FA[] = {
    instrux + 193,
};

static const struct itemplate * const itable_FB[] = {
    instrux + 1061,
};

static const struct itemplate * const itable_FC[] = {
    instrux + 191,
};

static const struct itemplate * const itable_FD[] = {
    instrux + 1059,
};

static const struct itemplate * const itable_FE[] = {
    instrux + 253,
    instrux + 493,
};

static const struct itemplate * const itable_FF[] = {
    instrux + 171,
    instrux + 172,
    instrux + 173,
    instrux + 174,
    instrux + 175,
    instrux + 176,
    instrux + 177,
    instrux + 178,
    instrux + 179,
    instrux + 180,
    instrux + 181,
    instrux + 182,
    instrux + 183,
    instrux + 184,
    instrux + 185,
    instrux + 186,
    instrux + 254,
    instrux + 255,
    instrux + 256,
    instrux + 494,
    instrux + 495,
    instrux + 496,
    instrux + 525,
    instrux + 526,
    instrux + 527,
    instrux + 528,
    instrux + 529,
    instrux + 530,
    instrux + 531,
    instrux + 532,
    instrux + 533,
    instrux + 534,
    instrux + 535,
    instrux + 536,
    instrux + 537,
    instrux + 538,
    instrux + 539,
    instrux + 540,
    instrux + 863,
    instrux + 864,
    instrux + 865,
};

static const struct itemplate * const itable_VEX01010[] = {
    instrux + 2630,
};

static const struct itemplate * const itable_VEX01011[] = {
    instrux + 2631,
};

static const struct itemplate * const itable_VEX01012[] = {
    instrux + 2578,
    instrux + 2579,
    instrux + 2591,
    instrux + 2592,
};

static const struct itemplate * const itable_VEX01013[] = {
    instrux + 2593,
};

static const struct itemplate * const itable_VEX01014[] = {
    instrux + 3001,
    instrux + 3002,
};

static const struct itemplate * const itable_VEX01015[] = {
    instrux + 2993,
    instrux + 2994,
};

static const struct itemplate * const itable_VEX01016[] = {
    instrux + 2583,
    instrux + 2584,
    instrux + 2586,
    instrux + 2587,
};

static const struct itemplate * const itable_VEX01017[] = {
    instrux + 2585,
};

static const struct itemplate * const itable_VEX01028[] = {
    instrux + 2554,
};

static const struct itemplate * const itable_VEX01029[] = {
    instrux + 2555,
};

static const struct itemplate * const itable_VEX0102B[] = {
    instrux + 2608,
};

static const struct itemplate * const itable_VEX0102E[] = {
    instrux + 2988,
};

static const struct itemplate * const itable_VEX0102F[] = {
    instrux + 2431,
};

static const struct itemplate * const itable_VEX01050[] = {
    instrux + 2598,
    instrux + 2599,
};

static const struct itemplate * const itable_VEX01051[] = {
    instrux + 2964,
};

static const struct itemplate * const itable_VEX01052[] = {
    instrux + 2942,
};

static const struct itemplate * const itable_VEX01053[] = {
    instrux + 2938,
};

static const struct itemplate * const itable_VEX01054[] = {
    instrux + 2002,
    instrux + 2003,
};

static const struct itemplate * const itable_VEX01055[] = {
    instrux + 2010,
    instrux + 2011,
};

static const struct itemplate * const itable_VEX01056[] = {
    instrux + 2652,
    instrux + 2653,
};

static const struct itemplate * const itable_VEX01057[] = {
    instrux + 3009,
    instrux + 3010,
};

static const struct itemplate * const itable_VEX01058[] = {
    instrux + 1982,
    instrux + 1983,
};

static const struct itemplate * const itable_VEX01059[] = {
    instrux + 2640,
    instrux + 2641,
};

static const struct itemplate * const itable_VEX0105A[] = {
    instrux + 2446,
};

static const struct itemplate * const itable_VEX0105B[] = {
    instrux + 2434,
};

static const struct itemplate * const itable_VEX0105C[] = {
    instrux + 2975,
    instrux + 2976,
};

static const struct itemplate * const itable_VEX0105D[] = {
    instrux + 2542,
    instrux + 2543,
};

static const struct itemplate * const itable_VEX0105E[] = {
    instrux + 2478,
    instrux + 2479,
};

static const struct itemplate * const itable_VEX0105F[] = {
    instrux + 2530,
    instrux + 2531,
};

static const struct itemplate * const itable_VEX01077[] = {
    instrux + 3014,
};

static const struct itemplate * const itable_VEX010AE[] = {
    instrux + 2516,
    instrux + 2970,
};

static const struct itemplate * const itable_VEX010C2[] = {
    instrux + 2166,
    instrux + 2167,
    instrux + 2170,
    instrux + 2171,
    instrux + 2174,
    instrux + 2175,
    instrux + 2178,
    instrux + 2179,
    instrux + 2182,
    instrux + 2183,
    instrux + 2186,
    instrux + 2187,
    instrux + 2190,
    instrux + 2191,
    instrux + 2194,
    instrux + 2195,
    instrux + 2198,
    instrux + 2199,
    instrux + 2202,
    instrux + 2203,
    instrux + 2206,
    instrux + 2207,
    instrux + 2210,
    instrux + 2211,
    instrux + 2214,
    instrux + 2215,
    instrux + 2218,
    instrux + 2219,
    instrux + 2222,
    instrux + 2223,
    instrux + 2226,
    instrux + 2227,
    instrux + 2230,
    instrux + 2231,
    instrux + 2234,
    instrux + 2235,
    instrux + 2238,
    instrux + 2239,
    instrux + 2242,
    instrux + 2243,
    instrux + 2246,
    instrux + 2247,
    instrux + 2250,
    instrux + 2251,
    instrux + 2254,
    instrux + 2255,
    instrux + 2258,
    instrux + 2259,
    instrux + 2262,
    instrux + 2263,
    instrux + 2266,
    instrux + 2267,
    instrux + 2270,
    instrux + 2271,
    instrux + 2274,
    instrux + 2275,
    instrux + 2278,
    instrux + 2279,
    instrux + 2282,
    instrux + 2283,
    instrux + 2286,
    instrux + 2287,
    instrux + 2290,
    instrux + 2291,
    instrux + 2294,
    instrux + 2295,
};

static const struct itemplate * const itable_VEX010C6[] = {
    instrux + 2958,
    instrux + 2959,
};

static const struct itemplate * const itable_VEX01110[] = {
    instrux + 2626,
};

static const struct itemplate * const itable_VEX01111[] = {
    instrux + 2627,
};

static const struct itemplate * const itable_VEX01112[] = {
    instrux + 2588,
    instrux + 2589,
};

static const struct itemplate * const itable_VEX01113[] = {
    instrux + 2590,
};

static const struct itemplate * const itable_VEX01114[] = {
    instrux + 2997,
    instrux + 2998,
};

static const struct itemplate * const itable_VEX01115[] = {
    instrux + 2989,
    instrux + 2990,
};

static const struct itemplate * const itable_VEX01116[] = {
    instrux + 2580,
    instrux + 2581,
};

static const struct itemplate * const itable_VEX01117[] = {
    instrux + 2582,
};

static const struct itemplate * const itable_VEX01128[] = {
    instrux + 2550,
};

static const struct itemplate * const itable_VEX01129[] = {
    instrux + 2551,
    instrux + 2707,
    instrux + 2708,
};

static const struct itemplate * const itable_VEX0112B[] = {
    instrux + 2606,
};

static const struct itemplate * const itable_VEX0112E[] = {
    instrux + 2987,
};

static const struct itemplate * const itable_VEX0112F[] = {
    instrux + 2430,
};

static const struct itemplate * const itable_VEX01137[] = {
    instrux + 2715,
    instrux + 2716,
};

static const struct itemplate * const itable_VEX01150[] = {
    instrux + 2594,
    instrux + 2595,
};

static const struct itemplate * const itable_VEX01151[] = {
    instrux + 2962,
};

static const struct itemplate * const itable_VEX01154[] = {
    instrux + 1998,
    instrux + 1999,
};

static const struct itemplate * const itable_VEX01155[] = {
    instrux + 2006,
    instrux + 2007,
};

static const struct itemplate * const itable_VEX01156[] = {
    instrux + 2648,
    instrux + 2649,
};

static const struct itemplate * const itable_VEX01157[] = {
    instrux + 3005,
    instrux + 3006,
};

static const struct itemplate * const itable_VEX01158[] = {
    instrux + 1978,
    instrux + 1979,
};

static const struct itemplate * const itable_VEX01159[] = {
    instrux + 2636,
    instrux + 2637,
};

static const struct itemplate * const itable_VEX0115A[] = {
    instrux + 2440,
    instrux + 2441,
};

static const struct itemplate * const itable_VEX0115B[] = {
    instrux + 2444,
};

static const struct itemplate * const itable_VEX0115C[] = {
    instrux + 2971,
    instrux + 2972,
};

static const struct itemplate * const itable_VEX0115D[] = {
    instrux + 2538,
    instrux + 2539,
};

static const struct itemplate * const itable_VEX0115E[] = {
    instrux + 2474,
    instrux + 2475,
};

static const struct itemplate * const itable_VEX0115F[] = {
    instrux + 2526,
    instrux + 2527,
};

static const struct itemplate * const itable_VEX01160[] = {
    instrux + 2928,
    instrux + 2929,
};

static const struct itemplate * const itable_VEX01161[] = {
    instrux + 2930,
    instrux + 2931,
};

static const struct itemplate * const itable_VEX01162[] = {
    instrux + 2932,
    instrux + 2933,
};

static const struct itemplate * const itable_VEX01163[] = {
    instrux + 2659,
    instrux + 2660,
};

static const struct itemplate * const itable_VEX01164[] = {
    instrux + 2709,
    instrux + 2710,
};

static const struct itemplate * const itable_VEX01165[] = {
    instrux + 2711,
    instrux + 2712,
};

static const struct itemplate * const itable_VEX01166[] = {
    instrux + 2713,
    instrux + 2714,
};

static const struct itemplate * const itable_VEX01167[] = {
    instrux + 2663,
    instrux + 2664,
};

static const struct itemplate * const itable_VEX01168[] = {
    instrux + 2920,
    instrux + 2921,
};

static const struct itemplate * const itable_VEX01169[] = {
    instrux + 2922,
    instrux + 2923,
};

static const struct itemplate * const itable_VEX0116A[] = {
    instrux + 2924,
    instrux + 2925,
};

static const struct itemplate * const itable_VEX0116B[] = {
    instrux + 2661,
    instrux + 2662,
};

static const struct itemplate * const itable_VEX0116C[] = {
    instrux + 2934,
    instrux + 2935,
};

static const struct itemplate * const itable_VEX0116D[] = {
    instrux + 2926,
    instrux + 2927,
};

static const struct itemplate * const itable_VEX0116E[] = {
    instrux + 2560,
    instrux + 2561,
};

static const struct itemplate * const itable_VEX0116F[] = {
    instrux + 2566,
};

static const struct itemplate * const itable_VEX01170[] = {
    instrux + 2857,
};

static const struct itemplate * const itable_VEX01171[] = {
    instrux + 2872,
    instrux + 2873,
    instrux + 2884,
    instrux + 2885,
    instrux + 2892,
    instrux + 2893,
};

static const struct itemplate * const itable_VEX01172[] = {
    instrux + 2876,
    instrux + 2877,
    instrux + 2888,
    instrux + 2889,
    instrux + 2896,
    instrux + 2897,
};

static const struct itemplate * const itable_VEX01173[] = {
    instrux + 2866,
    instrux + 2867,
    instrux + 2868,
    instrux + 2869,
    instrux + 2880,
    instrux + 2881,
    instrux + 2900,
    instrux + 2901,
};

static const struct itemplate * const itable_VEX01174[] = {
    instrux + 2701,
    instrux + 2702,
};

static const struct itemplate * const itable_VEX01175[] = {
    instrux + 2703,
    instrux + 2704,
};

static const struct itemplate * const itable_VEX01176[] = {
    instrux + 2705,
    instrux + 2706,
};

static const struct itemplate * const itable_VEX0117C[] = {
    instrux + 2494,
    instrux + 2495,
};

static const struct itemplate * const itable_VEX0117D[] = {
    instrux + 2502,
    instrux + 2503,
};

static const struct itemplate * const itable_VEX0117E[] = {
    instrux + 2562,
    instrux + 2563,
};

static const struct itemplate * const itable_VEX0117F[] = {
    instrux + 2567,
};

static const struct itemplate * const itable_VEX011C2[] = {
    instrux + 2034,
    instrux + 2035,
    instrux + 2038,
    instrux + 2039,
    instrux + 2042,
    instrux + 2043,
    instrux + 2046,
    instrux + 2047,
    instrux + 2050,
    instrux + 2051,
    instrux + 2054,
    instrux + 2055,
    instrux + 2058,
    instrux + 2059,
    instrux + 2062,
    instrux + 2063,
    instrux + 2066,
    instrux + 2067,
    instrux + 2070,
    instrux + 2071,
    instrux + 2074,
    instrux + 2075,
    instrux + 2078,
    instrux + 2079,
    instrux + 2082,
    instrux + 2083,
    instrux + 2086,
    instrux + 2087,
    instrux + 2090,
    instrux + 2091,
    instrux + 2094,
    instrux + 2095,
    instrux + 2098,
    instrux + 2099,
    instrux + 2102,
    instrux + 2103,
    instrux + 2106,
    instrux + 2107,
    instrux + 2110,
    instrux + 2111,
    instrux + 2114,
    instrux + 2115,
    instrux + 2118,
    instrux + 2119,
    instrux + 2122,
    instrux + 2123,
    instrux + 2126,
    instrux + 2127,
    instrux + 2130,
    instrux + 2131,
    instrux + 2134,
    instrux + 2135,
    instrux + 2138,
    instrux + 2139,
    instrux + 2142,
    instrux + 2143,
    instrux + 2146,
    instrux + 2147,
    instrux + 2150,
    instrux + 2151,
    instrux + 2154,
    instrux + 2155,
    instrux + 2158,
    instrux + 2159,
    instrux + 2162,
    instrux + 2163,
};

static const struct itemplate * const itable_VEX011C4[] = {
    instrux + 2787,
    instrux + 2788,
    instrux + 2789,
    instrux + 2790,
};

static const struct itemplate * const itable_VEX011C5[] = {
    instrux + 2761,
    instrux + 2762,
    instrux + 2763,
};

static const struct itemplate * const itable_VEX011C6[] = {
    instrux + 2954,
    instrux + 2955,
};

static const struct itemplate * const itable_VEX011D0[] = {
    instrux + 1990,
    instrux + 1991,
};

static const struct itemplate * const itable_VEX011D1[] = {
    instrux + 2890,
    instrux + 2891,
};

static const struct itemplate * const itable_VEX011D2[] = {
    instrux + 2894,
    instrux + 2895,
};

static const struct itemplate * const itable_VEX011D3[] = {
    instrux + 2898,
    instrux + 2899,
};

static const struct itemplate * const itable_VEX011D4[] = {
    instrux + 2673,
    instrux + 2674,
};

static const struct itemplate * const itable_VEX011D5[] = {
    instrux + 2843,
    instrux + 2844,
};

static const struct itemplate * const itable_VEX011D6[] = {
    instrux + 2559,
};

static const struct itemplate * const itable_VEX011D7[] = {
    instrux + 2823,
    instrux + 2824,
};

static const struct itemplate * const itable_VEX011D8[] = {
    instrux + 2916,
    instrux + 2917,
};

static const struct itemplate * const itable_VEX011D9[] = {
    instrux + 2918,
    instrux + 2919,
};

static const struct itemplate * const itable_VEX011DA[] = {
    instrux + 2817,
    instrux + 2818,
};

static const struct itemplate * const itable_VEX011DB[] = {
    instrux + 2685,
    instrux + 2686,
};

static const struct itemplate * const itable_VEX011DC[] = {
    instrux + 2679,
    instrux + 2680,
};

static const struct itemplate * const itable_VEX011DD[] = {
    instrux + 2681,
    instrux + 2682,
};

static const struct itemplate * const itable_VEX011DE[] = {
    instrux + 2805,
    instrux + 2806,
};

static const struct itemplate * const itable_VEX011DF[] = {
    instrux + 2687,
    instrux + 2688,
};

static const struct itemplate * const itable_VEX011E0[] = {
    instrux + 2689,
    instrux + 2690,
};

static const struct itemplate * const itable_VEX011E1[] = {
    instrux + 2882,
    instrux + 2883,
};

static const struct itemplate * const itable_VEX011E2[] = {
    instrux + 2886,
    instrux + 2887,
};

static const struct itemplate * const itable_VEX011E3[] = {
    instrux + 2691,
    instrux + 2692,
};

static const struct itemplate * const itable_VEX011E4[] = {
    instrux + 2837,
    instrux + 2838,
};

static const struct itemplate * const itable_VEX011E5[] = {
    instrux + 2841,
    instrux + 2842,
};

static const struct itemplate * const itable_VEX011E6[] = {
    instrux + 2464,
    instrux + 2465,
};

static const struct itemplate * const itable_VEX011E7[] = {
    instrux + 2602,
};

static const struct itemplate * const itable_VEX011E8[] = {
    instrux + 2912,
    instrux + 2913,
};

static const struct itemplate * const itable_VEX011E9[] = {
    instrux + 2914,
    instrux + 2915,
};

static const struct itemplate * const itable_VEX011EA[] = {
    instrux + 2813,
    instrux + 2814,
};

static const struct itemplate * const itable_VEX011EB[] = {
    instrux + 2851,
    instrux + 2852,
};

static const struct itemplate * const itable_VEX011EC[] = {
    instrux + 2675,
    instrux + 2676,
};

static const struct itemplate * const itable_VEX011ED[] = {
    instrux + 2677,
    instrux + 2678,
};

static const struct itemplate * const itable_VEX011EE[] = {
    instrux + 2801,
    instrux + 2802,
};

static const struct itemplate * const itable_VEX011EF[] = {
    instrux + 2936,
    instrux + 2937,
};

static const struct itemplate * const itable_VEX011F1[] = {
    instrux + 2870,
    instrux + 2871,
};

static const struct itemplate * const itable_VEX011F2[] = {
    instrux + 2874,
    instrux + 2875,
};

static const struct itemplate * const itable_VEX011F3[] = {
    instrux + 2878,
    instrux + 2879,
};

static const struct itemplate * const itable_VEX011F4[] = {
    instrux + 2847,
    instrux + 2848,
};

static const struct itemplate * const itable_VEX011F5[] = {
    instrux + 2795,
    instrux + 2796,
};

static const struct itemplate * const itable_VEX011F6[] = {
    instrux + 2853,
    instrux + 2854,
};

static const struct itemplate * const itable_VEX011F7[] = {
    instrux + 2517,
};

static const struct itemplate * const itable_VEX011F8[] = {
    instrux + 2904,
    instrux + 2905,
};

static const struct itemplate * const itable_VEX011F9[] = {
    instrux + 2906,
    instrux + 2907,
};

static const struct itemplate * const itable_VEX011FA[] = {
    instrux + 2908,
    instrux + 2909,
};

static const struct itemplate * const itable_VEX011FB[] = {
    instrux + 2910,
    instrux + 2911,
};

static const struct itemplate * const itable_VEX011FC[] = {
    instrux + 2667,
    instrux + 2668,
};

static const struct itemplate * const itable_VEX011FD[] = {
    instrux + 2669,
    instrux + 2670,
};

static const struct itemplate * const itable_VEX011FE[] = {
    instrux + 2671,
    instrux + 2672,
};

static const struct itemplate * const itable_VEX01210[] = {
    instrux + 2620,
    instrux + 2621,
    instrux + 2622,
};

static const struct itemplate * const itable_VEX01211[] = {
    instrux + 2623,
    instrux + 2624,
    instrux + 2625,
};

static const struct itemplate * const itable_VEX01212[] = {
    instrux + 2618,
};

static const struct itemplate * const itable_VEX01216[] = {
    instrux + 2616,
};

static const struct itemplate * const itable_VEX0122A[] = {
    instrux + 2456,
    instrux + 2457,
    instrux + 2458,
    instrux + 2459,
};

static const struct itemplate * const itable_VEX0122C[] = {
    instrux + 2472,
    instrux + 2473,
};

static const struct itemplate * const itable_VEX0122D[] = {
    instrux + 2462,
    instrux + 2463,
};

static const struct itemplate * const itable_VEX01251[] = {
    instrux + 2968,
    instrux + 2969,
};

static const struct itemplate * const itable_VEX01252[] = {
    instrux + 2944,
    instrux + 2945,
};

static const struct itemplate * const itable_VEX01253[] = {
    instrux + 2940,
    instrux + 2941,
};

static const struct itemplate * const itable_VEX01258[] = {
    instrux + 1988,
    instrux + 1989,
};

static const struct itemplate * const itable_VEX01259[] = {
    instrux + 2646,
    instrux + 2647,
};

static const struct itemplate * const itable_VEX0125A[] = {
    instrux + 2460,
    instrux + 2461,
};

static const struct itemplate * const itable_VEX0125B[] = {
    instrux + 2468,
};

static const struct itemplate * const itable_VEX0125C[] = {
    instrux + 2981,
    instrux + 2982,
};

static const struct itemplate * const itable_VEX0125D[] = {
    instrux + 2548,
    instrux + 2549,
};

static const struct itemplate * const itable_VEX0125E[] = {
    instrux + 2484,
    instrux + 2485,
};

static const struct itemplate * const itable_VEX0125F[] = {
    instrux + 2536,
    instrux + 2537,
};

static const struct itemplate * const itable_VEX0126F[] = {
    instrux + 2572,
};

static const struct itemplate * const itable_VEX01270[] = {
    instrux + 2858,
};

static const struct itemplate * const itable_VEX0127E[] = {
    instrux + 2558,
};

static const struct itemplate * const itable_VEX0127F[] = {
    instrux + 2573,
};

static const struct itemplate * const itable_VEX012C2[] = {
    instrux + 2364,
    instrux + 2365,
    instrux + 2366,
    instrux + 2367,
    instrux + 2368,
    instrux + 2369,
    instrux + 2370,
    instrux + 2371,
    instrux + 2372,
    instrux + 2373,
    instrux + 2374,
    instrux + 2375,
    instrux + 2376,
    instrux + 2377,
    instrux + 2378,
    instrux + 2379,
    instrux + 2380,
    instrux + 2381,
    instrux + 2382,
    instrux + 2383,
    instrux + 2384,
    instrux + 2385,
    instrux + 2386,
    instrux + 2387,
    instrux + 2388,
    instrux + 2389,
    instrux + 2390,
    instrux + 2391,
    instrux + 2392,
    instrux + 2393,
    instrux + 2394,
    instrux + 2395,
    instrux + 2396,
    instrux + 2397,
    instrux + 2398,
    instrux + 2399,
    instrux + 2400,
    instrux + 2401,
    instrux + 2402,
    instrux + 2403,
    instrux + 2404,
    instrux + 2405,
    instrux + 2406,
    instrux + 2407,
    instrux + 2408,
    instrux + 2409,
    instrux + 2410,
    instrux + 2411,
    instrux + 2412,
    instrux + 2413,
    instrux + 2414,
    instrux + 2415,
    instrux + 2416,
    instrux + 2417,
    instrux + 2418,
    instrux + 2419,
    instrux + 2420,
    instrux + 2421,
    instrux + 2422,
    instrux + 2423,
    instrux + 2424,
    instrux + 2425,
    instrux + 2426,
    instrux + 2427,
    instrux + 2428,
    instrux + 2429,
};

static const struct itemplate * const itable_VEX012E6[] = {
    instrux + 2432,
};

static const struct itemplate * const itable_VEX01310[] = {
    instrux + 2610,
    instrux + 2611,
    instrux + 2612,
};

static const struct itemplate * const itable_VEX01311[] = {
    instrux + 2613,
    instrux + 2614,
    instrux + 2615,
};

static const struct itemplate * const itable_VEX01312[] = {
    instrux + 2564,
};

static const struct itemplate * const itable_VEX0132A[] = {
    instrux + 2452,
    instrux + 2453,
    instrux + 2454,
    instrux + 2455,
};

static const struct itemplate * const itable_VEX0132C[] = {
    instrux + 2470,
    instrux + 2471,
};

static const struct itemplate * const itable_VEX0132D[] = {
    instrux + 2448,
    instrux + 2449,
};

static const struct itemplate * const itable_VEX01351[] = {
    instrux + 2966,
    instrux + 2967,
};

static const struct itemplate * const itable_VEX01358[] = {
    instrux + 1986,
    instrux + 1987,
};

static const struct itemplate * const itable_VEX01359[] = {
    instrux + 2644,
    instrux + 2645,
};

static const struct itemplate * const itable_VEX0135A[] = {
    instrux + 2450,
    instrux + 2451,
};

static const struct itemplate * const itable_VEX0135C[] = {
    instrux + 2979,
    instrux + 2980,
};

static const struct itemplate * const itable_VEX0135D[] = {
    instrux + 2546,
    instrux + 2547,
};

static const struct itemplate * const itable_VEX0135E[] = {
    instrux + 2482,
    instrux + 2483,
};

static const struct itemplate * const itable_VEX0135F[] = {
    instrux + 2534,
    instrux + 2535,
};

static const struct itemplate * const itable_VEX01370[] = {
    instrux + 2859,
};

static const struct itemplate * const itable_VEX0137C[] = {
    instrux + 2498,
    instrux + 2499,
};

static const struct itemplate * const itable_VEX0137D[] = {
    instrux + 2506,
    instrux + 2507,
};

static const struct itemplate * const itable_VEX013C2[] = {
    instrux + 2298,
    instrux + 2299,
    instrux + 2300,
    instrux + 2301,
    instrux + 2302,
    instrux + 2303,
    instrux + 2304,
    instrux + 2305,
    instrux + 2306,
    instrux + 2307,
    instrux + 2308,
    instrux + 2309,
    instrux + 2310,
    instrux + 2311,
    instrux + 2312,
    instrux + 2313,
    instrux + 2314,
    instrux + 2315,
    instrux + 2316,
    instrux + 2317,
    instrux + 2318,
    instrux + 2319,
    instrux + 2320,
    instrux + 2321,
    instrux + 2322,
    instrux + 2323,
    instrux + 2324,
    instrux + 2325,
    instrux + 2326,
    instrux + 2327,
    instrux + 2328,
    instrux + 2329,
    instrux + 2330,
    instrux + 2331,
    instrux + 2332,
    instrux + 2333,
    instrux + 2334,
    instrux + 2335,
    instrux + 2336,
    instrux + 2337,
    instrux + 2338,
    instrux + 2339,
    instrux + 2340,
    instrux + 2341,
    instrux + 2342,
    instrux + 2343,
    instrux + 2344,
    instrux + 2345,
    instrux + 2346,
    instrux + 2347,
    instrux + 2348,
    instrux + 2349,
    instrux + 2350,
    instrux + 2351,
    instrux + 2352,
    instrux + 2353,
    instrux + 2354,
    instrux + 2355,
    instrux + 2356,
    instrux + 2357,
    instrux + 2358,
    instrux + 2359,
    instrux + 2360,
    instrux + 2361,
    instrux + 2362,
    instrux + 2363,
};

static const struct itemplate * const itable_VEX013D0[] = {
    instrux + 1994,
    instrux + 1995,
};

static const struct itemplate * const itable_VEX013E6[] = {
    instrux + 2436,
    instrux + 2437,
};

static const struct itemplate * const itable_VEX013F0[] = {
    instrux + 2513,
};

static const struct itemplate * const itable_VEX01410[] = {
    instrux + 2632,
};

static const struct itemplate * const itable_VEX01411[] = {
    instrux + 2633,
};

static const struct itemplate * const itable_VEX01414[] = {
    instrux + 3003,
    instrux + 3004,
};

static const struct itemplate * const itable_VEX01415[] = {
    instrux + 2995,
    instrux + 2996,
};

static const struct itemplate * const itable_VEX01428[] = {
    instrux + 2556,
};

static const struct itemplate * const itable_VEX01429[] = {
    instrux + 2557,
};

static const struct itemplate * const itable_VEX0142B[] = {
    instrux + 2609,
};

static const struct itemplate * const itable_VEX01450[] = {
    instrux + 2600,
    instrux + 2601,
};

static const struct itemplate * const itable_VEX01451[] = {
    instrux + 2965,
};

static const struct itemplate * const itable_VEX01452[] = {
    instrux + 2943,
};

static const struct itemplate * const itable_VEX01453[] = {
    instrux + 2939,
};

static const struct itemplate * const itable_VEX01454[] = {
    instrux + 2004,
    instrux + 2005,
};

static const struct itemplate * const itable_VEX01455[] = {
    instrux + 2012,
    instrux + 2013,
};

static const struct itemplate * const itable_VEX01456[] = {
    instrux + 2654,
    instrux + 2655,
};

static const struct itemplate * const itable_VEX01457[] = {
    instrux + 3011,
    instrux + 3012,
};

static const struct itemplate * const itable_VEX01458[] = {
    instrux + 1984,
    instrux + 1985,
};

static const struct itemplate * const itable_VEX01459[] = {
    instrux + 2642,
    instrux + 2643,
};

static const struct itemplate * const itable_VEX0145A[] = {
    instrux + 2447,
};

static const struct itemplate * const itable_VEX0145B[] = {
    instrux + 2435,
};

static const struct itemplate * const itable_VEX0145C[] = {
    instrux + 2977,
    instrux + 2978,
};

static const struct itemplate * const itable_VEX0145D[] = {
    instrux + 2544,
    instrux + 2545,
};

static const struct itemplate * const itable_VEX0145E[] = {
    instrux + 2480,
    instrux + 2481,
};

static const struct itemplate * const itable_VEX0145F[] = {
    instrux + 2532,
    instrux + 2533,
};

static const struct itemplate * const itable_VEX01477[] = {
    instrux + 3013,
};

static const struct itemplate * const itable_VEX014C2[] = {
    instrux + 2168,
    instrux + 2169,
    instrux + 2172,
    instrux + 2173,
    instrux + 2176,
    instrux + 2177,
    instrux + 2180,
    instrux + 2181,
    instrux + 2184,
    instrux + 2185,
    instrux + 2188,
    instrux + 2189,
    instrux + 2192,
    instrux + 2193,
    instrux + 2196,
    instrux + 2197,
    instrux + 2200,
    instrux + 2201,
    instrux + 2204,
    instrux + 2205,
    instrux + 2208,
    instrux + 2209,
    instrux + 2212,
    instrux + 2213,
    instrux + 2216,
    instrux + 2217,
    instrux + 2220,
    instrux + 2221,
    instrux + 2224,
    instrux + 2225,
    instrux + 2228,
    instrux + 2229,
    instrux + 2232,
    instrux + 2233,
    instrux + 2236,
    instrux + 2237,
    instrux + 2240,
    instrux + 2241,
    instrux + 2244,
    instrux + 2245,
    instrux + 2248,
    instrux + 2249,
    instrux + 2252,
    instrux + 2253,
    instrux + 2256,
    instrux + 2257,
    instrux + 2260,
    instrux + 2261,
    instrux + 2264,
    instrux + 2265,
    instrux + 2268,
    instrux + 2269,
    instrux + 2272,
    instrux + 2273,
    instrux + 2276,
    instrux + 2277,
    instrux + 2280,
    instrux + 2281,
    instrux + 2284,
    instrux + 2285,
    instrux + 2288,
    instrux + 2289,
    instrux + 2292,
    instrux + 2293,
    instrux + 2296,
    instrux + 2297,
};

static const struct itemplate * const itable_VEX014C6[] = {
    instrux + 2960,
    instrux + 2961,
};

static const struct itemplate * const itable_VEX01510[] = {
    instrux + 2628,
};

static const struct itemplate * const itable_VEX01511[] = {
    instrux + 2629,
};

static const struct itemplate * const itable_VEX01514[] = {
    instrux + 2999,
    instrux + 3000,
};

static const struct itemplate * const itable_VEX01515[] = {
    instrux + 2991,
    instrux + 2992,
};

static const struct itemplate * const itable_VEX01528[] = {
    instrux + 2552,
};

static const struct itemplate * const itable_VEX01529[] = {
    instrux + 2553,
};

static const struct itemplate * const itable_VEX0152B[] = {
    instrux + 2607,
};

static const struct itemplate * const itable_VEX01550[] = {
    instrux + 2596,
    instrux + 2597,
};

static const struct itemplate * const itable_VEX01551[] = {
    instrux + 2963,
};

static const struct itemplate * const itable_VEX01554[] = {
    instrux + 2000,
    instrux + 2001,
};

static const struct itemplate * const itable_VEX01555[] = {
    instrux + 2008,
    instrux + 2009,
};

static const struct itemplate * const itable_VEX01556[] = {
    instrux + 2650,
    instrux + 2651,
};

static const struct itemplate * const itable_VEX01557[] = {
    instrux + 3007,
    instrux + 3008,
};

static const struct itemplate * const itable_VEX01558[] = {
    instrux + 1980,
    instrux + 1981,
};

static const struct itemplate * const itable_VEX01559[] = {
    instrux + 2638,
    instrux + 2639,
};

static const struct itemplate * const itable_VEX0155A[] = {
    instrux + 2442,
    instrux + 2443,
};

static const struct itemplate * const itable_VEX0155B[] = {
    instrux + 2445,
};

static const struct itemplate * const itable_VEX0155C[] = {
    instrux + 2973,
    instrux + 2974,
};

static const struct itemplate * const itable_VEX0155D[] = {
    instrux + 2540,
    instrux + 2541,
};

static const struct itemplate * const itable_VEX0155E[] = {
    instrux + 2476,
    instrux + 2477,
};

static const struct itemplate * const itable_VEX0155F[] = {
    instrux + 2528,
    instrux + 2529,
};

static const struct itemplate * const itable_VEX0156F[] = {
    instrux + 2568,
    instrux + 2570,
};

static const struct itemplate * const itable_VEX0157C[] = {
    instrux + 2496,
    instrux + 2497,
};

static const struct itemplate * const itable_VEX0157D[] = {
    instrux + 2504,
    instrux + 2505,
};

static const struct itemplate * const itable_VEX0157F[] = {
    instrux + 2569,
    instrux + 2571,
};

static const struct itemplate * const itable_VEX015C2[] = {
    instrux + 2036,
    instrux + 2037,
    instrux + 2040,
    instrux + 2041,
    instrux + 2044,
    instrux + 2045,
    instrux + 2048,
    instrux + 2049,
    instrux + 2052,
    instrux + 2053,
    instrux + 2056,
    instrux + 2057,
    instrux + 2060,
    instrux + 2061,
    instrux + 2064,
    instrux + 2065,
    instrux + 2068,
    instrux + 2069,
    instrux + 2072,
    instrux + 2073,
    instrux + 2076,
    instrux + 2077,
    instrux + 2080,
    instrux + 2081,
    instrux + 2084,
    instrux + 2085,
    instrux + 2088,
    instrux + 2089,
    instrux + 2092,
    instrux + 2093,
    instrux + 2096,
    instrux + 2097,
    instrux + 2100,
    instrux + 2101,
    instrux + 2104,
    instrux + 2105,
    instrux + 2108,
    instrux + 2109,
    instrux + 2112,
    instrux + 2113,
    instrux + 2116,
    instrux + 2117,
    instrux + 2120,
    instrux + 2121,
    instrux + 2124,
    instrux + 2125,
    instrux + 2128,
    instrux + 2129,
    instrux + 2132,
    instrux + 2133,
    instrux + 2136,
    instrux + 2137,
    instrux + 2140,
    instrux + 2141,
    instrux + 2144,
    instrux + 2145,
    instrux + 2148,
    instrux + 2149,
    instrux + 2152,
    instrux + 2153,
    instrux + 2156,
    instrux + 2157,
    instrux + 2160,
    instrux + 2161,
    instrux + 2164,
    instrux + 2165,
};

static const struct itemplate * const itable_VEX015C6[] = {
    instrux + 2956,
    instrux + 2957,
};

static const struct itemplate * const itable_VEX015D0[] = {
    instrux + 1992,
    instrux + 1993,
};

static const struct itemplate * const itable_VEX015E6[] = {
    instrux + 2466,
    instrux + 2467,
};

static const struct itemplate * const itable_VEX015E7[] = {
    instrux + 2603,
    instrux + 2604,
};

static const struct itemplate * const itable_VEX01612[] = {
    instrux + 2619,
};

static const struct itemplate * const itable_VEX01616[] = {
    instrux + 2617,
};

static const struct itemplate * const itable_VEX0165B[] = {
    instrux + 2469,
};

static const struct itemplate * const itable_VEX0166F[] = {
    instrux + 2574,
    instrux + 2576,
};

static const struct itemplate * const itable_VEX0167F[] = {
    instrux + 2575,
    instrux + 2577,
};

static const struct itemplate * const itable_VEX016E6[] = {
    instrux + 2433,
};

static const struct itemplate * const itable_VEX01712[] = {
    instrux + 2565,
};

static const struct itemplate * const itable_VEX0177C[] = {
    instrux + 2500,
    instrux + 2501,
};

static const struct itemplate * const itable_VEX0177D[] = {
    instrux + 2508,
    instrux + 2509,
};

static const struct itemplate * const itable_VEX017D0[] = {
    instrux + 1996,
    instrux + 1997,
};

static const struct itemplate * const itable_VEX017E6[] = {
    instrux + 2438,
    instrux + 2439,
};

static const struct itemplate * const itable_VEX017F0[] = {
    instrux + 2514,
    instrux + 2515,
};

static const struct itemplate * const itable_VEX02100[] = {
    instrux + 2855,
    instrux + 2856,
};

static const struct itemplate * const itable_VEX02101[] = {
    instrux + 2770,
    instrux + 2771,
};

static const struct itemplate * const itable_VEX02102[] = {
    instrux + 2772,
    instrux + 2773,
};

static const struct itemplate * const itable_VEX02103[] = {
    instrux + 2774,
    instrux + 2775,
};

static const struct itemplate * const itable_VEX02104[] = {
    instrux + 2797,
    instrux + 2798,
};

static const struct itemplate * const itable_VEX02105[] = {
    instrux + 2777,
    instrux + 2778,
};

static const struct itemplate * const itable_VEX02106[] = {
    instrux + 2779,
    instrux + 2780,
};

static const struct itemplate * const itable_VEX02107[] = {
    instrux + 2781,
    instrux + 2782,
};

static const struct itemplate * const itable_VEX02108[] = {
    instrux + 2860,
    instrux + 2861,
};

static const struct itemplate * const itable_VEX02109[] = {
    instrux + 2862,
    instrux + 2863,
};

static const struct itemplate * const itable_VEX0210A[] = {
    instrux + 2864,
    instrux + 2865,
};

static const struct itemplate * const itable_VEX0210B[] = {
    instrux + 2839,
    instrux + 2840,
};

static const struct itemplate * const itable_VEX0210C[] = {
    instrux + 2737,
};

static const struct itemplate * const itable_VEX0210D[] = {
    instrux + 2717,
};

static const struct itemplate * const itable_VEX0210E[] = {
    instrux + 2983,
};

static const struct itemplate * const itable_VEX0210F[] = {
    instrux + 2985,
};

static const struct itemplate * const itable_VEX02114[] = {
    instrux + 2027,
};

static const struct itemplate * const itable_VEX02115[] = {
    instrux + 2023,
};

static const struct itemplate * const itable_VEX02117[] = {
    instrux + 2902,
};

static const struct itemplate * const itable_VEX02118[] = {
    instrux + 2030,
};

static const struct itemplate * const itable_VEX0211C[] = {
    instrux + 2656,
};

static const struct itemplate * const itable_VEX0211D[] = {
    instrux + 2657,
};

static const struct itemplate * const itable_VEX0211E[] = {
    instrux + 2658,
};

static const struct itemplate * const itable_VEX02120[] = {
    instrux + 2825,
};

static const struct itemplate * const itable_VEX02121[] = {
    instrux + 2826,
};

static const struct itemplate * const itable_VEX02122[] = {
    instrux + 2827,
};

static const struct itemplate * const itable_VEX02123[] = {
    instrux + 2828,
};

static const struct itemplate * const itable_VEX02124[] = {
    instrux + 2829,
};

static const struct itemplate * const itable_VEX02125[] = {
    instrux + 2830,
};

static const struct itemplate * const itable_VEX02128[] = {
    instrux + 2849,
    instrux + 2850,
};

static const struct itemplate * const itable_VEX0212A[] = {
    instrux + 2605,
};

static const struct itemplate * const itable_VEX0212B[] = {
    instrux + 2665,
    instrux + 2666,
};

static const struct itemplate * const itable_VEX0212C[] = {
    instrux + 2518,
};

static const struct itemplate * const itable_VEX0212D[] = {
    instrux + 2522,
};

static const struct itemplate * const itable_VEX0212E[] = {
    instrux + 2520,
};

static const struct itemplate * const itable_VEX0212F[] = {
    instrux + 2524,
};

static const struct itemplate * const itable_VEX02130[] = {
    instrux + 2831,
};

static const struct itemplate * const itable_VEX02131[] = {
    instrux + 2832,
};

static const struct itemplate * const itable_VEX02132[] = {
    instrux + 2833,
};

static const struct itemplate * const itable_VEX02133[] = {
    instrux + 2834,
};

static const struct itemplate * const itable_VEX02134[] = {
    instrux + 2835,
};

static const struct itemplate * const itable_VEX02135[] = {
    instrux + 2836,
};

static const struct itemplate * const itable_VEX02138[] = {
    instrux + 2811,
    instrux + 2812,
};

static const struct itemplate * const itable_VEX02139[] = {
    instrux + 2815,
    instrux + 2816,
};

static const struct itemplate * const itable_VEX0213A[] = {
    instrux + 2819,
    instrux + 2820,
};

static const struct itemplate * const itable_VEX0213B[] = {
    instrux + 2821,
    instrux + 2822,
};

static const struct itemplate * const itable_VEX0213C[] = {
    instrux + 2799,
    instrux + 2800,
};

static const struct itemplate * const itable_VEX0213D[] = {
    instrux + 2803,
    instrux + 2804,
};

static const struct itemplate * const itable_VEX0213E[] = {
    instrux + 2807,
    instrux + 2808,
};

static const struct itemplate * const itable_VEX0213F[] = {
    instrux + 2809,
    instrux + 2810,
};

static const struct itemplate * const itable_VEX02140[] = {
    instrux + 2845,
    instrux + 2846,
};

static const struct itemplate * const itable_VEX02141[] = {
    instrux + 2776,
};

static const struct itemplate * const itable_VEX02196[] = {
    instrux + 3078,
    instrux + 3079,
    instrux + 3082,
    instrux + 3083,
    instrux + 3086,
    instrux + 3087,
    instrux + 3090,
    instrux + 3091,
};

static const struct itemplate * const itable_VEX02197[] = {
    instrux + 3174,
    instrux + 3175,
    instrux + 3178,
    instrux + 3179,
    instrux + 3182,
    instrux + 3183,
    instrux + 3186,
    instrux + 3187,
};

static const struct itemplate * const itable_VEX02198[] = {
    instrux + 3030,
    instrux + 3031,
    instrux + 3034,
    instrux + 3035,
    instrux + 3038,
    instrux + 3039,
    instrux + 3042,
    instrux + 3043,
};

static const struct itemplate * const itable_VEX02199[] = {
    instrux + 3318,
    instrux + 3319,
    instrux + 3320,
    instrux + 3321,
    instrux + 3322,
    instrux + 3323,
    instrux + 3324,
    instrux + 3325,
};

static const struct itemplate * const itable_VEX0219A[] = {
    instrux + 3126,
    instrux + 3127,
    instrux + 3130,
    instrux + 3131,
    instrux + 3134,
    instrux + 3135,
    instrux + 3138,
    instrux + 3139,
};

static const struct itemplate * const itable_VEX0219B[] = {
    instrux + 3342,
    instrux + 3343,
    instrux + 3344,
    instrux + 3345,
    instrux + 3346,
    instrux + 3347,
    instrux + 3348,
    instrux + 3349,
};

static const struct itemplate * const itable_VEX0219C[] = {
    instrux + 3222,
    instrux + 3223,
    instrux + 3226,
    instrux + 3227,
    instrux + 3230,
    instrux + 3231,
    instrux + 3234,
    instrux + 3235,
};

static const struct itemplate * const itable_VEX0219D[] = {
    instrux + 3366,
    instrux + 3367,
    instrux + 3368,
    instrux + 3369,
    instrux + 3370,
    instrux + 3371,
    instrux + 3372,
    instrux + 3373,
};

static const struct itemplate * const itable_VEX0219E[] = {
    instrux + 3270,
    instrux + 3271,
    instrux + 3274,
    instrux + 3275,
    instrux + 3278,
    instrux + 3279,
    instrux + 3282,
    instrux + 3283,
};

static const struct itemplate * const itable_VEX0219F[] = {
    instrux + 3390,
    instrux + 3391,
    instrux + 3392,
    instrux + 3393,
    instrux + 3394,
    instrux + 3395,
    instrux + 3396,
    instrux + 3397,
};

static const struct itemplate * const itable_VEX021A6[] = {
    instrux + 3094,
    instrux + 3095,
    instrux + 3098,
    instrux + 3099,
    instrux + 3102,
    instrux + 3103,
    instrux + 3106,
    instrux + 3107,
};

static const struct itemplate * const itable_VEX021A7[] = {
    instrux + 3190,
    instrux + 3191,
    instrux + 3194,
    instrux + 3195,
    instrux + 3198,
    instrux + 3199,
    instrux + 3202,
    instrux + 3203,
};

static const struct itemplate * const itable_VEX021A8[] = {
    instrux + 3046,
    instrux + 3047,
    instrux + 3050,
    instrux + 3051,
    instrux + 3054,
    instrux + 3055,
    instrux + 3058,
    instrux + 3059,
};

static const struct itemplate * const itable_VEX021A9[] = {
    instrux + 3326,
    instrux + 3327,
    instrux + 3328,
    instrux + 3329,
    instrux + 3330,
    instrux + 3331,
    instrux + 3332,
    instrux + 3333,
};

static const struct itemplate * const itable_VEX021AA[] = {
    instrux + 3142,
    instrux + 3143,
    instrux + 3146,
    instrux + 3147,
    instrux + 3150,
    instrux + 3151,
    instrux + 3154,
    instrux + 3155,
};

static const struct itemplate * const itable_VEX021AB[] = {
    instrux + 3350,
    instrux + 3351,
    instrux + 3352,
    instrux + 3353,
    instrux + 3354,
    instrux + 3355,
    instrux + 3356,
    instrux + 3357,
};

static const struct itemplate * const itable_VEX021AC[] = {
    instrux + 3238,
    instrux + 3239,
    instrux + 3242,
    instrux + 3243,
    instrux + 3246,
    instrux + 3247,
    instrux + 3250,
    instrux + 3251,
};

static const struct itemplate * const itable_VEX021AD[] = {
    instrux + 3374,
    instrux + 3375,
    instrux + 3376,
    instrux + 3377,
    instrux + 3378,
    instrux + 3379,
    instrux + 3380,
    instrux + 3381,
};

static const struct itemplate * const itable_VEX021AE[] = {
    instrux + 3286,
    instrux + 3287,
    instrux + 3290,
    instrux + 3291,
    instrux + 3294,
    instrux + 3295,
    instrux + 3298,
    instrux + 3299,
};

static const struct itemplate * const itable_VEX021AF[] = {
    instrux + 3398,
    instrux + 3399,
    instrux + 3400,
    instrux + 3401,
    instrux + 3402,
    instrux + 3403,
    instrux + 3404,
    instrux + 3405,
};

static const struct itemplate * const itable_VEX021B6[] = {
    instrux + 3110,
    instrux + 3111,
    instrux + 3114,
    instrux + 3115,
    instrux + 3118,
    instrux + 3119,
    instrux + 3122,
    instrux + 3123,
};

static const struct itemplate * const itable_VEX021B7[] = {
    instrux + 3206,
    instrux + 3207,
    instrux + 3210,
    instrux + 3211,
    instrux + 3214,
    instrux + 3215,
    instrux + 3218,
    instrux + 3219,
};

static const struct itemplate * const itable_VEX021B8[] = {
    instrux + 3062,
    instrux + 3063,
    instrux + 3066,
    instrux + 3067,
    instrux + 3070,
    instrux + 3071,
    instrux + 3074,
    instrux + 3075,
};

static const struct itemplate * const itable_VEX021B9[] = {
    instrux + 3334,
    instrux + 3335,
    instrux + 3336,
    instrux + 3337,
    instrux + 3338,
    instrux + 3339,
    instrux + 3340,
    instrux + 3341,
};

static const struct itemplate * const itable_VEX021BA[] = {
    instrux + 3158,
    instrux + 3159,
    instrux + 3162,
    instrux + 3163,
    instrux + 3166,
    instrux + 3167,
    instrux + 3170,
    instrux + 3171,
};

static const struct itemplate * const itable_VEX021BB[] = {
    instrux + 3358,
    instrux + 3359,
    instrux + 3360,
    instrux + 3361,
    instrux + 3362,
    instrux + 3363,
    instrux + 3364,
    instrux + 3365,
};

static const struct itemplate * const itable_VEX021BC[] = {
    instrux + 3254,
    instrux + 3255,
    instrux + 3258,
    instrux + 3259,
    instrux + 3262,
    instrux + 3263,
    instrux + 3266,
    instrux + 3267,
};

static const struct itemplate * const itable_VEX021BD[] = {
    instrux + 3382,
    instrux + 3383,
    instrux + 3384,
    instrux + 3385,
    instrux + 3386,
    instrux + 3387,
    instrux + 3388,
    instrux + 3389,
};

static const struct itemplate * const itable_VEX021BE[] = {
    instrux + 3302,
    instrux + 3303,
    instrux + 3306,
    instrux + 3307,
    instrux + 3310,
    instrux + 3311,
    instrux + 3314,
    instrux + 3315,
};

static const struct itemplate * const itable_VEX021BF[] = {
    instrux + 3406,
    instrux + 3407,
    instrux + 3408,
    instrux + 3409,
    instrux + 3410,
    instrux + 3411,
    instrux + 3412,
    instrux + 3413,
};

static const struct itemplate * const itable_VEX021DB[] = {
    instrux + 1976,
};

static const struct itemplate * const itable_VEX021DC[] = {
    instrux + 1968,
    instrux + 1969,
};

static const struct itemplate * const itable_VEX021DD[] = {
    instrux + 1970,
    instrux + 1971,
};

static const struct itemplate * const itable_VEX021DE[] = {
    instrux + 1972,
    instrux + 1973,
};

static const struct itemplate * const itable_VEX021DF[] = {
    instrux + 1974,
    instrux + 1975,
};

static const struct itemplate * const itable_VEX0250C[] = {
    instrux + 2738,
};

static const struct itemplate * const itable_VEX0250D[] = {
    instrux + 2718,
};

static const struct itemplate * const itable_VEX0250E[] = {
    instrux + 2984,
};

static const struct itemplate * const itable_VEX0250F[] = {
    instrux + 2986,
};

static const struct itemplate * const itable_VEX02514[] = {
    instrux + 2029,
};

static const struct itemplate * const itable_VEX02515[] = {
    instrux + 2025,
};

static const struct itemplate * const itable_VEX02517[] = {
    instrux + 2903,
};

static const struct itemplate * const itable_VEX02518[] = {
    instrux + 2031,
};

static const struct itemplate * const itable_VEX02519[] = {
    instrux + 2032,
};

static const struct itemplate * const itable_VEX0251A[] = {
    instrux + 2033,
};

static const struct itemplate * const itable_VEX0252C[] = {
    instrux + 2519,
};

static const struct itemplate * const itable_VEX0252D[] = {
    instrux + 2523,
};

static const struct itemplate * const itable_VEX0252E[] = {
    instrux + 2521,
};

static const struct itemplate * const itable_VEX0252F[] = {
    instrux + 2525,
};

static const struct itemplate * const itable_VEX02596[] = {
    instrux + 3080,
    instrux + 3081,
    instrux + 3084,
    instrux + 3085,
    instrux + 3088,
    instrux + 3089,
    instrux + 3092,
    instrux + 3093,
};

static const struct itemplate * const itable_VEX02597[] = {
    instrux + 3176,
    instrux + 3177,
    instrux + 3180,
    instrux + 3181,
    instrux + 3184,
    instrux + 3185,
    instrux + 3188,
    instrux + 3189,
};

static const struct itemplate * const itable_VEX02598[] = {
    instrux + 3032,
    instrux + 3033,
    instrux + 3036,
    instrux + 3037,
    instrux + 3040,
    instrux + 3041,
    instrux + 3044,
    instrux + 3045,
};

static const struct itemplate * const itable_VEX0259A[] = {
    instrux + 3128,
    instrux + 3129,
    instrux + 3132,
    instrux + 3133,
    instrux + 3136,
    instrux + 3137,
    instrux + 3140,
    instrux + 3141,
};

static const struct itemplate * const itable_VEX0259C[] = {
    instrux + 3224,
    instrux + 3225,
    instrux + 3228,
    instrux + 3229,
    instrux + 3232,
    instrux + 3233,
    instrux + 3236,
    instrux + 3237,
};

static const struct itemplate * const itable_VEX0259E[] = {
    instrux + 3272,
    instrux + 3273,
    instrux + 3276,
    instrux + 3277,
    instrux + 3280,
    instrux + 3281,
    instrux + 3284,
    instrux + 3285,
};

static const struct itemplate * const itable_VEX025A6[] = {
    instrux + 3096,
    instrux + 3097,
    instrux + 3100,
    instrux + 3101,
    instrux + 3104,
    instrux + 3105,
    instrux + 3108,
    instrux + 3109,
};

static const struct itemplate * const itable_VEX025A7[] = {
    instrux + 3192,
    instrux + 3193,
    instrux + 3196,
    instrux + 3197,
    instrux + 3200,
    instrux + 3201,
    instrux + 3204,
    instrux + 3205,
};

static const struct itemplate * const itable_VEX025A8[] = {
    instrux + 3048,
    instrux + 3049,
    instrux + 3052,
    instrux + 3053,
    instrux + 3056,
    instrux + 3057,
    instrux + 3060,
    instrux + 3061,
};

static const struct itemplate * const itable_VEX025AA[] = {
    instrux + 3144,
    instrux + 3145,
    instrux + 3148,
    instrux + 3149,
    instrux + 3152,
    instrux + 3153,
    instrux + 3156,
    instrux + 3157,
};

static const struct itemplate * const itable_VEX025AC[] = {
    instrux + 3240,
    instrux + 3241,
    instrux + 3244,
    instrux + 3245,
    instrux + 3248,
    instrux + 3249,
    instrux + 3252,
    instrux + 3253,
};

static const struct itemplate * const itable_VEX025AE[] = {
    instrux + 3288,
    instrux + 3289,
    instrux + 3292,
    instrux + 3293,
    instrux + 3296,
    instrux + 3297,
    instrux + 3300,
    instrux + 3301,
};

static const struct itemplate * const itable_VEX025B6[] = {
    instrux + 3112,
    instrux + 3113,
    instrux + 3116,
    instrux + 3117,
    instrux + 3120,
    instrux + 3121,
    instrux + 3124,
    instrux + 3125,
};

static const struct itemplate * const itable_VEX025B7[] = {
    instrux + 3208,
    instrux + 3209,
    instrux + 3212,
    instrux + 3213,
    instrux + 3216,
    instrux + 3217,
    instrux + 3220,
    instrux + 3221,
};

static const struct itemplate * const itable_VEX025B8[] = {
    instrux + 3064,
    instrux + 3065,
    instrux + 3068,
    instrux + 3069,
    instrux + 3072,
    instrux + 3073,
    instrux + 3076,
    instrux + 3077,
};

static const struct itemplate * const itable_VEX025BA[] = {
    instrux + 3160,
    instrux + 3161,
    instrux + 3164,
    instrux + 3165,
    instrux + 3168,
    instrux + 3169,
    instrux + 3172,
    instrux + 3173,
};

static const struct itemplate * const itable_VEX025BC[] = {
    instrux + 3256,
    instrux + 3257,
    instrux + 3260,
    instrux + 3261,
    instrux + 3264,
    instrux + 3265,
    instrux + 3268,
    instrux + 3269,
};

static const struct itemplate * const itable_VEX025BE[] = {
    instrux + 3304,
    instrux + 3305,
    instrux + 3308,
    instrux + 3309,
    instrux + 3312,
    instrux + 3313,
    instrux + 3316,
    instrux + 3317,
};

static const struct itemplate * const itable_VEX03104[] = {
    instrux + 2739,
};

static const struct itemplate * const itable_VEX03105[] = {
    instrux + 2719,
};

static const struct itemplate * const itable_VEX03108[] = {
    instrux + 2948,
};

static const struct itemplate * const itable_VEX03109[] = {
    instrux + 2946,
};

static const struct itemplate * const itable_VEX0310A[] = {
    instrux + 2952,
    instrux + 2953,
};

static const struct itemplate * const itable_VEX0310B[] = {
    instrux + 2950,
    instrux + 2951,
};

static const struct itemplate * const itable_VEX0310C[] = {
    instrux + 2018,
    instrux + 2019,
};

static const struct itemplate * const itable_VEX0310D[] = {
    instrux + 2014,
    instrux + 2015,
};

static const struct itemplate * const itable_VEX0310E[] = {
    instrux + 2695,
    instrux + 2696,
};

static const struct itemplate * const itable_VEX0310F[] = {
    instrux + 2683,
    instrux + 2684,
};

static const struct itemplate * const itable_VEX03114[] = {
    instrux + 2758,
    instrux + 2759,
    instrux + 2760,
};

static const struct itemplate * const itable_VEX03115[] = {
    instrux + 2764,
    instrux + 2765,
    instrux + 2766,
};

static const struct itemplate * const itable_VEX03116[] = {
    instrux + 2767,
    instrux + 2768,
    instrux + 2769,
};

static const struct itemplate * const itable_VEX03117[] = {
    instrux + 2493,
};

static const struct itemplate * const itable_VEX03120[] = {
    instrux + 2783,
    instrux + 2784,
    instrux + 2785,
    instrux + 2786,
};

static const struct itemplate * const itable_VEX03121[] = {
    instrux + 2511,
    instrux + 2512,
};

static const struct itemplate * const itable_VEX03122[] = {
    instrux + 2791,
    instrux + 2792,
    instrux + 2793,
    instrux + 2794,
};

static const struct itemplate * const itable_VEX03140[] = {
    instrux + 2488,
    instrux + 2489,
};

static const struct itemplate * const itable_VEX03141[] = {
    instrux + 2486,
    instrux + 2487,
};

static const struct itemplate * const itable_VEX03142[] = {
    instrux + 2634,
    instrux + 2635,
};

static const struct itemplate * const itable_VEX03144[] = {
    instrux + 3020,
    instrux + 3021,
    instrux + 3022,
    instrux + 3023,
    instrux + 3024,
    instrux + 3025,
    instrux + 3026,
    instrux + 3027,
    instrux + 3028,
    instrux + 3029,
};

static const struct itemplate * const itable_VEX03148[] = {
    instrux + 2741,
    instrux + 2742,
    instrux + 2745,
    instrux + 2746,
    instrux + 2749,
    instrux + 2750,
    instrux + 2753,
    instrux + 2754,
};

static const struct itemplate * const itable_VEX03149[] = {
    instrux + 2721,
    instrux + 2722,
    instrux + 2725,
    instrux + 2726,
    instrux + 2729,
    instrux + 2730,
    instrux + 2733,
    instrux + 2734,
};

static const struct itemplate * const itable_VEX0314A[] = {
    instrux + 2026,
};

static const struct itemplate * const itable_VEX0314B[] = {
    instrux + 2022,
};

static const struct itemplate * const itable_VEX0314C[] = {
    instrux + 2693,
    instrux + 2694,
};

static const struct itemplate * const itable_VEX03160[] = {
    instrux + 2698,
};

static const struct itemplate * const itable_VEX03161[] = {
    instrux + 2697,
};

static const struct itemplate * const itable_VEX03162[] = {
    instrux + 2700,
};

static const struct itemplate * const itable_VEX03163[] = {
    instrux + 2699,
};

static const struct itemplate * const itable_VEX031DF[] = {
    instrux + 1977,
};

static const struct itemplate * const itable_VEX03504[] = {
    instrux + 2740,
};

static const struct itemplate * const itable_VEX03505[] = {
    instrux + 2720,
};

static const struct itemplate * const itable_VEX03506[] = {
    instrux + 2757,
};

static const struct itemplate * const itable_VEX03508[] = {
    instrux + 2949,
};

static const struct itemplate * const itable_VEX03509[] = {
    instrux + 2947,
};

static const struct itemplate * const itable_VEX0350C[] = {
    instrux + 2020,
    instrux + 2021,
};

static const struct itemplate * const itable_VEX0350D[] = {
    instrux + 2016,
    instrux + 2017,
};

static const struct itemplate * const itable_VEX03518[] = {
    instrux + 2510,
};

static const struct itemplate * const itable_VEX03519[] = {
    instrux + 2492,
};

static const struct itemplate * const itable_VEX03540[] = {
    instrux + 2490,
    instrux + 2491,
};

static const struct itemplate * const itable_VEX03548[] = {
    instrux + 2743,
    instrux + 2744,
    instrux + 2747,
    instrux + 2748,
    instrux + 2751,
    instrux + 2752,
    instrux + 2755,
    instrux + 2756,
};

static const struct itemplate * const itable_VEX03549[] = {
    instrux + 2723,
    instrux + 2724,
    instrux + 2727,
    instrux + 2728,
    instrux + 2731,
    instrux + 2732,
    instrux + 2735,
    instrux + 2736,
};

static const struct itemplate * const itable_VEX0354A[] = {
    instrux + 2028,
};

static const struct itemplate * const itable_VEX0354B[] = {
    instrux + 2024,
};

static const struct disasm_index itable_VEX010[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01010, 1 },
    { itable_VEX01011, 1 },
    { itable_VEX01012, 4 },
    { itable_VEX01013, 1 },
    { itable_VEX01014, 2 },
    { itable_VEX01015, 2 },
    { itable_VEX01016, 4 },
    { itable_VEX01017, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01028, 1 },
    { itable_VEX01029, 1 },
    { NULL, 0 },
    { itable_VEX0102B, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0102E, 1 },
    { itable_VEX0102F, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01050, 2 },
    { itable_VEX01051, 1 },
    { itable_VEX01052, 1 },
    { itable_VEX01053, 1 },
    { itable_VEX01054, 2 },
    { itable_VEX01055, 2 },
    { itable_VEX01056, 2 },
    { itable_VEX01057, 2 },
    { itable_VEX01058, 2 },
    { itable_VEX01059, 2 },
    { itable_VEX0105A, 1 },
    { itable_VEX0105B, 1 },
    { itable_VEX0105C, 2 },
    { itable_VEX0105D, 2 },
    { itable_VEX0105E, 2 },
    { itable_VEX0105F, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01077, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX010AE, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX010C2, 66 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX010C6, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_VEX011[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01110, 1 },
    { itable_VEX01111, 1 },
    { itable_VEX01112, 2 },
    { itable_VEX01113, 1 },
    { itable_VEX01114, 2 },
    { itable_VEX01115, 2 },
    { itable_VEX01116, 2 },
    { itable_VEX01117, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01128, 1 },
    { itable_VEX01129, 3 },
    { NULL, 0 },
    { itable_VEX0112B, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0112E, 1 },
    { itable_VEX0112F, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01137, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01150, 2 },
    { itable_VEX01151, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01154, 2 },
    { itable_VEX01155, 2 },
    { itable_VEX01156, 2 },
    { itable_VEX01157, 2 },
    { itable_VEX01158, 2 },
    { itable_VEX01159, 2 },
    { itable_VEX0115A, 2 },
    { itable_VEX0115B, 1 },
    { itable_VEX0115C, 2 },
    { itable_VEX0115D, 2 },
    { itable_VEX0115E, 2 },
    { itable_VEX0115F, 2 },
    { itable_VEX01160, 2 },
    { itable_VEX01161, 2 },
    { itable_VEX01162, 2 },
    { itable_VEX01163, 2 },
    { itable_VEX01164, 2 },
    { itable_VEX01165, 2 },
    { itable_VEX01166, 2 },
    { itable_VEX01167, 2 },
    { itable_VEX01168, 2 },
    { itable_VEX01169, 2 },
    { itable_VEX0116A, 2 },
    { itable_VEX0116B, 2 },
    { itable_VEX0116C, 2 },
    { itable_VEX0116D, 2 },
    { itable_VEX0116E, 2 },
    { itable_VEX0116F, 1 },
    { itable_VEX01170, 1 },
    { itable_VEX01171, 6 },
    { itable_VEX01172, 6 },
    { itable_VEX01173, 8 },
    { itable_VEX01174, 2 },
    { itable_VEX01175, 2 },
    { itable_VEX01176, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0117C, 2 },
    { itable_VEX0117D, 2 },
    { itable_VEX0117E, 2 },
    { itable_VEX0117F, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX011C2, 66 },
    { NULL, 0 },
    { itable_VEX011C4, 4 },
    { itable_VEX011C5, 3 },
    { itable_VEX011C6, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX011D0, 2 },
    { itable_VEX011D1, 2 },
    { itable_VEX011D2, 2 },
    { itable_VEX011D3, 2 },
    { itable_VEX011D4, 2 },
    { itable_VEX011D5, 2 },
    { itable_VEX011D6, 1 },
    { itable_VEX011D7, 2 },
    { itable_VEX011D8, 2 },
    { itable_VEX011D9, 2 },
    { itable_VEX011DA, 2 },
    { itable_VEX011DB, 2 },
    { itable_VEX011DC, 2 },
    { itable_VEX011DD, 2 },
    { itable_VEX011DE, 2 },
    { itable_VEX011DF, 2 },
    { itable_VEX011E0, 2 },
    { itable_VEX011E1, 2 },
    { itable_VEX011E2, 2 },
    { itable_VEX011E3, 2 },
    { itable_VEX011E4, 2 },
    { itable_VEX011E5, 2 },
    { itable_VEX011E6, 2 },
    { itable_VEX011E7, 1 },
    { itable_VEX011E8, 2 },
    { itable_VEX011E9, 2 },
    { itable_VEX011EA, 2 },
    { itable_VEX011EB, 2 },
    { itable_VEX011EC, 2 },
    { itable_VEX011ED, 2 },
    { itable_VEX011EE, 2 },
    { itable_VEX011EF, 2 },
    { NULL, 0 },
    { itable_VEX011F1, 2 },
    { itable_VEX011F2, 2 },
    { itable_VEX011F3, 2 },
    { itable_VEX011F4, 2 },
    { itable_VEX011F5, 2 },
    { itable_VEX011F6, 2 },
    { itable_VEX011F7, 1 },
    { itable_VEX011F8, 2 },
    { itable_VEX011F9, 2 },
    { itable_VEX011FA, 2 },
    { itable_VEX011FB, 2 },
    { itable_VEX011FC, 2 },
    { itable_VEX011FD, 2 },
    { itable_VEX011FE, 2 },
    { NULL, 0 },
};

static const struct disasm_index itable_VEX012[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01210, 3 },
    { itable_VEX01211, 3 },
    { itable_VEX01212, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01216, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0122A, 4 },
    { NULL, 0 },
    { itable_VEX0122C, 2 },
    { itable_VEX0122D, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01251, 2 },
    { itable_VEX01252, 2 },
    { itable_VEX01253, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01258, 2 },
    { itable_VEX01259, 2 },
    { itable_VEX0125A, 2 },
    { itable_VEX0125B, 1 },
    { itable_VEX0125C, 2 },
    { itable_VEX0125D, 2 },
    { itable_VEX0125E, 2 },
    { itable_VEX0125F, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0126F, 1 },
    { itable_VEX01270, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0127E, 1 },
    { itable_VEX0127F, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX012C2, 66 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX012E6, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_VEX013[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01310, 3 },
    { itable_VEX01311, 3 },
    { itable_VEX01312, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0132A, 4 },
    { NULL, 0 },
    { itable_VEX0132C, 2 },
    { itable_VEX0132D, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01351, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01358, 2 },
    { itable_VEX01359, 2 },
    { itable_VEX0135A, 2 },
    { NULL, 0 },
    { itable_VEX0135C, 2 },
    { itable_VEX0135D, 2 },
    { itable_VEX0135E, 2 },
    { itable_VEX0135F, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01370, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0137C, 2 },
    { itable_VEX0137D, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX013C2, 66 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX013D0, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX013E6, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX013F0, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_VEX014[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01410, 1 },
    { itable_VEX01411, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01414, 2 },
    { itable_VEX01415, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01428, 1 },
    { itable_VEX01429, 1 },
    { NULL, 0 },
    { itable_VEX0142B, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01450, 2 },
    { itable_VEX01451, 1 },
    { itable_VEX01452, 1 },
    { itable_VEX01453, 1 },
    { itable_VEX01454, 2 },
    { itable_VEX01455, 2 },
    { itable_VEX01456, 2 },
    { itable_VEX01457, 2 },
    { itable_VEX01458, 2 },
    { itable_VEX01459, 2 },
    { itable_VEX0145A, 1 },
    { itable_VEX0145B, 1 },
    { itable_VEX0145C, 2 },
    { itable_VEX0145D, 2 },
    { itable_VEX0145E, 2 },
    { itable_VEX0145F, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01477, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX014C2, 66 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX014C6, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_VEX015[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01510, 1 },
    { itable_VEX01511, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01514, 2 },
    { itable_VEX01515, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01528, 1 },
    { itable_VEX01529, 1 },
    { NULL, 0 },
    { itable_VEX0152B, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01550, 2 },
    { itable_VEX01551, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01554, 2 },
    { itable_VEX01555, 2 },
    { itable_VEX01556, 2 },
    { itable_VEX01557, 2 },
    { itable_VEX01558, 2 },
    { itable_VEX01559, 2 },
    { itable_VEX0155A, 2 },
    { itable_VEX0155B, 1 },
    { itable_VEX0155C, 2 },
    { itable_VEX0155D, 2 },
    { itable_VEX0155E, 2 },
    { itable_VEX0155F, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0156F, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0157C, 2 },
    { itable_VEX0157D, 2 },
    { NULL, 0 },
    { itable_VEX0157F, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX015C2, 66 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX015C6, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX015D0, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX015E6, 2 },
    { itable_VEX015E7, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_VEX016[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01612, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01616, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0165B, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0166F, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0167F, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX016E6, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_VEX017[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX01712, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0177C, 2 },
    { itable_VEX0177D, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX017D0, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX017E6, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX017F0, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_VEX021[256] = {
    { itable_VEX02100, 2 },
    { itable_VEX02101, 2 },
    { itable_VEX02102, 2 },
    { itable_VEX02103, 2 },
    { itable_VEX02104, 2 },
    { itable_VEX02105, 2 },
    { itable_VEX02106, 2 },
    { itable_VEX02107, 2 },
    { itable_VEX02108, 2 },
    { itable_VEX02109, 2 },
    { itable_VEX0210A, 2 },
    { itable_VEX0210B, 2 },
    { itable_VEX0210C, 1 },
    { itable_VEX0210D, 1 },
    { itable_VEX0210E, 1 },
    { itable_VEX0210F, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX02114, 1 },
    { itable_VEX02115, 1 },
    { NULL, 0 },
    { itable_VEX02117, 1 },
    { itable_VEX02118, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0211C, 1 },
    { itable_VEX0211D, 1 },
    { itable_VEX0211E, 1 },
    { NULL, 0 },
    { itable_VEX02120, 1 },
    { itable_VEX02121, 1 },
    { itable_VEX02122, 1 },
    { itable_VEX02123, 1 },
    { itable_VEX02124, 1 },
    { itable_VEX02125, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX02128, 2 },
    { NULL, 0 },
    { itable_VEX0212A, 1 },
    { itable_VEX0212B, 2 },
    { itable_VEX0212C, 1 },
    { itable_VEX0212D, 1 },
    { itable_VEX0212E, 1 },
    { itable_VEX0212F, 1 },
    { itable_VEX02130, 1 },
    { itable_VEX02131, 1 },
    { itable_VEX02132, 1 },
    { itable_VEX02133, 1 },
    { itable_VEX02134, 1 },
    { itable_VEX02135, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX02138, 2 },
    { itable_VEX02139, 2 },
    { itable_VEX0213A, 2 },
    { itable_VEX0213B, 2 },
    { itable_VEX0213C, 2 },
    { itable_VEX0213D, 2 },
    { itable_VEX0213E, 2 },
    { itable_VEX0213F, 2 },
    { itable_VEX02140, 2 },
    { itable_VEX02141, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX02196, 8 },
    { itable_VEX02197, 8 },
    { itable_VEX02198, 8 },
    { itable_VEX02199, 8 },
    { itable_VEX0219A, 8 },
    { itable_VEX0219B, 8 },
    { itable_VEX0219C, 8 },
    { itable_VEX0219D, 8 },
    { itable_VEX0219E, 8 },
    { itable_VEX0219F, 8 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX021A6, 8 },
    { itable_VEX021A7, 8 },
    { itable_VEX021A8, 8 },
    { itable_VEX021A9, 8 },
    { itable_VEX021AA, 8 },
    { itable_VEX021AB, 8 },
    { itable_VEX021AC, 8 },
    { itable_VEX021AD, 8 },
    { itable_VEX021AE, 8 },
    { itable_VEX021AF, 8 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX021B6, 8 },
    { itable_VEX021B7, 8 },
    { itable_VEX021B8, 8 },
    { itable_VEX021B9, 8 },
    { itable_VEX021BA, 8 },
    { itable_VEX021BB, 8 },
    { itable_VEX021BC, 8 },
    { itable_VEX021BD, 8 },
    { itable_VEX021BE, 8 },
    { itable_VEX021BF, 8 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX021DB, 1 },
    { itable_VEX021DC, 2 },
    { itable_VEX021DD, 2 },
    { itable_VEX021DE, 2 },
    { itable_VEX021DF, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_VEX025[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0250C, 1 },
    { itable_VEX0250D, 1 },
    { itable_VEX0250E, 1 },
    { itable_VEX0250F, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX02514, 1 },
    { itable_VEX02515, 1 },
    { NULL, 0 },
    { itable_VEX02517, 1 },
    { itable_VEX02518, 1 },
    { itable_VEX02519, 1 },
    { itable_VEX0251A, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0252C, 1 },
    { itable_VEX0252D, 1 },
    { itable_VEX0252E, 1 },
    { itable_VEX0252F, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX02596, 8 },
    { itable_VEX02597, 8 },
    { itable_VEX02598, 8 },
    { NULL, 0 },
    { itable_VEX0259A, 8 },
    { NULL, 0 },
    { itable_VEX0259C, 8 },
    { NULL, 0 },
    { itable_VEX0259E, 8 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX025A6, 8 },
    { itable_VEX025A7, 8 },
    { itable_VEX025A8, 8 },
    { NULL, 0 },
    { itable_VEX025AA, 8 },
    { NULL, 0 },
    { itable_VEX025AC, 8 },
    { NULL, 0 },
    { itable_VEX025AE, 8 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX025B6, 8 },
    { itable_VEX025B7, 8 },
    { itable_VEX025B8, 8 },
    { NULL, 0 },
    { itable_VEX025BA, 8 },
    { NULL, 0 },
    { itable_VEX025BC, 8 },
    { NULL, 0 },
    { itable_VEX025BE, 8 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_VEX031[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX03104, 1 },
    { itable_VEX03105, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX03108, 1 },
    { itable_VEX03109, 1 },
    { itable_VEX0310A, 2 },
    { itable_VEX0310B, 2 },
    { itable_VEX0310C, 2 },
    { itable_VEX0310D, 2 },
    { itable_VEX0310E, 2 },
    { itable_VEX0310F, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX03114, 3 },
    { itable_VEX03115, 3 },
    { itable_VEX03116, 3 },
    { itable_VEX03117, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX03120, 4 },
    { itable_VEX03121, 2 },
    { itable_VEX03122, 4 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX03140, 2 },
    { itable_VEX03141, 2 },
    { itable_VEX03142, 2 },
    { NULL, 0 },
    { itable_VEX03144, 10 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX03148, 8 },
    { itable_VEX03149, 8 },
    { itable_VEX0314A, 1 },
    { itable_VEX0314B, 1 },
    { itable_VEX0314C, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX03160, 1 },
    { itable_VEX03161, 1 },
    { itable_VEX03162, 1 },
    { itable_VEX03163, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX031DF, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_VEX035[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX03504, 1 },
    { itable_VEX03505, 1 },
    { itable_VEX03506, 1 },
    { NULL, 0 },
    { itable_VEX03508, 1 },
    { itable_VEX03509, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX0350C, 2 },
    { itable_VEX0350D, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX03518, 1 },
    { itable_VEX03519, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX03540, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_VEX03548, 8 },
    { itable_VEX03549, 8 },
    { itable_VEX0354A, 1 },
    { itable_VEX0354B, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_0F24[256] = {
    { itable_0F2400, 2 },
    { itable_0F2401, 2 },
    { itable_0F2402, 2 },
    { itable_0F2403, 2 },
    { itable_0F2404, 2 },
    { itable_0F2405, 2 },
    { itable_0F2406, 2 },
    { itable_0F2407, 2 },
    { itable_0F2408, 2 },
    { itable_0F2409, 2 },
    { itable_0F240A, 2 },
    { itable_0F240B, 2 },
    { itable_0F240C, 2 },
    { itable_0F240D, 2 },
    { itable_0F240E, 2 },
    { itable_0F240F, 2 },
    { itable_0F2410, 2 },
    { itable_0F2411, 2 },
    { itable_0F2412, 2 },
    { itable_0F2413, 2 },
    { itable_0F2414, 2 },
    { itable_0F2415, 2 },
    { itable_0F2416, 2 },
    { itable_0F2417, 2 },
    { itable_0F2418, 2 },
    { itable_0F2419, 2 },
    { itable_0F241A, 2 },
    { itable_0F241B, 2 },
    { itable_0F241C, 2 },
    { itable_0F241D, 2 },
    { itable_0F241E, 2 },
    { itable_0F241F, 2 },
    { itable_0F2420, 2 },
    { itable_0F2421, 2 },
    { itable_0F2422, 2 },
    { itable_0F2423, 2 },
    { itable_0F2424, 2 },
    { itable_0F2425, 2 },
    { itable_0F2426, 2 },
    { itable_0F2427, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F2440, 2 },
    { itable_0F2441, 2 },
    { itable_0F2442, 2 },
    { itable_0F2443, 2 },
    { itable_0F2444, 2 },
    { itable_0F2445, 2 },
    { itable_0F2446, 2 },
    { itable_0F2447, 2 },
    { itable_0F2448, 2 },
    { itable_0F2449, 2 },
    { itable_0F244A, 2 },
    { itable_0F244B, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F2485, 1 },
    { itable_0F2486, 1 },
    { itable_0F2487, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F248E, 1 },
    { itable_0F248F, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F2495, 1 },
    { itable_0F2496, 1 },
    { itable_0F2497, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F249E, 1 },
    { itable_0F249F, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F24A6, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F24B6, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_0F25[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F252C, 17 },
    { itable_0F252D, 17 },
    { itable_0F252E, 17 },
    { itable_0F252F, 17 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F254C, 9 },
    { itable_0F254D, 9 },
    { itable_0F254E, 9 },
    { itable_0F254F, 9 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F256C, 9 },
    { itable_0F256D, 9 },
    { itable_0F256E, 9 },
    { itable_0F256F, 9 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_0F38[256] = {
    { itable_0F3800, 2 },
    { itable_0F3801, 2 },
    { itable_0F3802, 2 },
    { itable_0F3803, 2 },
    { itable_0F3804, 2 },
    { itable_0F3805, 2 },
    { itable_0F3806, 2 },
    { itable_0F3807, 2 },
    { itable_0F3808, 2 },
    { itable_0F3809, 2 },
    { itable_0F380A, 2 },
    { itable_0F380B, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F3810, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F3814, 1 },
    { itable_0F3815, 1 },
    { NULL, 0 },
    { itable_0F3817, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F381C, 2 },
    { itable_0F381D, 2 },
    { itable_0F381E, 2 },
    { NULL, 0 },
    { itable_0F3820, 1 },
    { itable_0F3821, 1 },
    { itable_0F3822, 1 },
    { itable_0F3823, 1 },
    { itable_0F3824, 1 },
    { itable_0F3825, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F3828, 1 },
    { itable_0F3829, 1 },
    { itable_0F382A, 1 },
    { itable_0F382B, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F3830, 1 },
    { itable_0F3831, 1 },
    { itable_0F3832, 1 },
    { itable_0F3833, 1 },
    { itable_0F3834, 1 },
    { itable_0F3835, 1 },
    { NULL, 0 },
    { itable_0F3837, 1 },
    { itable_0F3838, 1 },
    { itable_0F3839, 1 },
    { itable_0F383A, 1 },
    { itable_0F383B, 1 },
    { itable_0F383C, 1 },
    { itable_0F383D, 1 },
    { itable_0F383E, 1 },
    { itable_0F383F, 1 },
    { itable_0F3840, 1 },
    { itable_0F3841, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F3880, 2 },
    { itable_0F3881, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F38DB, 1 },
    { itable_0F38DC, 1 },
    { itable_0F38DD, 1 },
    { itable_0F38DE, 1 },
    { itable_0F38DF, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F38F0, 5 },
    { itable_0F38F1, 6 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_0F3A[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F3A08, 5 },
    { itable_0F3A09, 1 },
    { itable_0F3A0A, 1 },
    { itable_0F3A0B, 1 },
    { itable_0F3A0C, 1 },
    { itable_0F3A0D, 1 },
    { itable_0F3A0E, 1 },
    { itable_0F3A0F, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F3A14, 3 },
    { itable_0F3A15, 3 },
    { itable_0F3A16, 2 },
    { itable_0F3A17, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F3A20, 2 },
    { itable_0F3A21, 1 },
    { itable_0F3A22, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F3A40, 1 },
    { itable_0F3A41, 1 },
    { itable_0F3A42, 1 },
    { NULL, 0 },
    { itable_0F3A44, 5 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F3A60, 1 },
    { itable_0F3A61, 1 },
    { itable_0F3A62, 1 },
    { itable_0F3A63, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F3ADF, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_0F7A[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F7A10, 1 },
    { itable_0F7A11, 1 },
    { itable_0F7A12, 1 },
    { itable_0F7A13, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F7A30, 1 },
    { itable_0F7A31, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F7A41, 1 },
    { itable_0F7A42, 1 },
    { itable_0F7A43, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F7A46, 1 },
    { itable_0F7A47, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F7A4B, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F7A51, 1 },
    { itable_0F7A52, 1 },
    { itable_0F7A53, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F7A56, 1 },
    { itable_0F7A57, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F7A5B, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F7A61, 1 },
    { itable_0F7A62, 1 },
    { itable_0F7A63, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_0FA6[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0FA6C0, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0FA6C8, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0FA6D0, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_0FA7[256] = {
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0FA7C0, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0FA7C8, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0FA7D0, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0FA7D8, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0FA7E0, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0FA7E8, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
};

static const struct disasm_index itable_0F[256] = {
    { itable_0F00, 24 },
    { itable_0F01, 33 },
    { itable_0F02, 10 },
    { itable_0F03, 10 },
    { NULL, 0 },
    { itable_0F05, 2 },
    { itable_0F06, 1 },
    { itable_0F07, 2 },
    { itable_0F08, 1 },
    { itable_0F09, 1 },
    { NULL, 0 },
    { itable_0F0B, 1 },
    { NULL, 0 },
    { itable_0F0D, 2 },
    { itable_0F0E, 1 },
    { itable_0F0F, 26 },
    { itable_0F10, 8 },
    { itable_0F11, 8 },
    { itable_0F12, 5 },
    { itable_0F13, 2 },
    { itable_0F14, 2 },
    { itable_0F15, 2 },
    { itable_0F16, 4 },
    { itable_0F17, 2 },
    { itable_0F18, 28 },
    { itable_0F19, 24 },
    { itable_0F1A, 24 },
    { itable_0F1B, 24 },
    { itable_0F1C, 24 },
    { itable_0F1D, 24 },
    { itable_0F1E, 24 },
    { itable_0F1F, 27 },
    { itable_0F20, 2 },
    { itable_0F21, 2 },
    { itable_0F22, 2 },
    { itable_0F23, 2 },
    { itable_0F24, -1 },
    { itable_0F25, -1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F28, 4 },
    { itable_0F29, 4 },
    { itable_0F2A, 6 },
    { itable_0F2B, 4 },
    { itable_0F2C, 8 },
    { itable_0F2D, 10 },
    { itable_0F2E, 2 },
    { itable_0F2F, 2 },
    { itable_0F30, 1 },
    { itable_0F31, 1 },
    { itable_0F32, 1 },
    { itable_0F33, 1 },
    { itable_0F34, 1 },
    { itable_0F35, 1 },
    { itable_0F36, 1 },
    { itable_0F37, 2 },
    { itable_0F38, -1 },
    { itable_0F39, 1 },
    { itable_0F3A, -1 },
    { NULL, 0 },
    { itable_0F3C, 1 },
    { itable_0F3D, 1 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_0F40, 6 },
    { itable_0F41, 6 },
    { itable_0F42, 6 },
    { itable_0F43, 6 },
    { itable_0F44, 6 },
    { itable_0F45, 6 },
    { itable_0F46, 6 },
    { itable_0F47, 6 },
    { itable_0F48, 6 },
    { itable_0F49, 6 },
    { itable_0F4A, 6 },
    { itable_0F4B, 6 },
    { itable_0F4C, 6 },
    { itable_0F4D, 6 },
    { itable_0F4E, 6 },
    { itable_0F4F, 6 },
    { itable_0F50, 5 },
    { itable_0F51, 5 },
    { itable_0F52, 3 },
    { itable_0F53, 2 },
    { itable_0F54, 3 },
    { itable_0F55, 3 },
    { itable_0F56, 2 },
    { itable_0F57, 2 },
    { itable_0F58, 5 },
    { itable_0F59, 5 },
    { itable_0F5A, 5 },
    { itable_0F5B, 4 },
    { itable_0F5C, 5 },
    { itable_0F5D, 5 },
    { itable_0F5E, 5 },
    { itable_0F5F, 4 },
    { itable_0F60, 2 },
    { itable_0F61, 2 },
    { itable_0F62, 2 },
    { itable_0F63, 2 },
    { itable_0F64, 2 },
    { itable_0F65, 2 },
    { itable_0F66, 2 },
    { itable_0F67, 2 },
    { itable_0F68, 2 },
    { itable_0F69, 2 },
    { itable_0F6A, 2 },
    { itable_0F6B, 2 },
    { itable_0F6C, 1 },
    { itable_0F6D, 1 },
    { itable_0F6E, 8 },
    { itable_0F6F, 5 },
    { itable_0F70, 7 },
    { itable_0F71, 6 },
    { itable_0F72, 6 },
    { itable_0F73, 6 },
    { itable_0F74, 2 },
    { itable_0F75, 2 },
    { itable_0F76, 2 },
    { itable_0F77, 1 },
    { itable_0F78, 5 },
    { itable_0F79, 5 },
    { itable_0F7A, -1 },
    { itable_0F7B, 5 },
    { itable_0F7C, 3 },
    { itable_0F7D, 3 },
    { itable_0F7E, 10 },
    { itable_0F7F, 5 },
    { itable_0F80, 3 },
    { itable_0F81, 3 },
    { itable_0F82, 3 },
    { itable_0F83, 3 },
    { itable_0F84, 3 },
    { itable_0F85, 3 },
    { itable_0F86, 3 },
    { itable_0F87, 3 },
    { itable_0F88, 3 },
    { itable_0F89, 3 },
    { itable_0F8A, 3 },
    { itable_0F8B, 3 },
    { itable_0F8C, 3 },
    { itable_0F8D, 3 },
    { itable_0F8E, 3 },
    { itable_0F8F, 3 },
    { itable_0F90, 2 },
    { itable_0F91, 2 },
    { itable_0F92, 2 },
    { itable_0F93, 2 },
    { itable_0F94, 2 },
    { itable_0F95, 2 },
    { itable_0F96, 2 },
    { itable_0F97, 2 },
    { itable_0F98, 2 },
    { itable_0F99, 2 },
    { itable_0F9A, 2 },
    { itable_0F9B, 2 },
    { itable_0F9C, 2 },
    { itable_0F9D, 2 },
    { itable_0F9E, 2 },
    { itable_0F9F, 2 },
    { itable_0FA0, 1 },
    { itable_0FA1, 1 },
    { itable_0FA2, 1 },
    { itable_0FA3, 6 },
    { itable_0FA4, 6 },
    { itable_0FA5, 6 },
    { itable_0FA6, -1 },
    { itable_0FA7, -1 },
    { itable_0FA8, 1 },
    { itable_0FA9, 1 },
    { itable_0FAA, 1 },
    { itable_0FAB, 6 },
    { itable_0FAC, 6 },
    { itable_0FAD, 6 },
    { itable_0FAE, 13 },
    { itable_0FAF, 6 },
    { itable_0FB0, 2 },
    { itable_0FB1, 6 },
    { itable_0FB2, 2 },
    { itable_0FB3, 6 },
    { itable_0FB4, 2 },
    { itable_0FB5, 2 },
    { itable_0FB6, 4 },
    { itable_0FB7, 2 },
    { itable_0FB8, 6 },
    { itable_0FB9, 1 },
    { itable_0FBA, 12 },
    { itable_0FBB, 6 },
    { itable_0FBC, 6 },
    { itable_0FBD, 9 },
    { itable_0FBE, 4 },
    { itable_0FBF, 2 },
    { itable_0FC0, 2 },
    { itable_0FC1, 6 },
    { itable_0FC2, 38 },
    { itable_0FC3, 2 },
    { itable_0FC4, 4 },
    { itable_0FC5, 2 },
    { itable_0FC6, 4 },
    { itable_0FC7, 6 },
    { itable_0FC8, 2 },
    { itable_0FC9, 2 },
    { itable_0FCA, 2 },
    { itable_0FCB, 2 },
    { itable_0FCC, 2 },
    { itable_0FCD, 2 },
    { itable_0FCE, 2 },
    { itable_0FCF, 2 },
    { itable_0FD0, 2 },
    { itable_0FD1, 2 },
    { itable_0FD2, 2 },
    { itable_0FD3, 2 },
    { itable_0FD4, 2 },
    { itable_0FD5, 2 },
    { itable_0FD6, 4 },
    { itable_0FD7, 2 },
    { itable_0FD8, 2 },
    { itable_0FD9, 2 },
    { itable_0FDA, 2 },
    { itable_0FDB, 2 },
    { itable_0FDC, 2 },
    { itable_0FDD, 2 },
    { itable_0FDE, 2 },
    { itable_0FDF, 2 },
    { itable_0FE0, 2 },
    { itable_0FE1, 2 },
    { itable_0FE2, 2 },
    { itable_0FE3, 2 },
    { itable_0FE4, 2 },
    { itable_0FE5, 2 },
    { itable_0FE6, 3 },
    { itable_0FE7, 2 },
    { itable_0FE8, 2 },
    { itable_0FE9, 2 },
    { itable_0FEA, 2 },
    { itable_0FEB, 2 },
    { itable_0FEC, 2 },
    { itable_0FED, 2 },
    { itable_0FEE, 2 },
    { itable_0FEF, 2 },
    { itable_0FF0, 1 },
    { itable_0FF1, 2 },
    { itable_0FF2, 2 },
    { itable_0FF3, 2 },
    { itable_0FF4, 2 },
    { itable_0FF5, 2 },
    { itable_0FF6, 2 },
    { itable_0FF7, 2 },
    { itable_0FF8, 2 },
    { itable_0FF9, 2 },
    { itable_0FFA, 2 },
    { itable_0FFB, 2 },
    { itable_0FFC, 2 },
    { itable_0FFD, 2 },
    { itable_0FFE, 2 },
    { itable_0FFF, 1 },
};

const struct disasm_index itable[256] = {
    { itable_00, 2 },
    { itable_01, 6 },
    { itable_02, 2 },
    { itable_03, 6 },
    { itable_04, 1 },
    { itable_05, 3 },
    { itable_06, 2 },
    { itable_07, 1 },
    { itable_08, 2 },
    { itable_09, 6 },
    { itable_0A, 2 },
    { itable_0B, 6 },
    { itable_0C, 1 },
    { itable_0D, 3 },
    { itable_0E, 2 },
    { itable_0F, -1 },
    { itable_10, 2 },
    { itable_11, 6 },
    { itable_12, 2 },
    { itable_13, 6 },
    { itable_14, 1 },
    { itable_15, 3 },
    { itable_16, 2 },
    { itable_17, 1 },
    { itable_18, 2 },
    { itable_19, 6 },
    { itable_1A, 2 },
    { itable_1B, 6 },
    { itable_1C, 1 },
    { itable_1D, 3 },
    { itable_1E, 2 },
    { itable_1F, 1 },
    { itable_20, 2 },
    { itable_21, 6 },
    { itable_22, 2 },
    { itable_23, 6 },
    { itable_24, 1 },
    { itable_25, 3 },
    { NULL, 0 },
    { itable_27, 1 },
    { itable_28, 2 },
    { itable_29, 6 },
    { itable_2A, 2 },
    { itable_2B, 6 },
    { itable_2C, 1 },
    { itable_2D, 3 },
    { NULL, 0 },
    { itable_2F, 1 },
    { itable_30, 2 },
    { itable_31, 6 },
    { itable_32, 2 },
    { itable_33, 6 },
    { itable_34, 1 },
    { itable_35, 3 },
    { NULL, 0 },
    { itable_37, 1 },
    { itable_38, 2 },
    { itable_39, 6 },
    { itable_3A, 2 },
    { itable_3B, 6 },
    { itable_3C, 1 },
    { itable_3D, 3 },
    { NULL, 0 },
    { itable_3F, 1 },
    { itable_40, 2 },
    { itable_41, 2 },
    { itable_42, 2 },
    { itable_43, 2 },
    { itable_44, 2 },
    { itable_45, 2 },
    { itable_46, 2 },
    { itable_47, 2 },
    { itable_48, 2 },
    { itable_49, 2 },
    { itable_4A, 2 },
    { itable_4B, 2 },
    { itable_4C, 2 },
    { itable_4D, 2 },
    { itable_4E, 2 },
    { itable_4F, 2 },
    { itable_50, 3 },
    { itable_51, 3 },
    { itable_52, 3 },
    { itable_53, 3 },
    { itable_54, 3 },
    { itable_55, 3 },
    { itable_56, 3 },
    { itable_57, 3 },
    { itable_58, 3 },
    { itable_59, 3 },
    { itable_5A, 3 },
    { itable_5B, 3 },
    { itable_5C, 3 },
    { itable_5D, 3 },
    { itable_5E, 3 },
    { itable_5F, 3 },
    { itable_60, 3 },
    { itable_61, 3 },
    { itable_62, 2 },
    { itable_63, 3 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_68, 4 },
    { itable_69, 9 },
    { itable_6A, 5 },
    { itable_6B, 9 },
    { itable_6C, 1 },
    { itable_6D, 2 },
    { itable_6E, 1 },
    { itable_6F, 2 },
    { itable_70, 1 },
    { itable_71, 1 },
    { itable_72, 1 },
    { itable_73, 1 },
    { itable_74, 1 },
    { itable_75, 1 },
    { itable_76, 1 },
    { itable_77, 1 },
    { itable_78, 1 },
    { itable_79, 1 },
    { itable_7A, 1 },
    { itable_7B, 1 },
    { itable_7C, 1 },
    { itable_7D, 1 },
    { itable_7E, 1 },
    { itable_7F, 1 },
    { itable_80, 16 },
    { itable_81, 40 },
    { NULL, 0 },
    { itable_83, 88 },
    { itable_84, 3 },
    { itable_85, 9 },
    { itable_86, 4 },
    { itable_87, 12 },
    { itable_88, 2 },
    { itable_89, 6 },
    { itable_8A, 2 },
    { itable_8B, 6 },
    { itable_8C, 3 },
    { itable_8D, 3 },
    { itable_8E, 3 },
    { itable_8F, 3 },
    { itable_90, 9 },
    { itable_91, 6 },
    { itable_92, 6 },
    { itable_93, 6 },
    { itable_94, 6 },
    { itable_95, 6 },
    { itable_96, 6 },
    { itable_97, 6 },
    { itable_98, 3 },
    { itable_99, 3 },
    { itable_9A, 5 },
    { NULL, 0 },
    { itable_9C, 4 },
    { itable_9D, 4 },
    { itable_9E, 1 },
    { itable_9F, 1 },
    { itable_A0, 1 },
    { itable_A1, 3 },
    { itable_A2, 1 },
    { itable_A3, 3 },
    { itable_A4, 1 },
    { itable_A5, 3 },
    { itable_A6, 1 },
    { itable_A7, 3 },
    { itable_A8, 1 },
    { itable_A9, 3 },
    { itable_AA, 1 },
    { itable_AB, 3 },
    { itable_AC, 1 },
    { itable_AD, 3 },
    { itable_AE, 1 },
    { itable_AF, 3 },
    { itable_B0, 1 },
    { itable_B1, 1 },
    { itable_B2, 1 },
    { itable_B3, 1 },
    { itable_B4, 1 },
    { itable_B5, 1 },
    { itable_B6, 1 },
    { itable_B7, 1 },
    { itable_B8, 3 },
    { itable_B9, 3 },
    { itable_BA, 3 },
    { itable_BB, 3 },
    { itable_BC, 3 },
    { itable_BD, 3 },
    { itable_BE, 3 },
    { itable_BF, 3 },
    { itable_C0, 7 },
    { itable_C1, 21 },
    { itable_C2, 2 },
    { itable_C3, 2 },
    { itable_C4, 2 },
    { itable_C5, 2 },
    { itable_C6, 2 },
    { itable_C7, 6 },
    { itable_C8, 1 },
    { itable_C9, 1 },
    { itable_CA, 1 },
    { itable_CB, 1 },
    { itable_CC, 1 },
    { itable_CD, 1 },
    { itable_CE, 1 },
    { itable_CF, 4 },
    { itable_D0, 7 },
    { itable_D1, 21 },
    { itable_D2, 7 },
    { itable_D3, 21 },
    { itable_D4, 2 },
    { itable_D5, 2 },
    { itable_D6, 1 },
    { itable_D7, 2 },
    { itable_D8, 24 },
    { itable_D9, 41 },
    { itable_DA, 17 },
    { itable_DB, 27 },
    { itable_DC, 20 },
    { itable_DD, 17 },
    { itable_DE, 21 },
    { itable_DF, 18 },
    { itable_E0, 8 },
    { itable_E1, 8 },
    { itable_E2, 4 },
    { itable_E3, 3 },
    { itable_E4, 1 },
    { itable_E5, 2 },
    { itable_E6, 1 },
    { itable_E7, 2 },
    { itable_E8, 6 },
    { itable_E9, 3 },
    { itable_EA, 5 },
    { itable_EB, 1 },
    { itable_EC, 1 },
    { itable_ED, 2 },
    { itable_EE, 1 },
    { itable_EF, 2 },
    { NULL, 0 },
    { itable_F1, 2 },
    { NULL, 0 },
    { NULL, 0 },
    { itable_F4, 1 },
    { itable_F5, 1 },
    { itable_F6, 8 },
    { itable_F7, 23 },
    { itable_F8, 1 },
    { itable_F9, 1 },
    { itable_FA, 1 },
    { itable_FB, 1 },
    { itable_FC, 1 },
    { itable_FD, 1 },
    { itable_FE, 2 },
    { itable_FF, 41 },
};

const struct disasm_index * const itable_VEX[32][8] = {
    {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        itable_VEX010,
        itable_VEX011,
        itable_VEX012,
        itable_VEX013,
        itable_VEX014,
        itable_VEX015,
        itable_VEX016,
        itable_VEX017,
    }, {
        NULL,
        itable_VEX021,
        NULL,
        NULL,
        NULL,
        itable_VEX025,
        NULL,
        NULL,
    }, {
        NULL,
        itable_VEX031,
        NULL,
        NULL,
        NULL,
        itable_VEX035,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    }, {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    },
};
