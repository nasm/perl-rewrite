#!/usr/bin/env perl
#
# Sync the output file list between Makefiles
# Use the mkdep.pl parameters to get the filename syntax
#
# The first file is the source file; the other ones target.
#
use strict;
use warnings;

our %def_hints = qw{
    object-ending  .o
    path-separator /
    continuation   \\
};

sub do_transform($\%) {
    my($l, $h) = @_;
    my($ps) = $$h{'path-separator'};

    $l =~ s/\x01/$$h{'object-ending'}/g;
    $l =~ s/\x03/$$h{'continuation'}/g;

    if ($ps eq '') {
	# Remove the path separator and the preceeding directory
	$l =~ s/[^\s\=]*\x02//g;
    } else {
	# Convert the path separator
	$l =~ s/\x02/$ps/g;
    }

    return $l;
}


our @file_list;

my $first = 1;
my $first_file = $ARGV[0];
die unless (defined($first_file));

for my $filename (@ARGV) {
    open( FILE, '<', $filename ) or die;

    # First, read the syntax hints
    my %hints = %def_hints;
    while( my $line = <FILE> ){
	if ( $line =~ /^\s*\#\s*@([a-z0-9-]+):\s*\"([^\"]*)\"/ ) {
	    $hints{$1} = $2;
	}
    }

    # Read and process the file
    seek(FILE,0,0);
    my @lines;
    my $processing = 0;
    while( my $line = <FILE> ){
	chomp $line;
	if ($processing) {
	    if ($line eq '#-- End File Lists --#') {
		push(@lines, $line."\n");
		$processing = 0;
	    } elsif ($first) {
		my $xl = $line;
		my $oe = "\Q$hints{'object-ending'}";
		my $ps = "\Q$hints{'path-separator'}";
		my $cn = "\Q$hints{'continuation'}";

		$xl =~ s/${oe}(\s|$)/\x01$1/g;
		$xl =~ s/${ps}/\x02/g;
		$xl =~ s/${cn}$/\x03/;
		push(@file_list, $xl);
		push(@lines, $line);
	    }
	} else {
	    push(@lines, $line."\n");
	    if ($line eq '#-- Begin File Lists --#') {
		$processing = 1;
		if (!$first) {
		    push(@lines, "# Edit in $first_file, not here!\n");
		    for my $l (@file_list) {
			push(@lines, do_transform($l, %hints)."\n");
		    }
		}
	    }
	}
    }
    close(FILE);

    # Write the file back out
    if (!$first) {
	open( FILE, '>', $filename ) or die;
	print FILE @lines;
	close(FILE);
    }

    $first = 0;
}
